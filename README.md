# Master's Program in Computational Physics

This repository contains all my notes and code related to my master's program in Computational Physics at the Physics Department of Aristotle University of Thessaloniki.
This repository serves as a comprehensive compilation of my journey through the program, including detailed notes and code for each course. The materials here encompass a wide range of topics covered throughout my master's program, reflecting both the required tasks and my personal explorations beyond the curriculum.

## Key Points

- **Documention again 'n again**: For each course, this repository contains both notes and code for given tasks, ensuring that everything is well-documented.
- **Shifted Timeline**: You might notice that some courses do not align with their expected semester. This deviation is due to adjustments I made to my curriculum, tailoring my education to better suit my availability and long-term goals.
- **Beyond the Requirements**: While fulfilling course requirements, I often went beyond what was asked, treating many assignments as opportunities for personal exercise and exploration. As such, you'll find that some codes and solutions might be more elaborate than expected.
- **A Note to Future Students**: This repository is intended to be a resource and a source of inspiration. However, it's important to approach these materials with a critical eye. I encourage all future students of the MSc program to use this repository as a supplementary tool. Always prioritize the professors' notes and lectures, and treat this repository as just one of many resources at your disposal. Take everything you find here with a grain of salt and use it as a helping hand, not as a primary source.

## Contents

- [[SM0] Practice Tasks](#sm0-practice-tasks) : Practice tasks for the entry test.
- [[SM1] 1st Semester Courses](#sm1-1st-semester-courses) : Courses of 1st Semester
- [[SM2] 2nd Semester Courses](#sm2-2nd-semester-courses) : Courses of 2nd Semester
- [[SM3] 3rd Semester Courses](#sm3-3rd-semester-courses) : Courses of 3rd Semester
- [[DTh] Diploma Thesis](#dth-diploma-thesis) : Diploma Thesis

### [SM1] 1st Semester Courses

Courses of the 1st Semester:

- **Computational Mathematics I**
  - Description: [To be added.]
- **Programming Languages**
  - Description: [To be added.]
- **Programming Tools**
  - Description: [To be added.]
- **Data Analysis**
  - Description: [To be added.]

### [SM2] 2nd Semester Courses

Courses of the 2nd Semester:

- **Computational Mathematics II**
  - Description: [To be added.]
- **Computational Astrodynamics**
  - Description: [To be added.]
- **OpenGL Graphics**
  - Description: [To be added.]

### [SM3] 3rd Semester Courses

Courses of the 3rd Semester:

- **Computational Statistical Physics**
  - Description: [To be added.]
- **Computational Biophysics**
  - Description: [To be added.]
- **Non-Linear Dynamics**
  - Description: [To be added.]

### [DTh] Diploma Thesis

Diploma Thesis:

- Description: [To be added.]

