# The user inputs two times in the format HH:MM (24-hour format). The program should calculate the difference between
# these times in minutes. If the second time is earlier than the first, consider it as the next day's time. For
# example, the difference between 23:45 and 00:15 should be considered as 30 minutes.

# Prompt user for the first time with error handling
while True:
    try:
        first_time = input("Please input the first time (in HH:MM format): ")
        first_hours, first_minutes = map(int, first_time.split(':'))
        if 0 <= first_hours < 24 and 0 <= first_minutes < 60:
            break
        else:
            print("Time is not in correct format. Please use HH:MM format in 24-hour clock.")
    except ValueError:
        print("Invalid input. Please use HH:MM format.")

# Convert first time to total minutes
first_time_total_minutes = first_hours * 60 + first_minutes

# Prompt user for the second time with error handling
while True:
    try:
        second_time = input("Please input the second time (in HH:MM format): ")
        second_hours, second_minutes = map(int, second_time.split(':'))
        if 0 <= second_hours < 24 and 0 <= second_minutes < 60:
            break
        else:
            print("Time is not in correct format. Please use HH:MM format in 24-hour clock.")
    except ValueError:
        print("Invalid input. Please use HH:MM format.")

# Convert second time to total minutes
second_time_total_minutes = second_hours * 60 + second_minutes

# Adjust for next day if the second time is earlier than the first
if second_time_total_minutes < first_time_total_minutes:
    second_time_total_minutes += 24 * 60  # add 24 hours in minutes

# Calculate the difference in minutes
time_difference_minutes = second_time_total_minutes - first_time_total_minutes

# Output the result
print(f"The difference between the two times is {time_difference_minutes} minutes.")

# GRADE
#  I'd give this a B+. The solution correctly calculates the difference between two times, even when the second time
#  is the next day. It handles user input well, and is written clearly. However, there are opportunities to improve the
#  code by making it more concise and by handling edge cases.
