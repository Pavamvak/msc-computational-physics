# The user is asked to provide a string. The program should return the number of words in the string that have even
# lengths. For this purpose, consider a word as a sequence of characters separated by spaces.

# Prompt the user to enter a phrase
phrase = input("Please write something: ")

# Count how many words have an even number of characters.
even_word_count = 0
for word in phrase.split():
    if len(word) % 2 == 0:
        even_word_count += 1

# Display the result
if even_word_count == 0:
    print("There are no words that have an even number of characters.")
elif even_word_count == 1:
    print("There is 1 word that has an even number of characters.")
else:
    print(f"There are {even_word_count} words that have an even number of characters.")

# GRADE
# I'd give this an A-. The solution works for the stated problem and is easy to follow. The slight optimization points
# do not drastically change the outcome but might be considered for refining the code.
