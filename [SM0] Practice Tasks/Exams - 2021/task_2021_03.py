# Given a positive integer n, write a program that counts the total number of digit 1 appearing in all positive
# integers less than or equal to n.

# Prompt the user to enter a positive integer. Through an error message if the user does not input a positive integer,
# an integer or a number at all.
while True:
    try:
        inputted_number = int(input("Please enter a positive integer:"))
        if inputted_number > 0:
            break
        else:
            print("Inputted integer was not positive. Please enter a positive integer!")
    except ValueError:
        print(f"Input was not a number. Please try again! ")

# Calculate how many times the digit one appears in all the numbers up until input.
sum = 0
for _ in range(1, inputted_number+1):
    sum += str(_).count('1')

print(f"The times the digit 1 appears in all the integers up until {inputted_number} is: {sum}")

# GRADE
#  I'd give this an A-. Your code provides a clear solution to the problem, handles errors well, and has a
#  user-friendly output. There's a small area for improvement in terms of naming, but overall, it's a commendable
#  attempt!
