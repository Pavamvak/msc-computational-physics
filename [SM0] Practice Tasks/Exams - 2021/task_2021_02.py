# Write a program in which the user has to input three real numbers. The program shall return "TRUE" if the
# difference between the largest and the middle number is equal to the difference between the middle and the
# smallest number.

# Initialize variables for the three numbers
number1 = 0.0
number2 = 0.0
number3 = 0.0

# Collect the three numbers from the user
for i in range(3):
    while True:
        try:
            if i == 0:
                number1 = float(input("Please enter the first real number: "))
            elif i == 1:
                number2 = float(input("Please enter the second real number: "))
            else:
                number3 = float(input("Please enter the third real number: "))
            break
        except ValueError:
            print("Input was not a number. Try again!")

# Determine the largest, middle, and smallest numbers manually
if number1 >= number2 and number1 >= number3:
    largest_number = number1
    if number2 >= number3:
        middle_number = number2
        smallest_number = number3
    else:
        middle_number = number3
        smallest_number = number2
elif number2 >= number1 and number2 >= number3:
    largest_number = number2
    if number1 >= number3:
        middle_number = number1
        smallest_number = number3
    else:
        middle_number = number3
        smallest_number = number1
else:
    largest_number = number3
    if number1 >= number2:
        middle_number = number1
        smallest_number = number2
    else:
        middle_number = number2
        smallest_number = number1

# Calculate the differences
first_difference = largest_number - middle_number
second_difference = middle_number - smallest_number

# Compare the two differences and print TRUE or FALSE
if first_difference == second_difference:
    print("TRUE")
else:
    print("FALSE")

# GRADE
# I'd give this a B. Your code achieves the primary objective. There's room for improvement in terms of optimization
# and fixing the bug, but overall, it's a solid attempt.
