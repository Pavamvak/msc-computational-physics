# Write a program in which the user inputs the time in the format HH:MM (24-hour format). The program shall then
# calculate the angle between the hour and the minute pointer in an analog clock face.

# Prompt the user to input time in 24-hour format
while True:
    try:
        inputted_time = input("Please enter time (in HH:MM) in 24-hour format: ")
        hh, mm = inputted_time.split(':')
        hh = int(hh)
        mm = int(mm)
        if hh < 0 or hh > 23 or mm < 0 or mm > 59:
            raise ValueError
        break
    except ValueError:
        print("Input was invalid or in the wrong format. Please try again!")

# Convert 24-hour time to 12-hour format for the calculation
hour = hh % 12
minutes = mm

# Calculate angles
# Each hour is 30 degrees (360 degrees / 12 hours)
# Each minute is 6 degrees (360 degrees / 60 minutes)
hour_angle = (hour * 30) + (minutes * 0.5)  # The hour hand also moves with the minute hand
minute_angle = minutes * 6

# Calculate the smaller angle between the two hands
angle_difference = abs(hour_angle - minute_angle)
angle_difference = min(angle_difference, 360 - angle_difference)

print(f"The angle difference between the hour and the minute pointer for the time {inputted_time} is "
      f"{angle_difference} degrees.")

# GRADE
#  I'd give this a B+. The primary functionality is well-implemented and covers edge cases. There are areas for
#  optimization and refining the logic, but overall, a good job!
