# Write a program that takes a positive integer n as input and returns a list of the prime numbers up until n. A prime
# number is a number greater than 1 that is not divisible by any positive integer other than 1 and itself.

# Prompt the user to enter a positive integer
while True:
    try:
        input_number = int(input("Please enter a positive integer: "))
        if input_number > 0:
            break
        else:
            print("The inputted number was not a positive number.")
    except ValueError:
        print("Input was invalid or not a positive integer! Enter only a positive integer number (with no spaces). "
              "Please try again!")

# Displaying prime numbers up to the input_number
print(f"Prime numbers up until {input_number} are:")

if input_number >= 2:  # As 2 is the first prime number
    for num in range(2, input_number + 1):
        prime = True
        for i in range(2, int(num**0.5) + 1):
            if num % i == 0:
                prime = False
                break
        if prime:
            print(num, end=" ")  # Prints each prime number on the same line separated by space
else:
    print("No prime numbers until 1. Prime numbers are greater than 1!")

# GRADE
# I'd give this a C.The program has the right intent, but the inefficiencies, lack of optimization, and some logical
# redundancies affected the grade.
