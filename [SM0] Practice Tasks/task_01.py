# Write a program where the user inputs five integer numbers. The program should calculate and return the second
# highest and second-lowest number from the given input. If there are duplicate numbers, consider them only once
# for this ranking.

# Initialize variables to store the highest, second highest, lowest, and second lowest numbers.
highest = second_highest = lowest = second_lowest = None

# Prompt user to enter five integers, updating the relevant values with each input.
for _ in range(5):
    while True:
        try:
            number = int(input("Enter an integer: "))
            # Update highest and second highest
            if highest is None or number > highest:
                second_highest = highest
                highest = number
            elif (second_highest is None or number > second_highest) and number != highest:
                second_highest = number

            # Update lowest and second lowest
            if lowest is None or number < lowest:
                second_lowest = lowest
                lowest = number
            elif (second_lowest is None or number < second_lowest) and number != lowest:
                second_lowest = number

            break
        except ValueError:
            print("Invalid input. Please enter an integer.")

# Display the results
if second_highest is not None and second_lowest is not None:
    print(f"Second highest number: {second_highest}")
    print(f"Second lowest number: {second_lowest}")
else:
    print("Not enough unique numbers to determine second highest and second lowest.")

# GRADE
#  I'd give this a B. While the core functionality is correct, there's room for optimization and the need to handle
#  certain boundary conditions. The use of lists is understandable, but there are more efficient data structures
#  (like sets) for the specific task at hand.
