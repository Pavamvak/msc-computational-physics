# A Fibonacci sequence is a series of numbers in which each number is the sum of the two preceding ones, usually
# starting with 0 and 1 (0, 1, 1, 2, 3, 5, ...). Write a program where the user provides a positive integer n. The
# program should then return the n-th number in the Fibonacci sequence.

# Prompt the user to input a positive integer for the Fibonacci sequence.
while True:
    try:
        fib_number = int(input("Please enter the number you want from the Fibonacci sequence (the input has to be a "
                               "positive integer): "))
        if fib_number >= 0:
            break
        else:
            print("Your input was not a positive integer. Please try again!")
    except ValueError:
        print("Your input was not a positive integer. Please try again!")

# Function to calculate the n-th Fibonacci number
def fibonacci(n):
    if n <= 0:
        return 0
    elif n == 1:
        return 1
    else:
        a, b = 0, 1
        for _ in range(2, n + 1):
            a, b = b, a + b
        return b

# Print the n-th Fibonacci number
result = fibonacci(fib_number)
print(f"The {fib_number}-th number of the Fibonacci Series is: {result}")

# GRADE
# I'd give this a B+. The solution correctly calculates the Fibonacci sequence and ensures valid user input. The
# optimization points suggested can help improve efficiency, especially for large Fibonacci numbers, and improve
# readability.
