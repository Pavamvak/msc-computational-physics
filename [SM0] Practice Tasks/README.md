# Repository Overview

The [SM0] Practice Tasks directory of this repository is dedicated to the practice tasks and exam solutions related to the entry test for the Master's program in Computational Physics.

It contains the exam tasks of the 2021 and 2023, as well as practice tasks to further practice. Each solution it is graded, in terms of correctiveness, structure and clarity.

The tasks and their solutions focus on basic programming concepts, such as variables, conditions, and loops. To align with the exam's requirements, **the use of external libraries is prohibited**. Solutions strictly adhere to utilizing Python's built-in functions, ensuring that they are accessible to individuals with a fundamental understanding of programming.

In keeping with the exam guidelines, **the use of advanced data structures like lists and matrices is not permitted**. This restriction ensures that all solutions are executable with a basic understanding of programming, without reliance on complex Python features not covered in the exam's scope.

## Repository Structure

The directory is organized as follows:
```
├── Exams - 2021/
│ ├── task_2021_01.py
│ ├── task_2021_02.py
│ ├── task_2021_03.py
│ ├── task_2021_04.py
└── Exams - 2023/
│ ├── task_2023_01.py
│ ├── task_2023_02.py
│ ├── task_2023_03.py
├── README.md
├── task_01.py
├── task_02.py
├── task_03.py
├── task_04.py
├── task_05.py
```
### Tasks
- `task_01.py`:
Write a program where the user inputs five integer numbers. The program should calculate and return the second highest and second-lowest number from the given input. If there are duplicate numbers, consider them only once for this ranking.
- `task_02.py`: The user is asked to provide a string. The program should return the number of words in the string that have even lengths. For this purpose, consider a word as a sequence of characters separated by spaces.
- `task_03.py`: A Fibonacci sequence is a series of numbers in which each number is the sum of the two preceding ones, usually starting with 0 and 1 (0, 1, 1, 2, 3, 5, ...). Write a program where the user provides a positive integer n. The program should then return the n-th number in the Fibonacci sequence.
- `task_04.py`: The user inputs two times in the format HH:MM (24-hour format). The program should calculate the difference between these times in minutes. If the second time is earlier than the first, consider it as the next day's time. For example, the difference between 23:45 and 00:15 should be considered as 30 minutes.
- `task_05.py`: Write a program that takes a positive integer n as input and returns a list of the prime numbers up until n. A prime number is a number greater than 1 that is not divisible by any positive integer other than 1 and itself.

The `Exams - 2021` and `Exams - 2023` directories contain tasks specific to the exams conducted in those years:

- **Exams - 2021:**
  - `task_2021_01.py`: Write a program in which the user has to input three natural numbers. These numbers are to be added but if any number is in the range [90,100] it is to be excluded from the sum unless it is number 91 or 97. The sum shall be returned to the user.
  - `task_2021_02.py`: Write a program in which the user has to input three real numbers. The program shall return "TRUE" if the difference between the largest and the middle number is equal to the difference between the middle and the smallest number.
  - `task_2021_03.py`: Given a positive integer n, write a program that counts the total number of digit 1 appearing in all positive integers less than or equal to n.
  - `task_2021_04.py`: Write a program in which the user inputs the time in the format HH:MM (24-hour format). The program shall then calculate the angle between the hour and the minute pointer in an analog clock face.

- **Exams - 2023:**
  - `task_2023_01.py`: Write a program in which the user has to input three natural numbers. These numbers are to be added but if any number is in the range [90,100] it is to be excluded from the sum unless it is number 91 or 97. The sum shall be returned to the user.
  - `task_2023_02.py`: Write a program in which the user has to input three real numbers. The program shall return "TRUE" if the difference between the largest and the middle number is equal to the difference between the middle and the smallest number.
  - `task_2023_03.py`: Write a program that calculate the sum of the series 1/N, where N = 1, 2, ..., 1000. The result shall be a real number.

## How to Use

1. **Cloning the Repository:**
    ```bash
    git clone https://gitlab.com/Pavamvak/msc-computational-physics.git
    cd "msc-computational-physics/[SM0] Practice Tasks"
    ```
2. Run the executable from the build directory:
    ```bash
    ./task_0x.py
    ```