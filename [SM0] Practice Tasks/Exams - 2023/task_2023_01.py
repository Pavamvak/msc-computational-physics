# Write a program in which the user has to input three natural numbers. These numbers are to be added but if any
# number is in the range [90,100] it is to be excluded from the sum unless it is number 91 or 97. The sum shall be
# returned to the user.

# Set variables for storing the inputted numbers
number1 = 0
number2 = 0
number3 = 0

# Loop to get the first number
while True:
    try:
        number1 = int(input("Enter first number: "))
        if not (90 <= number1 <= 100 and number1 not in [91, 97]):
            break
    except ValueError:
        print("Input was not a number! Please try again!")

# Loop to get the second number
while True:
    try:
        number2 = int(input("Enter second number: "))
        if not (90 <= number2 <= 100 and number2 not in [91, 97]):
            break
    except ValueError:
        print("Input was not a number! Please try again!")

# Loop to get the third number
while True:
    try:
        number3 = int(input("Enter third number: "))
        if not (90 <= number3 <= 100 and number3 not in [91, 97]):
            break
    except ValueError:
        print("Input was not a number! Please try again!")

# Calculate the total sum
total_sum = number1 + number2 + number3

# If the sum is different from zero, print it. If not inform the user that no numbers were added.
if total_sum != 0:
    print(total_sum)
else:
    print("None of the numbers inputted are to be added!")

# GRADE
#  I'd give this a B+ or A-. The code achieves the objective and handles errors well. Some minor optimizations would
#  make it more concise. Great job!
