# Write a program that calculate the sum of the series 1/N, where N = 1, 2, ..., 1000. The result shall be a real
# number.

# Initiate variable to store the sum
sum = 0.0

# Calculate the sum
for i in range(1, 1001):
    sum += 1 / i

# Print the result
print(f"The sum of the series 1/N where N belongs in [1,1000] is: {sum}")

# GRADE
# I'd give this an A. Your code is correct and functional, but there is room for improvement in terms of
# variable naming and result formatting.
