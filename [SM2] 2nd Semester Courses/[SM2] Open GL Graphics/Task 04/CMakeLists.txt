cmake_minimum_required(VERSION 3.0.0)
project(minecraft VERSION 0.1.0)
cmake_policy(SET CMP0072 NEW)

find_package(OpenGL REQUIRED)

add_executable(minecraft 
    src/glad.c
    src/main.cpp
    src/shader_util.cpp
    src/stb_image.cpp
    src/model_util.cpp
    src/input_util.cpp
    src/terrain_generator.cpp
    src/audio_util.cpp
    src/config_reader.cpp
    src/color_geometry.cpp
)

target_link_libraries(minecraft 
    glfw
    OpenGL::GL)