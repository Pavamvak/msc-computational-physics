# OpenGL Minecraft Project

This project demonstrates the use of modern OpenGL to create a basic minecraft scene with a plain environment, implementing a basic control scheme. The task demands to create a cube with four sides, having textures on the outside and color on the inside. It also includes basic audio and sky texture. Utilizing:
- GLFW for window management,
- GLAD for OpenGL function loading or GLEW if used in a windows enviroment (expiremental),
- GLM for vector and matrix operations,
- FastNoiseLite for noise set up, utilized in terrain generation,
- Nlohmann's JSON for json parsing configuration parameters,
- Miniaudio utilized for audio playback,
- STB for image processing utilized in texturing.

An example of the project's result can be seen in the images below.

<p align="center">
  <img src="../Task 04/assets/images/Result_01.png" alt="First Image Description" width="40%"/>
  <img src="../Task 04/assets/images/Result_02.png" alt="Second Image Description" width="40%"/>
</p>

## System Requirements

- **Graphics Card:** GPU with OpenGL 3.3 support.
- **Operating System:** Linux.
- **Libraries:** GLFW, GLAD, GLM.

## Project Directory Structure

This section describes the overall structure of the project directories and the content they hold:

- **/assets/**: Contains all static files used by the project like audio, models, and textures.
  - **/audio/**: Audio files for background music and sound effects.
  - **/blend_files/**: Blender files that include source models (with packed the necessary files).
  - **/models/**: 3D models used in the project (in .obj formats).
  - **/textures/**: Image files used as textures in the 3D models.

- **/include/**: Header files for various libraries and utilities used in the project.
  - **/fastnoise/**: Header files for the FastNoiseLite library, used for noise generation in terrain.
  - **/glad/**: Headers for the GLAD library.
  - **/glew/**: Headers for the GLEW library, if used for managing OpenGL extensions.
  - **/json/**: Contains the nlohmann json header files for JSON parsing.
  - **/KHR/**: Khronos Group's headers, for cross-platform extensions.
  - **/miniaudio/**: Headers for the miniaudio library for audio playback.
  - **/stb/**: Headers for the stb single-file public domain libraries for image loading.
  - **/util/**: Utility headers that include helpers for shaders, models, terrain generation etc.
    - **audio_util.h**: Defines functions and structures for managing audio operations within the project, leveraging external libraries as needed for audio playback.
    - **camera.h**: Contains the definition of the Camera class or struct, which manages the viewing perspective within the OpenGL scene, including position, orientation, and projection calculations.
    - **color_geometry.h**: Provides interfaces for setting up and manipulating colored geometrical objects, which might include setting vertex data for colors and rendering them.
    - **config_reader.h**: Header for the configuration loading functionality, which parses external configuration files (typically JSON) and populates a structure used throughout the project.
    - **input_util.h**: Defines utility functions or classes for handling user input from devices such as keyboards and mice, often interfacing with GLFW for event handling.
    - **model_util.h**: Contains functions for loading and managing 3D models, including parsing model data from files and preparing them for rendering in OpenGL.
    - **shader_util.h**: Utilities related to shader program compilation, linking, and management, such as loading shaders from files, compiling them, and checking for errors.
    - **terrain_generator.h**: Provides the declarations for terrain generation functionalities, which might involve procedural generation techniques using noise functions and other algorithms to dynamically create terrain.

- **/src/**: Source files for the project.
  - **audio_util.cpp**: Utilities for handling audio functionalities.
  - **color_geometry.cpp**: Functions to handle colored cube.
  - **config_reader.cpp**: Configuration loader using JSON.
  - **glad.c**: GLAD source file for OpenGL function loading.
  - **input_util.cpp**: Utilities for handling user input.
  - **main.cpp**: The main entry point of the application.
  - **model_util.cpp**: Utilities for loading and managing 3D models.
  - **shader_util.cpp, shader_fragment.glsl, shader_vertex.glsl, simple_shader_fragment.glsl, simple_shader_vertex.glsl, sky_shader_fragment.glsl, sky_shader_vertex.glsl**: Shader programs and utilities for shader management.
  - **terrain_generator.cpp**: Functions for generating terrain dynamically.
  - **stb_image.cpp**: Implementation of the stb_image library.

- **CMakeLists.txt**: CMake configuration file for building the project.

- **config.json**: JSON configuration file that contains setup parameters for the project.

- **README.md**: The markdown file providing documentation for the project.

- **.gitignore**: Specifies intentionally untracked files to ignore.

## Setup and Installation

1. **Clone the Repository:**
   ```bash
   git clone https://gitlab.com/Pavamvak/msc-computational-physics.git
   cd "msc-computational-physics/[SM2] 2nd Semester Courses/[SM2] Open GL Graphics/Task 04"
   ```
2. **Install Dependencies:**
- On Ubuntu:
    ```bash
    sudo apt-get install libglfw3 libglfw3-dev libglm-dev
- Ensure GLAD is included as part of the project (no installation needed).
3. **Build the Project:**
Using CMake:
    ```bash
    mkdir build
    cd build
    cmake ..
    make
    ```

## Usage
Run the executable from the build directory:
```bash
./minecraft
```
ESC key: Close the application.

## Controls

The application allows users to interact with the 3D scene using both keyboard and mouse inputs. Below are the controls available to navigate and manipulate the camera as well as other aspects of the environment:

### Keyboard Controls

- **W, A, S, D**: Move the camera forward (W), left (A), backward (S), and right (D).
- **Space**: Move the camera upwards.
- **Left Crtl**: Move the camera downwards.
- **Left Shift**: Sprint.
- **ESC**: Exit the application.

### Mouse Controls

- **Mouse Movement**: Control the camera's direction. Moving the mouse changes the viewing angle of the camera.

These controls are designed to offer intuitive and flexible navigation within the 3D environment, providing an engaging user experience.

## Configuration
Modify the config.json file located in the assets directory to change parameters such as cube vertices, colors, and terrain settings. The project expects the JSON file to have a specific structure corresponding to the fields in the Config struct.
Expected JSON Structure Example:
```json
{
    "cube": {
        "vertices": [0.0, 1.0, ...],
        "texCoords": [0.0, 1.0, ...],
        "colors": [1.0, 0.0, 0.0, ...],
        "position": [0.0, 0.0, 0.0],
        "scale": [1.0, 1.0, 1.0]
    },
    "terrain": {
        "cube_path": "../path/to/cube.obj",
        "radius": 100,
        "frequency": 0.01,
        "lacunarity": 2.0,
        "gain": 0.35,
        "altitude_multiplier": 20.0,
        "seed": 75257
    },
    "texture": {
        "grass_path": "../path/to/grass.png",
        "dirt_path": "../path/to/dirt.png",
        "sky_path": "../path/to/sky.png",
        "cube_01_path": "../path/to/texture01.png",
        "cube_02_path": "../path/to/texture02.png",
        "cube_03_path": "../path/to/texture03.png",
        "cube_04_path": "../path/to/texture04.png"
    },
    "audio": {
        "music_path": "../path/to/ambient_music.mp3",
        "nature_path": "../path/to/nature_audio.mp3"
    },
    "camera_n_control": {
        "start_position": [0.0, 10.0, 3.0]
    }
}
```

## Contributing
Feel free to fork the repository and submit pull requests. You can also open issues to discuss potential improvements or report bugs.

## License
This project is licensed under the MIT License - see the LICENSE.md file for details.