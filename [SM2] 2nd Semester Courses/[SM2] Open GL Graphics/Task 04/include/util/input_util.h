#ifndef INPUT_UTIL_H
#define INPUT_UTIL_H

#include "../glad/glad.h"
#include <glm/glm.hpp>
#include <GLFW/glfw3.h>
#include "camera.h"

// Function declaration
void processInput(GLFWwindow* window, Camera& camera, float deltaTime);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);

#endif // INPUT_UTIL_H