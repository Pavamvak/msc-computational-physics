#ifndef TERRAIN_GENERATOR_H
#define TERRAIN_GENERATOR_H

#include <vector>
#include "../glad/glad.h"

// Function declaration
void generateTerrain(int radius, float cubeSize, GLuint shaderProgram, GLuint textureID1, GLuint textureID2, GLuint VAO, const std::vector<unsigned int>& indices, glm::vec3 cameraPos, float frequency, float lacunarity, float gain, float seed, float altitude_multiplier);
void setupNoise(float frequency, float lacunarity, float gain, float seed);
float getNoise(float x, float z);
void setupSkyDomeMesh(GLuint &VAO, GLuint &VBO, GLuint &EBO, const std::vector<Vertex>& vertices, const std::vector<unsigned int>& indices);

#endif // TERRAIN_GENERATOR_H