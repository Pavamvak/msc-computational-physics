#ifndef MODEL_UTIL_H
#define MODEL_UTIL_H

#include <vector>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

// Structure to hold model data
struct Vertex {
    glm::vec3 position;
    glm::vec2 texCoord;
};

// Function declaration
GLuint loadTexture(const char* path);
bool loadOBJ(const char* path, std::vector<Vertex>& out_vertices, std::vector<unsigned int>& out_indices);

#endif //MODEL_UTIL_H

