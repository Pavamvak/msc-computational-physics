#ifndef CONFIG_READER_H
#define CONFIG_READER_H

#include <vector>
#include <string>
#include <glm/glm.hpp>

// Structure to hold configuration parameters
struct Config {
    std::vector<float> cubeVertices;
    std::vector<float> cubeColors;
    std::vector<float> cubeTexCoords;

    const char* terrain_cube_path;

    const char* grass_texture_path;
    const char* dirt_texture_path;
    const char* sky_texture_path;
    const char* cube_texture_path_01;
    const char* cube_texture_path_02;
    const char* cube_texture_path_03;
    const char* cube_texture_path_04;

    const char* music_audio_path;
    const char* nature_audio_path;

    glm::vec3 Camera_Start_Position;

    int terrainRadius;

    glm::vec3 cube_position;
    glm::vec3 cube_scale;

    float terrain_frequency;
    float terrain_lacunarity;
    float terrain_gain;
    float terrain_seed;
    float terrain_altitude_multiplier;
};

// Function declaration
Config loadModelData(const std::string& filename);

#endif // CONFIG_READER_H