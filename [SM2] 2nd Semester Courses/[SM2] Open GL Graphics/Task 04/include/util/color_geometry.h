#ifndef COLOR_GEOMETRY_H
#define COLOR_GEOMETRY_H

#include "../glad/glad.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "config_reader.h"

// Function declaration
void setupColorMeshesAndBuffers(Config& config, GLuint& cubeVAO, GLuint& cubeVBO);
void renderColoredGeometry(GLuint shaderProgram, GLuint cubeVAO, glm::mat4& view, glm::mat4& projection, std::vector<GLuint> textures, glm::vec3 cube_position, glm::vec3 cube_scale);

#endif // COLOR_GEOMETRY_H