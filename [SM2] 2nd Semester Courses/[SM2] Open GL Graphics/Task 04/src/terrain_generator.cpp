/**
 * @file terrain_generator.cpp
 * @brief This file contains functions for generating procedural terrain in an OpenGL application.
 *
 * It leverages noise algorithms to dynamically create terrain around a specified camera position. Functions include
 * setting up noise for terrain elevation, generating terrain grids based on camera position, and configuring
 * meshes for elements like sky domes. These functionalities support rendering visually appealing and varied landscapes
 * in real-time, which can be particularly useful in open-world or simulation games where expansive and interesting
 * environments are key.
 */

// Native Libraries
#include <vector>

// OpenGL Libraries
#include "../include/glad/glad.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

// Custom Libraries
#include "../include/util/model_util.h"

// Noise Libraries
#include "../include/fastnoise/FastNoiseLite.h"

FastNoiseLite noise;

/**
 * Initializes the noise generator with specific settings suitable for terrain generation.
 * This function configures the noise type, fractal attributes, and frequency to produce
 * aesthetically pleasing terrain variations.
 * 
 * @param frequency Set frequency of noise function.
 * @param lacunarity Set lacunarity to affect the gap between features.
 * @param gain Set gain to control the amplitude of the noise function.
 * @param seed Set noise function seed.
 */
void setupNoise(float frequency, float lacunarity, float gain, float seed) {
    noise.SetNoiseType(FastNoiseLite::NoiseType_OpenSimplex2);  // Uses Open Simplex 2 noise for natural appearance.
    noise.SetFractalType(FastNoiseLite::FractalType_FBm);       // Fractal Brownian Motion for smooth fractal noise.
    noise.SetFrequency(frequency);                              // Low frequency for broad, smooth features.
    noise.SetFractalOctaves(5);                                 // Multiple octaves for detail at different scales.
    noise.SetFractalLacunarity(lacunarity);                     // Lacunarity affects the gap between features.
    noise.SetFractalGain(gain);                                 // Gain controls the amplitude of noise changes.
    noise.SetSeed(seed);                                        // Set seed for random terrain generation.
}

/**
 * Computes a noise value for a given x, z coordinate.
 * 
 * @param x The x coordinate in the terrain grid.
 * @param z The z coordinate in the terrain grid.
 * @return The computed noise value, scaled to represent terrain heights.
 */
float getNoise(float x, float z) {
    return noise.GetNoise(x, z);
}

/**
 * Generates terrain dynamically around a specified camera position, using noise-based height adjustments.
 * This function creates a grid of terrain blocks within a defined radius from the camera position,
 * using the noise function to determine the height of each terrain block.
 *
 * @param radius The radius around the camera within which to generate terrain.
 * @param cubeSize The size of each cube in the terrain.
 * @param shaderProgram The shader program used for rendering.
 * @param textureID1 The ID of the texture to apply to the terrain. This relates to the grass texture.
 * @param textureID2 The ID of the texture to apply to the terrain. This relates to the dirt texture.
 * @param VAO The Vertex Array Object that contains terrain rendering information.
 * @param indices The indices used for drawing the terrain elements.
 * @param cameraPos The current position of the camera around which terrain is generated.
 * @param frequency Set frequency of noise function.
 * @param lacunarity Set lacunarity to affect the gap between features.
 * @param gain Set gain to control the amplitude of the noise function.
 * @param seed Set noise function seed.
 * @param altitude_multiplier Set multiplier to increase/ decrease the altitude of hills.
 */
void generateTerrain(int radius, float cubeSize, GLuint shaderProgram, GLuint textureID1, GLuint textureID2, GLuint VAO, const std::vector<unsigned int>& indices, glm::vec3 cameraPos, float frequency, float lacunarity, float gain, float seed, float altitude_multiplier) {

    glBindVertexArray(VAO);
    
    setupNoise(frequency, lacunarity, gain, seed);

    int startX = static_cast<int>(cameraPos.x) - radius;
    int endX = static_cast<int>(cameraPos.x) + radius;
    int startZ = static_cast<int>(cameraPos.z) - radius;
    int endZ = static_cast<int>(cameraPos.z) + radius;

    for (int x = startX; x <= endX; x++) {
        for (int z = startZ; z <= endZ; z++) {
            float height = getNoise(x, z) * altitude_multiplier;  // Generate height based on noise

            // Grass Cube
            glBindTexture(GL_TEXTURE_2D, textureID1);
            glm::mat4 model = glm::mat4(1.0f);
            model = glm::translate(model, glm::vec3(x * cubeSize, height, z * cubeSize));
            glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "model"), 1, GL_FALSE, &model[0][0]);
            glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);

            // Dirt Cube
            glBindTexture(GL_TEXTURE_2D, textureID2);
            model = glm::translate(model, glm::vec3(0.0f, -cubeSize, 0.0f)); // Move down by one cube size
            glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "model"), 1, GL_FALSE, &model[0][0]);
            glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
        }
    }
    
    glBindVertexArray(0);
}

void setupSkyDomeMesh(GLuint &VAO, GLuint &VBO, GLuint &EBO, const std::vector<Vertex>& vertices, const std::vector<unsigned int>& indices) {
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    glBindVertexArray(VAO);

    // Bind and set vertex buffer(s)
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), vertices.data(), GL_STATIC_DRAW);

    // Bind and set indices buffer(s)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), indices.data(), GL_STATIC_DRAW);

    // Configure vertex attribute(s)
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, position));
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, texCoord));
    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}