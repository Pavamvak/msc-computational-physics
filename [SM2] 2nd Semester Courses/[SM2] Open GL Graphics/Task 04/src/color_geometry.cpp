/**
 * @file color_geometry.cpp
 * @brief This file includes functions for setting up and rendering the colored cube needed for the task using OpenGL.
 *
 * Functions within this file handle the initialization of vertex array and buffer objects for the cube,
 * configure vertex attributes for position, color, and texture coordinates, and facilitate the rendering
 * process using these configurations. The focus is on integrating configurable geometric data and textures
 * with shaders to render complex 3D scenes effectively.
 */

// Custom Libraries
#include "../include/util/color_geometry.h"

/**
 * Initializes vertex array and buffer objects for cube geometry with configured data.
 *
 * @param config Configuration structure containing vertices, colors, and texture coordinates.
 * @param cubeVAO Vertex Array Object for the cube.
 * @param cubeVBO Vertex Buffer Object for the cube.
 * 
 * This function sets up the necessary OpenGL objects for rendering a cube, including vertex array
 * and buffer objects. It loads vertex positions, color data, and texture coordinates into the GPU's
 * memory, setting up appropriate buffer subdivisions and attribute pointers for rendering.
 */
void setupColorMeshesAndBuffers(Config& config, GLuint& cubeVAO, GLuint& cubeVBO) {
    // Cube setup
    glGenVertexArrays(1, &cubeVAO);
    glGenBuffers(1, &cubeVBO);
    glBindVertexArray(cubeVAO);
    glBindBuffer(GL_ARRAY_BUFFER, cubeVBO);

    // Calculate buffer sizes
    size_t verticesBytes = config.cubeVertices.size() * sizeof(float);
    size_t colorsBytes = config.cubeColors.size() * sizeof(float);
    size_t texCoordsBytes = config.cubeTexCoords.size() * sizeof(float);

    // Buffer data setup
    glBufferData(GL_ARRAY_BUFFER, verticesBytes + colorsBytes + texCoordsBytes, NULL, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, verticesBytes, config.cubeVertices.data());
    glBufferSubData(GL_ARRAY_BUFFER, verticesBytes, colorsBytes, config.cubeColors.data());
    glBufferSubData(GL_ARRAY_BUFFER, verticesBytes + colorsBytes, texCoordsBytes, config.cubeTexCoords.data());

    // Set attribute pointers
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)(verticesBytes));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), (void*)(verticesBytes + colorsBytes));
    glEnableVertexAttribArray(2);
}

/**
 * Renders colored geometry using the configured shader program and vertex arrays.
 *
 * @param shaderProgram The OpenGL shader program used for rendering.
 * @param cubeVAO Vertex Array Object for the cube.
 * @param view View matrix for the current camera view.
 * @param projection Projection matrix for the scene.
 * @param textures List of texture IDs to be applied to the geometry.
 * @param cube_position Set position of the colored cube on scene.
 * @param cube_scale Set scale multiplier of the colored cube.
 * 
 * This function uses the provided shader program to render the cube in the scene, applying
 * textures and transforming the geometry as necessary. It sets shader uniforms for model, view,
 * and projection matrices, as well as texture samplers for multi-texturing.
 */
void renderColoredGeometry(GLuint shaderProgram, GLuint cubeVAO, glm::mat4& view, glm::mat4& projection, std::vector<GLuint> textures, glm::vec3 cube_position, glm::vec3 cube_scale) {
    glUseProgram(shaderProgram);

    // Uniform locations
    GLint modelLoc = glGetUniformLocation(shaderProgram, "model");
    GLint viewLoc = glGetUniformLocation(shaderProgram, "view");
    GLint projLoc = glGetUniformLocation(shaderProgram, "projection");
    GLint textureSamplerLoc = glGetUniformLocation(shaderProgram, "textureSampler");

    // Set the view and projection matrices in the shader
    glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));

    // Prepare to draw the cube
    glBindVertexArray(cubeVAO);
    glDisable(GL_CULL_FACE);

    glm::mat4 model = glm::translate(glm::mat4(1.0f), cube_position);
    model = glm::scale(model, cube_scale); // Adjust scaling if necessary
    
    for (int i = 0; i < 4; i++) {  // Assuming there are 4 faces
        
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, textures[i % textures.size()]);
        glUniform1i(textureSamplerLoc, 0);

        glDrawArrays(GL_TRIANGLE_FAN, i * 4, 4);  // Draw each face with 4 vertices
    }
}