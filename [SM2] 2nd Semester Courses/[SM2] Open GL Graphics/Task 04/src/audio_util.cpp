/**
 * @file audio_util.cpp
 * @brief This file handles audio playback functionality using the miniaudio library.
 *        It demonstrates initializing audio devices, decoding audio files, and playing
 *        them asynchronously in a separate thread.
 */

#define MINIAUDIO_IMPLEMENTATION

// Native Libraries
#include <thread>
#include <iostream>

// Audio Libraries
#include "../include/miniaudio/miniaudio.h"

/**
 * @brief Callback function for audio data processing.
 * 
 * This function is called by miniaudio to fill the output buffer with audio frames.
 * It reads PCM frames from a decoder and sends them to the audio output device.
 * 
 * @param pDevice Pointer to the audio device being used for playback.
 * @param pOutput Pointer to the output buffer to which audio data should be written.
 * @param pInput Pointer to the input buffer containing audio data to be read (not used).
 * @param frameCount The number of audio frames to process.
 */
void data_callback(ma_device* pDevice, void* pOutput, const void* pInput, ma_uint32 frameCount) {
    ma_decoder* pDecoder = (ma_decoder*)pDevice->pUserData;
    if (pDecoder == NULL) {
        return;
    }

    ma_decoder_read_pcm_frames(pDecoder, pOutput, frameCount, NULL);
    (void)pInput; // Unused parameter
}

/**
 * @brief Plays an audio file using the specified filename.
 * 
 * This function initializes a decoder and an audio device, configures the device, and starts
 * playback. It blocks and runs indefinitely until playback is stopped.
 * 
 * @param filename The path to the audio file to play.
 */
void playAudio(const char* filename) {
    ma_decoder decoder;
    ma_device device;
    ma_device_config deviceConfig;

    if (ma_decoder_init_file(filename, NULL, &decoder) != MA_SUCCESS) {
        std::cerr << "Failed to load audio file: " << filename << std::endl;
        return;
    }

    deviceConfig = ma_device_config_init(ma_device_type_playback);
    deviceConfig.playback.format = decoder.outputFormat;
    deviceConfig.playback.channels = decoder.outputChannels;
    deviceConfig.sampleRate = decoder.outputSampleRate;
    deviceConfig.dataCallback = data_callback;
    deviceConfig.pUserData = &decoder;

    if (ma_device_init(NULL, &deviceConfig, &device) != MA_SUCCESS) {
        ma_decoder_uninit(&decoder);
        std::cerr << "Failed to initialize audio device." << std::endl;
        return;
    }

    if (ma_device_start(&device) != MA_SUCCESS) {
        ma_device_uninit(&device);
        ma_decoder_uninit(&decoder);
        std::cerr << "Failed to start audio playback." << std::endl;
        return;
    }

    // Keep playing in the background
    while (true) {
        std::this_thread::sleep_for(std::chrono::seconds(1));
        if (ma_device_is_started(&device) == MA_FALSE) {
            break;
        }
    }

    ma_device_uninit(&device);
    ma_decoder_uninit(&decoder);
}

/**
 * @brief Starts the audio playback in a new detached thread.
 * 
 * This function creates a new thread to handle the audio playback, allowing the audio
 * playback to operate independently of the main application flow.
 * 
 * @param filename The path to the audio file to play.
 */
void startAudioPlayback(const char* filename) {
    std::thread audioThread(playAudio, filename);
    audioThread.detach(); // Allow the thread to run independently
}