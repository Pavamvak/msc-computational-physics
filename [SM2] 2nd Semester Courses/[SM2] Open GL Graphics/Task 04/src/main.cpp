/**
 * @file main.cpp
 * @brief Main entry point for the OpenGL project for a simple minecraft scene, with a four-sided cube 
 * featuring textures on the outside and colors on the inside.
 * 
 * This program showcases a dynamic 3D scene featuring a simple minecraft terrain enviroment,
 * rendered using OpenGL. It demonstrates the use of GLFW for window management, this program uses GLAD or 
 * GLEW for loading OpenGL functions (GLAD is used when compiling on Linux, and GLEW is used on Windows), 
 * and GLM for mathematical operations. 
 * 
 * The application initializes a GLFW window, sets up models and textures, and employs shader programs to 
 * handle rendering and transformations. A key feature of this program is the interactive camera control, 
 * which allows users to move around and explore the scene. This is achieved through the implementation of 
 * a camera model using GLM's lookAt and perspective projection functions.
 * 
 * * Key functionalities covered in this program:
 * - **Window Creation**: Utilizes GLFW to create and manage an application window, providing the context in which all 
 *   OpenGL rendering is performed. The program supports both windowed and full-screen modes, dynamically adjusting based
 *   on the primary monitor's resolution.
 * - **OpenGL Initialization**: GLAD or GLEW is used for loading and binding OpenGL function pointers, a crucial step for 
 *   enabling OpenGL commands to be issued.
 * - **Shader Compilation and Linking**: Custom vertex and fragment shaders are compiled and linked into shader programs 
 *   which are then used to render various objects within the scene.
 * - **3D Model Loading**: Geometry for the scene, such as cubes and terrain, is loaded from OBJ files, demonstrating 
 *   the handling of external 3D model data.
 * - **Texture Application**: Textures are applied to the 3D models using UV mapping, adding visual complexity and 
 *   appeal to the simple geometries.
 * - **Camera Control**: Interactive camera controls allow the user to explore the scene dynamically. This is 
 *   facilitated through capturing mouse and keyboard input to adjust the camera's position and orientation.
 * - **Audio Playback**: Background music and environmental sounds are played using a simple audio management system, 
 *   enhancing the immersive experience of the scene.
 * - **Scene Rendering**: Implements rendering loops where models are drawn according to the camera's perspective,
 *   supported by lighting and texturing shaders.
 * 
 * The project is configured through a json file.
 * 
 * Detailed Explanation of JSON Structure:
 * - "cube": Contains arrays for vertices, texture coordinates, and colors of a cube.
 * - "terrain": Contains path and settings for terrain generation.
 * - "texture": Defines paths for various texture images used in the project.
 * - "audio": Specifies paths for audio files used for background music and effects.
 * - "camera_n_control": Contains initial camera settings and user control parameters.
 *
 * The JSON file should be structured with specific keys matching those expected by the code,
 * ensuring that each array or object provides the correct type and format of data.
 *
 * Example of Expected JSON structure:
 * {
 *     "cube": {
 *         "vertices": [0.0, 1.0, ...],
 *         "texCoords": [0.0, 1.0, ...],
 *         "colors": [1.0, 0.0, 0.0, ...],
 *         "position": [0.0, 0.0, 0.0],
 *         "scale": [1.0, 1.0, 1.0]
 *     },
 *     "terrain": {
 *         "cube_path": "../path/to/cube.obj",
 *         "radius": 100,
 *         "frequency": 0.01,
 *         "lacunarity": 2.0,
 *         "gain": 0.35,
 *         "altitude_multiplier": 20.0,
 *         "seed": 75257
 *     },
 *     "texture": {
 *         "grass_path": "../path/to/grass.png",
 *         "dirt_path": "../path/to/dirt.png",
 *         "sky_path": "../path/to/sky.png",
 *         "cube_01_path": "../path/to/texture01.png",
 *         "cube_02_path": "../path/to/texture02.jpg",
 *         "cube_03_path": "../path/to/texture03.png",
 *         "cube_04_path": "../path/to/texture04.jpg"
 *     },
 *     "audio": {
 *         "music_path": "../path/to/ambient_music.mp3",
 *         "nature_path": "../path/to/nature_audio.mp3"
 *     },
 *     "camera_n_control": {
 *         "start_position": [0.0, 10.0, 3.0]
 *     }
 * }
 * @author [Panagiotis Vamvakas]
 * @date [12-06-2024]
 * @version [1.1]
 */

// Platform specific includes and library setup
#ifdef _WIN32
    // Windows platform
    #define GLEW_STATIC
    #include "../include/glew/glew.h"
#else
    // Linux platform
    #include "../include/glad/glad.h"
#endif

// Native Libraries
#include <iostream>
#include <vector>

// OpenGL Libraries
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Custom Libraries
#include "../include/util/shader_util.h"
#include "../include/util/model_util.h"
#include "../include/util/input_util.h"
#include "../include/util/camera.h"
#include "../include/util/terrain_generator.h"
#include "../include/util/audio_util.h"
#include "../include/util/config_reader.h"
#include "../include/util/color_geometry.h"
#include "../include/json/json.hpp"
using json = nlohmann::json;

GLFWwindow* window;

int main() {

    Config config = loadModelData("../config.json");

    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Get the primary monitor and the video mode
    GLFWmonitor* monitor = glfwGetPrimaryMonitor();
    const GLFWvidmode* mode = glfwGetVideoMode(monitor);

    // Create a fullscreen window using the screen resolution
    GLFWwindow* window = glfwCreateWindow(mode->width, mode->height, "My 3D object", monitor, NULL);
    if (!window) {
        fprintf(stderr, "ERROR: could not open window with GLFW3\n");
        glfwTerminate();
        return 1;
    }
    glfwMakeContextCurrent(window);

    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    
    // Initialize Camera
    Camera camera(config.Camera_Start_Position, glm::vec3(0.0f, 1.0f, 0.0f), -90.0f, 0.0f);
    glfwSetWindowUserPointer(window, &camera);                                                  // Set the camera as the user pointer
    glfwSetCursorPosCallback(window, mouse_callback);                                           // Set mouse callback

    // Set the key callback to handle the escape key
    glfwSetKeyCallback(window, [](GLFWwindow* window, int key, int scancode, int action, int mods) {
        if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
            glfwSetWindowShouldClose(window, GLFW_TRUE);
    });

    // Initialize GLAD or GLEW
    #ifdef _WIN32
        glewExperimental = GL_TRUE;
        if (glewInit() != GLEW_OK) {
            std::cerr << "Failed to initialize GLEW" << std::endl;
            return -1;
        }
    #else
        if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)) {
            std::cerr << "Failed to initialize GLAD" << std::endl;
            return -1;
        }
    #endif

    // Set up depth and culling
    glEnable(GL_DEPTH_TEST);

    std::vector<Vertex> vertices;
    std::vector<unsigned int> indices;
    if (!loadOBJ(config.terrain_cube_path, vertices, indices)) {
        std::cerr << "Failed to load OBJ file!" << std::endl;
        return -1;
    }

    // Load shader source from files and compile them
    GLuint shaderProgram = UberSetupShaders("../src/shader_vertex.glsl", "../src/shader_fragment.glsl");

    GLuint VBO, VAO, EBO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, position));
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, texCoord));
    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    // Load textures needed
    GLuint texture_grass = loadTexture(config.grass_texture_path);
    GLuint texture_dirt = loadTexture(config.dirt_texture_path);

    // Define transformations
    glm::mat4 model = glm::mat4(1.0f);
    model = glm::translate(model, glm::vec3(0.0f, 0.0f, -3.0f));  // Move the cube back a bit
    glm::mat4 view = glm::lookAt(glm::vec3(2.0f, 2.0f, 5.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    glm::mat4 projection = glm::perspective(glm::radians(45.0f), 800.0f / 600.0f, 0.1f, 100.0f);

    GLuint skyShaderProgram = UberSetupShaders("../src/sky_shader_vertex.glsl", "../src/sky_shader_fragment.glsl");
    GLuint skyTexture = loadTexture(config.sky_texture_path);

    std::vector<Vertex> skyDomeVertices;
    std::vector<unsigned int> skyDomeIndices;
    loadOBJ("../assets/models/skydome.obj", skyDomeVertices, skyDomeIndices);

    GLuint skyDomeVAO, skyDomeVBO, skyDomeEBO;
    setupSkyDomeMesh(skyDomeVAO, skyDomeVBO, skyDomeEBO, skyDomeVertices, skyDomeIndices);

    std::vector<GLuint> textures;
    textures.push_back(loadTexture(config.cube_texture_path_01));
    textures.push_back(loadTexture(config.cube_texture_path_02));
    textures.push_back(loadTexture(config.cube_texture_path_03));
    textures.push_back(loadTexture(config.cube_texture_path_04));
    GLuint simpleShaderProgram = UberSetupShaders("../src/simple_shader_vertex.glsl", "../src/simple_shader_fragment.glsl");
    GLuint cubeVAO, cubeVBO;
    setupColorMeshesAndBuffers(config, cubeVAO, cubeVBO);

    // Grid dimensions
    int terrainRadius = config.terrainRadius;
    float cubeSize = 2.0f;

    float lastFrame = 0.0f;

    // Call playAudio to start music playback
    startAudioPlayback(config.music_audio_path);
    startAudioPlayback(config.nature_audio_path);

    while (!glfwWindowShouldClose(window)) {
        float currentFrame = glfwGetTime();
        float deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        processInput(window, camera, deltaTime);

        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glm::mat4 projection = glm::perspective(glm::radians(45.0f), static_cast<float>(mode->width) / static_cast<float>(mode->height), 0.1f, 100.0f);

        // Render the sky dome first
        glUseProgram(skyShaderProgram);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, skyTexture);
        glUniform1i(glGetUniformLocation(skyShaderProgram, "skyTexture"), 0);

        // Make sure to use a view matrix without translation for the sky dome to make it appear infinite
        glm::mat4 skyView = glm::mat4(glm::mat3(camera.GetViewMatrix())); // Remove translation component
        glUniformMatrix4fv(glGetUniformLocation(skyShaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(skyView));
        glUniformMatrix4fv(glGetUniformLocation(skyShaderProgram, "projection"), 1, GL_FALSE, glm::value_ptr(projection));

        glBindVertexArray(skyDomeVAO);
        glDisable(GL_DEPTH_TEST);  // Disable depth testing to ensure the sky dome is always in the background
        glDrawElements(GL_TRIANGLES, skyDomeIndices.size(), GL_UNSIGNED_INT, 0);
        glEnable(GL_DEPTH_TEST);   // Re-enable depth testing for other objects

        glm::mat4 view = camera.GetViewMatrix();
        // Render colored cube and axes
        glUseProgram(simpleShaderProgram);
        glUniformMatrix4fv(glGetUniformLocation(simpleShaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
        glUniformMatrix4fv(glGetUniformLocation(simpleShaderProgram, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
        renderColoredGeometry(simpleShaderProgram, cubeVAO, view, projection, textures, config.cube_position, config.cube_scale); // Render the cube

        // Render terrain and other objects
        glUseProgram(shaderProgram);
        glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
        glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "projection"), 1, GL_FALSE, glm::value_ptr(projection));

        // Last known camera position for checking movement
        glm::vec3 cameraPosition = camera.GetPosition();
        generateTerrain(terrainRadius, cubeSize, shaderProgram, texture_grass, texture_dirt, VAO, indices, cameraPosition, config.terrain_frequency, config.terrain_lacunarity, config.terrain_gain, config.terrain_seed, config.terrain_altitude_multiplier);

        glBindVertexArray(0);

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);

    glfwTerminate();
    return 0;
}