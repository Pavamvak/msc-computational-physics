#version 330 core
out vec4 FragColor;

in vec3 vertexColor;
in vec2 texCoord;

uniform sampler2D texture1;

void main()
{
    if (gl_FrontFacing) {
        FragColor = texture(texture1, texCoord);
    } else {
        FragColor = vec4(vertexColor, 1.0);
    }
}