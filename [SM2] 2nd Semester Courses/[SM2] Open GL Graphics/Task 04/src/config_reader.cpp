/**
 * @file config_reader.cpp
 * @brief File responsible for loading project parameters from a JSON configuration file.
 *
 * This file contains the implementation of the `loadModelData` function, which reads and
 * parses project data stored in a JSON format. 
 * 
 * Detailed Explanation of JSON Structure:
 * - "cube": Contains arrays for vertices, texture coordinates, and colors of a cube.
 * - "terrain": Contains path and settings for terrain generation.
 * - "texture": Defines paths for various texture images used in the project.
 * - "audio": Specifies paths for audio files used for background music and effects.
 * - "camera_n_control": Contains initial camera settings and user control parameters.
 *
 * @note The JSON file should be structured with specific keys matching those expected by the code,
 *       ensuring that each array or object provides the correct type and format of data.
 *
 * Example of Expected JSON structure:
 * {
 *     "cube": {
 *         "vertices": [0.0, 1.0, ...],
 *         "texCoords": [0.0, 1.0, ...],
 *         "colors": [1.0, 0.0, 0.0, ...],
 *         "position": [0.0, 0.0, 0.0],
 *         "scale": [1.0, 1.0, 1.0]
 *     },
 *     "terrain": {
 *         "cube_path": "../path/to/cube.obj",
 *         "radius": 100,
 *         "frequency": 0.01,
 *         "lacunarity": 2.0,
 *         "gain": 0.35,
 *         "altitude_multiplier": 20.0,
 *         "seed": 75257
 *     },
 *     "texture": {
 *         "grass_path": "../path/to/grass.png",
 *         "dirt_path": "../path/to/dirt.png",
 *         "sky_path": "../path/to/sky.png",
 *         "cube_01_path": "../path/to/texture01.png",
 *         "cube_02_path": "../path/to/texture02.jpg",
 *         "cube_03_path": "../path/to/texture03.png",
 *         "cube_04_path": "../path/to/texture04.jpg"
 *     },
 *     "audio": {
 *         "music_path": "../path/to/ambient_music.mp3",
 *         "nature_path": "../path/to/nature_audio.mp3"
 *     },
 *     "camera_n_control": {
 *         "start_position": [0.0, 10.0, 3.0]
 *     }
 * }
 */

// Native Libraries
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

// JSON Libraries
#include "../include/json/json.hpp"

// Custom Libraries
#include "../include/util/config_reader.h"

/**
 * @brief Loads model data from a JSON file into a Config structure.
 *
 * This function opens a JSON file specified by the filename and extracts predefined fields
 * such as vertices and colors for a cube and coordinate axes. The data is expected to be
 * organized in a specific structure within the JSON file, with separate arrays for cube
 * vertices, cube colors, axes vertices, and axes colors. Each array should contain floating
 * point numbers representing the geometric and color data for the models.
 *
 * @param filename A string representing the path to the JSON file containing the model data.
 * @return Config A Config structure populated with the vertices and colors data from the JSON file.
 *
 */
Config loadModelData(const std::string& filename) {
    
    std::ifstream file(filename);
    nlohmann::json j;
    file >> j;

    static std::string terrain_cube, grass_texture, dirt_texture, sky_texture;
    static std::string cube_tex_01, cube_tex_02, cube_tex_03, cube_tex_04;
    static std::string music_audio, nature_audio;

    Config config;
    // Assuming each vertex entry in the JSON now has 5 elements: x, y, z, u, v
    config.cubeVertices = j["cube"]["vertices"].get<std::vector<float>>();
    config.cubeTexCoords = j["cube"]["texCoords"].get<std::vector<float>>();
    config.cubeColors = j["cube"]["colors"].get<std::vector<float>>();

    config.cube_position = glm::vec3(
        j["cube"]["position"][0],
        j["cube"]["position"][1],
        j["cube"]["position"][2]
    );
    config.cube_scale = glm::vec3(
        j["cube"]["scale"][0],
        j["cube"]["scale"][1],
        j["cube"]["scale"][2]
    );

    config.terrainRadius = j["terrain"]["radius"][0];
    config.terrain_frequency = j["terrain"]["frequency"][0];
    config.terrain_lacunarity = j["terrain"]["lacunarity"][0];
    config.terrain_gain = j["terrain"]["gain"][0];
    config.terrain_altitude_multiplier = j["terrain"]["altitude_multiplier"][0];
    config.terrain_seed = j["terrain"]["seed"][0];

    terrain_cube = j["terrain"]["cube_path"][0];
    grass_texture = j["texture"]["grass_path"][0];
    dirt_texture = j["texture"]["dirt_path"][0];
    sky_texture = j["texture"]["sky_path"][0];
    cube_tex_01 = j["texture"]["cube_01_path"][0];
    cube_tex_02 = j["texture"]["cube_02_path"][0];
    cube_tex_03 = j["texture"]["cube_03_path"][0];
    cube_tex_04 = j["texture"]["cube_04_path"][0];
    music_audio = j["audio"]["music_path"][0];
    nature_audio = j["audio"]["nature_path"][0];

    config.terrain_cube_path = terrain_cube.c_str();
    config.grass_texture_path = grass_texture.c_str();
    config.dirt_texture_path = dirt_texture.c_str();
    config.sky_texture_path = sky_texture.c_str();
    config.cube_texture_path_01 = cube_tex_01.c_str();
    config.cube_texture_path_02 = cube_tex_02.c_str();
    config.cube_texture_path_03 = cube_tex_03.c_str();
    config.cube_texture_path_04 = cube_tex_04.c_str();
    config.music_audio_path = music_audio.c_str();
    config.nature_audio_path = nature_audio.c_str();

    config.Camera_Start_Position = glm::vec3(
        j["camera_n_control"]["start_position"][0],
        j["camera_n_control"]["start_position"][1],
        j["camera_n_control"]["start_position"][2]
    );
    
    return config;
}