#version 330 core
out vec4 FragColor;

in vec3 TexCoords;
uniform sampler2D skyTexture;

void main() {    
    vec2 scaledTexCoords = TexCoords.xy * 0.0015 + 0.5; // Adjust this scale factor as needed
    FragColor = texture(skyTexture, scaledTexCoords);
}
