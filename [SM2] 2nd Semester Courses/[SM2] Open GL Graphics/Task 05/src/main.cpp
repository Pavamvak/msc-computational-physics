/**
 * @file main.cpp
 * @brief Main entry point for a 3D solar system project using OpenGL.
 *        The goal of this project was to create a scene where an object would be rendered,
 *        with a light source lighting it and a second object showing the position of the 
 *        light source. The user should be able to control the position of the light source.
 * 
 *        This program initializes a windowed 3D scene with interactive camera control,
 *        dynamic lighting, and multiple rendered objects including Earth, a surrounding dome,
 *        and the Sun. Users can control the light direction and the position of the Sun
 *        using the arrow keys, simulating a basic solar system environment.
 *
 * This simulation demonstrates:
 * - Loading and using custom shaders for different objects.
 * - Implementing a camera system with basic movement controls.
 * - Dynamic updates to vertex buffer objects for real-time interaction.
 * - Use of textures and simple lighting models to enhance the scene realism.
 *
 * Dependencies:
 * - GLFW for window management and input handling.
 * - GLAD or GLEW for loading OpenGL functions.
 * - GLM for mathematics and transformations.
 * - Custom utility libraries for shader compilation, model loading, and texture processing.
 *
 * Instructions:
 * - Use WASD keys to move the camera laterally.
 * - Use SHIFT to accelerate the camera movement.
 * - Use SPACE and CTRL to move the camera vertically.
 * - Use the arrow keys to adjust the sun's azimuth and polar angle and the scene lighting direction.
 * - Press ESC to exit the application.
 * 
 * @author [Panagiotis Vamvakas]
 * @date [16-06-2024]
 * @version [1.0]
 */

// Libraries
#include "../include/util/common.h"

// Custom Libraries
#include "../include/util/shader_util.h"
#include "../include/util/model_util.h"
#include "../include/util/input_util.h"
#include "../include/util/camera.h"
#include "../include/util/audio_util.h"
#include "../include/util/config_reader.h"
#include "../include/util/model_buffers.h"
#include "../include/json/json.hpp"
using json = nlohmann::json;

GLFWwindow* window;

// Initialize global variables
float horizontalAngle = 0.0f;
float verticalAngle = 0.0f;
float sunRadius = 4000.0f;

int main() {
    // Load project's configuration
    Config config = loadModelData("../config.json");

    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Get the primary monitor and the video mode
    GLFWmonitor* monitor = glfwGetPrimaryMonitor();
    const GLFWvidmode* mode = glfwGetVideoMode(monitor);

    // Create a fullscreen window using the screen resolution
    GLFWwindow* window = glfwCreateWindow(mode->width, mode->height, "My 3D object", monitor, NULL);
    if (!window) {
        fprintf(stderr, "ERROR: could not open window with GLFW3\n");
        glfwTerminate();
        return 1;
    }
    glfwMakeContextCurrent(window);

    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    
    // Initialize Camera
    Camera camera(config.Camera_Start_Position, glm::vec3(0.0f, 1.0f, 0.0f), -90.0f, 0.0f);
    glfwSetWindowUserPointer(window, &camera);                                                  // Set the camera as the user pointer
    glfwSetCursorPosCallback(window, mouse_callback);                                           // Set mouse callback

    // Set the key callback to handle the escape key
    glfwSetKeyCallback(window, [](GLFWwindow* window, int key, int scancode, int action, int mods) {
        if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
            glfwSetWindowShouldClose(window, GLFW_TRUE);
    });

    // Initialize GLAD or GLEW
    #ifdef _WIN32
        glewExperimental = GL_TRUE;
        if (glewInit() != GLEW_OK) {
            std::cerr << "Failed to initialize GLEW" << std::endl;
            return -1;
        }
    #else
        if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)) {
            std::cerr << "Failed to initialize GLAD" << std::endl;
            return -1;
        }
    #endif

    // Set up depth testing
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    // Disable face culling for debugging purposes
    glDisable(GL_CULL_FACE);

    // Load Model
    std::vector<Vertex> vertices;
    std::vector<unsigned int> indices;
    if (!loadOBJ(config.model_path, vertices, indices)){
        std::cerr << "Failed to load OBJ file!" << std::endl;
        return -1;
    }

    // Load shader source from files and complile them - Earth Model
    GLuint shaderProgram = UberSetupShaders("../shaders/shader_vertex.glsl", "../shaders/shader_fragment.glsl");
    GLuint VBO1, VAO1;
    setup_mesh(VAO1, VBO1, vertices);
    GLuint earth_texture = loadTexture(config.earth_texture_path);

    // Load shader source from files and complile them - Dome Model
    GLuint domeshaderProgram = UberSetupShaders("../shaders/shader_dome_vertex.glsl", "../shaders/shader_dome_fragment.glsl");
    GLuint VBO2, VAO2;
    setup_mesh(VAO2, VBO2, vertices);
    GLuint dome_texture = loadTexture(config.dome_texture_path);

    // Load shader source from files and complile them - Sun Model
    GLuint sunshaderProgram = UberSetupShaders("../shaders/shader_sun_vertex.glsl", "../shaders/shader_sun_fragment.glsl");
    GLuint VBO3, VAO3;
    setup_mesh(VAO3, VBO3, vertices);
    GLuint sun_texture = loadTexture(config.sun_texture_path);

    // Load shader source from files and complile them - Sun/Earth Line
    GLuint lineShaderProgram = UberSetupShaders("../shaders/shader_line_vertex.glsl", "../shaders/shader_line_fragment.glsl");
    GLuint lineVAO, lineVBO;
    glGenVertexArrays(1, &lineVAO);
    glGenBuffers(1, &lineVBO);
    glBindVertexArray(lineVAO);
    glBindBuffer(GL_ARRAY_BUFFER, lineVBO);
    glBufferData(GL_ARRAY_BUFFER, 2 * sizeof(glm::vec3), nullptr, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    // Define Earth transformations
    glm::mat4 earth_model = glm::mat4(1.0f);
    earth_model = glm::scale(earth_model, glm::vec3(20.0f, 20.0f, 20.0f));
    earth_model = glm::translate(earth_model, glm::vec3(0.0f, 0.0f, 0.0f));
    glm::mat4 view = glm::lookAt(glm::vec3(2.0f, 2.0f, 5.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    glm::mat4 projection = glm::perspective(glm::radians(45.0f), 800.0f / 600.0f, 0.1f, 10000.0f);

    float lastFrame = 0.0f;

    // Call playAudio to start music playback
    startAudioPlayback(config.music_audio_path);

    while (!glfwWindowShouldClose(window)) {
        // Set viewport correct size
        int width, height;
        glfwGetFramebufferSize(window, &width, &height);
        glViewport(0, 0, width, height);

        // Parameters for animation
        float currentFrame = glfwGetTime();
        float deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        processInput(window, camera, deltaTime);

        // Calculate light and sun position
        glm::vec3 lightDir(
            cos(verticalAngle) * sin(horizontalAngle),
            sin(verticalAngle),
            cos(verticalAngle) * cos(horizontalAngle)
        );
        glm::vec3 lightColor(1.0f, 1.0f, 1.0f);
        glm::vec3 sunPosition = sunRadius * lightDir;

        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glm::mat4 projection = glm::perspective(glm::radians(45.0f), static_cast<float>(mode->width) / static_cast<float>(mode->height), 0.1f, 10000.0f);
        glm::mat4 view = camera.GetViewMatrix();

        // Earth rotation animation
        float angle = glfwGetTime() * glm::radians(0.01f);
        earth_model = glm::rotate(earth_model, angle, glm::vec3(0.0f, 1.0f, 0.0f));

        // Render Earth model
        render_model(shaderProgram, earth_texture, VAO1, earth_model, view, projection, lightDir, lightColor, vertices.size());

        // Set sun model
        glm::mat4 sunModel = glm::mat4(1.0f);
        sunModel = glm::translate(sunModel, sunPosition);
        sunModel = glm::scale(sunModel, glm::vec3(100.0f, 100.0f, 100.0f));

        // Render Sun model
        render_model(sunshaderProgram, sun_texture, VAO3, sunModel, view, projection, glm::vec3(0.0f), glm::vec3(1.0f), vertices.size());

        // Setup the dome model
        glm::mat4 domeModel = glm::mat4(1.0f);
        domeModel = glm::scale(domeModel, glm::vec3(9000.0f, 9000.0f, 9000.0f));  // Increased scale to surround the entire scene
        domeModel = glm::translate(domeModel, glm::vec3(0.0f, 0.0f, 0.0f));

        // Render Dome model
        render_model(domeshaderProgram, dome_texture, VAO2, domeModel, view, projection, glm::vec3(0.0f), glm::vec3(1.0f), vertices.size());

        // Update line endpoints
        glm::vec3 linePoints[2] = {
            glm::vec3(sunModel[3]),      // Position of the sun extracted from the model matrix
            glm::vec3(earth_model[3])    // Position of the Earth extracted from the model matrix
        };
        glBindBuffer(GL_ARRAY_BUFFER, lineVBO);
        glBufferSubData(GL_ARRAY_BUFFER, 0, 2 * sizeof(glm::vec3), linePoints);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        // Draw the line
        glUseProgram(lineShaderProgram);
        glUniformMatrix4fv(glGetUniformLocation(lineShaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
        glUniformMatrix4fv(glGetUniformLocation(lineShaderProgram, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
        glBindVertexArray(lineVAO);
        glDrawArrays(GL_LINES, 0, 2);
        glBindVertexArray(0);

        // Last known camera position for checking movement
        glm::vec3 cameraPosition = camera.GetPosition();

        glfwSwapBuffers(window); // Swap the front and back buffers
        glfwPollEvents(); // Poll for and process events
    }

    glDeleteVertexArrays(1, &VAO1);
    glDeleteBuffers(1, &VBO1);
    glfwTerminate();
    return 0;
}