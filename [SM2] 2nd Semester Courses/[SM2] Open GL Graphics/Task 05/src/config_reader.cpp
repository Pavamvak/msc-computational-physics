/**
 * @file config_reader.cpp
 * @brief File responsible for loading project parameters from a JSON configuration file.
 *
 * This file contains the implementation of the `loadData` function, which reads and
 * parses project data stored in a JSON format. 
 * 
 * Detailed Explanation of JSON Structure:
 * - "models": Specifies paths for object files used as models.
 * - "textures": Specifies paths for image files used for texturing.
 * - "audio": Specifies paths for audio files used for background music and effects.
 * - "camera_n_control": Contains initial camera settings.
 *
 * @note The JSON file should be structured with specific keys matching those expected by the code,
 *       ensuring that each array or object provides the correct type and format of data.
 *
 * Example of Expected JSON structure:
 * {
 *     "models": {
 *          "earth_path":["../path/to/model.obj"]
 *     },
 *     "textures": {
 *          "earth_path":["../path/to/texture.jpg"],
 *          "sun_path": ["../path/to/texture.jpg"],
 *          "dome_path": ["../path/to/texture.jpg"]
 *     },
 *     "audio": {
 *          "music_path": "../path/to/ambient_music.mp3",
 *     },
 *     "camera_n_control": {
 *          "start_position": [0.0, 10.0, 3.0]
 *     }
 * }
 */

// Libraries
#include "../include/util/common.h"

// JSON Libraries
#include "../include/json/json.hpp"

// Custom Libraries
#include "../include/util/config_reader.h"

/**
 * @brief Loads data from a JSON file into a Config structure.
 *
 * This function opens a JSON file specified by the filename and extracts predefined fields. The data 
 * is expected to be organized in a specific structure within the JSON file (refer to README file for 
 * more information about the strusture).
 *
 * @param filename A string representing the path to the JSON file containing the model data.
 * @return Config A Config structure populated with the data from the JSON file.
 *
 */
Config loadModelData(const std::string& filename) {
    
    std::ifstream file(filename);
    nlohmann::json j;
    file >> j;

    Config config;
   
    static std::string music_audio = j["audio"]["music_path"][0];
    static std::string model = j["models"]["model_path"][0];
    static std::string earth_texture = j["textures"]["earth_path"][0];
    static std::string sun_texture = j["textures"]["sun_path"][0];
    static std::string dome_texture = j["textures"]["dome_path"][0];

    config.model_path = model.c_str();
    config.earth_texture_path = earth_texture.c_str();
    config.sun_texture_path = sun_texture.c_str();
    config.dome_texture_path = dome_texture.c_str();

    config.music_audio_path = music_audio.c_str();

    config.Camera_Start_Position = glm::vec3(
        j["camera_n_control"]["start_position"][0],
        j["camera_n_control"]["start_position"][1],
        j["camera_n_control"]["start_position"][2]
    );
    
    return config;
}