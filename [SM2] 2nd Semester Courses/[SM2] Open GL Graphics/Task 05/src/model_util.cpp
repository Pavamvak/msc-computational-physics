/**
 * @file model_util.cpp
 * @brief This file contains functions for loading object models and textures into an OpenGL application.
 *
 * It includes detailed implementations for reading .OBJ files and extracting vertex positions, texture
 * coordinates and normals, and for loading image files as textures using the stb_image library. These functionalities
 * support the rendering pipeline by providing necessary data like vertices, indices for indexed drawing, normals for 
 * lighting and texture data which are used to construct and render 3D models with textures in OpenGL.
 */

// Libraries
#include "../include/util/common.h"

// Image Libraries
#include "../include/stb/stb_image.h"

// Custom Libraries
#include "../include/util/model_util.h"

/**
 * Loads an OBJ file and extracts vertex positions, texture coordinates, and normals.
 * @param path The path to the OBJ file.
 * @param out_vertices A vector to store the vertices with positions, texture coordinates, and normals.
 * @param out_indices A vector to store the indices for indexed drawing.
 * @return True if the file was successfully loaded, false otherwise.
 */
bool loadOBJ(const char* path, std::vector<Vertex>& out_vertices, std::vector<unsigned int>& out_indices) {
    std::ifstream file(path);
    if (!file.is_open()) {
        std::cerr << "Could not open the file - '" << path << "'" << std::endl;
        return false;
    }

    std::string line;
    std::vector<glm::vec3> temp_positions;
    std::vector<glm::vec2> temp_texCoords;
    std::vector<glm::vec3> temp_normals;

    while (getline(file, line)) {
        std::stringstream ss(line);
        std::string prefix;
        ss >> prefix;

        if (prefix == "v") {
            glm::vec3 vertex;
            ss >> vertex.x >> vertex.y >> vertex.z;
            temp_positions.push_back(vertex);
        } else if (prefix == "vt") {
            glm::vec2 texCoord;
            ss >> texCoord.x >> texCoord.y;
            temp_texCoords.push_back(texCoord);
        } else if (prefix == "vn") {
            glm::vec3 normal;
            ss >> normal.x >> normal.y >> normal.z;
            temp_normals.push_back(normal);
        } else if (prefix == "f") {
            unsigned int vertexIndex[3], texCoordIndex[3], normalIndex[3];
            char slash; // to skip the '/' characters in face definitions
            for (int i = 0; i < 3; i++) {
                ss >> vertexIndex[i] >> slash >> texCoordIndex[i] >> slash >> normalIndex[i];
                // OBJ files use 1-based indexing, so we subtract 1 to convert to 0-based indexing
                Vertex vertex = {
                    temp_positions[vertexIndex[i] - 1],
                    temp_texCoords[texCoordIndex[i] - 1],
                    temp_normals[normalIndex[i] - 1]
                };
                out_vertices.push_back(vertex);
                out_indices.push_back(out_vertices.size() - 1);
            }
        }
    }
    file.close();

    return true;
}

/**
 * Loads a texture from a file and creates an OpenGL texture object.
 * @param path The path to the texture image file.
 * @return The ID of the created OpenGL texture object.
 */
GLuint loadTexture(const char* path) {
    GLuint textureID;
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_2D, textureID);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    int width, height, nrChannels;
    unsigned char* data = stbi_load(path, &width, &height, &nrChannels, 0);
    if (data) {
        GLenum format = nrChannels == 3 ? GL_RGB : GL_RGBA;
        glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);
    } else {
        std::cerr << "Failed to load texture" << std::endl;
    }
    stbi_image_free(data);

    return textureID;
}