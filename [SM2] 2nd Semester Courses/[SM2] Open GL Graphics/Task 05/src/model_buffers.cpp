/**
 * @file model_buffers.cpp
 * @brief Contains utility functions for setting up mesh data in VAOs and VBOs, and for rendering models with specific shaders, textures, and lighting.
 *
 * These utility functions abstract the OpenGL details involved in creating vertex buffers and attribute pointers for mesh data,
 * and managing the rendering pipeline with specific shaders for models in a 3D scene. This modularity aids in maintaining clean
 * code structure and reusability across different parts of the application.
 */
// Libraries
#include "../include/util/common.h"

// Custom Libraries
#include "../include/util/model_util.h"

/**
 * @brief Sets up a mesh by initializing a vertex array object (VAO) and a vertex buffer object (VBO) with given vertex data.
 * 
 * This function takes a reference to VAO and VBO, and a vector of vertices to set up the OpenGL buffers. It configures the
 * attribute pointers for vertex positions, normals, and texture coordinates which are necessary for rendering.
 *
 * @param VAO Reference to the OpenGL Vertex Array Object that will be created and configured.
 * @param VBO Reference to the OpenGL Vertex Buffer Object that will be created and filled with vertex data.
 * @param vertices The vector of vertices containing the mesh data.
 */
void setup_mesh(GLuint &VAO, GLuint &VBO, const std::vector<Vertex>& vertices) {
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);

    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), vertices.data(), GL_STATIC_DRAW);

    // Position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, position));
    glEnableVertexAttribArray(0);
    // Normal attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, normal));
    glEnableVertexAttribArray(1);
    // Texture coordinate attribute
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, texCoord));
    glEnableVertexAttribArray(2);

    glBindBuffer(GL_ARRAY_BUFFER, 0); // Unbind the buffer
    glBindVertexArray(0);             // Unbind the VAO
}

/**
 * @brief Renders a model using a specific shader program and texture, applying transformation matrices and lighting.
 * 
 * The function binds the shader program and texture, updates the shader uniforms with transformation matrices for model,
 * view, and projection, and also sets the light direction and color. It then draws the model using the provided VAO.
 *
 * @param shaderProgram The shader program to use for rendering the model.
 * @param texture The texture to apply to the model.
 * @param VAO The Vertex Array Object containing the model's vertex attributes.
 * @param model The model matrix for transforming the model.
 * @param view The view matrix for camera positioning.
 * @param projection The projection matrix for the camera's lens.
 * @param lightDir The direction of the light impacting the model.
 * @param lightColor The color of the light.
 * @param vertexCount The number of vertices to draw.
 */
void render_model(GLuint shaderProgram, GLuint texture, GLuint VAO, const glm::mat4& model, const glm::mat4& view, const glm::mat4& projection, const glm::vec3& lightDir, const glm::vec3& lightColor, int vertexCount) {
    glUseProgram(shaderProgram);
    glBindTexture(GL_TEXTURE_2D, texture);

    // Set transformation matrices
    glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
    glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "projection"), 1, GL_FALSE, glm::value_ptr(projection));

    // Set light properties
    glUniform3fv(glGetUniformLocation(shaderProgram, "lightDir"), 1, glm::value_ptr(lightDir));
    glUniform3fv(glGetUniformLocation(shaderProgram, "lightColor"), 1, glm::value_ptr(lightColor));

    // Draw the model
    glBindVertexArray(VAO);
    glDrawArrays(GL_TRIANGLES, 0, vertexCount);
    glBindVertexArray(0);
}