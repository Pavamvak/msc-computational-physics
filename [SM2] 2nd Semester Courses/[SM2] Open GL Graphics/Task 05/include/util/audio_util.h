#ifndef AUDIO_UTIL_H
#define AUDIO_UTIL_H

#include "../miniaudio/miniaudio.h"

// Function declaration
void data_callback(ma_device* pDevice, void* pOutput, const void* pInput, ma_uint32 frameCount);
void playAudio(const char* filename);
void startAudioPlayback(const char* filename);

#endif // AUDIO_UTIL_H