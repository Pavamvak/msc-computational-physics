#ifndef MODEL_UTIL_H
#define MODEL_UTIL_H

// Libraries
#include "common.h"

// Structure to hold model data
struct Vertex {
    glm::vec3 position;
    glm::vec2 texCoord;
    glm::vec3 normal;
};

// Function declaration
GLuint loadTexture(const char* path);
bool loadOBJ(const char* path, std::vector<Vertex>& out_vertices, std::vector<unsigned int>& out_indices);

#endif //MODEL_UTIL_H

