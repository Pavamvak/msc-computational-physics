#ifndef MODEL_BUFFERS_H
#define MODEL_BUFFERS_H

// Libraries
#include "common.h"
#include "model_util.h"

// Function declaration
void setup_mesh(GLuint &VAO, GLuint &VBO, const std::vector<Vertex>& vertices);
void render_model(GLuint shaderProgram, GLuint texture, GLuint VAO, const glm::mat4& model, const glm::mat4& view, const glm::mat4& projection, const glm::vec3& lightDir, const glm::vec3& lightColor, int vertexCount);

#endif // MODEL_BUFFERS_H