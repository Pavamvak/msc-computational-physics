#pragma once

// OpenGL Extension Wrangler
#ifdef _WIN32
    #define GLEW_STATIC
    #include "../glew/glew.h"
#else
    #include "../glad/glad.h"
#endif

// GLFW for window and input management
#include <GLFW/glfw3.h>

// GLM for mathematics
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Standard library includes
#include <iostream>
#include <vector>
#include <string>
#include <thread>
#include <fstream>
#include <sstream>
