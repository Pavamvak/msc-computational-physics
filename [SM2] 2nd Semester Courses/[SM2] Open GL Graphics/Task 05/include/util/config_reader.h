#ifndef CONFIG_READER_H
#define CONFIG_READER_H

// Libraries
#include "common.h"

// Structure to hold configuration parameters
struct Config {
    
    const char* model_path;

    const char* earth_texture_path;
    const char* sun_texture_path;
    const char* dome_texture_path;

    const char* music_audio_path;

    glm::vec3 Camera_Start_Position;

};

// Function declaration
Config loadModelData(const std::string& filename);

#endif // CONFIG_READER_H