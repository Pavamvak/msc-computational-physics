#ifndef INPUT_UTIL_H
#define INPUT_UTIL_H

// Libraries
#include "common.h"
#include "camera.h"

extern float horizontalAngle; // Angle around the Y axis
extern float verticalAngle;   // Angle around the X axis
extern float sunRadius;       // Radius for the sun's circular orbit

// Function declaration
void processInput(GLFWwindow* window, Camera& camera, float deltaTime);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);

#endif // INPUT_UTIL_H