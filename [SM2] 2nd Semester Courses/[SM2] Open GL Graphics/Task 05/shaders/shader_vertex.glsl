#version 330 core
layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;
layout(location = 2) in vec2 vertexTexCoord;

out vec2 TexCoord;
out vec3 Normal;
out vec3 FragPos;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main() {
    gl_Position = projection * view * model * vec4(vertexPosition, 1.0);
    TexCoord = vec2(vertexTexCoord.x, 1.0 - vertexTexCoord.y); // Pass texture coordinates to fragment shader
    FragPos = vec3(model * vec4(vertexPosition, 1.0)); // Position in world coordinates
    Normal = mat3(transpose(inverse(model))) * vertexNormal; // Transform normals to world space
}