#version 330 core
in vec2 TexCoord;
in vec3 Normal;
in vec3 FragPos;

out vec4 FragColor;

uniform sampler2D textureSampler; // Texture sampler
uniform vec3 lightDir;  // Direction of the light
uniform vec3 lightColor; // Color of the light

void main() {
    // Ambient light
    float ambientStrength = 0.01;
    vec3 ambient = ambientStrength * lightColor;

    // Diffuse light
    vec3 norm = normalize(Normal);
    float diff = max(dot(norm, normalize(lightDir)), 0.0);
    vec3 diffuse = diff * lightColor;

    // Combine results
    vec3 result = (ambient + diffuse) * texture(textureSampler, TexCoord).rgb;
    FragColor = vec4(result, 1.0);
}