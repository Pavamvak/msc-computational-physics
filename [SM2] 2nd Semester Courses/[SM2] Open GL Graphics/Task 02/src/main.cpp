/**
 * @file main.cpp
 * @brief Dynamic Wave Pattern Simulation using OpenGL.
 *
 * This program demonstrates the rendering of dynamic wave patterns using OpenGL. It includes
 * initialization of the GLFW window, configuration of OpenGL context settings, and the use of GLAD
 * for managing OpenGL function pointers. The program creates and uses shaders to manipulate vertex
 * data and to apply graphical effects through fragment shaders. This file contains the main function
 * where the application starts, initializes the graphics setup, enters the rendering loop, and handles
 * cleanup upon exiting.
 *
 * The shaders create a visual effect where two wave sources generate patterns that combine and
 * interact on a flat surface. User input is not handled in this version of the program, but the 
 * framework allows for expansion.
 *
 * @note The application assumes the availability of OpenGL 3.3 and above, and uses core-profile mode.
 *
 * Compilation dependencies:
 * - GLFW for window management.
 * - GLAD for loading OpenGL functions.
 * - GLM for mathematics.
 *
 * Usage:
 * - Compile the program with a C++ compiler that supports C++11 or above.
 * - Link against the libraries specified in the dependencies.
 * - Run the executable to view the simulation in a window titled "Dynamic Wave Pattern".
 *
 * Example compilation (if using g++ on Linux):
 * g++ main.cpp -o wave_pattern -lglfw -ldl -I/path/to/glad -I/path/to/glm
 *
 * @author Stella Kourtoglou, Panagiotis Vamvakas
 * @date YYYY-MM-DD
 */

// OpenGL and GLSL Libraries
#include "../include/glad/glad.h" 
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

// Standard Libraries
#include <iostream>
#include <vector>
#include <cmath>
#include <fstream>
#include <sstream>
#include <string>

// Vertex shader source code
const char* vertexShaderSource = "#version 330 core \n"
								 "layout (location = 0) in vec2 aPos; \n"
								 "layout (location = 1) in vec2 aTexCoords; \n"
								 "out vec2 TexCoords; \n"
								 "void main() \n"
								 "{ \n"
								 "    gl_Position = vec4(aPos, 0.0, 1.0);\n"
								 "    TexCoords = aTexCoords;\n"
								 "}";

// Fragment shader source code for creating a wave effect
const char* fragmentShaderSource = "#version 330 core \n"
								   "out vec4 FragColor; \n"
								   "in vec2 TexCoords; \n"
								   "uniform float time; \n" // Uniform variable for the animation
								   "uniform float centerY1; \n"
                                   "uniform float centerY2; \n"

								   "void main() \n"
								   "{"
    							   // Center of the first wave source
    							   "float centerX1 = 0.0 ; \n"
								   "float centerY1 = 0.7 ; \n"

                                    // Center of the second wave source
    							   "float centerX2 = 0.0 ; \n"
								   "float centerY2 = 0.2 ; \n"

    							   // Coordinates normalized to [0,1] range
    							   "float x = TexCoords.x; \n"
    							   "float y = TexCoords.y; \n"

                                   // Calculate distances from the wave centers
    							   "float dist_1 = sqrt((x - centerX1) * (x - centerX1) + (y - centerY1) * (y - centerY1)); \n"
                                   "float dist_2 = sqrt((x - centerX2) * (x - centerX2) + (y - centerY2) * (y - centerY2)); \n"

                                   // Create wave patterns based on distance and time for each center
                                   // The equation is sin(frequency*distance - time*velocity)
                                   "float wavePattern1 = sin(300.0 * dist_1 - time * 20.0); \n"
                                   "float wavePattern2 = sin(300.0 * dist_2 - time * 20.0); \n"
                                   
                                   // Calculate total wave effect (multiplication with 0.5 to normalize)
                                   "float wavePattern = (wavePattern1 + wavePattern2) * 0.5;"

                                   // Normalize result to [0,1] for display
                                   "float brightness = 0.5 + 0.5 * wavePattern; \n"
                                   "FragColor = vec4(vec3(brightness), 1.0); \n"
                                   "}";
								  
// Shader program setup and initialization
GLuint shaderProgram ;

/**
 * @brief Initializes and compiles the shaders and links them into a shader program.
 *
 * This function creates vertex and fragment shaders from source code, compiles them,
 * and links them into a shader program that the GPU can execute. The shader program
 * is stored in a global GLuint variable for use throughout the application.
 *
 * The shaders are responsible for rendering processes:
 * - Vertex Shader: Processes each vertex's position and texture coordinates.
 * - Fragment Shader: Calculates the color of each pixel by applying a wave effect
 *   based on vertex positions modified by time and wave equations.
 *
 * Global Variables:
 * - GLuint shaderProgram: Holds the shader program's ID used for rendering.
 */
void InitMyShaders()
{
    // Create vertex and fragment shader objects
    GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
    GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

    // Set the source code for each shader
    glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
    glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);

    // Compile the shaders
    glCompileShader(vertexShader);
    glCompileShader(fragmentShader);

    // Create a shader program
    shaderProgram = glCreateProgram();

    // Attach both shaders to the shader program
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);

    // Link the program
    glLinkProgram(shaderProgram);

    // Delete shaders
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
}

// // Geometry data: vertex positions and texture coordinates (the four corners of the window)
float vertices[] = {
    // positions    // texture coords
    1.0f,  1.0f,  1.0f, 1.0f,
    -1.0f,  1.0f,  0.0f, 1.0f,
    -1.0f, -1.0f,  0.0f, 0.0f,
    1.0f, -1.0f,  1.0f, 0.0f
};

unsigned int indices[] = {
    0, 1, 2, // first triangle
    0, 2, 3  // second triangle
};
								  
// OpenGL object setup for rendering
GLuint VAO, VBO, EBO ;

/**
 * @brief Initializes and configures the vertex buffer, element buffer, and vertex array objects.
 *
 * This function sets up the vertex and element buffer objects and associates them with vertex array
 * objects for use in drawing operations. It is critical for rendering as it organizes vertex data
 * and the indices used to draw the vertices in OpenGL.
 *
 * Global Variables:
 * - GLuint VAO: Vertex Array Object that stores all the state needed to supply vertex data.
 * - GLuint VBO: Vertex Buffer Object that stores the vertex data.
 * - GLuint EBO: Element Buffer Object that stores the indices of vertices.
 */
void SetUpData()
{
    // Step 1: Generate and bind the Vertex Array Object
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);

    // Step 2: Generate and bind the Vertex Buffer Object
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    // Step 3: Generate and bind the Element Buffer Object
    glGenBuffers(1, &EBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    // Step 4: Set vertex attribute pointers for vertex positions
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    // Step 5: Set vertex attribute pointers for texture coordinates
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)(2 * sizeof(float)));
    glEnableVertexAttribArray(1);

    // Note: The VAO now stores the VBO, EBO, and vertex attribute configurations
    // Unbind the VAO to avoid accidentally modifying it.
    glBindVertexArray(0);
}

/**
 * @brief Draws the first graphical object using pre-configured buffers and shader program.
 *
 * This function is responsible for rendering the first graphical object which is currently set up
 * in the vertex array object (VAO). It binds the necessary VAO, executes the drawing command
 * using the element buffer object (EBO) for indexed drawing, and then optionally unbinds the VAO.
 *
 * Usage:
 * - This function should be called within the main rendering loop after shader program binding
 *   and any required uniform updates.
 *
 * Example Usage:
 * ```
 * glUseProgram(shaderProgram);
 * DrawS1();  // Draw the first object
 * ```
 */
void DrawS1(void) 
{
	glBindVertexArray(VAO);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}

/**
 * @brief Draws the second graphical object, similar to the first, using the same pre-configured buffers.
 *
 * Functionally identical to `DrawS1`, this function can be adapted to render a second graphical object
 * with potentially different properties or states. Currently, it performs the same operations as `DrawS1`.
 *
 * Usage:
 * - Use this function to render a second object within the main rendering loop, ensuring it uses
 *   the same shader program and vertex array object setup as the first object.
 *
 * Example Usage:
 * ```
 * glUseProgram(shaderProgram);
 * DrawS2();  // Draw the second object
 * ```
 */
void DrawS2(void) 
{
	glBindVertexArray(VAO);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}

/**
 * @brief Entry point of the OpenGL application that demonstrates dynamic wave patterns.
 *
 * This function initializes the GLFW window, sets up OpenGL context and extensions with GLAD,
 * initializes shaders and vertex data, and enters the main rendering loop until the window
 * is closed. It handles initialization and termination processes of the OpenGL application,
 * manages the rendering loop, and processes user input or other window events.
 *
 * Note:
 * - The window dimensions are set to 800x600.
 * - OpenGL context version is set explicitly to 3.3.
 * - The application utilizes core-profile mode to enforce modern OpenGL features.
 *
 * Example Exit Conditions:
 * - User closes the GLFW window.
 * - Critical failure during GLFW initialization or while loading OpenGL extensions.
 */
int main()
{
    // Initialize GLFW library
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

     // Create a GLFW window
    GLFWwindow* window = glfwCreateWindow(800, 600, "Dynamic Wave Pattern", NULL, NULL);
    if (!window)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
        std::cerr << "Failed to initialize GLAD\n";
        return -1;
    }

    // Initialize shader program and set up vertex data
    InitMyShaders();
    SetUpData() ;

    // Location of 'time' uniform in the fragment shader
	GLuint timeLocation = glGetUniformLocation(shaderProgram, "time");

    // Main rendering loop
    while (!glfwWindowShouldClose(window))
    {
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // Use the shader program and update uniforms
        glUniform1f(timeLocation, glfwGetTime());
        glUseProgram(shaderProgram);

        // Draw the objects
        DrawS1() ;
        DrawS2() ;

        // Swap buffers and poll IO events
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    
    // Clean up
    glfwTerminate();
    return 0;
}
