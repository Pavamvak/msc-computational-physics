# OpenGL Dynamic Wave Pattern Simulation

This project demonstrates the use of modern OpenGL to simulate dynamic wave patterns on a flat surface. It features:
- GLFW for window management,
- GLAD for OpenGL function loading,
- GLM for mathematical computations.

## System Requirements

- **Graphics Card:** GPU that supports OpenGL 3.3 or higher.
- **Operating System:** Any OS that can compile and run C++ code with the required libraries (Windows, macOS, Linux).
- **Libraries:** GLFW, GLAD, GLM.

## Setup and Installation

1. **Clone the Repository:**
    ```bash
   git clone https://gitlab.com/Pavamvak/msc-computational-physics.git
   cd "msc-computational-physics/[SM2] 2nd Semester Courses/[SM2] Open GL Graphics/Task 02"
   ```
2. **Install Dependencies:**
- On Ubuntu:
    ```bash
    sudo apt-get install libglfw3 libglfw3-dev libglm-dev
    ```
- Ensure GLAD is included as part of the project (no installation needed).
3. **Build the Project:**
Using CMake:
    ```bash
    mkdir build
    cd build
    cmake ..
    make
    ```

## Usage
Run the executable from the build directory:
```bash
./wave_pattern
```
This will open a window displaying the dynamic wave pattern simulation. The simulation runs indefinitely until the window is closed.

## Configuration
The project is set up to use shader programs for the vertex and fragment shaders that create a dynamic wave effect. To change the behavior of the wave pattern, such as the frequency, speed, or source positions of the waves, modify the shader source code in main.cpp.

## Contributing
Contributions are welcome. To contribute:

- Fork the repository.
- Create a feature branch (git checkout -b new-feature).
- Commit your changes (git commit -am 'Add some feature').
- Push to the branch (git push origin new-feature).
- Open a pull request.

## License
This project is licensed under the MIT License - see the LICENSE.md file for details.

## Acknowledgements
- GLFW, GLAD, GLM: For providing essential graphics libraries.
- OpenGL Community: For tutorials and guidance on OpenGL programming.

### Notes

The shader code provided within the project is designed to simulate interacting wave patterns. Any modifications to the shader parameters (like wave frequency or amplitude) can be tested by altering the respective uniform variables in the fragment shader section.