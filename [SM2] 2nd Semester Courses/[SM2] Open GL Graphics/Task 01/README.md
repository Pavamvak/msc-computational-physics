# OpenGL Lifebuoy Rendering Project

This project demonstrates the use of modern OpenGL to render a 2D lifebuoy. Utilizing:
- GLFW for window management,
- GLAD for OpenGL function loading,
- GLM for vector and matrix operations.

## System Requirements

- **Graphics Card:** GPU with OpenGL 3.3 support.
- **Operating System:** Linux.
- **Libraries:** GLFW, GLAD, GLM.

## Setup and Installation

1. **Clone the Repository:**
   ```bash
   git clone https://gitlab.com/Pavamvak/msc-computational-physics.git
   cd "msc-computational-physics/[SM2] 2nd Semester Courses/[SM2] Open GL Graphics/Task 01"
   ```
2. **Install Dependencies:**
- On Ubuntu:
    ```bash
    sudo apt-get install libglfw3 libglfw3-dev libglm-dev
- Ensure GLAD is included as part of the project (no installation needed).
3. **Build the Project:**
Using CMake:
    ```bash
    mkdir build
    cd build
    cmake ..
    make
    ```

## Usage
Run the executable from the build directory:
```bash
./lifebuoy
```
ESC key: Close the application.

## Configuration
Modify the config.json file located in the assets directory to change parameters such as the inner and outer radii of the lifebuoy, color schemes, and pattern sizes.
The project expects the JSON file to have a specific structure corresponding to the fields in the Config struct. Here's an expected JSON structure example:
```json
{
    "inner_radius": 0.7,
    "outer_radius": 0.3,
    "pattern_size": 50,
    "primary_color": {"r": 1.0, "g": 0.0, "b": 0.0},
    "secondary_color": {"r": 1.0, "g": 1.0, "b": 1.0}
}
```


## Contributing
Feel free to fork the repository and submit pull requests. You can also open issues to discuss potential improvements or report bugs.

## License
This project is licensed under the MIT License - see the LICENSE.md file for details.

## Acknowledgements
- GLFW, GLAD, GLM: For providing essential graphics libraries.
- OpenGL Community: For tutorials and guidance on OpenGL programming.
