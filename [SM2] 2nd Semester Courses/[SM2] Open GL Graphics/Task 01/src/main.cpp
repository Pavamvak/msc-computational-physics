/**
 * @file main.cpp
 * @brief Main entry point for the OpenGL program to render a lifebuoy.
 *
 * This program demonstrates the use of modern OpenGL for rendering a 2D lifebuoy using GLFW,
 * GLAD, and GLM libraries. It showcases fundamental OpenGL concepts such as shader compilation,
 * vertex buffer and vertex array creation, and drawing commands within a rendering loop.
 *
 * Usage:
 * - The program creates a fullscreen window on the primary monitor.
 * - It reads vertex and fragment shader programs from external files to render a lifebuoy.
 * - Configuration parameters such as inner radius, outer radius, pattern size, and colors are read
 *   from a JSON file, allowing dynamic adjustments without recompiling the code.
 * - The window can be closed by pressing the ESC key.
 *
 * Configuration:
 * - Ensure that the 'config.json' file is present in the directory specified in the code, and
 *   it correctly specifies parameters.
 * - Example JSON structure:
 *   {
 *       "inner_radius": 0.7,
 *       "outer_radius": 0.3,
 *       "pattern_size": 50,
 *       "primary_color": {"r": 1.0, "g": 0.0, "b": 0.0},
 *       "secondary_color": {"r": 1.0, "g": 1.0, "b": 1.0}
 *   }
 *
 * @note Ensure that the shader files 'shader_vertex.glsl' and 'shader_fragment.glsl' are located
 *       in the '../src/' directory relative to the executable for proper shader loading.
 *
 * @author [Panagiotis Vamvakas]
 * @date [12-05-2024]
 * @version [3.1]
 */

// OpenGL Libraries
#include "../include/glad/glad.h" 
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

// Native Libraries
#include <iostream>
#include <vector>
#include <cmath>
#include <fstream>
#include <sstream>
#include <string>

// Custom Libraries
#include "../include/util/shader_util.h"
#include "../include/util/input_handlers.h"
#include "../include/util/geometry.h"
#include "../include/util/config_reader.h"
#include "../include/json/json.hpp"
using json = nlohmann::json;

// TODO: Add premium rendering features.

int main() {

    // Load configuration
    Config config = loadConfig("../config.json");

    // Initialize GLFW library
    if (!glfwInit()) {
        std::cerr << "Failed to initialize GLFW\n";
        return -1;
    }

    // Set OpenGL version to 3.3 and use core profile
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Set the window to be non-resizable
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

    // Get the primary monitor and the video mode
    GLFWmonitor* monitor = glfwGetPrimaryMonitor();
    const GLFWvidmode* mode = glfwGetVideoMode(monitor);

    // Create a fullscreen window using the screen resolution
    GLFWwindow* window = glfwCreateWindow(mode->width, mode->height, "Colorful Torus", monitor, NULL);
    if (!window) {
        std::cerr << "Failed to create GLFW window\n";
        glfwTerminate();
        return -1;
    }

    // Make the window's context current
    glfwMakeContextCurrent(window);

    // Set the function to be called whenever the window is resized
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    // Initialize GLAD before calling any OpenGL function
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
        std::cerr << "Failed to initialize GLAD\n";
        return -1;
    }

    // Load shader source from files and compile them
    GLuint shaderProgram = UberSetupShaders("../src/shader_vertex.glsl", "../src/shader_fragment.glsl");

    // After creating the shader program, get the location of the projection matrix uniform
    GLuint projLoc = glGetUniformLocation(shaderProgram, "projection");

    // Generate vertices for a torus
    std::vector<float> vertices = generateTorusVertices(config.inner_radius, config.outer_radius, 400, config.pattern_size, config.primary_color, config.secondary_color);
    
    // Set up vertex data (and buffer(s)) and configure vertex attributes
    GLuint VAO, VBO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);

    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float), vertices.data(), GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);

    // Set the background color
    glClearColor(0.57f, 0.8f, 0.95f, 1.0f);

    // Enable blending to handle transparency
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // Main loop
    while (!glfwWindowShouldClose(window)) {
        processInput(window); // Process input from keyboard
        
        // Calculate the aspect ratio each frame (or only on resize if optimizing)
        int width, height;
        glfwGetFramebufferSize(window, &width, &height); // Get the current size of the window
        float aspect_ratio = static_cast<float>(width) / static_cast<float>(height);
        glm::mat4 projection = glm::ortho(-aspect_ratio, aspect_ratio, -1.0f, 1.0f, -1.0f, 1.0f);

        // Pass the projection matrix to the shader
        glUseProgram(shaderProgram);
        glUniformMatrix4fv(projLoc, 1, GL_FALSE, &projection[0][0]);

        glClear(GL_COLOR_BUFFER_BIT); // Clear the color buffer

        // Render commands
        glBindVertexArray(VAO);
        glDrawArrays(GL_TRIANGLES, 0, vertices.size() / 6); // Draw the triangles

        glfwSwapBuffers(window); // Swap front and back buffers
        glfwPollEvents(); // Poll for and process events
    }

    // Clean up
    glfwDestroyWindow(window);
    glfwTerminate();
    return 0;
}