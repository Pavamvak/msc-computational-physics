// Native Libraries
#include <cmath>

// OpenGL Libraries
#include <glm/glm.hpp>

// Custom Libraries
#include "../include/util/geometry.h"

/**
 * Generates vertices for a torus and returns them in a vector.
 * 
 * @param r1 Radius of the inner circle of the torus.
 * @param r2 Radius of the outer circle of the torus.
 * @param n Number of segments along the circumference.
 * @param patternSize Number of segments after which the color pattern repeats.
 * @param primaryColor Primary color of the lifebuoy, generally this is used for the colored section of the lifebuoy.
 * @param secondaryColor Secondary color of the lifebuoy, generally this is white.
 * @return A vector of floating point numbers representing the vertex positions and colors.
 * 
 * This function calculates the positions and colors of vertices needed to draw a torus.
 * Each segment's color alternates based on the pattern size to create a striped effect.
 * Vertices are calculated using trigonometric functions to position them in 3D space.
 */
std::vector<float> generateTorusVertices(float r1, float r2, int n, int patternSize, const glm::vec3& primaryColor, const glm::vec3& secondaryColor) {
    // Initialize variable to save vertices
    std::vector<float> vertices;

    // Calculate angle step for circumference
    float dPhi = 2 * M_PI / n;
    
    for (int i = 0; i < n; ++i) {
        
        // Compute the index for color sequence based on pattern size
        int colorIndex = (i * n + i) / patternSize % 2;
        
        // Determine colors based on the sequence (red or white)
        glm::vec3 color = (colorIndex == 0) ? primaryColor : secondaryColor;

        float phi = i * dPhi, nextPhi = (i + 1) * dPhi;

        // Vertices for the inner and outer circles
        glm::vec3 inner1(r1 * cos(phi), r1 * sin(phi), 0.0);
        glm::vec3 inner2(r1 * cos(nextPhi), r1 * sin(nextPhi), 0.0);
        glm::vec3 outer1(r2 * cos(phi), r2 * sin(phi), 0.0);
        glm::vec3 outer2(r2 * cos(nextPhi), r2 * sin(nextPhi), 0.0);

        // First triangle
        vertices.push_back(inner1.x); vertices.push_back(inner1.y); vertices.push_back(inner1.z);
        vertices.push_back(color.r); vertices.push_back(color.g); vertices.push_back(color.b);

        vertices.push_back(outer1.x); vertices.push_back(outer1.y); vertices.push_back(outer1.z);
        vertices.push_back(color.r); vertices.push_back(color.g); vertices.push_back(color.b);

        vertices.push_back(inner2.x); vertices.push_back(inner2.y); vertices.push_back(inner2.z);
        vertices.push_back(color.r); vertices.push_back(color.g); vertices.push_back(color.b);

        // Second triangle
        vertices.push_back(inner2.x); vertices.push_back(inner2.y); vertices.push_back(inner2.z);
        vertices.push_back(color.r); vertices.push_back(color.g); vertices.push_back(color.b);

        vertices.push_back(outer1.x); vertices.push_back(outer1.y); vertices.push_back(outer1.z);
        vertices.push_back(color.r); vertices.push_back(color.g); vertices.push_back(color.b);

        vertices.push_back(outer2.x); vertices.push_back(outer2.y); vertices.push_back(outer2.z);
        vertices.push_back(color.r); vertices.push_back(color.g); vertices.push_back(color.b);
        
    }
    return vertices;
}
