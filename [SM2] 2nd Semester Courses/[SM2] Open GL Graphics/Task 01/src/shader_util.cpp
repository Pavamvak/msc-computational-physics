// Native Libraries
#include <fstream>
#include <sstream>
#include <iostream>

// Custom Libraries
#include "../include/glad/glad.h"

/**
 * Reads shader source code from a file and returns it as a string.
 * 
 * @param filePath The path to the shader file.
 * @return A string containing the shader source code.
 * 
 * This function opens the specified shader file, reads its contents into a string, and returns it.
 * If the file cannot be opened or read, it logs an error to the standard error output.
 */
std::string readShaderSource(const char* filePath) {
    std::string code;
    std::ifstream shaderFile;
    std::stringstream shaderStream;

    shaderFile.exceptions (std::ifstream::failbit | std::ifstream::badbit);
    try {
        shaderFile.open(filePath);
        shaderStream << shaderFile.rdbuf();		
        shaderFile.close();
        code = shaderStream.str();		
    }
    catch (std::ifstream::failure e) {
        std::cerr << "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ: " << filePath << std::endl;
    }
    return code;
}

/**
 * @brief Compiles a shader from given source code.
 *
 * This function takes the type of shader (vertex or fragment) and its source code as input,
 * compiles the shader, and returns the shader ID. It also handles and logs any compilation errors.
 *
 * @param type The type of the shader to compile. Use GL_VERTEX_SHADER or GL_FRAGMENT_SHADER.
 * @param source A C-string containing the source code of the shader.
 * @return GLuint The compiled shader's ID. Returns 0 if compilation fails.
 */
GLuint compileShader(GLenum type, const char* source) {
    GLuint shader = glCreateShader(type);
    glShaderSource(shader, 1, &source, NULL);
    glCompileShader(shader);

    int success;
    char infoLog[512];
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(shader, 512, NULL, infoLog);
        std::cerr << "ERROR::SHADER::COMPILATION_FAILED\n" << infoLog << std::endl;
    }
    return shader;
}

/**
 * @brief Creates a shader program from vertex and fragment shader source files.
 *
 * This function reads the source code of vertex and fragment shaders from the specified file paths,
 * compiles the shaders, and links them into a shader program. It checks for and logs any errors 
 * during compilation and linking. The shaders are deleted after they are linked into the shader program,
 * as they are no longer needed.
 *
 * @param vertexPath Path to the vertex shader source file.
 * @param fragmentPath Path to the fragment shader source file.
 * @return GLuint The ID of the linked shader program. Returns 0 if an error occurs.
 */
GLuint UberSetupShaders(const char* vertexPath, const char* fragmentPath) {
    // Read shader source code from files
    std::string vertexCode = readShaderSource(vertexPath);
    std::string fragmentCode = readShaderSource(fragmentPath);

    // Convert string to const GLchar* for shader compilation
    const GLchar* vShaderCode = vertexCode.c_str();
    const GLchar* fShaderCode = fragmentCode.c_str();

    // Compile shaders
    GLuint vertexShader = compileShader(GL_VERTEX_SHADER, vShaderCode);
    GLuint fragmentShader = compileShader(GL_FRAGMENT_SHADER, fShaderCode);

    // Create Shader Program
    GLuint shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);
    glLinkProgram(shaderProgram);

    // Check for linking errors
    int success;
    char infoLog[512];
    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
        std::cerr << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
    }

    // Delete the shaders as they're linked into our program now and no longer necessary
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    return shaderProgram;
}