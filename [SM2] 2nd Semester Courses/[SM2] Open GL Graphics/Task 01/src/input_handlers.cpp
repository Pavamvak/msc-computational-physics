// OpenGL Libraries
#include <GLFW/glfw3.h>

// Custom Libraries
#include "../include/util/input_handlers.h"

/**
 * @brief Adjusts the OpenGL viewport when the window size changes.
 *
 * This callback function is called by GLFW whenever the window size changes.
 * It recalculates the viewport dimensions based on the new window size to maintain
 * proper aspect ratios and centers the viewport within the window. This ensures
 * that the rendered scene scales proportionally and remains centered, avoiding
 * any distortion or stretching.
 *
 * @param window Pointer to the GLFWwindow that received the event.
 * @param width The new width of the window.
 * @param height The new height of the window.
 *
 * @note This function should be set as a callback with glfwSetFramebufferSizeCallback.
 * 
 * Example:
 *   GLFWwindow* window = glfwCreateWindow(800, 600, "Window", NULL, NULL);
 *   glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
 *
 * The calculation of viewport dimensions is based on the new window's aspect ratio.
 * If the window is wider than it is tall, the height is used to determine the size
 * of the viewport to maintain the aspect ratio. Conversely, if the window is taller
 * than it is wide, the width is used. The viewport is then centered within the
 * larger dimension by adjusting the x or y offset.
 */
void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
    // Calculate the aspect ratio dependent scale factor
    float aspectRatio = static_cast<float>(width) / static_cast<float>(height);
    int vpWidth, vpHeight;

    if (aspectRatio > 1) {
        // Wider than tall: Fit to height
        vpHeight = height;
        vpWidth = static_cast<int>(height * aspectRatio);
    } else {
        // Taller than wide: Fit to width
        vpWidth = width;
        vpHeight = static_cast<int>(width / aspectRatio);
    }

    // Center the viewport in the window
    int vpX = (width - vpWidth) / 2;
    int vpY = (height - vpHeight) / 2;
    glViewport(vpX, vpY, vpWidth, vpHeight);
}

/**
 * Processes user input.
 * 
 * @param window The GLFW window to check for user input.
 * 
 * This function checks if the user has pressed the ESC key. If so, it sets the window to close.
 */
void processInput(GLFWwindow* window) {
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
}
