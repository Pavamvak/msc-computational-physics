// Native Libraries
#include <fstream>

// Custom Libraries
#include "../include/util/config_reader.h"
#include "../include/json/json.hpp"

/**
 * @file config_reader.cpp
 * @brief Configuration reader implementation for loading rendering parameters from a JSON file.
 *
 * This source file provides the implementation for the `loadConfig` function, which is designed
 * to load and parse configuration parameters from a specified JSON file. The parameters include
 * settings for rendering a lifebuoy, such as inner and outer radius, color pattern sizes, and color values.
 *
 * Usage:
 * Config config = loadConfig("path/to/config.json");
 *
 * @param filename The path to the JSON configuration file.
 * @return Config A struct containing all the configuration parameters loaded from the JSON file.
 *
 * The function expects the JSON file to have a specific structure corresponding to the fields
 * in the Config struct. Here's an expected JSON structure example:
 * 
 * {
 *     "inner_radius": 0.7,
 *     "outer_radius": 0.3,
 *     "pattern_size": 50,
 *     "primary_color": {"r": 1.0, "g": 0.0, "b": 0.0},
 *     "secondary_color": {"r": 1.0, "g": 1.0, "b": 1.0}
 * }
 *
 * The function uses the nlohmann/json library for JSON parsing. It opens the specified file,
 * reads the JSON content into a `nlohmann::json` object, and then extracts each parameter,
 * converting them to the appropriate data types needed by the Config structure. Color values
 * are converted into `glm::vec3` types suitable for OpenGL color specifications.
 *
 * Exceptions:
 * - std::ifstream::failure if the file cannot be opened or read.
 * - nlohmann::json::parse_error if the JSON is malformed.
 * - nlohmann::json::type_error if type conversions fail (e.g., expecting a float but finding a string).
 *
 * Note: Users must ensure that the JSON file exists and adheres to the expected format to avoid exceptions.
 * The path provided should be relative to the executable or an absolute path.
 */
Config loadConfig(const std::string& filename) {
    std::ifstream i(filename);
    nlohmann::json j;
    i >> j;

    Config config;
    config.inner_radius = j["inner_radius"];
    config.outer_radius = j["outer_radius"];
    config.pattern_size = j["pattern_size"];
    config.primary_color = glm::vec3(
        j["primary_color"]["r"],
        j["primary_color"]["g"],
        j["primary_color"]["b"]
    );
    config.secondary_color = glm::vec3(
        j["secondary_color"]["r"],
        j["secondary_color"]["g"],
        j["secondary_color"]["b"]
    );

    return config;
}
