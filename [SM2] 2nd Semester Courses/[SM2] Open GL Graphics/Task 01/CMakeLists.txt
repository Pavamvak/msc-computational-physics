cmake_minimum_required(VERSION 3.0.0)
project(lifebuoy VERSION 0.1.0)
cmake_policy(SET CMP0072 NEW)

find_package(OpenGL REQUIRED)

add_executable(lifebuoy 
    src/glad.c
    src/main.cpp
    src/shader_util.cpp
    src/input_handlers.cpp
    src/geometry.cpp
    src/config_reader.cpp
)

target_link_libraries(lifebuoy 
    glfw
    OpenGL::GL)