#ifndef CONFIG_READER_H
#define CONFIG_READER_H

#include <glm/glm.hpp>
#include <string>

// Structure to hold configuration parameters
struct Config {
    float inner_radius;
    float outer_radius;
    int pattern_size;
    glm::vec3 primary_color;
    glm::vec3 secondary_color;
};

// Function to load configuration from a JSON file
Config loadConfig(const std::string& filename);

#endif // CONFIG_READER_H
