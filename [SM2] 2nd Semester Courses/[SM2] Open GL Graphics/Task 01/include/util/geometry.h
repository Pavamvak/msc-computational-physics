#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <vector>
#include <glm/glm.hpp>

// Function declaration
std::vector<float> generateTorusVertices(float r1, float r2, int n, int patternSize, const glm::vec3& primaryColor, const glm::vec3& secondaryColor);

#endif
