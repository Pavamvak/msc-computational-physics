#ifndef INPUT_HANDLERS_H
#define INPUT_HANDLERS_H

#include <GLFW/glfw3.h>

// Function declaration
void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow* window);

#endif
