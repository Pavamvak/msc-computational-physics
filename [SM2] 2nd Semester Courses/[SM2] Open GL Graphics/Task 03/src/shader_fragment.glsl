#version 330 core
in vec3 vrtxCol;
out vec4 FragColor;
void main()
{
    FragColor = vec4(vrtxCol, 1.0);
}