// Custom Libraries
#include "../include/util/config_reader.h"
#include "../include/util/buffer_setup.h"

/**
 * @brief Initializes vertex array objects (VAOs) and vertex buffer objects (VBOs) for a cube and axes.
 *
 * This function sets up the graphics data for rendering a cube and coordinate axes by initializing VAOs and VBOs.
 * It uploads vertex and color data to the GPU and configures vertex attribute pointers. The function expects
 * vertex and color data to be provided through a Config structure, which should contain arrays for cube vertices,
 * cube colors, axes vertices, and axes colors. For more information on the Config structure, see `config_reader.cpp`.
 *
 * @param config A Config structure containing arrays of vertex and color data for the cube and axes.
 * @param bufObjs A reference to a BufferObjects structure where the IDs of the generated VAOs and VBOs will be stored.
 *
 * After calling this function, the bufferObjects will contain the OpenGL IDs for VAOs and VBOs
 * which can be used for rendering the cube and axes in the graphics loop.
 */
void SetupVerticesData(Config config, BufferObjects& bufObjs)
{
    // Generate the cube VAO
    glGenVertexArrays(1, &bufObjs.VAO1);
    glBindVertexArray(bufObjs.VAO1);
    glGenBuffers(1, &bufObjs.VBO1);
    glBindBuffer(GL_ARRAY_BUFFER, bufObjs.VBO1);
    glBufferData(GL_ARRAY_BUFFER, config.cubeVertices.size() * sizeof(float), config.cubeVertices.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), 0);
    glEnableVertexAttribArray(0);

    // Colors for cube
    glGenBuffers(1, &bufObjs.VBOc1);
    glBindBuffer(GL_ARRAY_BUFFER, bufObjs.VBOc1);
    glBufferData(GL_ARRAY_BUFFER, config.cubeColors.size() * sizeof(float), config.cubeColors.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), 0);
    glEnableVertexAttribArray(1);

    // Generate the axes VAO
    glGenVertexArrays(1, &bufObjs.VAO2);
    glBindVertexArray(bufObjs.VAO2);
    glGenBuffers(1, &bufObjs.VBO2);
    glBindBuffer(GL_ARRAY_BUFFER, bufObjs.VBO2);
    glBufferData(GL_ARRAY_BUFFER, config.axesVertices.size() * sizeof(float), config.axesVertices.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), 0);
    glEnableVertexAttribArray(0);

    // Colors for axes
    glGenBuffers(1, &bufObjs.VBOc2);
    glBindBuffer(GL_ARRAY_BUFFER, bufObjs.VBOc2);
    glBufferData(GL_ARRAY_BUFFER, config.axesColors.size() * sizeof(float), config.axesColors.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), 0);
    glEnableVertexAttribArray(1);
}