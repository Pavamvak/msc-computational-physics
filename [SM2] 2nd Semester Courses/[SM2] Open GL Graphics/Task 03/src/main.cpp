/**
 * @file main.cpp
 * @brief Main entry point for the OpenGL project implementing camera control to a scene.
 *
 * This program showcases a dynamic 3D scene featuring a colored cube and coordinate axes,
 * rendered using OpenGL. It demonstrates the use of GLFW for window management, GLAD for
 * loading OpenGL functions, and GLM for mathematical operations. The application initializes
 * a GLFW window, sets up vertex and color data, and employs shader programs to handle rendering
 * and transformations. A key feature of this program is the interactive camera control, which
 * allows users to move around and explore the scene from different perspectives. This is achieved
 * through the implementation of a camera model using GLM's lookAt and perspective projection functions.
 *
 * The main function initializes the GLFW library, creates a window, and configures the context for
 * rendering. It loads the model data from a JSON configuration file, initializes OpenGL objects
 * and shaders, and enters a rendering loop. Within the loop, it handles user input, updates the
 * camera position based on keyboard and mouse input, and renders the scene every frame.
 *
 * Expected JSON structure:
 * {
 *     "cube": {
 *         "vertices": [0.0, 1.0, ...],
 *         "colors": [1.0, 0.0, 0.0, ...]
 *     },
 *     "axes": {
 *         "vertices": [0.0, 1.0, ...],
 *         "colors": [1.0, 1.0, 1.0, ...]
 *     }
 * }
 * 
 * The application aims to provide a foundational framework for developing more complex 3D
 * interactive applications, focusing on camera movement and basic 3D geometries.
 *
 * @author [Panagiotis Vamvakas]
 * @date [19-05-2024]
 * @version [1.1]
 */

// OpenGL Libraries
#include "../include/glad/glad.h" 
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Native Libraries
#include <iostream>

// Custom Libraries
#include "../include/util/shader_util.h"
#include "../include/util/config_reader.h"
#include "../include/util/buffer_setup.h"
#include "../include/util/rendering.h"
#include "../include/util/input_util.h"
#include "../include/json/json.hpp"
using json = nlohmann::json;

//Objects
//user's drawing world
float xmin = -2.0f, xmax = 2.0f, ymin = -2.0f, ymax = 2.0f, zmin=-10.0f, zmax=10.0f;

int main(void) {
    Config config = loadModelData("../config.json");

    BufferObjects bufObjs;

    // start GL context and O/S window using the GLFW helper library
    if (!glfwInit()) {
        fprintf(stderr, "ERROR: could not start GLFW3\n");
        return 1;
    } 
    
    // Get the primary monitor and the video mode
    GLFWmonitor* monitor = glfwGetPrimaryMonitor();
    const GLFWvidmode* mode = glfwGetVideoMode(monitor);

    // Create a fullscreen window using the screen resolution
    GLFWwindow* window = glfwCreateWindow(mode->width, mode->height, "My 3D object", monitor, NULL);
    if (!window) {
        fprintf(stderr, "ERROR: could not open window with GLFW3\n");
        glfwTerminate();
        return 1;
    }
    glfwMakeContextCurrent(window);
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    
    Camera camera;  // Camera instance
    glfwSetWindowUserPointer(window, &camera);  // Set the camera as the user pointer
    glfwSetCursorPosCallback(window, mouse_callback);  // Set mouse callback

    // Set the key callback to handle the escape key
    glfwSetKeyCallback(window, [](GLFWwindow* window, int key, int scancode, int action, int mods) {
        if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
            glfwSetWindowShouldClose(window, GLFW_TRUE);
    });

    // Initialize GLAD before calling any OpenGL function
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
        std::cerr << "Failed to initialize GLAD\n";
        return -1;
    }
    
    // Load shader source from files and compile them
    GLuint shaderProgram = UberSetupShaders("../src/shader_vertex.glsl", "../src/shader_fragment.glsl");

    SetupVerticesData(config, bufObjs);

    float lastFrame = 0.0f;
    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window))
    {
        float currentFrame = glfwGetTime();
        float deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        processInput(window, camera, deltaTime);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glEnable(GL_DEPTH_TEST);

        myInit(shaderProgram, mode->width, mode->height, camera);
        mydisplay1(shaderProgram, 0.0f, bufObjs);  // Adjust or remove angle as needed

        glfwSwapBuffers(window);
        glfwPollEvents();
    } 
    // close GL context and any other GLFW resources
    glfwTerminate();
    return 0;
}