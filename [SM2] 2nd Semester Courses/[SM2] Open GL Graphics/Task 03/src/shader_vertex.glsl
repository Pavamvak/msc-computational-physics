#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aCol;
uniform mat4 projection;
uniform mat4 modeltrans;
out vec3 vrtxCol;
void main()
{
    vrtxCol = aCol;
    gl_Position = projection*modeltrans*vec4(aPos.x, aPos.y, aPos.z, 1.0);
}