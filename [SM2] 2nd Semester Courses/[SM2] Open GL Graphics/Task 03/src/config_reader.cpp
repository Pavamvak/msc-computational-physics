// Native Libraries
#include <iostream>
#include <fstream>
#include <sstream>

// Custom Libraries
#include "../include/util/config_reader.h"
#include "../include/json/json.hpp"

/**
 * @brief Loads model data from a JSON file into a Config structure.
 *
 * This function opens a JSON file specified by the filename and extracts predefined fields
 * such as vertices and colors for a cube and coordinate axes. The data is expected to be
 * organized in a specific structure within the JSON file, with separate arrays for cube
 * vertices, cube colors, axes vertices, and axes colors. Each array should contain floating
 * point numbers representing the geometric and color data for the models.
 *
 * @param filename A string representing the path to the JSON file containing the model data.
 * @return Config A Config structure populated with the vertices and colors data from the JSON file.
 *
 * Expected JSON structure:
 * {
 *     "cube": {
 *         "vertices": [0.0, 1.0, ...],
 *         "colors": [1.0, 0.0, 0.0, ...]
 *     },
 *     "axes": {
 *         "vertices": [0.0, 1.0, ...],
 *         "colors": [1.0, 1.0, 1.0, ...]
 *     }
 * }
 *
 */
Config loadModelData(const std::string& filename) {
    
    std::ifstream file(filename);
    nlohmann::json j;
    file >> j;

    Config config;
    config.cubeVertices = j["cube"]["vertices"].get<std::vector<float>>();
    config.cubeColors = j["cube"]["colors"].get<std::vector<float>>();
    config.axesVertices = j["axes"]["vertices"].get<std::vector<float>>();
    config.axesColors = j["axes"]["colors"].get<std::vector<float>>();

    return config;
}