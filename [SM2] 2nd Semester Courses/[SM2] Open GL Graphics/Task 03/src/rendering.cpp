// Custom Libraries
#include <glm/glm.hpp>
#include "../include/glad/glad.h" 
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "../include/util/rendering.h"

/**
 * @brief Initializes the rendering environment, sets up the shader program, and configures the camera projection.
 * 
 * This function prepares the OpenGL context for rendering by setting the clear color and activating
 * the specified shader program. It sets up a perspective projection based on the given field of view,
 * aspect ratio, and near/far planes. It also updates the camera's view matrix based on its current
 * position and front vector, then passes the combined transformation matrix to the shader program.
 *
 * @param shaderProgram The OpenGL shader program ID to be used for rendering.
 * @param Wwidth0 The width of the window, used to calculate the aspect ratio.
 * @param Wheight0 The height of the window, used to calculate the aspect ratio.
 * @param camera Reference to a Camera object, which provides the position and front vector for the view matrix.
 */
void myInit(GLuint shaderProgram, int Wwidth0, int Wheight0, Camera& camera) {
    glClearColor(0.2, 0.2, 0.4, 0.0);
    glUseProgram(shaderProgram);

    // Set up perspective projection
    float fov = 45.0f; // Field of view, in degrees
    float aspectRatio = 1.0f * Wwidth0 / Wheight0;
    float nearPlane = 0.1f;
    float farPlane = 100.0f;
    glm::mat4 myprojectionmatrix = glm::perspective(glm::radians(fov), aspectRatio, nearPlane, farPlane);

    // Update camera's LookAt matrix
    glm::vec3 viewDirection = camera.position + camera.front;
    glm::mat4 mycamera = glm::lookAt(camera.position, viewDirection, camera.up);

    // Inform GLSL
    glm::mat4 ttm = myprojectionmatrix * mycamera; // Total transformation matrix
    int transform_matrix_location = glGetUniformLocation(shaderProgram, "projection");
    glUniformMatrix4fv(transform_matrix_location, 1, GL_FALSE, glm::value_ptr(ttm));
}

/**
 * @brief Renders the scene objects, applying transformations to display them at a specific orientation.
 *
 * This function is responsible for rendering the geometry configured by the vertex arrays stored in bufObjs.
 * It first applies a rotation transformation around the X-axis by the specified angle to the model matrix.
 * The transformed matrix is then uploaded to the shader program. It renders the objects by binding the VAOs
 * for a cube and axes, setting the line width, and making the draw calls.
 *
 * @param shaderProgram The OpenGL shader program ID to be used for rendering.
 * @param angle The rotation angle in degrees, applied around the X-axis.
 * @param bufObjs Structure containing OpenGL buffer and array object IDs used for rendering.
 */
void mydisplay1(GLuint shaderProgram, float angle, BufferObjects& bufObjs)  //body fixed lines
{
    glm::mat4 myIdentitymatrix = glm::mat4(1.0f);
    glm::mat4 mymodelmatrix = glm::rotate(myIdentitymatrix, glm::radians(angle), glm::vec3(1.0f, 0.0f, 0.0f));
    int transform_matrix_location = glGetUniformLocation(shaderProgram, "modeltrans");
    glUniformMatrix4fv(transform_matrix_location, 1, GL_FALSE, glm::value_ptr(mymodelmatrix));
    //--------------------------
    glBindVertexArray(bufObjs.VAO1);
    glDrawArrays(GL_QUADS, 0, 16);
    glLineWidth(4.0); 
    glBindVertexArray(bufObjs.VAO2);
    glDrawArrays(GL_LINES, 0, 6);
}