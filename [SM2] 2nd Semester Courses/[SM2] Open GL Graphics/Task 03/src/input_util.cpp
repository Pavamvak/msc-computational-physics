#include "../include/util/input_util.h"

/**
 * @brief Processes keyboard input to update camera positioning.
 * 
 * This function handles keyboard inputs to control the camera's movement in the 3D space.
 * The W, S, A, D keys are used to move the camera forward, backward, left, and right, respectively.
 * Space and Left Shift control vertical movement up and down. The function calculates movement
 * based on the camera speed and the delta time from the last frame, ensuring consistent movement
 * speeds regardless of frame rate.
 * 
 * @param window Pointer to the GLFWwindow that is capturing the keyboard input.
 * @param camera Reference to the Camera object that will be manipulated based on the input.
 * @param deltaTime Time elapsed since the last frame, used to scale the camera's movement speed.
 */
void processInput(GLFWwindow* window, Camera& camera, float deltaTime) {
    const float cameraSpeed = 2.5f * deltaTime;
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        camera.position += cameraSpeed * glm::normalize(glm::vec3(camera.front.x, 0.0f, camera.front.z)); // Move forward
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        camera.position -= cameraSpeed * glm::normalize(glm::vec3(camera.front.x, 0.0f, camera.front.z)); // Move backward
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        camera.position -= glm::normalize(glm::cross(camera.front, camera.up)) * cameraSpeed; // Move left
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        camera.position += glm::normalize(glm::cross(camera.front, camera.up)) * cameraSpeed; // Move right

    // Add vertical movement
    if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
        camera.position.y += cameraSpeed; // Move up
    if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
        camera.position.y -= cameraSpeed; // Move down
}

/**
 * @brief Handles mouse movement input to control the camera's orientation.
 *
 * This callback function updates the camera's yaw and pitch based on the mouse's current position
 * relative to its last position, effectively controlling the camera's viewing direction.
 * It applies a sensitivity factor to the mouse movement to smooth the camera's rotation.
 * When the mouse is captured for the first time, it resets the last position coordinates to prevent sudden jumps.
 *
 * @param window Pointer to the GLFWwindow where the mouse movement is being captured.
 * @param xpos The new x-coordinate of the mouse cursor.
 * @param ypos The new y-coordinate of the mouse cursor.
 */
void mouse_callback(GLFWwindow* window, double xpos, double ypos) {
    Camera* camera = static_cast<Camera*>(glfwGetWindowUserPointer(window));
    if (camera->firstMouse) {
        camera->lastX = xpos;
        camera->lastY = ypos;
        camera->firstMouse = false;
    }

    float xoffset = xpos - camera->lastX;
    float yoffset = camera->lastY - ypos; // Coordinates go from bottom to top
    camera->lastX = xpos;
    camera->lastY = ypos;

    const float sensitivity = 0.1f;
    xoffset *= sensitivity;
    yoffset *= sensitivity;

    camera->yaw += xoffset;
    camera->pitch += yoffset;

    // Constrain the pitch to prevent flipping
    if (camera->pitch > 89.0f)
        camera->pitch = 89.0f;
    if (camera->pitch < -89.0f)
        camera->pitch = -89.0f;

    glm::vec3 front;
    front.x = cos(glm::radians(camera->yaw)) * cos(glm::radians(camera->pitch));
    front.y = sin(glm::radians(camera->pitch));
    front.z = sin(glm::radians(camera->yaw)) * cos(glm::radians(camera->pitch));
    camera->front = glm::normalize(front);
}