# OpenGL Camera Control Project

This project demonstrates the use of modern OpenGL to control the camera around the scene. Utilizing:
- GLFW for window management,
- GLAD for OpenGL function loading,
- GLM for vector and matrix operations.

## System Requirements

- **Graphics Card:** GPU with OpenGL 3.3 support.
- **Operating System:** Linux.
- **Libraries:** GLFW, GLAD, GLM.

## Setup and Installation

1. **Clone the Repository:**
   ```bash
   git clone https://gitlab.com/Pavamvak/msc-computational-physics.git
   cd "msc-computational-physics/[SM2] 2nd Semester Courses/[SM2] Open GL Graphics/Task 03"
   ```
2. **Install Dependencies:**
- On Ubuntu:
    ```bash
    sudo apt-get install libglfw3 libglfw3-dev libglm-dev
- Ensure GLAD is included as part of the project (no installation needed).
3. **Build the Project:**
Using CMake:
    ```bash
    mkdir build
    cd build
    cmake ..
    make
    ```

## Usage
Run the executable from the build directory:
```bash
./camera_control
```
ESC key: Close the application.

## Configuration
Modify the config.json file located in the assets directory to change parameters cube vertices and colors, as well as the axes vertices and colors.
The project expects the JSON file to have a specific structure corresponding to the fields in the Config struct. Here's an expected JSON structure example:
```json
{
    "cube": {
        "vertices": [0.0, 1.0, ...],
        "colors": [1.0, 0.0, 0.0, ...]
    },
    "axes": {
        "vertices": [0.0, 1.0, ...],
        "colors": [1.0, 1.0, 1.0, ...]
    }
}
```

## Contributing
Feel free to fork the repository and submit pull requests. You can also open issues to discuss potential improvements or report bugs.

## License
This project is licensed under the MIT License - see the LICENSE.md file for details.

## Acknowledgements
- GLFW, GLAD, GLM: For providing essential graphics libraries.
- OpenGL Community: For tutorials and guidance on OpenGL programming.