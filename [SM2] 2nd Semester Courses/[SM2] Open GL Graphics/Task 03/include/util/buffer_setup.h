#ifndef BUFFER_SETUP_H
#define BUFFER_SETUP_H

#include "config_reader.h"
#include "../glad/glad.h" 

// Structure to hold buffers
struct BufferObjects {
    unsigned int VAO1, VAO2;
    unsigned int VBO1, VBOc1, VBO2, VBOc2;
};

// Function declaration
void SetupVerticesData(Config config, BufferObjects& bufObjs);

#endif // BUFFER_SETUP_H