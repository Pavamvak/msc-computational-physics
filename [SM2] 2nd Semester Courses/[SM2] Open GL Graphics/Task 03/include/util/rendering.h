#ifndef RENDERING_H
#define RENDERING_H

#include <glm/glm.hpp>
#include "config_reader.h"
#include "buffer_setup.h"

// Structure to hold camera setup
struct Camera {
    glm::vec3 position;
    glm::vec3 front;
    glm::vec3 up;
    float lastX, lastY;
    float yaw, pitch;
    bool firstMouse;

    Camera() : position(glm::vec3(1.0f, 1.0f, 1.0f)), front(glm::vec3(0.0f, 0.0f, -1.0f)), 
               up(glm::vec3(0.0f, 1.0f, 0.0f)), lastX(400), lastY(400), yaw(-90.0f), pitch(0.0f), firstMouse(true) {}
};

// Function declaration
void myInit(GLuint shaderProgram, int Wwidth0, int Wheight0, Camera& camera);
void mydisplay1(GLuint shaderProgram, float angle, BufferObjects& bufObjs);

#endif // RENDERING_H