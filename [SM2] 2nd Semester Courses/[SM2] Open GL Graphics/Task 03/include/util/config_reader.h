#ifndef CONFIG_READER_H
#define CONFIG_READER_H

#include <vector>
#include <string>

// Structure to hold configuration parameters
struct Config {
    std::vector<float> cubeVertices;
    std::vector<float> cubeColors;
    std::vector<float> axesVertices;
    std::vector<float> axesColors;
};

// Function declaration
Config loadModelData(const std::string& filename);

#endif // CONFIG_READER_H