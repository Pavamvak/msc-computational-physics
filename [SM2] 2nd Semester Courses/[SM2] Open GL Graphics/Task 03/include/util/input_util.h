#ifndef INPUT_UTIL_H
#define INPUT_UTIL_H

#include "../glad/glad.h"
#include <GLFW/glfw3.h>
#include "rendering.h"

// Function declaration
void processInput(GLFWwindow* window, Camera& camera, float deltaTime);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);

#endif // INPUT_UTIL_H