#ifndef SHADER_UTIL_H
#define SHADER_UTIL_H

#include <string>
#include "../glad/glad.h"

// Function declaration
std::string readShaderSource(const char* filePath);
GLuint compileShader(GLenum type, const char* source);
GLuint UberSetupShaders(const char* vertexPath, const char* fragmentPath);

#endif
