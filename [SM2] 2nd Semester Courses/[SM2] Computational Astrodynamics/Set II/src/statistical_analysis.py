"""
This script is a helper for post-processing orbital data. It analyzes and compares
generated state vectors with reference data from STK (Satellite Tool Kit) files.
It calculates errors for each state vector component and provides visual plots 
and statistical summaries.

This helper script is used primarely to analyze and compare the accuracy of the 
propagation using different time steps and/or different propagation durations,
with reference CSV files from AGI's STK.

When using the script, be sure to set in the `main` the following:
- Name of the satellite at `name` parameter.
- Inclination for the propagation at `i` parameter.
- Time step of the propagation at `dt` parameter.
- Duration of the propagation at `T` parameter.
- Propagated CSV file path at `generated_file` parameter.
- STK CSV file path at `stk_file` parameter.

The propagated CSV files, should be included in the `output` directory. If, no
file exists, use first the `main.py` to create new files (be sure to set the 
satellite's parameters and set to `True` to save the state vectors). STK files
for the cases analyzed are included in the 'stk_files` directory, unzip to use.

Important: for the analysis to be valid, be sure the scenario/orbit analyzed is 
the same both in the propagated file and the STK file. To analyze any scenario
different than the ones already analyzed, the STK file should be created first,
using the HPOP propagator and setting the same orbit parameters.

The naming scheme of the propagated CSV files is:
{Satellite's name}_state_vectors_rk4_{No. of periods}_{Inclination}_{Time step}.csv

The naming scheme of the STK CSV files is:
LEOSAT_Inertial_Position_Velocity_dt_{Time step}.csv (for the different time step 
scenarios at one orbit)
LEOSAT_Inertial_Position_Velocity_T_{No. of periods}.csv (for the different propagation 
duration scenarios - time step can be derived from the  file's logged time)

Functions:
- calculate_errors: Computes errors between generated and reference STK data.
- main: Main function to execute the analysis with predefined parameters.

Author    : [Panagiotis Vamvakas]
Date      : [29-06-2024]
Version   : [1.0]
"""

import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def calculate_errors(generated_file, stk_file, name, inc, dt, T):
    """
    Calculates and plots the errors between generated state vectors and STK reference data.

    Parameters:
    - generated_file  : Path to the CSV file with generated state vectors.
    - stk_file        : Path to the CSV file with STK reference state vectors.
    - name            : Name of the satellite (used for output file naming).
    - inc             : Inclination of the satellite orbit.
    - dt              : Time step used in the simulation.
    - T               : Number of periods simulated.

    Raises:
    - FileNotFoundError : If the specified files do not exist.
    - AssertionError    : If the number of rows in the files do not match.
    """

    # Check if the files exist
    if not os.path.exists(generated_file):
        raise FileNotFoundError(f"The generated file does not exist: {generated_file}")
    if not os.path.exists(stk_file):
        raise FileNotFoundError(f"The reference STK file does not exist: {stk_file}")
    
    # Load data from the Excel and CSV files
    df_generated = pd.read_csv(generated_file)
    df_stk = pd.read_csv(stk_file)
    
    # Select the relevant columns for comparison
    df_generated_selected = df_generated.iloc[:, :6]  # 1st to 6th columns
    df_stk_selected = df_stk.iloc[:, 1:7]   # 2nd to 7th columns
    
    # Ensure both selected dataframes have the same number of rows
    assert df_generated_selected.shape[0] == df_stk_selected.shape[0], "Number of rows do not match between the files."
    
    # Calculate the errors
    errors = (df_stk_selected.values - df_generated_selected.values)
    
    # Define plot styles and colors
    plot_styles = ['#FF9999', '#66B2FF', '#99FF99', '#FFCC99', '#FF99FF']
    columns = df_generated_selected.columns
    
    # Create figure and subplots
    fig, axes = plt.subplots(len(columns), 1, figsize=(10, len(columns) * 2))
    fig.suptitle('Errors between Generated and STK values', color='white')
    
    # Set figure background color
    fig.patch.set_facecolor('#2E2E2E')
    
    for i, column in enumerate(columns):
        ax = axes[i]
        ax.plot(errors[:, i], plot_styles[i % len(plot_styles)])
        ax.set_title(f'Error in {column}', color='white')
        ax.set_xlabel('Row', color='white')
        ax.set_ylabel('Error', color='white')
        
        # Set axes and grid color
        ax.set_facecolor('#2E2E2E')
        ax.xaxis.label.set_color('white')
        ax.yaxis.label.set_color('white')
        ax.tick_params(axis='x', colors='white')
        ax.tick_params(axis='y', colors='white')
        ax.spines['top'].set_color('white')
        ax.spines['bottom'].set_color('white')
        ax.spines['left'].set_color('white')
        ax.spines['right'].set_color('white')
        ax.grid(True, which='both', linestyle='--', linewidth=0.5, color='grey')
    
    plt.tight_layout(rect=[0, 0, 1, 0.95])
    
    output_dir = os.path.join(os.path.dirname(__file__), '../graphs')
    os.makedirs(output_dir, exist_ok=True)
    graph_path = os.path.join(output_dir, f'{name}_Cartesian_Elements_Error_Graph_{inc}_{dt}_{T}.jpg')
    plt.savefig(graph_path, dpi=300)
    
    plt.show()
    
    # Calculate and print the average error and standard deviation for each column
    for i, column in enumerate(columns):
        avg_error = np.mean(errors[:, i])
        std_error = np.std(errors[:, i])
        print(f'Column: {column}')
        print(f'  Average Error:                  {avg_error}')
        print(f'  Standard Deviation of Error:    {std_error}')
       
    print("---------------------------------------------------------------")


def main():
    """
    Main function to perform analysis by comparing generated and STK data files.
    It sets parameters, file paths, and calls the error calculation function.
    """
    #Parameters for logging
    name  = 'LEOSAT'
    i     = 90
    dt    = 100
    T     = 1000
    
    print("Analyzing files...")
    print("---------------------------------------------------------------")
    
    # Get the directory of the current script
    script_dir = os.path.dirname(os.path.abspath(__file__))
    
    # File paths relative to the script directory
    generated_file = os.path.join(script_dir, '..', 'output', f'LEOSAT_state_vectors_rk4_{T}_90_{dt}.csv')
    stk_file = os.path.join(script_dir, '..', 'stk_files', f'LEOSAT_Inertial_Position_Velocity_T_{T}.csv')
    
    print(f"Reference STK file: {stk_file}")
    print(f"Generated file: {generated_file}")
    print("---------------------------------------------------------------")
    
    # Calculate errors and perform analysis
    calculate_errors(generated_file, stk_file, name, i, dt, T)

if __name__ == "__main__":
    main()
