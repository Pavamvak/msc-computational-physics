"""
This script is a helper for post-processing Keplerian elements from CSV files. It calculates the rates of change for 
orbital elements to compare with the averaged theory, for various inclinations of the LEOSAT and MOLSAT. The script 
processes saved Keplerian elements to find element change rates, plots them, and prints results for analysis.

Note that it is expected to explicitly set all the files to be analyzed on the `file_list` parameter of the `main`
function. All the files shall be located in a directory named `output`. If no output files are present, use the 
`main.py` first to generate the .csv files for various inclinations (you can change the inclination on the 
`parameters.json`).

Also be sure to set the range on the call of `plot_rates` from the `main` to match the range of the inclinations 
you want to analyze and the files you have included in the `file_list` parameter.

The naming scheme of the .csv files is:
{Satellite's name}_{Data type: keplerian_elements or state_vectors}_rk4_{No. of periods}_{Inclination}_{Time step}.csv

Functions:
- calculate_rates: Computes the rates of change for each Keplerian element.
- print_rates: Displays the calculated rates for each file.
- plot_rates: Visualizes the rates of change for files with a specific prefix.
- main: Executes the script by calculating, printing, and plotting the rates for provided files.

Author    : [Panagiotis Vamvakas]
Date      : [29-06-2024]
Version   : [1.0]
"""
import os
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression

def calculate_rates(file_list):
    """
    Calculates the rates for each Keplerian element in the file list.

    Parameters:
    file_list (list): List of file paths to the CSV files.

    Returns:
    list: List of dictionaries containing the file name and the calculated rates for each element.
    """
    rates = []

    for file in file_list:
        abs_path = os.path.abspath(file)
        if not os.path.exists(abs_path):
            raise FileNotFoundError(f"The file does not exist: {abs_path}")

        # Load data from the CSV file
        df = pd.read_csv(abs_path)
        
        # Ensure the file has at least 7 columns (including time)
        if df.shape[1] < 7:
            raise ValueError(f"The file {abs_path} does not have at least 7 columns.")
        
        # Extract time and elements, handling NaN values
        time = df.iloc[:, 6].values.reshape(-1, 1)  # 7th column is time
        elements = {
            'a'      : df.iloc[:, 0].values,  # Semi-major axis
            'e'      : df.iloc[:, 1].values,  # Eccentricity
            'i'      : df.iloc[:, 2].values,  # Inclination
            'RAAN'   : df.iloc[:, 3].values,  # Right Ascension of Ascending Node
            'ω'      : df.iloc[:, 4].values,  # Argument of Periapsis
            'ν'      : df.iloc[:, 5].values,  # True Anomaly
        }

        rates_file = {'file': os.path.basename(file)}
        for key, values in elements.items():
            values = pd.Series(values).ffill().bfill().values
            model = LinearRegression()
            model.fit(time, values)
            rate = model.coef_[0] * 86400  # Convert from units/sec to units/day
            rates_file[f'd{key}/dt'] = rate

        rates.append(rates_file)
    
    return rates

def print_rates(rates):
    """
    Prints the rates for each file.

    Parameters:
    rates (list): List of dictionaries containing the file name and the calculated rates for each element.
    """
    for rate in rates:
        print(f"File: {rate['file']}")
        for key, value in rate.items():
            if key != 'file':
                print(f"{key}: {value} units/day")
        print("---------------------------------------------------------------")

def plot_rates(rates, prefix, x_range):
    """
    Plots the rates for files with a specific prefix.

    Parameters:
    rates (list)    : List of dictionaries containing the file name and the calculated rates for each element.
    prefix (str)    : Prefix of the files to be plotted (e.g., 'LEOSAT', 'MOLSAT').
    x_range (list)  : Range of x values for the plot (e.g., [85, 105] for LEOSAT, [58, 68] for MOLSAT).
    """
    x_values = []
    rates_dict = {f'd{key}/dt': [] for key in ['a', 'e', 'i', 'RAAN', 'ω', 'ν']}

    for rate in rates:
        if rate['file'].startswith(prefix):
            inclination = float(rate['file'].split('_')[5])
            x_values.append(inclination)
            for key in rates_dict.keys():
                rates_dict[key].append(rate[key])

    fig, axes = plt.subplots(3, 2, figsize=(15, 10))
    axes = axes.flatten()
    fig.patch.set_facecolor('#2E2E2E')

    for i, key in enumerate(rates_dict.keys()):
        axes[i].plot(x_values, rates_dict[key], marker='o', linestyle='-', color='white')
        axes[i].set_title(f'Mean {key}', color='white')
        axes[i].set_xlabel('Inclination (degrees)', color='white')
        axes[i].set_ylabel(f'{key} (units/day)', color='white')
        axes[i].set_facecolor('#2E2E2E')
        axes[i].xaxis.label.set_color('white')
        axes[i].yaxis.label.set_color('white')
        axes[i].tick_params(axis='x', colors='white')
        axes[i].tick_params(axis='y', colors='white')
        axes[i].spines['top'].set_color('white')
        axes[i].spines['bottom'].set_color('white')
        axes[i].spines['left'].set_color('white')
        axes[i].spines['right'].set_color('white')
        axes[i].grid(True, which='both', linestyle='--', linewidth=0.5, color='grey')

    plt.tight_layout()
    
    output_dir = os.path.join(os.path.dirname(__file__), '../graphs')
    os.makedirs(output_dir, exist_ok=True)
    graph_path = os.path.join(output_dir, f'{prefix}_Cartesian_Elements_Rates_Graph.jpg')
    plt.savefig(graph_path, dpi=300)
    
    plt.show()
    
    
def main():
    """
    Main function to execute the script. It calculates the derivatives for a list of CSV files,
    prints the mean and standard deviation of the derivatives for each file and plots the average 
    derivatives for LEOSAT and MOLSAT files.
    
    Note that it is expected to explicitly set all the files to be analyzed on the `file_list` parameter. All 
    the files shall be located in a directory named `output`. If no output files are present, use the `main.py` 
    first to generate the .csv files for various inclinations (you can change the inclination on the `parameters.json`).

    Also be sure to set the range on the call of `plot_rates` to match the range of the inclinations you want to analyze 
    and the files you have included in the `file_list` parameter.

    The naming scheme of the .csv files is:
    {Satellite's name}_{Data type: keplerian_elements or state_vectors}_rk4_{No. of periods}_{Inclination}_{Time step}.csv
    """
    # Directory paths
    script_dir = os.path.dirname(os.path.abspath(__file__))
    output_dir = os.path.join(script_dir, '..', 'output')

    # List of files
    file_list = [
        os.path.join(output_dir, 'LEOSAT_keplerian_elements_rk4_1000_85_100.csv'),
        os.path.join(output_dir, 'LEOSAT_keplerian_elements_rk4_1000_86_100.csv'),
        os.path.join(output_dir, 'LEOSAT_keplerian_elements_rk4_1000_87_100.csv'),
        os.path.join(output_dir, 'LEOSAT_keplerian_elements_rk4_1000_88_100.csv'),
        os.path.join(output_dir, 'LEOSAT_keplerian_elements_rk4_1000_89_100.csv'),
        os.path.join(output_dir, 'LEOSAT_keplerian_elements_rk4_1000_90_100.csv'),
        os.path.join(output_dir, 'LEOSAT_keplerian_elements_rk4_1000_91_100.csv'),
        os.path.join(output_dir, 'LEOSAT_keplerian_elements_rk4_1000_92_100.csv'),
        os.path.join(output_dir, 'LEOSAT_keplerian_elements_rk4_1000_93_100.csv'),
        os.path.join(output_dir, 'LEOSAT_keplerian_elements_rk4_1000_94_100.csv'),
        os.path.join(output_dir, 'LEOSAT_keplerian_elements_rk4_1000_95_100.csv'),
        os.path.join(output_dir, 'LEOSAT_keplerian_elements_rk4_1000_96_100.csv'),
        os.path.join(output_dir, 'LEOSAT_keplerian_elements_rk4_1000_97_100.csv'),
        os.path.join(output_dir, 'LEOSAT_keplerian_elements_rk4_1000_98_100.csv'),
        os.path.join(output_dir, 'LEOSAT_keplerian_elements_rk4_1000_99_100.csv'),
        os.path.join(output_dir, 'LEOSAT_keplerian_elements_rk4_1000_100_100.csv'),
        os.path.join(output_dir, 'LEOSAT_keplerian_elements_rk4_1000_101_100.csv'),
        os.path.join(output_dir, 'LEOSAT_keplerian_elements_rk4_1000_102_100.csv'),
        os.path.join(output_dir, 'LEOSAT_keplerian_elements_rk4_1000_103_100.csv'),
        os.path.join(output_dir, 'LEOSAT_keplerian_elements_rk4_1000_104_100.csv'),
        os.path.join(output_dir, 'LEOSAT_keplerian_elements_rk4_1000_105_100.csv'),
        os.path.join(output_dir, 'MOLSAT_keplerian_elements_rk4_1000_58.0_100.csv'),
        os.path.join(output_dir, 'MOLSAT_keplerian_elements_rk4_1000_59.0_100.csv'),
        os.path.join(output_dir, 'MOLSAT_keplerian_elements_rk4_1000_60.0_100.csv'),
        os.path.join(output_dir, 'MOLSAT_keplerian_elements_rk4_1000_61.0_100.csv'),
        os.path.join(output_dir, 'MOLSAT_keplerian_elements_rk4_1000_62.0_100.csv'),
        os.path.join(output_dir, 'MOLSAT_keplerian_elements_rk4_1000_63.0_100.csv'),
        os.path.join(output_dir, 'MOLSAT_keplerian_elements_rk4_1000_64.0_100.csv'),
        os.path.join(output_dir, 'MOLSAT_keplerian_elements_rk4_1000_65.0_100.csv'),
        os.path.join(output_dir, 'MOLSAT_keplerian_elements_rk4_1000_66.0_100.csv'),
        os.path.join(output_dir, 'MOLSAT_keplerian_elements_rk4_1000_67.0_100.csv'),
        os.path.join(output_dir, 'MOLSAT_keplerian_elements_rk4_1000_68.0_100.csv')
    ]
    
    print("Calculating derivatives...")
    print("---------------------------------------------------------------")
    
    # Calculate derivatives
    rates = calculate_rates(file_list)
    
    # Print results
    print_rates(rates)
    
    # Plot average derivatives for LEOSAT files
    plot_rates(rates, 'LEOSAT', range(85, 106))
    
    # Plot average derivatives for MOLSAT files
    plot_rates(rates, 'MOLSAT', range(58, 69))

if __name__ == "__main__":
    main()
