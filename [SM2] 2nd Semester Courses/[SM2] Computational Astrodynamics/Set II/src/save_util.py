"""
This module provides functions for saving state vectors and Cartesian elements
of a satellite's orbit to CSV files. It is used for post-processing the results
of orbital simulations.

The CSV files are saved in the `output` directory (if it does not exist it is created).

The naming scheme of the .csv files is:
{Satellite's name}_{Data type: keplerian_elements or state_vectors}_rk4_{No. of periods}_{Inclination}_{Time step}.csv

Functions:
- save_state_vectors_to_csv: Saves state vectors (position and velocity) to a CSV file.
- save_cartesian_elements_to_csv: Saves orbital elements derived from state vectors to a CSV file.

Author    : [Panagiotis Vamvakas]
Date      : [29-06-2024]
Version   : [1.0]
"""

import os
import pandas as pd
import orbital_util as orb_util

def save_state_vectors_to_csv(xn, yn, zn, vxn, vyn, vzn, tn, selected_satellite, number_of_periods, inclination, dt):
    """
    Saves the state vectors (position and velocity) of a satellite to a CSV file.

    Parameters:
    - xn, yn, zn              :Arrays of x, y, and z positions (in km).
    - vxn, vyn, vzn           :Arrays of x, y, and z velocities (in km/sec).
    - tn                      :Array of time points (in seconds).
    - selected_satellite      :Name of the satellite (str).
    - number_of_periods       :Number of orbital periods simulated (int).
    - inclination             :Inclination of the orbit (in degrees).
    - dt                      :Time step of the simulation (in seconds).
    """
    output_dir = os.path.join(os.path.dirname(__file__), '../output')
    os.makedirs(output_dir, exist_ok=True)
    file_path = os.path.join(output_dir, f'{selected_satellite}_state_vectors_rk4_{number_of_periods}_{inclination}_{dt}.csv')
    df = pd.DataFrame({
        'Position X (km)'    : xn,
        'Position Y (km)'    : yn,
        'Position Z (km)'    : zn,
        'Velocity X (km/sec)': vxn,
        'Velocity Y (km/sec)': vyn,
        'Velocity Z (km/sec)': vzn,
        f'Time {dt} sec'     : tn
    })
    df.to_csv(file_path, index=False)

def save_cartesian_elements_to_csv(state_vectors, tn, selected_satellite, number_of_periods, inclination, dt, mu):
    """
    Saves the orbital elements derived from state vectors to a CSV file.

    Parameters:
    - state_vectors       : Array of state vectors containing positions and velocities.
    - tn                  : Array of time points (in seconds).
    - selected_satellite  : Name of the satellite (str).
    - number_of_periods   : Number of orbital periods simulated (int).
    - inclination         : Inclination of the orbit (in degrees).
    - dt                  : Time step of the simulation (in seconds).
    - mu                  : Gravitational parameter of the central body (in km^3/sec^2).
    """
    a_arr, e_arr, i_arr, RAAN_arr, w_arr, TA_arr = [], [], [], [], [], []

    for state in state_vectors:
        R = state[:3]
        V = state[3:]
        a, e, i, RAAN, w, TA = orb_util.coe_from_sv(R, V, mu)
        a_arr.append(a)
        e_arr.append(e)
        i_arr.append(i)
        RAAN_arr.append(RAAN)
        w_arr.append(w)
        TA_arr.append(TA)

    output_dir = os.path.join(os.path.dirname(__file__), '../output')
    os.makedirs(output_dir, exist_ok=True)
    file_path = os.path.join(output_dir, f'{selected_satellite}_cartesian_elements_rk4_{number_of_periods}_{inclination}_{dt}.csv')
    
    df = pd.DataFrame({
        'Semi-major Axis (km)'         : a_arr,
        'Eccentricity'                 : e_arr,
        'Inclination (degrees)'        : i_arr,
        'RAAN (degrees)'               : RAAN_arr,
        'Argument of Perigee (degrees)': w_arr,
        'True Anomaly (degrees)'       : TA_arr,
        f'Time {dt} sec'               : tn
    })
    df.to_csv(file_path, index=False)