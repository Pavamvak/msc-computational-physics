"""
This file contains functions used for orbit propagation and conversion between state vectors and classical orbital elements.
The functions use numerical methods such as Runge-Kutta for integration and handle the effects of J2 perturbation.

Functions:
- sv_from_coe      : Converts classical orbital elements to state vectors.
- coe_from_sv      : Converts state vectors to classical orbital elements.
- runge_kutta_step : Performs a single Runge-Kutta step for orbit propagation.
- runge_kutta_4    : Integrates the orbit using the 4th-order Runge-Kutta method.
- fx, fy, fz       : Compute position derivatives.
- fv_x, fv_y, fv_z : Compute velocity derivatives considering J2 perturbation.

For more details on the functions see README file.

Author    : [Panagiotis Vamvakas]
Date      : [29-06-2024]
Version   : [1.0]
"""
import numpy as np
from numba import jit
from tqdm import tqdm

def sv_from_coe(coe,mu):
    """
    This function computes the state vector (r,v) (in ECI) from the
    classical orbital elements (coe) (in degrees).

    input: 
    coe  - classical orbital elements [a,e,i,RAAN,w,TA] (in degrees)
    mu   - gravitational parameter of central body (km^3/s^2)

    output:
    R[0]    - X position (km)
    R[1]    - Y position (km)
    R[2]    - Z position (km)
    V[0]    - X position (km/sec)
    V[1]    - Y position (km/sec)
    V[2]    - Z position (km/sec)
    
    Functions required: none
    """

    # Extract classical orbital elements
    a, e, i, RAAN, w, TA = coe

    # Convert angles from degrees to radians
    i = np.radians(i)
    RAAN = np.radians(RAAN)
    w = np.radians(w)
    TA = np.radians(TA)
    
    # Compute the distance (r) and speed (v) in the orbit plane
    r = a * (1 - e**2) / (1 + e * np.cos(TA))
    h = np.sqrt(mu * a * (1 - e**2))
    
    # Position in the perifocal frame
    x_orbit = r * np.cos(TA)
    y_orbit = r * np.sin(TA)
    z_orbit = 0
    
    # Velocity in the perifocal frame
    vx_orbit = - np.sqrt(mu / (a * (1 - e**2))) * np.sin(TA)
    vy_orbit = np.sqrt(mu / (a * (1 - e**2))) * (e + np.cos(TA))
    vz_orbit = 0
    
    # Rotation matrices
    R3_W = np.array([[np.cos(RAAN), np.sin(RAAN), 0],
                     [-np.sin(RAAN), np.cos(RAAN), 0],
                     [0, 0, 1]])
    
    R1_i = np.array([[1, 0, 0],
                     [0, np.cos(i), np.sin(i)],
                     [0, -np.sin(i), np.cos(i)]])
    
    R3_w = np.array([[np.cos(w), np.sin(w), 0],
                     [-np.sin(w), np.cos(w), 0],
                     [0, 0, 1]])
    
    # Combined rotation matrix
    Q_pX = np.transpose(R3_W) @ np.transpose(R1_i) @ np.transpose(R3_w)
    
    # Position and velocity vectors in the perifocal frame
    R_orbit = np.array([x_orbit, y_orbit, z_orbit])
    V_orbit = np.array([vx_orbit, vy_orbit, vz_orbit])
    
    # Compute position and velocity vectors in the ECI frame
    R = Q_pX @ R_orbit
    V = Q_pX @ V_orbit
    
    return R[0], R[1], R[2], V[0], V[1], V[2]

def coe_from_sv(R,V,mu):
    """
    This function computes the classical orbital elements (coe)
    from the state vector (R,V).
    
    input:
    mu   - gravitational parameter of central body (km^3/s^2)
    R    - position vector (km)
    V    - velocity vector (km/s)
    
    output:
    coe  - classical orbital elements [a,e,i,RAAN,w,TA]
    
    Functions required: none
    """
    
    # Compute the specific angular momentum and it normal
    h = np.cross(R, V)
    h_norm = np.linalg.norm(h)
    
    # Compute the position and velocity vector normals
    r = np.linalg.norm(R)
    v = np.linalg.norm(V)
    
    # Compute the eccentricity vector and it normal
    e_vec = (1/mu) * ((v**2 - mu/r) * R - np.dot(R, V) * V)
    e = np.linalg.norm(e_vec)
    
    # Compute the node vector and it normal
    K = np.array([0, 0, 1])  # Z axis unit vector
    N = np.cross(K, h)
    N_norm = np.linalg.norm(N)
    
    # Compute the inclination
    i = np.arccos(h[2] / h_norm)
    
    # Compute the right ascension of the ascending node (RAAN)
    if N_norm != 0:
        RAAN = np.arccos(N[0] / N_norm)
        if N[1] < 0:
            RAAN = 2 * np.pi - RAAN
    else:
        RAAN = 0
    
    # Compute the argument of perigee
    if N_norm != 0 and e != 0:
        w = np.arccos(np.dot(N, e_vec) / (N_norm * e))
        if e_vec[2] < 0:
            w = 2 * np.pi - w
    else:
        w = 0
    
    # Compute the true anomaly
    if e != 0:
        TA = np.arccos(np.dot(e_vec, R) / (e * r))
        if np.dot(R, V) < 0:
            TA = 2 * np.pi - TA
    else:
        cp = np.cross(N, R)
        if cp[2] >= 0:
            TA = np.arccos(np.dot(N, R) / (N_norm * r))
        else:
            TA = 2 * np.pi - np.arccos(np.dot(N, R) / (N_norm * r))
    
    # Compute the semi-major axis
    a = h_norm**2 / (mu * (1 - e**2))
    
    # Return the classical orbital elements
    coe = [a, e, np.degrees(i), np.degrees(RAAN), np.degrees(w), np.degrees(TA)]
    
    return coe

@jit(nopython=True)
def fx(v_x):
    """
    Computes the derivative of x with respect to time (dx/dt).

    Parameters:
    v_x (float) - Velocity in the x-direction (km/s).

    Returns:
    float - Derivative of x (km/s).
    """
    return v_x

@jit(nopython=True)
def fy(v_y):
    """
    Computes the derivative of y with respect to time (dy/dt).

    Parameters:
    v_y (float) - Velocity in the y-direction (km/s).

    Returns:
    float - Derivative of y (km/s).
    """
    return v_y

@jit(nopython=True)
def fz(v_z):
    """
    Computes the derivative of z with respect to time (dz/dt).

    Parameters:
    v_z (float) - Velocity in the z-direction (km/s).

    Returns:
    float - Derivative of z (km/s).
    """
    return v_z

@jit(nopython=True)
def fv_x(x, y, z, mu, J2, Re):
    """
    Computes the derivative of vx with respect to time (dvx/dt), considering gravitational 
    and J2 perturbation effects.

    Parameters:
    x, y, z (float) - Position components (km).
    mu (float)      - Gravitational parameter (km^3/s^2).
    J2 (float)      - Second zonal harmonic of the geopotential.
    Re (float)      - Earth's radius (km).

    Returns:
    float - Derivative of vx (km/s^2).
    """
    r = np.sqrt(x**2 + y**2 + z**2)
    ax_grav = -mu * x / r**3
    ax_j2 = -3/2 * J2 * mu * Re**2 / r**5 * x * (1 - 5 * (z**2 / r**2))
    return ax_grav + ax_j2

@jit(nopython=True)
def fv_y(x, y, z, mu, J2, Re):
    """
    Computes the derivative of vy with respect to time (dvy/dt), considering gravitational 
    and J2 perturbation effects.

    Parameters:
    x, y, z (float) - Position components (km).
    mu (float)      - Gravitational parameter (km^3/s^2).
    J2 (float)      - Second zonal harmonic of the geopotential.
    Re (float)      - Earth's radius (km).

    Returns:
    float - Derivative of vy (km/s^2).
    """
    r = np.sqrt(x**2 + y**2 + z**2)
    ay_grav = -mu * y / r**3
    ay_j2 = -3/2 * J2 * mu * Re**2 / r**5 * y * (1 - 5 * (z**2 / r**2))
    return ay_grav + ay_j2

@jit(nopython=True)
def fv_z(x, y, z, mu, J2, Re):
    """
    Computes the derivative of vz with respect to time (dvz/dt), considering gravitational 
    and J2 perturbation effects.

    Parameters:
    x, y, z (float) - Position components (km).
    mu (float)      - Gravitational parameter (km^3/s^2).
    J2 (float)      - Second zonal harmonic of the geopotential.
    Re (float)      - Earth's radius (km).

    Returns:
    float: Derivative of vz (km/s^2).
    """
    r = np.sqrt(x**2 + y**2 + z**2)
    az_grav = -mu * z / r**3
    az_j2 = -3/2 * J2 * mu * Re**2 / r**5 * z * (3 - 5 * (z**2 / r**2))
    return az_grav + az_j2


@jit(nopython=True)
def runge_kutta_step(x, y, z, vx, vy, vz, mu, J2, Re, h):
    """
    Performs a single Runge-Kutta 4th-order integration step for orbit propagation.

    Parameters:
    x, y, z (float)    - Initial position components (km).
    vx, vy, vz (float) - Initial velocity components (km/s).
    mu (float)         - Gravitational parameter (km^3/s^2).
    J2 (float)         - Second zonal harmonic of the geopotential.
    Re (float)         - Earth's radius (km).
    h (float)          - Integration step size (s).

    Returns:
    tuple - Updated position and velocity components.
    """
    k1_x = fx(vx)
    k1_y = fy(vy)
    k1_z = fz(vz)
    k1_vx = fv_x(x, y, z, mu, J2, Re)
    k1_vy = fv_y(x, y, z, mu, J2, Re)
    k1_vz = fv_z(x, y, z, mu, J2, Re)

    mid_x = x + k1_x * h / 2
    mid_y = y + k1_y * h / 2
    mid_z = z + k1_z * h / 2
    mid_vx = vx + k1_vx * h / 2
    mid_vy = vy + k1_vy * h / 2
    mid_vz = vz + k1_vz * h / 2

    k2_x = fx(mid_vx)
    k2_y = fy(mid_vy)
    k2_z = fz(mid_vz)
    k2_vx = fv_x(mid_x, mid_y, mid_z, mu, J2, Re)
    k2_vy = fv_y(mid_x, mid_y, mid_z, mu, J2, Re)
    k2_vz = fv_z(mid_x, mid_y, mid_z, mu, J2, Re)

    mid_x = x + k2_x * h / 2
    mid_y = y + k2_y * h / 2
    mid_z = z + k2_z * h / 2
    mid_vx = vx + k2_vx * h / 2
    mid_vy = vy + k2_vy * h / 2
    mid_vz = vz + k2_vz * h / 2

    k3_x = fx(mid_vx)
    k3_y = fy(mid_vy)
    k3_z = fz(mid_vz)
    k3_vx = fv_x(mid_x, mid_y, mid_z, mu, J2, Re)
    k3_vy = fv_y(mid_x, mid_y, mid_z, mu, J2, Re)
    k3_vz = fv_z(mid_x, mid_y, mid_z, mu, J2, Re)

    mid_x = x + k3_x * h
    mid_y = y + k3_y * h
    mid_z = z + k3_z * h
    mid_vx = vx + k3_vx * h
    mid_vy = vy + k3_vy * h
    mid_vz = vz + k3_vz * h

    k4_x = fx(mid_vx)
    k4_y = fy(mid_vy)
    k4_z = fz(mid_vz)
    k4_vx = fv_x(mid_x, mid_y, mid_z, mu, J2, Re)
    k4_vy = fv_y(mid_x, mid_y, mid_z, mu, J2, Re)
    k4_vz = fv_z(mid_x, mid_y, mid_z, mu, J2, Re)

    x_next = x + (h / 6) * (k1_x + 2 * k2_x + 2 * k3_x + k4_x)
    y_next = y + (h / 6) * (k1_y + 2 * k2_y + 2 * k3_y + k4_y)
    z_next = z + (h / 6) * (k1_z + 2 * k2_z + 2 * k3_z + k4_z)
    vx_next = vx + (h / 6) * (k1_vx + 2 * k2_vx + 2 * k3_vx + k4_vx)
    vy_next = vy + (h / 6) * (k1_vy + 2 * k2_vy + 2 * k3_vy + k4_vy)
    vz_next = vz + (h / 6) * (k1_vz + 2 * k2_vz + 2 * k3_vz + k4_vz)

    return x_next, y_next, z_next, vx_next, vy_next, vz_next

def runge_kutta_4(x_0, y_0, z_0, vx_0, vy_0, vz_0, mu, J2, Re, t_start, tmax, h):
    """
    Integrates the orbit using the 4th-order Runge-Kutta method.

    Parameters:
    x_0, y_0, z_0 (float)    - Initial position components (km).
    vx_0, vy_0, vz_0 (float) - Initial velocity components (km/s).
    mu (float)               - Gravitational parameter (km^3/s^2).
    J2 (float)               - Second zonal harmonic of the geopotential.
    Re (float)               - Earth's radius (km).
    t_start (float)          - Start time of the integration (s).
    tmax (float)             - End time of the integration (s).
    h (float)                - Integration step size (s).

    Returns:
    tuple - Arrays of position and velocity components over time, and time array.
    """
    total_steps = int((tmax - t_start) / h)
    tn = np.zeros(total_steps + 1)
    xn = np.zeros(total_steps + 1)
    yn = np.zeros(total_steps + 1)
    zn = np.zeros(total_steps + 1)
    vxn = np.zeros(total_steps + 1)
    vyn = np.zeros(total_steps + 1)
    vzn = np.zeros(total_steps + 1)

    # Set initial conditions
    tn[0] = t_start
    xn[0] = x_0
    yn[0] = y_0
    zn[0] = z_0
    vxn[0] = vx_0
    vyn[0] = vy_0
    vzn[0] = vz_0

    # Main RK4 loop with progress bar
    for counter in tqdm(range(total_steps), desc="Integrating Orbit"):
        x_0 = xn[counter]
        y_0 = yn[counter]
        z_0 = zn[counter]
        vx_0 = vxn[counter]
        vy_0 = vyn[counter]
        vz_0 = vzn[counter]

        x_next, y_next, z_next, vx_next, vy_next, vz_next = runge_kutta_step(x_0, y_0, z_0, vx_0, vy_0, vz_0, mu, J2, Re, h)

        xn[counter + 1] = x_next
        yn[counter + 1] = y_next
        zn[counter + 1] = z_next
        vxn[counter + 1] = vx_next
        vyn[counter + 1] = vy_next
        vzn[counter + 1] = vz_next
        tn[counter + 1] = tn[counter] + h

    return xn, yn, zn, vxn, vyn, vzn, tn