"""
This script analyzes the results of orbital integrations and plots the orbital elements over time.

Functions:
- analyze_and_plot: Computes orbital elements from state vectors and plots them.

Parameters:
- t (array)              : Array of time points.
- state_vectors (array)  : Array of state vectors [x, y, z, vx, vy, vz] at each time point.
- mu (float)             : Gravitational parameter of the central body (km^3/s^2).
- name (str)             : Name of the satellite or object.
- inc (float)            : Inclination of the orbit.
- dt (float)             : Time step of the integration.
- T (int)                : Total number of periods for the integration.

Author    : [Panagiotis Vamvakas]
Date      : [29-06-2024]
Version   : [1.0]
"""
import os
import matplotlib.pyplot as plt
from orbital_util import coe_from_sv

def analyze_and_plot(t, state_vectors, mu, name, inc, dt, T):
    """
    Analyzes the results of the integration and plots the orbital elements over time.
    
    Parameters:
    - t: array of time points
    - state_vectors: array of state vectors [x, y, z, vx, vy, vz] at each time point
    - mu: gravitational parameter of the central body (km^3/s^2)
    """
    # Initialize arrays to store orbital elements
    a_arr = []
    e_arr = []
    i_arr = []
    RAAN_arr = []
    w_arr = []
    TA_arr = []
    
    # Loop over state vectors to compute orbital elements
    for state in state_vectors:
        R = state[:3]
        V = state[3:]
        a, e, i, RAAN, w, TA = coe_from_sv(R, V, mu)
        a_arr.append(a)
        e_arr.append(e)
        i_arr.append(i)
        RAAN_arr.append(RAAN)
        w_arr.append(w)
        TA_arr.append(TA)
    
    # Define plot styles and colors
    plot_styles = ['#FF9999', '#66B2FF', '#99FF99', '#FFCC99', '#FF99FF']
    labels = ['Semi-major Axis (a)', 'Eccentricity (e)', 'Inclination (i)', 'RAAN (Ω)', 'Argument of Perigee (ω)']
    y_labels = ['a (km)', 'e', 'i (deg)', 'Ω (deg)', 'ω (deg)']
    data = [a_arr, e_arr, i_arr, RAAN_arr, w_arr]
    
    # Create figure and subplots
    fig, axes = plt.subplots(3, 2, figsize=(15, 12))
    axes = axes.flatten()
    
    # Set figure background color
    fig.patch.set_facecolor('#2E2E2E')
    
    for i in range(5):
        ax = axes[i]
        ax.plot(t, data[i], plot_styles[i % len(plot_styles)])
        ax.set_title(labels[i], color='white')
        ax.set_xlabel('Time (s)', color='white')
        ax.set_ylabel(y_labels[i], color='white')
        
        # Set axes and grid color
        ax.set_facecolor('#2E2E2E')
        ax.xaxis.label.set_color('white')
        ax.yaxis.label.set_color('white')
        ax.tick_params(axis='x', colors='white')
        ax.tick_params(axis='y', colors='white')
        ax.spines['top'].set_color('white')
        ax.spines['bottom'].set_color('white')
        ax.spines['left'].set_color('white')
        ax.spines['right'].set_color('white')
        ax.grid(True, which='both', linestyle='--', linewidth=0.5, color='grey')
    
    # Hide the last subplot (empty)
    axes[-1].axis('off')
    plt.tight_layout()
    
    output_dir = os.path.join(os.path.dirname(__file__), '../graphs')
    os.makedirs(output_dir, exist_ok=True)
    graph_path = os.path.join(output_dir, f'{name}_Keplerian_Elements_Graph_{inc}_{dt}_{T}.jpg')
    plt.savefig(graph_path, dpi=300)
    
    plt.show()