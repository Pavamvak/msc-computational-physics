"""
This script calculates the theoretical rates of change for the argument of perigee (ω) 
and the right ascension of the ascending node (Ω) based on the averaged orbital theory.
It outputs the results in a LaTeX formatted table for various inclinations.

Constants:
- Re: Earth's radius in kilometers.
- mu: Earth's gravitational parameter in km^3/s^2.
- J2: J2 perturbation coefficient.

Parameters:
- a: Semi-major axis in kilometers.
- e: Eccentricity.

Theoretical Equations:
- The rates dω/dt and dΩ/dt are calculated using the perturbative effects of Earth's 
  oblateness (J2) on a satellite's orbit, which causes long-term changes in these 
  orbital elements.

Output:
- The script prints a LaTeX table containing the rates of change for ω and Ω 
  in degrees per day for inclinations ranging from 58 to 68 degrees.
  
Author    : [Panagiotis Vamvakas]
Date      : [29-06-2024]
Version   : [1.0]
"""

import numpy as np

# Constants
Re = 6378.137           # Earth's radius in km
mu = 398600             # Earth's gravitational parameter in km^3/s^2
J2 = 1.0826e-3          # J2 perturbation coefficient

# Given semi-major axis and eccentricity
a = 26600               # Semi-major axis in km
e = 0.74                # Eccentricity

# Calculate the mean motion
n = np.sqrt(mu / a**3)  # Mean motion in rad/s

# Calculate dω/dt and dΩ/dt for i in the range of 85 to 105 degrees
inclinations = np.arange(58, 69, 1)
domega_dt_list = []
dOmega_dt_list = []

for i in inclinations:
    i_rad = np.radians(i)  # Convert inclination to radians
    domega_dt = (3/2) * J2 * (Re**2 * n / (a**2 * (1 - e**2)**2)) * (2 - (5/2) * np.sin(i_rad)**2)
    dOmega_dt = -(3/2) * J2 * (Re**2 * n / (a**2 * (1 - e**2)**2)) * np.cos(i_rad)
    domega_dt_deg_day = np.degrees(domega_dt) * 86400  # Convert rad/s to deg/day
    dOmega_dt_deg_day = np.degrees(dOmega_dt) * 86400  # Convert rad/s to deg/day
    domega_dt_list.append(domega_dt_deg_day)
    dOmega_dt_list.append(dOmega_dt_deg_day)

# Print results in LaTeX format
latex_str = "\\begin{table}[H]\n\\centering\n\\caption{Long-term evolution of $\\omega$ and $\\Omega$ for MOLSAT}\n\\label{tab:MOLSAT_rates}\n\\begin{tabular}{ccc}\n\\hline\n\\textbf{Inclination (degrees)} & \\textbf{$\\frac{d\\omega}{dt}$ (deg/day)} & \\textbf{$\\frac{d\\Omega}{dt}$ (deg/day)} \\\\\n\\hline\n"
for i, domega_dt, dOmega_dt in zip(inclinations, domega_dt_list, dOmega_dt_list):
    latex_str += f"{i} & {domega_dt:.6f} & {dOmega_dt:.6f} \\\\\n"
latex_str += "\\hline\n\\end{tabular}\n\\end{table}"

print(latex_str)