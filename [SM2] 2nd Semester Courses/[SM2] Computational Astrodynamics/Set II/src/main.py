"""
This script simulates the orbital motion of selected satellites using the Runge-Kutta 4th order method.
It reads configuration parameters from a JSON file, which includes satellite specifications,
physical constants, and options for saving and plotting results. 

The script calculates and
prints the initial and final state vectors, saves the state vectors and orbital elements to CSV files,
and optionally plots the Cartesian elements.

Example JSON structure:
{
    "selected_satellite"     : "LEOSAT",
    "save_state_vectors"     : false,
    "save_cartesian_elements": true,
    "plot_cartesian_elements": false,
    "number_of_periods"      : 1000,
    "dt"                     : 100,
    "constants": {
      "mu"                   : 398600,
      "Re"                   : 6378.137,
      "J2"                   : 0.0010826
    },
    
    "satellites": {
      "LEOSAT": [6778, 
                 0, 
                 90, 
                 0, 
                 0, 
                 0],

      "MOLSAT": [26600, 
                 0.74, 
                 58.0, 
                 0, 
                 270, 
                 0]
    }
}

Author    : [Panagiotis Vamvakas]
Date      : [29-06-2024]
Version   : [1.0]
"""
import json
import numpy as np

import orbital_util as orb_util
import analysis_util as plt_anl
import save_util as save_util

# Load parameters from a JSON file
def load_parameters(json_path):
    with open(json_path, 'r') as file:
        return json.load(file)

params = load_parameters('parameters.json')

selected_satellite      = params['selected_satellite']
save_state_vectors      = params['save_state_vectors']
save_cartesian_elements = params['save_cartesian_elements']
plot_cartesian_elements = params['plot_cartesian_elements']
number_of_periods       = params['number_of_periods']
dt                      = params['dt']

print("Simulating:")
print(f"Satellite                       = {selected_satellite}")
print(f"Save State Vectors              = {save_state_vectors}")
print(f"Save Cartesian Elements         = {save_cartesian_elements}")
print("--------------------------------------------------------------")

print(f"Processing {selected_satellite}'s orbit for {number_of_periods} orbits...")
print("--------------------------------------------------------------")

# Constants
mu = params['constants']['mu']
Re = params['constants']['Re']
J2 = params['constants']['J2']

print("Parameters:")
print(f"Earth's Gravitational Parameter  = {mu} Km^3/sec^2")
print(f"Earth's Radius                   = {Re} Km")
print(f"J2 Pertubation Coefficient       = {J2}")
print(f"Time Step                        = {dt} sec")
print("--------------------------------------------------------------")

# Satellite setup
satellites = params['satellites']

initial_cartesian = satellites[selected_satellite]
# Initial conditions
x_0, y_0, z_0, vx_0, vy_0, vz_0 = orb_util.sv_from_coe(initial_cartesian, mu)

print("Initial Keplerian Elements:")
print("---------------------------")
print(f"Semi-major Axis                  = {initial_cartesian[0]} Km")
print(f"Eccentricity                     = {initial_cartesian[1]}")
print(f"Inclination                      = {initial_cartesian[2]} degrees")
print(f"Right Ascension of Ascending Node = {initial_cartesian[3]} degrees")
print(f"Argument of Periapsis            = {initial_cartesian[4]} degrees")
print(f"True Anomaly                     = {initial_cartesian[5]} degrees")

print("Initial State Vectors:")
print("---------------------------")
print(f"Position X                       = {x_0} Km")
print(f"Position Y                       = {y_0} Km")
print(f"Position Z                       = {z_0} Km")
print(f"Velocity X                       = {vx_0} Km/sec")
print(f"Velocity Y                       = {vy_0} Km/sec")
print(f"Velocity Z                       = {vz_0} Km/sec")
print("--------------------------------------------------------------")

# Calculate orbit period
T = 2 * np.pi * np.sqrt(initial_cartesian[0]**3 / mu)
print(f"Orbit Period                     = {T} sec")
print("--------------------------------------------------------------")

# Calculated propagation duration
t_span = number_of_periods * T

# Integrate the orbit using RK4
xn, yn, zn, vxn, vyn, vzn, tn = orb_util.runge_kutta_4(x_0, y_0, z_0, vx_0, vy_0, vz_0, mu, J2, Re, 0, t_span, dt)

# Print the last state vectors after integration
print("Final State Vectors after Integration:")
print("--------------------------------------")
print(f"Position X                       = {xn[-1]} Km")
print(f"Position Y                       = {yn[-1]} Km")
print(f"Position Z                       = {zn[-1]} Km")
print(f"Velocity X                       = {vxn[-1]} Km/sec")
print(f"Velocity Y                       = {vyn[-1]} Km/sec")
print(f"Velocity Z                       = {vzn[-1]} Km/sec")
print("--------------------------------------------------------------")

if save_state_vectors:
    save_util.save_state_vectors_to_csv(xn, yn, zn, vxn, vyn, vzn, tn, selected_satellite, number_of_periods, initial_cartesian[2], dt)

if save_cartesian_elements:
    state_vectors = np.vstack((xn, yn, zn, vxn, vyn, vzn)).T
    save_util.save_cartesian_elements_to_csv(state_vectors, tn, selected_satellite, number_of_periods, initial_cartesian[2], dt, mu)

if plot_cartesian_elements:
    state_vectors = np.vstack((xn, yn, zn, vxn, vyn, vzn)).T
    plt_anl.analyze_and_plot(tn, state_vectors, mu, selected_satellite, initial_cartesian[2], dt, number_of_periods)
