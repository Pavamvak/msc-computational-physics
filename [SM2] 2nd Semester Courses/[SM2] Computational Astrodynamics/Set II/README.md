# Computational Astrodynamics: J2 Perturbation Analysis

This repository contains the code and solutions for the Computational Astrodynamics course, specifically for Set II, which covers the topic of Numerical Integration with J2 perturbation and Comparison with the Averaged Theory.

## Table of Contents

1. [Introduction](#introduction)
2. [Repository Structure](#repository-structure)
3. [Key Components](#key-components)
   - [Equations of Motion](#equations-of-motion)
   - [Numerical Integration](#numerical-integration)
   - [Time Step and Orbit Period Analysis](#time-step-and-orbit-period-analysis)
   - [Averaged Theory Comparison](#averaged-theory-comparison)
4. [Dependencies](#dependencies)
5. [Installation](#installation)
6. [Usage](#usage)

## Introduction
The project examines two satellite cases:
1. **Low Earth Orbit (LEO)**: A nearly polar, circular orbit at 400 km altitude (named LEOSAT).
2. **Molniya Orbit**: A highly elliptical orbit with an eccentricity of 0.74 (MOLSAT).

The goal is to compute the trajectories over multiple orbital periods, analyze the orbital elements' evolution, and compare them with the averaged theory predictions.

## Repository Structure
The repository is structured as follows:
```
├── graphs
├── output
├── src
│ ├── average_theory_analysis.py
│ ├── main.py
│ ├── orbital_util.py
│ ├── save_util.py
│ ├── analysis_util.py
├── stk_files
│ ├── LEOSAT_Classical_Orbit_Elements.bmp
│ ├── LEOSAT_Inertial_Position_Velocity_various_dt.zip
│ ├── LEOSAT_Inertial_Position_Velocity_various_T.zip
├── .gitignore
├── parameters.json
├── README.md
├── Set II - Exercises.pdf
├── Vamvakas_Panagiotis - Set_02.pdf
```

## Key Components

### Equations of Motion

The motion is governed by Newton's law of gravitation, including the effects of Earth's oblateness via the J2 perturbation term. The total acceleration accounts for both gravitational and J2 perturbative components.

$$
\begin{aligned}
a_x &= -\frac{\mu}{r^3} x - \frac{3}{2} J_2 \frac{\mu R_e^2}{r^5} x \left( 1 - \frac{5z^2}{r^2} \right), \\
a_y &= -\frac{\mu}{r^3} y - \frac{3}{2} J_2 \frac{\mu R_e^2}{r^5} y \left( 1 - \frac{5z^2}{r^2} \right), \\
a_z &= -\frac{\mu}{r^3} z - \frac{3}{2} J_2 \frac{\mu R_e^2}{r^5} z \left( 3 - \frac{5z^2}{r^2} \right).
\end{aligned}
$$

### Numerical Integration

The project uses the Runge-Kutta 4th order method (RK4) for numerical integration, chosen for its balance between computational efficiency and accuracy.

$$
\begin{aligned}
k_1 &= f(t_n, y_n), \\
k_2 &= f\left(t_n + \frac{h}{2}, y_n + \frac{h}{2} k_1 \right), \\
k_3 &= f\left(t_n + \frac{h}{2}, y_n + \frac{h}{2} k_2 \right), \\
k_4 &= f\left(t_n + h, y_n + h k_3 \right), \\
y_{n+1} &= y_n + \frac{h}{6} (k_1 + 2k_2 + 2k_3 + k_4),
\end{aligned}
$$

### Time Step and Orbit Period Analysis

Initial validation was conducted using different time steps to ensure accuracy against AGI's STK high-precision results. Propagation was performed over varying durations to assess long-term accuracy, showing that errors increased with propagation duration, primarily due to unmodeled forces in the numerical propagator.

<div align="center">
    <img src="graphs/LEOSAT_Keplerian_Elements_Error_Graph_5_10_1.jpg" alt="Image 1" style="width:45%; display:inline-block; margin-right:5%;" />
    <img src="graphs/LEOSAT_Keplerian_Elements_Error_Graph_5_1_100.jpg" alt="Image 2" style="width:45%; display:inline-block;" />
</div>

### Averaged Theory Comparison

Averaged theory isolates long-term secular changes from short-term oscillations. The theoretical long-term rates of change for the argument of periapsis (ω) and the right ascension of the ascending node (Ω) were compared to those derived from simulation over multiple orbits.

<div align="center">
    <img src="graphs/LEOSAT_Keplerian_Elements_Rates_Graph.jpg" alt="Image 1" style="width:45%; display:inline-block; margin-right:5%;" />
    <img src="graphs/MOLSAT_Cartesian_Elements_Rates_Graph.jpg" alt="Image 2" style="width:45%; display:inline-block;" />
</div>

## Dependencies

The following Python packages are required to run the code:
- `os`
- `numpy`
- `numba`
- `pandas`
- `scipy`
- `tqdm`
- `matplotlib`
- `scikit-learn`

## Installation

To install the required packages, run:
```bash
pip install -r requirements.txt
```

## Usage

Clone the repository:
```bash
git clone https://gitlab.com/Pavamvak/msc-computational-physics/-/tree/main/%5BSM2%5D%202nd%20Semester%20Courses/%5BSM2%5D%20Computational%20Astrodynamics/Set%20I?ref_type=heads
```
Run the Python scripts to see the results:
```bash
python code.py
```
Important notes:
1. **Configuration**: Modify `parameters.json` to set up the simulation parameters, including satellite specifications and output options.
2. **Execution**: Run `main.py` to perform the simulation and generate results.
3. **Post-Processing**: Use `average_theory_analysis.py` and `theoretical_rates.py` for detailed analysis and comparison with theoretical predictions.
