# Computational Astrodynamics - Set I

This repository contains the code and solutions for the Computational Astrodynamics course, specifically for Set I, which covers the following topics:
- Time in Earth’s shadow
- Ground tracks
- Porkchop plot

## Table of Contents

1. [Introduction](#introduction)
2. [Repository Structure](#repository-structure)
3. [Exercises](#exercises)
   - [Exercise 1: Time in Earth’s Shadow](#exercise-1-time-in-earths-shadow)
   - [Exercise 2: Ground Tracks](#exercise-2-ground-tracks)
   - [Exercise 3: Porkchop Plot](#exercise-3-porkchop-plot)
4. [Dependencies](#dependencies)
5. [Installation](#installation)
6. [Usage](#usage)
7. [References](#references)

## Introduction

This repository contains the solutions for the assignments in the Computational Astrodynamics course at Aristotle University of Thessaloniki. The assignments focus on simulating various astrodynamics scenarios and visualizing the results. The selected coding language for this set is Python.

## Repository Structure

The repository is structured as follows:
```
.
├── Exercise 1
│ ├── main.py
│ ├── fig.jpg
├── Exercise 2
| ├── Results
│ ├── 2k_earth_daymap.jpg
│ ├── astrodynamicslibrary.py
│ ├── lambertsolver.py
│ ├── main.py
├── Exercise 3
│ ├── astrodynamicslibrary.py
│ ├── lambertsolver.py
│ ├── main.py
│ ├── porkchop_plot.png
├── README.md
├── Set I - Exercises.pdf
├── Vamvakas_Panagiotis - Set_01.pdf
└── requirements.txt
```

## Exercises

### Exercise 1: Time in Earth’s Shadow

This exercise involves calculating the time a spacecraft spends in Earth's shadow given its elliptical trajectory around the Earth. The solution involves calculating the true anomaly of the entry and exit eclipse points and determining the eclipse duration.

- **Problem Statement**: Calculate the time a spacecraft spends in Earth's shadow.
- **Solution**: Derive the equations for the eclipse entry and exit points using orbital mechanics principles.

<p align="center">
  <img src="Exercise%201/fig.jpg" alt="Exercise 1" width="200"/>
</p>

### Exercise 2: Ground Tracks

This exercise involves plotting the ground tracks of various satellites over a 24-hour period. The solution requires filling in the required functions in `astrodynamicslibrary.py` to compute and plot the ground tracks.

- **Problem Statement**: Plot the 24-hour ground tracks for various satellites.
- **Solution**: Implement the necessary functions to calculate and plot the ground tracks.

<p align="center">
  <img src="Exercise%202/Results/ISS_1_orbit.png" alt="Ground Track ISS" width="400"/>
  <img src="Exercise%202/Results/Molniya_24_hours.png" alt="Ground Track Sentinel" width="400"/>
</p>

### Exercise 3: Porkchop Plot

This exercise involves creating a porkchop plot for an Earth-Mars transfer window in 2018. The solution involves solving Lambert's problem for the given planetary positions and time of flight.

- **Problem Statement**: Create a porkchop plot for Earth-Mars transfer in 2018.
- **Solution**: Solve Lambert's problem to find the optimal transfer windows and plot the porkchop plot.

<p align="center">
  <img src="Exercise%203/porkchop_plot.png" alt="Porkchop Plot" width="400"/>
</p>

## Dependencies

The following Python packages are required to run the code:
- `numpy`
- `scipy`
- `matplotlib`

## Installation

To install the required packages, run:
```bash
pip install -r requirements.txt
```

## Usage

Clone the repository:
```bash
git clone https://gitlab.com/Pavamvak/msc-computational-physics/-/tree/main/%5BSM2%5D%202nd%20Semester%20Courses/%5BSM2%5D%20Computational%20Astrodynamics/Set%20I?ref_type=heads
```
Navigate to the exercise directory:
```bash
cd Exercise\ 1
```
Run the Python scripts to see the results:
```bash
python code.py
```

## References
- Howard D. Curtis, "Orbital Mechanics for Engineering Students," Butterworth-Heinemann, 3rd edition, 2013.
- Richard Fitzpatrick, "An Introduction to Celestial Mechanics," The University of Texas at Austin, 2nd edition, 2008.
