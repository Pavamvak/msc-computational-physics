import numpy as np
from scipy.optimize import fsolve
import matplotlib.pyplot as plt

# Given parameters
a = 32000  # km
e = 0.2
mu_earth = 398600.433  # km^3/s^2
R_earth = 6378  # km

# Calculate the orbital period
T = 2 * np.pi * np.sqrt(a**3 / mu_earth)  # Orbital period in seconds

print(f"Orbital period: {T:.2f} seconds")

def equation_entry(theta):
    """
    Define the function to solve for eclipse entry point
    """
    return (a * (1 - e**2) / (1 + e * np.cos(theta))) * np.sin(theta) - R_earth

def equation_exit(theta):
    """
    Define the function to solve for eccentric anomaly for true anomaly.
    """
    return (a * (1 - e**2) / (1 + e * np.cos(theta))) * np.sin(theta) + R_earth

def true_to_eccentric_anomaly(nu, e):
    """
    Calculate the eccentric anomaly from the true anomaly.
    
    Parameters:
    nu : float
        True anomaly in radians.
    e : float
        Eccentricity of the orbit.
    
    Returns:
    E : float
        Eccentric anomaly in radians.
    """
    # Define the equation to solve for E
    def equation(E, nu, e):
        return np.cos(nu) - (np.cos(E) - e) / (1 - e * np.cos(E))
    
    # Initial guess for E
    E_guess = nu
    
    # Solve for E
    E_solution = fsolve(equation, E_guess, args=(nu, e))
    
    return E_solution[0]

def mean_motion(a, mu):
    """
    Calculate the mean motion.
    
    Parameters:
    a : float
        Semi-major axis (km).
    mu : float
        Standard gravitational parameter of the central body (km^3/s^2).
    
    Returns:
    n : float
        Mean motion (rad/s).
    """
    return np.sqrt(mu / a**3)

def flight_time_to_eccentric_anomaly(E, e, a, mu):
    """
    Calculate the flight time to reach a given eccentric anomaly.
    
    Parameters:
    E : float
        Eccentric anomaly (radians).
    e : float
        Eccentricity of the orbit.
    a : float
        Semi-major axis of the orbit (km).
    mu : float
        Standard gravitational parameter of the central body (km^3/s^2).
    
    Returns:
    t : float
        Flight time (seconds).
    """
    # Calculate mean motion
    n = mean_motion(a, mu)
    
    # Calculate flight time
    t = (E - e * np.sin(E)) / n
    
    return t

# Initial guesses for solving the equations (in radians)
initial_guess = 1 * np.pi

# Solving for true anomaly angles where y = R_earth
theta_solution_entry = fsolve(equation_entry, initial_guess)

# Calculate eccentric anomaly of the entry true anomaly
E_entry = true_to_eccentric_anomaly(theta_solution_entry, e)

# Flight time to reach the eccentric anomaly
entry_time = flight_time_to_eccentric_anomaly(E_entry, e, a, mu_earth)

# Solving for true anomaly angles where y = - R_earth
theta_solutions_exit = fsolve(equation_exit, initial_guess)

# Calculate eccentric anomaly of the entry true anomaly
E_exit = true_to_eccentric_anomaly(theta_solutions_exit, e)

# Flight time to reach the eccentric anomaly
exit_time = flight_time_to_eccentric_anomaly(E_exit, e, a, mu_earth)

# Convert the solutions to degrees
theta_solutions_degrees_entry = [np.degrees(theta) for theta in theta_solution_entry]

# Convert the solutions to degrees
theta_solutions_degrees_exit = [np.degrees(theta) for theta in theta_solutions_exit]

print(f"Entry Point True Anomaly: {theta_solution_entry} degrees")
print(f"Exit Point True Anomaly: {theta_solutions_degrees_exit} degrees")
print("-----------------------------------------------")
print(f"Entry Point Eccentric Anomaly: {np.degrees(E_entry)} degrees")
print(f"Exit Point Eccentric Anomaly: {np.degrees(E_exit)} degrees")
print("-----------------------------------------------")
print(f"Entry Point Flight time: {entry_time} seconds")
print(f"Exit Point Flight time: {exit_time} seconds")
print("-----------------------------------------------")

# Calculate the eclipse duration
eclipse_time = exit_time - entry_time
print(f"Eclipse duration: {eclipse_time} seconds")

# Plotting the geometry (optional, for visualization purposes)
theta = np.linspace(0, 2 * np.pi, 1000)
r = a * (1 - e**2) / (1 + e * np.cos(theta))

fig, ax = plt.subplots(figsize=(10, 10))
ax.plot(r * np.cos(theta), r * np.sin(theta), label='Satellite Orbit')
ax.plot(R_earth * np.cos(theta), R_earth * np.sin(theta), 'r--', label='Earth Radius')

# Mark the entry points
for theta in np.radians(theta_solutions_degrees_entry):
    r_sol = a * (1 - e**2) / (1 + e * np.cos(theta))
    ax.plot(r_sol * np.cos(theta), r_sol * np.sin(theta), 'go', label='Eclipse Entry')

# Mark the exit points
for theta in np.radians(theta_solutions_degrees_exit):
    r_sol = a * (1 - e**2) / (1 + e * np.cos(theta))
    ax.plot(r_sol * np.cos(theta), r_sol * np.sin(theta), 'ro', label='Eclipse Exit')

# Add dotted horizontal lines at entry and exit heights
ax.axhline(y=R_earth, color='g', linestyle='dotted')
ax.axhline(y=-R_earth, color='r', linestyle='dotted')

# Earth
earth_circle = plt.Circle((0, 0), R_earth, color='b', alpha=0.3, label='Earth')
ax.add_artist(earth_circle)

ax.set_xlim(-2*a, 2*a)
ax.set_ylim(-2*a, 2*a)
ax.set_aspect('equal', 'box')

# Ensure legend entries for 'Eclipse Entry' and 'Eclipse Exit' are not duplicated
handles, labels = plt.gca().get_legend_handles_labels()
by_label = dict(zip(labels, handles))
ax.legend(by_label.values(), by_label.keys())

ax.set_title('Satellite Orbit and Earth\'s Shadow')

plt.xlabel('x (km)')
plt.ylabel('y (km)')
plt.grid(True, which='both', linestyle='--', linewidth=0.5)
ax.minorticks_on()
plt.savefig("Exercise 1/fig.jpg", dpi=300)
plt.show()