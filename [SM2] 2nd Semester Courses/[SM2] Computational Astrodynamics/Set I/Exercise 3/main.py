# import libraries
import astrodynamicslibrary as astro
import numpy as np
from scipy.optimize import minimize
import matplotlib.pyplot as plt

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# main script to create and plot the porkchop plot
# Requires: date2jd, interplanetary
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Departure window
# Start at 2018-03-01T00:00:00
# End at 2018-06-30T00:00:00
# Use 100 points
# Departure window
departure_start = astro.date2jd(2018, 3, 1, 0, 0, 0)
departure_end = astro.date2jd(2018, 6, 30, 0, 0, 0)
departure_dates = np.linspace(departure_start, departure_end, 100)

# Arrival window
# Start at 2018-09-01T00:00:00
# End at 2019-04-01T00:00:00
# Use 100 points
# Arrival window
arrival_start = astro.date2jd(2018, 9, 1, 0, 0, 0)
arrival_end = astro.date2jd(2019, 4, 1, 0, 0, 0)
arrival_dates = np.linspace(arrival_start, arrival_end, 100)

# Arrays to hold the computed values
C3 = np.zeros((len(departure_dates), len(arrival_dates)))
VinfA = np.zeros((len(departure_dates), len(arrival_dates)))
tof_days_list = np.zeros((len(departure_dates), len(arrival_dates)))

# Main loop to compute c3 and vinfA
valid_transfers = 0
for i, jd_dep in enumerate(departure_dates):
    for j, jd_arr in enumerate(arrival_dates):
        nvinfD, nvinfA = astro.interplanetary(2, 3, jd_dep, jd_arr)
        if nvinfD is not None and nvinfA is not None:
            C3[i, j] = nvinfD**2
            VinfA[i, j] = nvinfA
            tof_days_list[i, j] = jd_arr - jd_dep
            valid_transfers += 1
        else:
            C3[i, j] = np.nan
            VinfA[i, j] = np.nan
            tof_days_list[i, j] = np.nan

print(f"Number of valid transfers: {valid_transfers}")

# Objective functions for minimization
def objfunc3(X):
    jd_dep, jd_arr = X
    nvinfD, nvinfA = astro.interplanetary(2, 3, jd_dep, jd_arr)
    if nvinfD is not None and nvinfA is not None:
        return nvinfD**2
    else:
        return np.inf

def objfunvinf(X):
    jd_dep, jd_arr = X
    nvinfD, nvinfA = astro.interplanetary(2, 3, jd_dep, jd_arr)
    if nvinfD is not None and nvinfA is not None:
        return nvinfA
    else:
        return np.inf

# Find optimal solution using gradient descent
# X bounds
bnds = ((departure_start, departure_end), (arrival_start, arrival_end))
# Initial guess
X0 = [(departure_start + departure_end) / 2, (arrival_start + arrival_end) / 2]

# Find minimum for the c3 surface
minC3 = minimize(objfunc3, X0, bounds=bnds)
# Find minimum for the Vinf surface
minVinf = minimize(objfunvinf, X0, bounds=bnds)

# Print the optimal solutions
print("Optimal departure and arrival dates for minimum c3:")
print(minC3.x)
print("Optimal departure and arrival dates for minimum VinfA:")
print(minVinf.x)

# Set a single color for all contours
c3_color = 'blue'
vinfA_color = 'red'

# Ensure the levels are set correctly for the contour plot
C3_levels = np.linspace(np.nanmin(C3), np.nanmax(C3), 100)
VinfA_levels = np.linspace(np.nanmin(VinfA), np.nanmax(VinfA), 100)

plt.figure(figsize=(12, 8))

# Plot C3 contours with a single color
for level in C3_levels:
    C3_contour = plt.contour(departure_dates, arrival_dates, C3.T, levels=[level], colors=[c3_color], linestyles='solid', alpha=1)
    plt.clabel(C3_contour, inline=1, fontsize=8, fmt='%1.1f')

# Plot VinfA contours with a single color
for level in VinfA_levels:
    VinfA_contour = plt.contour(departure_dates, arrival_dates, VinfA.T, levels=[level], colors=[vinfA_color], linestyles='dashed', alpha=1)
    plt.clabel(VinfA_contour, inline=1, fontsize=8, fmt='%1.1f')

# Add optimal points
plt.plot(minC3.x[0], minC3.x[1], 'bo', label='Optimal C3')
plt.plot(minVinf.x[0], minVinf.x[1], 'ro', label='Optimal VinfA')

# Add legend
plt.legend()

# Add labels and title
plt.xlabel('Departure Date [JD]')
plt.ylabel('Arrival Date [JD]')
plt.title('Porkchop Plot for Earth-Mars Transfer (2018)')

# Show grid
plt.grid(True)

# Show plot
plt.show()