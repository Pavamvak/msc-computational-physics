# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#libraries
import numpy as np
from scipy import optimize
from lambertsolver import lambertizzo2015 as lambert
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

GME = 398600.4418

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def coe_from_sv(R,V,mu):
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#  This function computes the classical orbital elements (coe)
#  from the state vector (R,V).
#
#  input:
#  mu   - gravitational parameter of central body (km^3/s^2)
#  R    - position vector (km)
#  V    - velocity vector (km/s)
#
#  output:
#  coe  - classical orbital elements [a,e,i,RAAN,w,TA]
#
#  Functions required: none
# ---------------------------------------------

    # Compute the specific angular momentum and it normal
    h = np.cross(R, V)
    h_norm = np.linalg.norm(h)
    
    # Compute the position and velocity vector normals
    r = np.linalg.norm(R)
    v = np.linalg.norm(V)
    
    # Compute the eccentricity vector and it normal
    e_vec = (1/mu) * ((v**2 - mu/r) * R - np.dot(R, V) * V)
    e = np.linalg.norm(e_vec)
    
    # Compute the node vector and it normal
    K = np.array([0, 0, 1])  # Z axis unit vector
    N = np.cross(K, h)
    N_norm = np.linalg.norm(N)
    
    # Compute the inclination
    i = np.arccos(h[2] / h_norm)
    
    # Compute the right ascension of the ascending node (RAAN)
    if N_norm != 0:
        RAAN = np.arccos(N[0] / N_norm)
        if N[1] < 0:
            RAAN = 2 * np.pi - RAAN
    else:
        RAAN = 0
    
    # Compute the argument of perigee
    if N_norm != 0 and e != 0:
        w = np.arccos(np.dot(N, e_vec) / (N_norm * e))
        if e_vec[2] < 0:
            w = 2 * np.pi - w
    else:
        w = 0
    
    # Compute the true anomaly
    if e != 0:
        TA = np.arccos(np.dot(e_vec, R) / (e * r))
        if np.dot(R, V) < 0:
            TA = 2 * np.pi - TA
    else:
        cp = np.cross(N, R)
        if cp[2] >= 0:
            TA = np.arccos(np.dot(N, R) / (N_norm * r))
        else:
            TA = 2 * np.pi - np.arccos(np.dot(N, R) / (N_norm * r))
    
    # Compute the semi-major axis
    a = h_norm**2 / (mu * (1 - e**2))
    
    # Return the classical orbital elements
    coe = [a, e, np.degrees(i), np.degrees(RAAN), np.degrees(w), np.degrees(TA)]
    
    return coe
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def sv_from_coe(coe,mu):
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#  This function computes the state vector (r,v) (in ECI) from the
#  classical orbital elements (coe).
#
#  input: 
#  coe  - classical orbital elements [a,e,i,RAAN,w,TA]
#  mu   - gravitational parameter of central body (km^3/s^2)
#
#  output:
#  R    - position vector  (km)
#  V    - velocity vector  (km/s)
#  
#  Functions required: none
# ----------------------------------------------

    # Extract classical orbital elements
    a, e, i, RAAN, w, TA = coe

    # Convert angles from degrees to radians
    i = np.radians(i)
    RAAN = np.radians(RAAN)
    w = np.radians(w)
    TA = np.radians(TA)
    
    # Compute the distance (r) and speed (v) in the orbit plane
    r = a * (1 - e**2) / (1 + e * np.cos(TA))
    h = np.sqrt(mu * a * (1 - e**2))
    
    # Position in the perifocal frame
    x_orbit = r * np.cos(TA)
    y_orbit = r * np.sin(TA)
    z_orbit = 0
    
    # Velocity in the perifocal frame
    vx_orbit = - np.sqrt(mu / (a * (1 - e**2))) * np.sin(TA)
    vy_orbit = np.sqrt(mu / (a * (1 - e**2))) * (e + np.cos(TA))
    vz_orbit = 0
    
    # Rotation matrices
    R3_W = np.array([[np.cos(RAAN), np.sin(RAAN), 0],
                     [-np.sin(RAAN), np.cos(RAAN), 0],
                     [0, 0, 1]])
    
    R1_i = np.array([[1, 0, 0],
                     [0, np.cos(i), np.sin(i)],
                     [0, -np.sin(i), np.cos(i)]])
    
    R3_w = np.array([[np.cos(w), np.sin(w), 0],
                     [-np.sin(w), np.cos(w), 0],
                     [0, 0, 1]])
    
    # Combined rotation matrix
    Q_pX = np.transpose(R3_W) @ np.transpose(R1_i) @ np.transpose(R3_w)
    
    # Position and velocity vectors in the perifocal frame
    R_orbit = np.array([x_orbit, y_orbit, z_orbit])
    V_orbit = np.array([vx_orbit, vy_orbit, vz_orbit])
    
    # Compute position and velocity vectors in the ECI frame
    R = Q_pX @ R_orbit
    V = Q_pX @ V_orbit
    
    return R, V
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def date2jd(year,month,day,hour,minute,second):
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#  This function computes the julian day from an input calendar date.
#
#  input:
#  year   - year in YYYY
#  month  - month in MM
#  day    - day in DD
#  hour   - hour in HH
#  minute - minutes in MM
#  second - seconds in SS.
#
#  output:
#  jd - julian day
#
#  Functions required: none
#---------------------------------------------

    #...Equation 5.48:
    j0 = 367*year - np.floor(7*(year + np.floor((month + 9)/12))/4) + np.floor(275*month/9) + day + 1721013.5;

    ut = (hour + minute/60 + second/3600)/24
    
    #...Equation 5.47
    jd = j0 + ut

    return jd
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    



# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def M2E(e, M, tol=1e-8, max_iter=10000):
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#  This function uses Newton's method to solve Kepler's 
#  equation  E - e*sin(E) = M  for the eccentric anomaly,
#  given the eccentricity and the mean anomaly.
#
#  input:
#  M  - mean anomaly (radians)
#  e  - eccentricity
#
#  output:
#  E  - eccentric anomaly (radians) 
# ----------------------------------------------

    # Initial guess
    if e < 0.8:
        E = M
    else:
        E = np.pi

    F = E - e * np.sin(E) - M
    count = 0
    while (np.abs(F) > tol) and (count < max_iter):
        E = E - F / (1 - e * np.cos(E))
        F = E - e * np.sin(E) - M
        count += 1
    
    return E

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def E2M(e,E):
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#  This function computes the the mean anomaly
#  from the eccentric anomaly and the eccentricity.
#
#  input:
#  E  - eccentric anomaly (radians)
#  e  - eccentricity
# 
#  output:
#  M  - mean anomaly (radians) 
#
#  Functions required: none
# ----------------------------------------------

    M = E - e * np.sin(E)

    return M
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def E2f(e,E):
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#  This function computes the the true anomaly
#  from the eccentric anomaly and the eccentricity.
#
#  input:
#  E  - eccentric anomaly (radians)
#  e  - eccentricity
#
#  output:
#  f  - true anomaly (radians)
#
#  Functions required: none 
# ----------------------------------------------  
    
    f = 2 * np.arctan2(np.sqrt(1 + e) * np.sin(E / 2), np.sqrt(1 - e) * np.cos(E / 2))
    
    return f
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def f2E(e,f):
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#  This function computes the the eccentric anomaly
#  from the true anomaly and the eccentricity.
#
#  input:
#  e  - eccentricity
#  f  - true anomaly (radians)
#
#  output:
#  E  - eccentric anomaly (radians)
#
#  Functions required: none 
# ----------------------------------------------
    
    E = 2 * np.arctan2(np.sqrt(1 - e) * np.sin(f / 2), np.sqrt(1 + e) * np.cos(f / 2))
    
    return E
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def kep_propagate(kep,  DT, mu):
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#  This function propages an initial Keplerian state
#  for a time interval DT.
#
#  input:
#  kep - initial keplerian state [a,e,i,RAAN,w,TA]
#  DT - time interval to propagate
#  mu   - gravitational parameter of central body (km^3/s^2)
# 
#  output:
#  r - position vector after DT (km)
#  v - velocity vector after DT (km/s)
#
#  Functions required: f2E, E2M, M2E, E2f, sv_from_coe 
# ----------------------------------------------  
    
    # Unpack the initial Keplerian elements
    a, e, i, RAAN, w, TA = kep

    # Convert true anomaly to eccentric anomaly
    E = f2E(e, np.radians(TA))
    
    # Calculate the mean anomaly
    M = E2M(e, E)
    
    # Calculate the mean motion
    n = np.sqrt(mu / a**3)
    
    # Propagate mean anomaly
    M_new = M + n * DT
    
    # Solve Kepler's equation for the new eccentric anomaly
    E_new = M2E(e, M_new)
    
    # Convert eccentric anomaly to true anomaly
    TA_new = np.degrees(E2f(e, E_new))
    
    # New Keplerian elements
    kep_new = [a, e, i, RAAN, w, TA_new]
    
    # Convert to state vector
    R, V = sv_from_coe(kep_new, mu)
    
    return R, V
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def groundtrack(kep, ug0, DT, steps, mu):
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#  This function computes the groundtrack of satellite given
#  the initial keplerian state vector and the Greenwich
#  hour angle at t=0
#
#  input:
#  kep  - initial keplerian state [a,e,i,RAAN,w,TA]
#  ug0  - initial Greenwich hour angle
#  DT   - timestep
#  step - number of steps
#  mu   - gravitational parameter of central body (km^3/s^2)
# 
#  output:
#  phi_list    - list of latitudes at each time-step  
#  lambda_list - list of longitudes at each time-step
#
#  Functions required: kep_propagate
# ----------------------------------------------
     # Earth's rotational rate (rad/s)
    n_earth = 2 * np.pi / 86164.09053083288 

    # Initialize lists for latitude and longitude
    phi_list = []
    lambda_list = []

    # Loop over each time step
    for step in range(steps):
        # Propagate the Keplerian elements
        R, V = kep_propagate(kep, DT * step, mu)
        
        # Convert ECI position to ECEF
        theta_G = ug0 + n_earth * DT * step  # Earth's rotation angle
        cos_theta = np.cos(theta_G)
        sin_theta = np.sin(theta_G)
        R_ECEF = np.array([
            cos_theta * R[0] + sin_theta * R[1],
            -sin_theta * R[0] + cos_theta * R[1],
            R[2]
        ])
        
        # Calculate latitude (phi) and longitude (lambda) in ECEF
        r_norm = np.linalg.norm(R_ECEF)
        phi = np.arcsin(R_ECEF[2] / r_norm)  # latitude
        lambda_e = np.arctan2(R_ECEF[1], R_ECEF[0])  # longitude
        
        # Convert to degrees
        phi_deg = np.degrees(phi)
        lambda_deg = np.degrees(lambda_e)
        
        # Normalize longitude to the range [-180, 180]
        lambda_deg = (lambda_deg + 180) % 360 - 180
        
        # Append to lists
        phi_list.append(phi_deg)
        lambda_list.append(lambda_deg)
    
    return lambda_list, phi_list
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def make_groundtrack_lines(lambda_data,phi_data):
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#  This function separates the groundtrack data into 
#  continuous lines for plotting purposes
#
#  input:
#  phi_list    - list of latitudes at each time-step  
#  lambda_list - list of longitudes at each time-step
#
#  output:
#  curves_phi    - continuous latitude curves
#  curves_lambda - continuous longitude curves
#
#  Functions required: none
# ----------------------------------------------
    tol = 180
    curves_lambda = []
    curves_phi = []
    current_line_lambda = []
    current_line_phi = []
    for i in range(len(phi_data)):
        if i == 0:
            pass
        elif abs(lambda_data[i]-lambda_data[i-1]) > tol:
            curves_lambda.append(current_line_lambda)
            curves_phi.append(current_line_phi)
            current_line_lambda = []
            current_line_phi = []
        current_line_lambda.append(lambda_data[i])
        current_line_phi.append(phi_data[i])
    curves_lambda.append(current_line_lambda)
    curves_phi.append(current_line_phi)
    return curves_lambda, curves_phi
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def interplanetary(planet1, planet2, jd_dep, jd_arr):
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #  This function computes the hyperbolic excess velocity at 
    #  the departure and arrival for a transfer between planet 1 and 2
    #  with specific departure and arrival dates.
    #
    #  input:
    #  planet1  - departure planet
    #  planet2  - arrival planet
    #  jd_dep   - departure day in jd
    #  jd_arr   - arrival day in jd
    #
    #  output:
    #  nvinfD   - norm of the departure v_infinity
    #  nvinfA   - norm of the arrival v_infinity
    #
    #  Functions required: lambert, planet_elements_and_sv
    # ----------------------------------------------
    mu = 1.327124e11  # Gravitational parameter of the sun (km^3/s^2)
    
    try:
        # Get state vectors of the departure and arrival planets
        R1, V1, _ = planet_elements_and_sv(planet1, jd_dep)
        R2, V2, _ = planet_elements_and_sv(planet2, jd_arr)
        
        # Time of flight in seconds
        tof = (jd_arr - jd_dep) * 86400
        
        # Use the Lambert solver to find the spacecraft's velocity at departure and arrival
        V_dep, V_arr = lambert(mu, R1, R2, tof)
        
        # Hyperbolic excess velocities
        V_inf_dep = V_dep - V1
        V_inf_arr = V_arr - V2
        
        # Norm of the hyperbolic excess velocities
        nvinfD = np.linalg.norm(V_inf_dep)
        nvinfA = np.linalg.norm(V_inf_arr)
        
        return nvinfD, nvinfA
    except Exception as e:
        print("We entered hell!")
        return None, None
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def planetary_elements(planet_id):
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#    This function extracts a planet's J2000 orbital elements and
#    centennial rates from Table 8.1.
#
#    input:
#    planet_id - 0 through 8, for Mercury through Pluto
#
#    output:
#    J2000_coe - vector of J2000_elements corresponding to "planet_id", with au converted to km
#    rates - row vector of cent_rates corresponding to "planet_id", with au converted to km and arcseconds converted to degrees
#
#  Functions required: none
#--------------------------------------------------------------------
    J2000_elements = np.array([[ 0.38709893, 0.20563069, 7.00487, 48.33167, 77.45645, 252.25084],
                               [ 0.72333199, 0.00677323, 3.39471, 76.68069, 131.53298, 181.97973],
                               [ 1.00000011, 0.01671022, 0.00005, -11.26064, 102.94719, 100.46435],
                               [ 1.52366231, 0.09341233, 1.85061, 49.57854, 336.04084, 355.45332],
                               [ 5.20336301, 0.04839266, 1.30530, 100.55615, 14.75385, 34.40438],
                               [ 9.53707032, 0.05415060, 2.48446, 113.71504, 92.43194, 49.94432],
                               [ 19.19126393, 0.04716771, 0.76986, 74.22988, 170.96424, 313.23218],
                               [ 30.06896348, 0.00858587, 1.76917, 131.72169, 44.97135, 304.88003],
                               [ 39.48168677, 0.24880766, 17.14175, 110.30347, 224.06676, 238.92881]])
    
    cent_rates = np.array([[ 0.00000066, 0.00002527, -23.51, -446.30, 573.57 ,538101628.29],
                           [ 0.00000092, -0.00004938, -2.86, -996.89, -108.80, 210664136.06],
                           [-0.00000005, -0.00003804, -46.94, -18228.25, 1198.28, 129597740.63],
                           [-0.00007221, 0.00011902 ,-25.47, -1020.19, 1560.78, 68905103.78],
                           [ 0.00060737, -0.00012880, -4.15, 1217.17, 839.93 ,10925078.35],
                           [-0.00301530, -0.00036762, 6.11 ,-1591.05, -1948.89, 4401052.95],
                           [ 0.00152025, -0.00019150, -2.09, -1681.4, 1312.56, 1542547.79],
                           [ -0.00125196, 0.00002514, -3.64 ,-151.25, -844.43, 786449.21],
                           [-0.00076912, 0.00006465, 11.07, -37.33, -132.25 ,522747.90]])
    
    J2000_coe = J2000_elements[planet_id,:]
    rates = cent_rates[planet_id,:]
    #...Convert from AU to km:
    au = 149597871
    J2000_coe[0] = J2000_coe[0]*au
    rates[0] = rates[0]*au
    #...Convert from arcseconds to fractions of a degree:
    rates[2:6] = rates[2:6]/3600
    return J2000_coe, rates
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def planet_elements_and_sv(planet_id, jd):
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#  This function calculates the orbital elements and the state  
#  vector of a planet from the date in Julian days.
#
#  input:  
#  planet_id - planet identifier:
#               0 = Mercury
#               1 = Venus
#               2 = Earth
#               3 = Mars
#               4 = Jupiter
#               5 = Uranus
#               7 = Neptune
#               8 = Pluto
#  jd - epoch to retrieve the planet's state vector in julian days
#
#  output:
#  r,v - position [km] and velocity [km/s] vector of the planet with respect
#        to a heliocentric reference frame
#  coe       - vector of heliocentric orbital elements
#              [a  e  incl  RA  w  TA ],
#              where
#               a     = semimajor axis                      (km)
#               e     = eccentricity
#               incl  = inclination                         
#               RA    = right ascension                     
#               w     = argument of perihelion              
#               TA    = true anomaly                        
# 
#  Functions required: M2E, sv_from_coe, planetary_elements
# --------------------------------------------------------------------
    mu = 1.327124e11
    
    #...Obtain the data for the selected planet from Table 8.1:
    [J2000_coe, rates] = planetary_elements(planet_id)

    #...Equation 8.93a:
    t0 = (jd - 2451545)/36525

    #...Equation 8.93b:
    elements = J2000_coe + rates*t0

    a      = elements[0];
    e      = elements[1];
    #...Reduce the angular elements to within the range 0 - 360 degrees:
    incl   = elements[2];
    RA     = np.mod(elements[3],360);
    w_hat  = np.mod(elements[4],360);
    L      = np.mod(elements[5],360);
    w      = np.mod(w_hat - RA ,360);
    M      = np.mod(L - w_hat  ,360);
    
    #...Algorithm 3.1 (for which M must be in radians)
    E = M2E(e, np.deg2rad(M))  #rad
    
    #...Equation 3.13 (converting the result to degrees):
    TA     = np.rad2deg(np.mod(2*np.arctan(np.sqrt((1 + e)/(1 - e))*np.tan(E/2)),2*np.pi))
    coe    = [a, e, incl, RA, w, TA]
    
    #...Algorithm 4.5:
    [r, v] = sv_from_coe(coe, mu)

    return r,v,coe
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~