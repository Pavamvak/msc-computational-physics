import astrodynamicslibrary as astro
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as image

mu_earth = 398600.433  # km^3/s^2

# Greenwich hour angle at t = 0
ug0 = 0.0

# Keplerian elements for each satellite (a, e, i, RAAN, w, TA)
satellites = {
    "ISS": [6790.0, 0.0003, 51.64, 112.0, 178.0, 326.0],
    "Sentinel": [7178.0, 0.0001, 98.63, 204.8, 79.8, 280.3],
    "Molniya": [26600.0, 0.74, 63.4, 0.0, 270.0, 0.0],
    "Geostationary": [42164.0, 0.0001, 0.0, 22.9444, 0.0, 0.0],
    "Geosynchronous": [42164.0, 0.01, 40.0, 0.0, 0.0, 0.0]
}

# Keplerian elements at t = 0
kep0 = satellites["Geostationary"]

# Calculate the orbital period
T = 2 * np.pi * np.sqrt(kep0[0]**3 / mu_earth)  # Orbital period in seconds

# Compute the groundtrack
step = 60 # sec
duration = 24*60*60  # use x*T to calculate for x orbits if needed.
nsteps = int(duration/step)

# Initialize the plot
im = plt.imread("./Exercise 2/2k_earth_daymap.jpg")
fig, ax = plt.subplots()
im = ax.imshow(im, extent=[-180, 180, -90, 90], alpha=0.7)

# Compute the groundtrack for the given satellite
[lambda_data, phi_data] = astro.groundtrack(kep0, np.radians(ug0), step, nsteps, astro.GME)
curves_lambda, curves_phi = astro.make_groundtrack_lines(lambda_data, phi_data)

# Plot the groundtrack
for i in range(len(curves_lambda)):
    ax.plot(curves_lambda[i], curves_phi[i], linewidth=5, color="red")

ax.set_aspect('auto')
plt.title("Groundtrack")
plt.grid()
plt.legend()
plt.show()
