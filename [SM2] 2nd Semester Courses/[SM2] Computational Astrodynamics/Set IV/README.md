# Computational Astrodynamics: Galactic Potential Analysis

This repository contains the code and solutions for the Computational Astrodynamics course, specifically for Set IV, which covers the topic of Galactic Potential.

## Table of Contents

1. [Introduction](#introduction)
2. [Repository Structure](#repository-structure)
3. [Key Components](#key-components)
   - [Equilibrium Points](#equilibrium-points)
   - [Energy Values for Bounded Motion](#energy-values-for-bounded-motion)
   - [Poincaré Plot](#poincaré-plot)
   - [Calculate Trajectories](#calculate-trajectories)
4. [Dependencies](#dependencies)
5. [Installation](#installation)
6. [Usage](#usage)

## Introduction

The project examines a galactic potential defined by the Henon-Heiles model with the following potential function:

$$
V (x, y) = \frac{a}{2}(x^2 + y^2) + \epsilon \left( x^2 y - \frac{y^3}{3} \right)
$$

Given the parameter values in Table I, the tasks are to:
1. Find the equilibrium points.
2. Calculate for which energy values we have bounded motions around the point (0,0).
3. Plot the Poincaré section of the system for the given parameters.
4. Plot the corresponding trajectories in the (x, y) plane and characterize them.

## Repository Structure

The repository is structured as follows:
```
├── SET IV - Code.nb
├── README.md
├── Set_IV - Exercises.pdf
├── Vamvakas_Panagiotis - Set_04.pdf
```

## Key Components

### Equilibrium Points

The equilibrium points of the system are found by solving the Hamiltonian equations for zero derivatives of coordinates and momenta.

$$
\begin{aligned}
ẋ &= p_x, \\
ẏ &= p_y, \\
ṗ_x &= - \frac{\partial V}{\partial x}, \\
ṗ_y &= - \frac{\partial V}{\partial y},
\end{aligned}
$$

The Mathematica code provided in the document computes the equilibrium points by solving these equations.

### Energy Values for Bounded Motion

To determine the energy values for bounded motion, a contour plot of the potential function is used to visualize the potential landscape and identify regions of bounded motion around the point (0, 0).

### Poincaré Plot

For a given energy level, the Poincaré section is plotted with \( y = 0 \) and \( p_y > 0 \). This plot provides a visualization of the bounded trajectories in the phase space.

### Calculate Trajectories

The trajectories are calculated by solving the Hamiltonian equations with given initial conditions. The nature of the trajectories (periodic, quasi-periodic, or chaotic) is characterized by analyzing the plots.

## Usage
Clone the repository:
```
git clone https://gitlab.com/Pavamvak/msc-computational-physics/-/tree/main/%5BSM2%5D%202nd%20Semester%20Courses/%5BSM2%5D%20Computational%20Astrodynamics/Set%20IV?ref_type=heads
```

For more details, refer to the provided PDF document Vamvakas_Panagiotis - Set_04.pdf which contains the theoretical background and detailed explanations of the solutions.