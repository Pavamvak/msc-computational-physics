# Standard Map Simulation

## Overview
This project simulates the trajectories of the Standard Map, a mathematical model used to study chaotic behavior in dynamical systems. The Standard Map models the kicked rotor, where the angle and momentum are influenced by periodic kicks, leading to complex, nonlinear dynamics. This simulation is intended to visualize the diversity in behavior across different initial conditions and chaos parameters.

## Features
- Simulation of the Standard Map for multiple initial conditions.
- Visualization of trajectories in the phase space.
- Customizable parameters for chaos intensity and simulation depth.

## Installation

### Prerequisites
- Python 3.x
- NumPy
- Matplotlib

### Setup
Clone this repository to your local machine using the following command:
```bash
git clone https://gitlab.com/Pavamvak/msc-computational-physics.git
```
Navigate to the cloned directory:

```bash
cd "msc-computational-physics/[SM2] 2nd Semester Courses/[SM2] Computational Astrodynamics/Task 01/src"
```
No further installation is required, as the project uses Python's standard libraries along with NumPy and Matplotlib for numerical operations and visualization.

## Usage
To run the simulation, execute the script simulate.py from the command line:

```bash
python main.py
```
You can modify the main.py script to change parameters such as the chaos parameter k, the number of iterations, and the number of initial conditions.

## Contributing
Contributions to the project are welcome. To contribute:
- Fork the repository.
- Create a new branch (git checkout -b feature-branch).
- Make your changes.
- Commit your changes (git commit -am 'Add some feature').
- Push to the branch (git push origin feature-branch).
- Create a new Pull Request.

## License
This project is licensed under the MIT License - see the LICENSE.md file for details.

## Acknowledgments
- Inspiration from chaos theory and dynamical systems studies.
- Anyone who's dedicated to improving and educating in the field of mathematical simulations.