import numpy as np

# Standard Map definition
def standard_map(x, y, k):
    """
    Compute the next point in the trajectory of the Standard Map.
    
    The Standard Map is a discrete-time dynamical system that models the behavior of a kicked rotor.
    It is often used in chaos theory to study chaotic behavior in dynamical systems. The map involves
    equations that include modulo operations, which simulate the wrap-around effect of angles in a circle.
    
    Parameters:
    - x (float): Current x value (angle in radians) of the map.
    - y (float): Current y value (angular momentum) of the map.
    - k (float): Chaos parameter, which influences the sensitivity of the map to initial conditions and the degree of chaos.
    
    Returns:
    - tuple: A tuple containing the next x and y values in the trajectory.
    
    The equations for the map are:
        x_{n+1} = (x_n + y_n) % (2π)
        y_{n+1} = (y_n + k * sin(x_n + y_n)) % (2π)
    
    These equations account for the periodic boundary conditions typical in rotational dynamics.
    """
    x_next = (x + y) % (2 * np.pi)
    y_next = (y + k * np.sin(x + y)) % (2 * np.pi)
    return x_next, y_next
