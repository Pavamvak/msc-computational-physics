"""
Standard Map Simulation

This script simulates the trajectories of the Standard Map, a mathematical model used in chaos theory
to study the behavior of dynamical systems that exhibit chaotic behavior. The Standard Map models the 
kicked rotor, where the angle and momentum are affected by periodic kicks, leading to complex, 
non-linear behavior.

The simulation generates trajectories for multiple initial conditions to observe the diversity in
behavior, especially how the system can exhibit both chaotic and regular dynamics based on different
starting points in the phase space.

Requirements:
- Python 3.x
- NumPy: For numerical operations
- Matplotlib: For plotting trajectories

The Standard Map is defined by the equations:
    x_{n+1} = x_n + y_n (mod 2π)
    y_{n+1} = y_n + k * sin(x_n + y_n) (mod 2π)
where 'x' represents the angle, 'y' the angular momentum, and 'k' is a parameter that affects the
map's sensitivity to initial conditions, influencing the level of chaos.

Parameters:
- k (float): Controls the chaotic behavior of the map. Higher values increase chaos.
- iterations (int): Number of iterations per trajectory, defining the length of the simulation.
- num_conditions (int): Number of random initial conditions to simulate.

Outputs:
- A plot of trajectories for different initial conditions, saved as a high-resolution image in the
  specified output directory.

Usage:
Run the script to perform the simulation and view the plot. Modify the parameters as needed to explore
different dynamics.

Author: Panagiotis Vamvakas
Date: 23-05-2024
Version: 1.0
"""

import numpy as np
import matplotlib.pyplot as plt
from standard_map import standard_map

# Parameters
k = 0.9  # Map parameter for chaotic behavior
iterations = 100000  # Number of iterations

# Generating random initial conditions
np.random.seed(42)
num_conditions = 10
initial_conditions = 2 * np.pi * np.random.rand(num_conditions, 2)

# Set up the plot
plt.figure(figsize=(10, 8))

# Loop over each initial condition
for (x0, y0) in initial_conditions:
    
    # Simulation of the map
    trajectory = np.zeros((iterations, 2))
    trajectory[0] = [x0, y0]

    for i in range(1, iterations):
        x_next, y_next = standard_map(trajectory[i-1][0], trajectory[i-1][1], k)
        trajectory[i] = [x_next, y_next]
    
    # Plotting the trajectory
    plt.scatter(trajectory[:, 0], trajectory[:, 1], s=0.5, label=f'Init: ({x0:.2f}, {y0:.2f})')

# Finalizing the plot
plt.title(f'Trajectories of the Standard Map for 10 Random Initial Conditions - k = {k}')
plt.xlabel('$x_n$')
plt.ylabel('$y_n$')
plt.legend(loc='upper left', bbox_to_anchor=(1.05,1))
plt.grid(True)
plt.savefig(f"../output/Standard_Map_{k}.jpg", dpi=300, bbox_inches='tight')
plt.show()
