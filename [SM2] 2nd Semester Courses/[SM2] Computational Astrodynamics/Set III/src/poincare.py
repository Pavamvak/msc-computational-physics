import os
import warnings
import rebound
import numpy as np
from tqdm import tqdm
import matplotlib.pyplot as plt

import orbital_util

# Define the system parameters
m1 = 5.32e11          # Mass of Didymos (kg)
m2 = 4.94e9           # Mass of Dimorphos (kg)
R1 = 0.410            # Radius of Didymos (km)
R2 = 0.090            # Radius of Dimorphos (km)

# Normalized units
mu = m2 / (m1 + m2)

primary_mass = 1.0 - mu
secondary_mass = mu
distance = 1.15  # Distance between the primary and secondary

L2 = orbital_util.L2(mu)
h2 = -orbital_util.C2(mu) / 2

# Set integrator and simulation parameters
integrator = "ias15"
dt = 1e-12
tmax = 100.0  # Maximum integration time
Noutputs = 1000  # Number of output times

# Prepare to store intersections from all runs
all_intersections_x = []
all_intersections_vx = []

# Function to check for intersections
def poincare_section(sim, last_y):
    intersections_x = []
    intersections_vx = []
    times = np.linspace(0, tmax, Noutputs)
    for time in times:
        try:
            with warnings.catch_warnings(record=True) as w:
                warnings.simplefilter("always")
                sim.integrate(time, exact_finish_time=0)
                if len(w) > 0 and issubclass(w[-1].category, RuntimeWarning):
                    #print(f"RuntimeWarning: {w[-1].message}")
                    return intersections_x, intersections_vx
                
        except rebound.Encounter as e:
            print(f"Encounter exception: {e}")
            break
        except rebound.Escape as e:
            print(f"Escape exception: {e}")
            break
        except Exception as e:
            print(f"General exception: {e}")
            break
        
        third_body = sim.particles[2]
        if third_body.y > 0 and last_y <= 0:
            intersections_x.append(third_body.x)
            intersections_vx.append(third_body.vx)
        last_y = third_body.y
    return intersections_x, intersections_vx

# Generate multiple initial conditions
num_initial_conditions = 5000  # Adjust this as needed
for _ in tqdm(range(num_initial_conditions), desc="Processing initial conditions"):
    try:
        # Create a new simulation for each initial condition
        sim = rebound.Simulation()

        # Add primary and secondary bodies
        sim.add(m=primary_mass)
        sim.add(m=secondary_mass, a=distance)

        # Generate initial condition
        initial_state = orbital_util.generate_initial_condition(L2, h2, mu)
        x0, y0, vx0, vy0 = initial_state
        sim.add(x=x0, y=y0, vx=vx0, vy=vy0, m=0)

        # Move to center of mass and set integrator
        sim.move_to_com()
        sim.integrator = integrator
        sim.dt = dt

        # Check for intersections and accumulate results
        last_y = y0
        intersections_x, intersections_vx = poincare_section(sim, last_y)
        all_intersections_x.extend(intersections_x)
        all_intersections_vx.extend(intersections_vx)
    except Exception as e:
        print(f"Exception for initial condition {_}: {e}")
        continue

# Plot the Poincaré map
fig, ax = plt.subplots(figsize=(8, 8))
fig.patch.set_facecolor('#2E2E2E')

plt.plot(all_intersections_x, all_intersections_vx, 'o', color='#AED581', markersize=1)

# Customize the plot to match the style
plt.xlabel('x (normalized)', fontsize=12)
plt.ylabel('vx (normalized)', fontsize=12)
plt.title('Poincaré Map of RTBP', fontsize=12, color='white')

# Customize grid
plt.grid(color='gray', linestyle='--', linewidth=0.5)
plt.axhline(0, color='white', linewidth=0.5)
plt.axvline(0, color='white', linewidth=0.5)

# Customize axis and background
ax = plt.gca()
ax.xaxis.label.set_color('white')
ax.yaxis.label.set_color('white')
ax.set_xlim(-1.2, 1.2)
ax.set_ylim(-3, 3)
ax.set_facecolor('#2E2E2E')
ax.spines['top'].set_color('white')
ax.spines['right'].set_color('white')
ax.spines['left'].set_color('white')
ax.spines['bottom'].set_color('white')
ax.tick_params(axis='x', colors='white')
ax.tick_params(axis='y', colors='white')

# Save the plot to a file (optional)
output_dir = os.path.join(os.path.dirname(__file__), '../graphs')
os.makedirs(output_dir, exist_ok=True)
graph_path = os.path.join(output_dir, f'poincare.jpg')
plt.savefig(graph_path, dpi=300)

# Show the plot
plt.show()