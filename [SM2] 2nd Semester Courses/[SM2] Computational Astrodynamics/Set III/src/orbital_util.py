import numpy as np
from numba import jit
from scipy.integrate import solve_ivp

@jit
def L1(mu):
    """
    Calculate the position of the L1 Lagrange point.
    
    Parameters:
    mu (float): The mass ratio of the two bodies.
    
    Returns:
    float: x-coordinate, y-coordinate of L1 in normalized units.
    """
    alpha = hill_radius(mu)
    x = 1 - mu - alpha + (alpha**2) / 3
    y = 0
    return (x, y)

@jit
def L2(mu):
    """
    Calculate the position of the L2 Lagrange point.
    Parameters:
    mu (float): The mass ratio of the two bodies.
    Returns:
    float: x-coordinate, y-coordinate of L2 in normalized units.
    """
    alpha = hill_radius(mu)
    x = 1 - mu + alpha + (alpha**2) / 3
    y = 0
    return (x, y)

@jit
def L3(mu):
    """
    Calculate the position of the L3 Lagrange point.
    
    Parameters:
    mu (float): The mass ratio of the two bodies.
    
    Returns:
    float: x-coordinate, y-coordinate of L3 in normalized units.
    """
    x = -1 - mu + (7 * mu) / (12 * (1 - mu))
    y = 0
    return (x, y)

@jit
def L4(mu):
    """
    Calculate the position of the L4 Lagrange point.
    
    Parameters:
    mu (float): The mass ratio of the two bodies.
    
    Returns:
    tuple: x-coordinate, y-coordinate of L4 in normalized units.
    """
    x = 0.5 - mu
    y = np.sqrt(3) / 2
    return (x, y)

@jit
def L5(mu):
    """
    Calculate the position of the L5 Lagrange point.
    
    Parameters:
    mu (float): The mass ratio of the two bodies.
    
    Returns:
    tuple: x-coordinate, y-coordinate coordinates of L5 in normalized units.
    """
    x = 0.5 - mu
    y = -np.sqrt(3) / 2
    return (x, y)

@jit
def hill_radius(mu):
    """
    Calculate the Hill radius.
    
    Parameters:
    mu (float): The mass ratio of the two bodies.
    
    Returns:
    float: Hill radius in normalized units.
    """
    return (mu / 3)**(1/3)

@jit
def C1(mu):
    """
    Calculate the value of Jacobi constant C1 the Lagrance point L1.
    
    Parameters:
    mu (float): The mass ratio of the two bodies.
    
    Returns:
    float: Value of C1.
    """
    return 3 + 3**(4/3) * mu**(2/3) - (10 * mu) / 3

@jit
def C2(mu):
    """
    Calculate the value of Jacobi constant C2 the Lagrance point L2.
    
    Parameters:
    mu (float): The mass ratio of the two bodies.
    
    Returns:
    float: Value of C2.
    """
    return 3 + 3**(4/3) * mu**(2/3) - (14 * mu) / 3

@jit
def C3(mu):
    """
    Calculate the value of Jacobi constant C3 the Lagrance point L3.
    
    Parameters:
    mu (float): The mass ratio of the two bodies.
    
    Returns:
    float: Value of C3.
    """
    return 3 + mu

@jit
def C45(mu):
    """
    Calculate the value of Jacobi constant C4,5 the Lagrance points L4 and L5.
    
    Parameters:
    mu (float): The mass ratio of the two bodies.
    
    Returns:
    float: Value of C4,5.
    """
    return 3 - mu

@jit
def equations_of_motion(t, state, mu):
    """
    Computes the equations of motion for the restricted three-body problem.

    Parameters:
    t (float)     : Time variable.
    state (list)  : State vector [x, y, vx, vy] where x and y are the positions,
                    and vx and vy are the velocities.
    mu (float)    : Mass ratio of the two primary bodies.

    Returns:
    list: Derivatives [dx/dt, dy/dt, dvx/dt, dvy/dt].
    """
    x, y, vx, vy = state
    r1 = np.sqrt((x + mu)**2 + y**2)
    r2 = np.sqrt((x - 1 + mu)**2 + y**2)
    
    ax = 2 * vy + x - (1 - mu) * (x + mu) / r1**3 - mu * (x - 1 + mu) / r2**3
    ay = -2 * vx + y - (1 - mu) * y / r1**3 - mu * y / r2**3
    
    return [vx, vy, ax, ay]

@jit
def generate_initial_condition(lagrange_point, h, mu, variance=0.1):
    """
    Generates an initial condition near a Lagrange point with small perturbations.

    Parameters:
    lagrange_point (tuple)  : Coordinates of the Lagrange point (x, y).
    h (float)               : Energy integral at the Lagrange point.
    mu (float)              : Mass ratio of the two primary bodies.
    variance (float)        : Variance for perturbation (default is 0.01).

    Returns:
    list: Initial state [x, y, vx, vy] with perturbations and calculated velocities.
    """
    perturbed_x = lagrange_point[0] + np.random.uniform(-variance, variance)
    perturbed_y = lagrange_point[1] + np.random.uniform(-variance, variance)
    
    # Assuming vx0 = 0
    vx0 = 0
    
    r1 = np.sqrt((perturbed_x + mu)**2 + perturbed_y**2)
    r2 = np.sqrt((perturbed_x - 1 + mu)**2 + perturbed_y**2)
    vy0 = np.sqrt(2 * (h + (1 - mu) / r1 + mu / r2 + 0.5 * (perturbed_x**2 + perturbed_y**2)))
    
    return [perturbed_x, perturbed_y, vx0, vy0]

def calculate_trajectory(initial_state, mu, t_span, t_eval):
    """
    Calculates the trajectory of the third body using numerical integration.

    Parameters:
    initial_state (list)   : Initial state [x, y, vx, vy] of the third body.
    mu (float)             : Mass ratio of the two primary bodies.
    t_span (tuple)         : Time span for the integration (start time, end time).
    t_eval (array)         : Array of time points at which to store the computed solution.

    Returns:
    tuple: Time points and state vectors [x, y, vx, vy] of the trajectory.
    """
    sol = solve_ivp(equations_of_motion, t_span, initial_state, args=(mu,), t_eval=t_eval, rtol=1e-9, atol=1e-9)
    return sol.t, sol.y

def characterize_trajectory(trajectory, R1, R2, r, mu):
    """
    Characterizes the trajectory of the third body by analyzing its path.

    Parameters:
    trajectory (array): State vectors [x, y, vx, vy] of the trajectory.
    R1 (float): Radius of Didymos.
    R2 (float): Radius of Dimorphos.
    r (float): Distance between Didymos and Dimorphos.
    mu (float): Mass ratio of the two primary bodies.

    Returns:
    str: Description of the trajectory ('Collision with Didymos', 'Collision with Dimorphos',
         'Escape trajectory', 'Completed orbit').
    """
    x, y = trajectory[0], trajectory[1]
    distance_to_didymos = np.sqrt((x + mu)**2 + y**2) * r
    distance_to_dimorphos = np.sqrt((x - 1 + mu)**2 + y**2) * r
    
    didymos_collision_time = np.inf
    dimorphos_collision_time = np.inf

    for i in range(len(x)):
        if distance_to_didymos[i] <= R1:
            didymos_collision_time = i
            break

    for i in range(len(x)):
        if distance_to_dimorphos[i] <= R2:
            dimorphos_collision_time = i
            break
    
    if didymos_collision_time < dimorphos_collision_time:
        return "Collision with Didymos"
    elif dimorphos_collision_time < didymos_collision_time:
        return "Collision with Dimorphos"
    elif np.max(np.sqrt(x**2 + y**2)) > 10:  # Arbitrary large distance to indicate escape
        return "Escape trajectory"
    else:
        return "Completed orbit"