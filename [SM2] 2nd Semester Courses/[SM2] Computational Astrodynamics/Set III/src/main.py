import numpy as np

import plot_util
import orbital_util as orb_util
import zero_velocity_contour as zvc

#--------------------------------- PARAMETERS ---------------------------------
# Constants
m1 = 5.32e11          # Mass of Didymos (kg)
m2 = 4.94e9           # Mass of Dimorphos (kg)
r  = 1.15             # Distance between Didymos and Dimorphos (km)
G  = 6.67430e-20      # Gravitational constant (km^3/kg/s^2)

# Calculate the orbital period
T = 2 * np.pi * np.sqrt(r**3 / (G * (m1 + m2)))

# Propagation duration
seconds = 100 * 60 * 60
normalized_time = seconds * 2 * np.pi / T

# Normalized units
mu = m2 / (m1 + m2)
R1 = 0.410            # Radius of Didymos (km)
R2 = 0.090            # Radius of Dimorphos (km)

#------------------------------ LAGRANGE POINTS ------------------------------
# Calculate Lagrange points (normalized units)
L1 = orb_util.L1(mu)
L2 = orb_util.L2(mu)
L3 = orb_util.L3(mu)
L4 = orb_util.L4(mu)
L5 = orb_util.L5(mu)

# Calculate Lagrange points (physical units)
l1 = (r * L1[0], r * L1[1])
l2 = (r * L2[0], r * L2[1])
l3 = (r * L3[0], r * L3[1])
l4 = (r * L4[0], r * L4[1])
l5 = (r * L5[0], r * L5[1])

# Calculate energy integrals h of Lagrange points
h1 =  -orb_util.C1(mu)/2
h2 =  -orb_util.C2(mu)/2
h3 =  -orb_util.C3(mu)/2
h4 =  -orb_util.C45(mu)/2
h5 =  -orb_util.C45(mu)/2

print("------------------------------------------------")
print("L1 (normalized units):", L1)
print("L2 (normalized units):", L2)
print("L3 (normalized units):", L3)
print("L4 (normalized units):", L4)
print("L5 (normalized units):", L5)

print("------------------------------------------------")
print("L1 (physical units):", l1)
print("L2 (physical units):", l2)
print("L3 (physical units):", l3)
print("L4 (physical units):", l4)
print("L5 (physical units):", l5)

print("------------------------------------------------")
print("Energy Integrals at Lagrange Points:")
print("Energy Integral for L1:", h1)
print("Energy Integral for L2:", h2)
print("Energy Integral for L3:", h3)
print("Energy Integral for L4:", h4)
print("Energy Integral for L5:", h5)

normalized_lagrange_points = [L1, L2, L3, L4, L5]

# Plot Lagrange points
plot_util.plot_system(normalized_lagrange_points, R1, R2, mu)

#--------- TRAJECTORY PROPAGATION - RANDOM INITIAL CONDITIONS ---------
# Generate random initial conditions
initial_state = orb_util.generate_initial_condition(L1, -1.55, mu)

t_span = (0, normalized_time)                     # Time span for integration
t_eval = np.linspace(t_span[0], t_span[1], 1000)  # Time points for evaluation

t, trajectory = orb_util.calculate_trajectory(initial_state, mu, t_span, t_eval)

# Plot calculated trajectory
characterization = orb_util.characterize_trajectory(trajectory, R1, R2, r, mu)

print("------------------------------------------------")
print(f"Propagating for t = {normalized_time} (normalized) ...")
print(f"Initial state: {initial_state}")
print(f"Characterization: {characterization}")

plot_util.plot_trajectory(normalized_lagrange_points, trajectory, R1, R2, mu)

zvc.zero_velocity_contour(mu, abs(h3), R1, R2, normalized_lagrange_points)