import os
import numpy as np
import matplotlib.pyplot as plt

import plot_util

def zero_velocity_contour(mu, h, R1, R2, lagrange_points, x_range=(-2, 2), y_range=(-2, 2), resolution=1000):
    """
    Calculate and plot the zero velocity contour for a given energy integral, including Didymos and Dimorphos.

    Parameters:
    mu (float): Mass ratio of the two primary bodies.
    h (float): Energy integral.
    R1 (float): Radius of Didymos.
    R2 (float): Radius of Dimorphos.
    x_range (tuple): Range of x values to consider (default is (-2, 2)).
    y_range (tuple): Range of y values to consider (default is (-2, 2)).
    resolution (int): Resolution of the grid (default is 1000).
    """
    x = np.linspace(x_range[0], x_range[1], resolution)
    y = np.linspace(y_range[0], y_range[1], resolution)
    X, Y = np.meshgrid(x, y)

    r1 = np.sqrt((X + mu)**2 + Y**2)
    r2 = np.sqrt((X - 1 + mu)**2 + Y**2)
    effective_potential = -0.5 * (X**2 + Y**2) - (1 - mu) / r1 - mu / r2

    # Calculate the Jacobi constant from the energy integral
    C = -2 * h

    fig, ax = plt.subplots(figsize=(8, 8))
    fig.patch.set_facecolor('#2E2E2E')
    
    # Plot the zero velocity limit curves
    contour = ax.contour(X, Y, 2 * effective_potential, levels=[C], colors='white', linewidths=2)
    ax.clabel(contour, inline=1, fontsize=10, colors='white')
    
    # Plot primary bodies
    didymos_circle = plt.Circle((-mu, 0), R1, color='#607D8B', fill=True, label='Didymos')
    dimorphos_circle = plt.Circle((1 - mu, 0), R2, color='#FFAB91', fill=True, label='Dimorphos')
    
    ax.add_patch(didymos_circle)
    ax.add_patch(dimorphos_circle)
    
    # Lagrange points
    for i, (x, y) in enumerate(lagrange_points):
        distance = plot_util.calculate_distance(x, y)
        ax.plot(x, y, 'o', color='#AED581', label=f'L{i+1} (d={distance:.2f})')
        ax.text(x, y+0.1, f'L{i+1}', color='white', fontsize=12)
    
    plt.title('Zero Velocity Limit Curves', color='white')
    plt.xlabel('x (normalized)', color='white')
    plt.ylabel('y (normalized)', color='white')
    plt.gca().set_facecolor('#2E2E2E')
    plt.gca().tick_params(axis='x', colors='white')
    plt.gca().tick_params(axis='y', colors='white')
    plt.gca().spines['top'].set_color('white')
    plt.gca().spines['bottom'].set_color('white')
    plt.gca().spines['left'].set_color('white')
    plt.gca().spines['right'].set_color('white')
    plt.grid(color='grey', linestyle='--', linewidth=0.5)
    plt.legend()
    
    output_dir = os.path.join(os.path.dirname(__file__), '../graphs')
    os.makedirs(output_dir, exist_ok=True)
    graph_path = os.path.join(output_dir, f'zero_velocity_graph.jpg')
    plt.savefig(graph_path, dpi=300)
    
    plt.show()
