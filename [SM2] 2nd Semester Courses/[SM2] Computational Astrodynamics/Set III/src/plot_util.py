import os
import numpy as np
import matplotlib.pyplot as plt
from numba import jit

@jit
def calculate_distance(x, y):
    """
    Calculate the distance of a point (x, y) from the origin.

    Parameters:
    x (float): x-coordinate of the point.
    y (float): y-coordinate of the point.

    Returns:
    float: Distance from the origin.
    """
    return np.sqrt(x**2 + y**2)

def plot_system(lagrange_points, R1, R2, mu):
    """
    Plot the primary bodies and Lagrange points.

    Parameters:
    lagrange_points (list of tuples): List of (x, y) coordinates of Lagrange points.
    R1 (float): Radius of the first primary body.
    R2 (float): Radius of the second primary body.
    mu (float): Mass ratio of the two bodies.
    """
    fig, ax = plt.subplots(figsize=(8, 8))
    fig.patch.set_facecolor('#2E2E2E')


    # Primary bodies
    body1_x = -mu
    body2_x = 1 - mu
    circle1 = plt.Circle((body1_x, 0), R1, color='#607D8B', alpha=0.8, label='Didymos')
    circle2 = plt.Circle((body2_x, 0), R2, color='#FFAB91', alpha=0.8, label='Dimorphos')

    ax.add_artist(circle1)
    ax.add_artist(circle2)

    # Lagrange points
    for i, (x, y) in enumerate(lagrange_points):
        distance = calculate_distance(x, y)
        ax.plot(x, y, 'o', color='#AED581', label=f'L{i+1} (d={distance:.2f})')
        ax.text(x, y+0.1, f'L{i+1}', color='white', fontsize=12)
        
    ax.set_facecolor('#2E2E2E')
    ax.set_xlim(-2, 2)
    ax.set_ylim(-2, 2)
    ax.set_aspect('equal', 'box')
    ax.xaxis.label.set_color('white')
    ax.yaxis.label.set_color('white')
    ax.tick_params(axis='x', colors='white')
    ax.tick_params(axis='y', colors='white')
    ax.spines['top'].set_color('white')
    ax.spines['bottom'].set_color('white')
    ax.spines['left'].set_color('white')
    ax.spines['right'].set_color('white')
    ax.grid(color='grey', linestyle='--', linewidth=0.5)
    ax.legend()
    plt.title('Primary Bodies and Lagrange Points', color='white')
    
    output_dir = os.path.join(os.path.dirname(__file__), '../graphs')
    os.makedirs(output_dir, exist_ok=True)
    graph_path = os.path.join(output_dir, f'Lagrange Points.jpg')
    plt.savefig(graph_path, dpi=300)

    plt.show()

def plot_trajectory(lagrange_points, trajectory, R1, R2, mu):
    """
    Plot the trajectory propagated from a specific Lagrange point.

    Parameters:
    lagrange_points (list of tuples): List of (x, y) coordinates of Lagrange points.
    trajectory: Trajectory positions
    R1 (float): Radius of the first primary body.
    R2 (float): Radius of the second primary body.
    mu (float): Mass ratio of the two bodies.
    """
    x, y = trajectory[0], trajectory[1]
    
    fig, ax = plt.subplots(figsize=(8, 8))
    fig.patch.set_facecolor('#2E2E2E')
    
    ax.plot(x, y, label='Trajectory')
    
    # Plot primary bodies
    didymos_circle = plt.Circle((-mu, 0), R1, color='#607D8B', fill=True, label='Didymos')
    dimorphos_circle = plt.Circle((1 - mu, 0), R2, color='#FFAB91', fill=True, label='Dimorphos')
    ax.add_patch(didymos_circle)
    ax.add_patch(dimorphos_circle)
    
    # Lagrange points
    for i, (x, y) in enumerate(lagrange_points):
        distance = calculate_distance(x, y)
        ax.plot(x, y, 'o', color='#AED581', label=f'L{i+1} (d={distance:.2f})')
        ax.text(x, y+0.1, f'L{i+1}', color='white', fontsize=10)
    
    ax.set_facecolor('#2E2E2E')
    ax.set_aspect('equal', 'box')
    ax.xaxis.label.set_color('white')
    ax.yaxis.label.set_color('white')
    ax.tick_params(axis='x', colors='white')
    ax.tick_params(axis='y', colors='white')
    ax.spines['top'].set_color('white')
    ax.spines['bottom'].set_color('white')
    ax.spines['left'].set_color('white')
    ax.spines['right'].set_color('white')
    ax.grid(color='grey', linestyle='--', linewidth=0.5)
    ax.legend()
    
    plt.xlabel('x (normalized)')
    plt.ylabel('y (normalized)')
    plt.title('Trajectory of the Third Body', color='white')
    
    output_dir = os.path.join(os.path.dirname(__file__), '../graphs')
    os.makedirs(output_dir, exist_ok=True)
    graph_path = os.path.join(output_dir, f'trajectory.jpg')
    plt.savefig(graph_path, dpi=300)
    
    plt.show()