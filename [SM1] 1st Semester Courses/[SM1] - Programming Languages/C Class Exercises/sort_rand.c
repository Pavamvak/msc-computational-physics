/*
Random Number Generator with Sorting Feature
This C script generates a specified number of random numbers within a user-defined range. It then sorts the numbers 
from lowest to highest using the bubble sort method. The script uses srand to seed the random number generator.

Functionality:
1. The user inputs two limits for the range of random numbers. The program can determine which is the maximum and 
which is the minimum in case of incorrect input order.
2. The user specifies how many random numbers to generate. This value must be an integer.
3. The program generates the specified number of random numbers within the given range.
4. It then uses the bubble sort algorithm to sort the generated set.

Author: Panagiotis Vamvakas
Date: 04/12/2023
Version: 1.1
*/

#include <stdio.h>   // Standard Input Output library: Used for functions such as printf() and scanf().
#include <stdlib.h>  // Standard Library: Includes functions involving memory allocation, process control, conversions, and others.
#include <time.h>    // Time library: Used to manipulate date and time information.

//----------------------------------------------------------------------------------------------------------------
// RANDOM NUMBER GENERATOR FUNCTION
//
// Generates an array of random floating point numbers within a specified range [min, max].
//
// Parameters:
// - int num: The number of random numbers to generate.
// - float min: The minimum value of the desired random range.
// - float max: The maximum value of the desired random range.
//
// Returns:
// - A pointer to the generated array of random numbers.
//
// How it works:
// - Allocates memory for an array of floats.
// - Fills the array with random numbers within the specified range using 'rand()'.
//----------------------------------------------------------------------------------------------------------------
float *random_number_generator(int num, float min, float max) {
    int counter;
    float *random_array_gen = malloc(num * sizeof(float)); // Allocate memory for the array

    // Generate random numbers within the specified range
    for (counter = 0; counter < num; counter++) {
        random_array_gen[counter] = min + (max - min) * (float)(rand()) / RAND_MAX;
    }
    return random_array_gen; // Return the pointer to the generated array
}

//----------------------------------------------------------------------------------------------------------------
// BUBBLE SORT FUNCTION
//
// Sorts an array of random numbers in ascending order using the bubble sort algorithm.
//
// Parameters:
// - float *random_array: The array to be sorted.
// - int num: The number of elements in the array.
//
// Returns:
// - A pointer to the sorted array.
//
// How it works:
// - Iterates over the array, repeatedly swapping adjacent elements if they are in the wrong order.
// - Continues until the array is sorted.
//----------------------------------------------------------------------------------------------------------------
float *bubble_sort(float *random_array, int num) {
    int counter;
    int sort_counter;
    float temp;

    for (counter = 0; counter < num - 1; counter++) {
        for (sort_counter = 0; sort_counter < num - counter - 1; sort_counter++) {
            if (random_array[sort_counter] > random_array[sort_counter + 1]) {
                temp = random_array[sort_counter];
                random_array[sort_counter] = random_array[sort_counter + 1];
                random_array[sort_counter + 1] = temp;
            }
        }
    }
    return random_array;
}

//----------------------------------------------------------------------------------------------------------------
// MAIN FUNCTION
//
// Entry point of the program. Initializes parameters, generates random numbers, sorts them, and prints the results.
//
// How it works:
// - Prompts the user for range limits and the number of random numbers to generate.
// - Generates random numbers within the specified range.
// - Sorts the generated random numbers using bubble sort.
// - Prints the unsorted and sorted random numbers.
//----------------------------------------------------------------------------------------------------------------
int main() {
    // Initialize range limit parameters
    float min;
    float max;

    // Initialize amount of numbers parameter
    int num;

    // Initialize parameters
    float temp_num;
    int counter;
    float *random_array;
    float *sorted_array;

    // Initialize the seed using the system's time
    time_t t;
    srand((unsigned)time(&t));

    // Ask the user for limits
    printf("Enter the minimum limit of the range:");
    scanf("%f", &min);

    printf("\nEnter the maximum limit of the range:");
    scanf("%f", &max);

    // Check if the limits are correctly inputted
    if (min > max) {
        temp_num = min;
        min = max;
        max = temp_num;
    }

    // Ask the user for how many numbers to generate
    printf("\nEnter how many numbers to generate:");
    scanf("%d", &num);

    // Generate random numbers
    random_array = random_number_generator(num, min, max);

    // Print the generated random numbers
    printf("\nThe random numbers that were generated are:");
    for (counter = 0; counter < num; counter++) {
        printf("\n%f", random_array[counter]);
    }

    // Sort the random numbers
    sorted_array = bubble_sort(random_array, num);

    // Print the sorted random numbers
    printf("\nThe random numbers sorted were:");
    for (counter = 0; counter < num; counter++) {
        printf("\n%f", sorted_array[counter]);
    }

    printf("\n");

    // Free the allocated memory
    free(random_array);

    return 0;
}
