/*
Random Number Generator
This C script generates random numbers using srand to change the seed of the random generator.
The user inputs:
- Two limits (the program determines the maximum and minimum in case of incorrect input).
- The number of random numbers to generate (this must be an integer).

Author: Panagiotis Vamvakas
Date: 27/11/2023
Version: 1.0
*/

#include <stdio.h>   // Standard Input Output library: Used for functions such as printf() and scanf().
#include <stdlib.h>  // Standard Library: Includes functions involving memory allocation, process control, conversions, and others.
#include <time.h>    // Time library: Used to manipulate date and time information.

//----------------------------------------------------------------------------------------------------------------
// MAIN FUNCTION
//
// Entry point of the program. Initializes parameters, generates random numbers, and prints the results.
//
// How it works:
// - Prompts the user for range limits and the number of random numbers to generate.
// - Generates random numbers within the specified range.
// - Prints the generated random numbers.
//----------------------------------------------------------------------------------------------------------------
int main() {
    // Initialize range limit parameters
    float min;
    float max;

    // Initialize amount of numbers parameter
    int num;

    // Initialize counters and temporary variable
    int counter;
    float temp_num;

    // Initialize the seed using the system's time
    time_t t;
    srand((unsigned) time(&t));

    // Ask the user for limits
    printf("Enter the minimum limit of the range:");
    scanf("%f", &min);

    printf("\nEnter the maximum limit of the range:");
    scanf("%f", &max);

    // Check if the limits are correctly inputted
    if (min > max) {
        temp_num = min;
        min = max;
        max = temp_num;
    }

    // Ask the user for how many numbers to generate
    printf("\nEnter how many numbers to generate:");
    scanf("%i", &num);

    // Generate and print the random numbers
    for (counter = 1; counter <= num; counter++) {
        temp_num = min + (max - min) * (float)(rand()) / RAND_MAX;
        printf("\n%f", temp_num);
    }

    printf("\n");
    return 0;
}
