/*
Random Number Generator and Sorter
This C script generates a specified number of random numbers within a given range and sorts them using merge sort.
The program includes functions to generate random numbers, custom division, merging arrays, and merge sort.

Author: Panagiotis Vamvakas
Date: 08/01/2024
Version: 1.0
*/

#include <stdio.h>   // Standard Input Output library: Used for functions such as printf() and scanf().
#include <stdlib.h>  // Standard Library: Includes functions involving memory allocation, process control, conversions, and others.
#include <time.h>    // Time library: Used to manipulate date and time information.

//----------------------------------------------------------------------------------------------------------------
// RANDOM NUMBER GENERATOR FUNCTION
//
// Generates an array of random double precision floating-point numbers within a specified range [min, max]. But what is great
// here is that it allocates memory to the array.
//
// Parameters:
// - int num: The number of random numbers to generate.
// - double min: The minimum value of the desired random range.
// - double max: The maximum value of the desired random range.
//
// Returns:
// - A pointer to the generated array of random numbers.
//
// How it works:
// - Allocates memory for an array of doubles.
// - Fills the array with random numbers within the specified range using 'rand()'.
//----------------------------------------------------------------------------------------------------------------
double *random_number_generator(int num, double min, double max) {
    // Initialize parameters
    int counter;
    double *random_array_gen = malloc(num * sizeof(double)); // Allocate memory for the array

    // Generate random numbers within the specified range
    for (counter = 0; counter < num; counter++) {
        random_array_gen[counter] = min + (max - min) * (double)(rand()) / RAND_MAX;
    }
    return random_array_gen; // Return the pointer to the generated array
}

//----------------------------------------------------------------------------------------------------------------
// CUSTOM DIVISION FUNCTION
//
// Performs a custom division operation on an index to adjust it for merge sort.
//
// Parameters:
// - int index: The index to be divided.
//
// Returns:
// - The adjusted index.
//
// How it works:
// - Checks if the index is odd or even.
// - If odd, divides by 2 and adds 1.
// - If even, divides by 2.
//----------------------------------------------------------------------------------------------------------------
int custom_devision(int index) {
    int rem = index % 2;
    if (rem != 0) {
        index = index / 2 + 1; // Adjust the index if it's odd
    } else {
        index = index / 2; // Regular division if it's even
    }
    return index;
}

//----------------------------------------------------------------------------------------------------------------
// MERGE FUNCTION
//
// Merges two halves of an array into a sorted array.
//
// Parameters:
// - double arr[]: The array to be sorted.
// - int l: The left index of the subarray.
// - int m: The middle index of the subarray.
// - int r: The right index of the subarray.
//
// How it works:
// - Creates temporary arrays for the left and right halves.
// - Merges the temp arrays back into the main array in sorted order.
//----------------------------------------------------------------------------------------------------------------
void merge(double arr[], int l, int m, int r) {
    int i, j, k;
    int n1 = m - l + 1;
    int n2 = r - m;

    // Create temp arrays
    double L[n1], R[n2];

    // Copy data to temp arrays
    for (i = 0; i < n1; i++)
        L[i] = arr[l + i];
    for (j = 0; j < n2; j++)
        R[j] = arr[m + 1 + j];

    // Merge the temp arrays back into arr[l..r]
    i = 0;
    j = 0;
    k = l;
    while (i < n1 && j < n2) {
        if (L[i] <= R[j]) {
            arr[k] = L[i];
            i++;
        } else {
            arr[k] = R[j];
            j++;
        }
        k++;
    }

    // Copy the remaining elements of L[], if there are any
    while (i < n1) {
        arr[k] = L[i];
        i++;
        k++;
    }

    // Copy the remaining elements of R[], if there are any
    while (j < n2) {
        arr[k] = R[j];
        j++;
        k++;
    }
}

//----------------------------------------------------------------------------------------------------------------
// MERGE SORT FUNCTION
//
// Sorts an array using the merge sort algorithm.
//
// Parameters:
// - double *array: The array to be sorted.
// - int l: The left index of the array to be sorted.
// - int r: The right index of the array to be sorted.
//
// Returns:
// - A pointer to the sorted array.
//
// How it works:
// - Recursively divides the array into two halves until each subarray has one element.
// - Merges the subarrays back together in sorted order.
//----------------------------------------------------------------------------------------------------------------
double *mergeSort(double *array, int l, int r) {
    if (l < r) {
        int m = l + (r - l) / 2; // Find the middle point

        // Sort first and second halves
        mergeSort(array, l, m);
        mergeSort(array, m + 1, r);

        // Merge the sorted halves
        merge(array, l, m, r);
    }
    return array; // Return the sorted array
}

//----------------------------------------------------------------------------------------------------------------
// MAIN FUNCTION
//
// Entry point of the program. Initializes parameters, generates random numbers,
// sorts them, and prints the results.
//
// How it works:
// - Prompts the user for range limits and the number of random numbers to generate.
// - Generates random numbers within the specified range.
// - Sorts the generated random numbers using merge sort.
// - Prints the unsorted and sorted random numbers.
//----------------------------------------------------------------------------------------------------------------
int main(int argc, char **argv) {
    // Initialize range limit parameters
    double min;
    double max;

    // Initialize amount of numbers parameter
    int num;

    // Initialize parameters
    double temp_num;
    int counter;
    
    // Initialize the seed using the system's time
    time_t t;
    srand((unsigned) time(&t));

    // Ask the user for limits
    printf("Enter the minimum limit of the range:");
    scanf("%lf", &min);

    printf("\nEnter the maximum limit of the range:");
    scanf("%lf", &max);

    // Ask the user for how many numbers
    printf("\nEnter how many numbers to generate:");
    scanf("%d", &num);

    // Check if the limits are correctly inputted
    if (min > max) {
        temp_num = min;
        min = max;
        max = temp_num;
    }

    // Generate random numbers
    double *random_array = random_number_generator(num, min, max);

    // Print the generated random numbers
    printf("\nThe random numbers that were generated are:");
    for (counter = 0; counter < num; counter++) {
        printf("\n%lf", random_array[counter]);
    }

    // Sort the random numbers
    mergeSort(random_array, 0, num - 1);

    // Print the sorted random numbers
    printf("\nThe random numbers sorted are:");
    for (counter = 0; counter < num; counter++) {
        printf("\n%lf", random_array[counter]);
    }

    // Free the allocated memory
    free(random_array);

    printf("\n");
    return 0;
}
