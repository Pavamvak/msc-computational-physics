/*
Random Number Generator and Sorter
This C program generates a specified number of random floating point numbers within a user-defined range.
It then sorts these numbers using the selection sort algorithm.

The program consists of three main functions:
1. swap_numbers() - swaps two elements in an array.
2. random_number_generator() - generates an array of random floating point numbers within a specified range.
3. selection_sort() - sorts the array of random numbers in ascending order using the selection sort algorithm.

The main() function drives the program, asking the user to input the range and the number of random numbers to generate.
It then prints the unsorted and sorted arrays.

Compilation: gcc program_name.c -o program_name
Execution: ./program_name

Author: Panagiotis Vamvakas
Date: 11/12/2023
Version: 1.1

Note:
- This program uses the stdlib.h and time.h libraries for random number generation.
- It is essential to ensure that the maximum limit is greater than or equal to the minimum limit.
- The program uses dynamic memory allocation for the array of random numbers. 
*/

#include <stdio.h>   // Standard Input Output library: Used for functions such as printf() and scanf().
#include <stdlib.h>  // Standard Library: Includes functions involving memory allocation, process control, conversions, and others.
#include <time.h>    // Time library: Used to manipulate date and time information.
#include <stddef.h>  // Standard definitions library: Defines several useful types and macros.
#define NR_END 1
#define FREE_ARG char*

//----------------------------------------------------------------------------------------------------------------
// ERROR HANDLING FUNCTION
//
// Prints an error message and exits the program.
//
// Parameters:
// - char error_text[]: The error message to be printed.
//
// How it works:
// - Uses fprintf to print the error message to stderr.
// - Exits the program using exit().
//----------------------------------------------------------------------------------------------------------------
void nrerror(char error_text[]) {
    fprintf(stderr, "Numerical Recipes run-time error...\n");
    fprintf(stderr, "%s\n", error_text);
    fprintf(stderr, "...now exiting to system...\n");
    exit(1);
}

//----------------------------------------------------------------------------------------------------------------
// MEMORY ALLOCATION FUNCTION
//
// Allocates memory for a double vector with subscript range v[nl..nh].
//
// Parameters:
// - long nl: The lower bound of the subscript range.
// - long nh: The upper bound of the subscript range.
//
// Returns:
// - A pointer to the allocated memory.
//
// How it works:
// - Allocates memory using malloc() and checks for allocation failure.
// - Returns the pointer to the allocated memory, adjusted for subscript range.
//----------------------------------------------------------------------------------------------------------------
double *dvector(long nl, long nh) {
    double *v;

    v = (double *)malloc((size_t)((nh - nl + 1 + NR_END) * sizeof(double)));
    if (!v) nrerror("allocation failure in dvector()");
    return v - nl + NR_END;
}

//----------------------------------------------------------------------------------------------------------------
// MEMORY DEALLOCATION FUNCTION
//
// Frees the memory allocated for a double vector.
//
// Parameters:
// - double *v: The pointer to the memory to be freed.
// - long nl: The lower bound of the subscript range.
// - long nh: The upper bound of the subscript range.
//
// How it works:
// - Frees the memory using free().
//----------------------------------------------------------------------------------------------------------------
void free_dvector(double *v, long nl, long nh) {
    free((FREE_ARG)(v + nl - NR_END));
}

//----------------------------------------------------------------------------------------------------------------
// SWAP NUMBERS FUNCTION
//
// Swaps two elements in an array.
//
// Parameters:
// - double *random_array: The array containing the elements to be swapped.
// - int index_1: The index of the first element.
// - int index_2: The index of the second element.
//
// Returns:
// - A pointer to the array with the elements swapped.
//
// How it works:
// - Uses a temporary variable to swap the elements at the specified indices.
//----------------------------------------------------------------------------------------------------------------
double *swap_numbers(double *random_array, int index_1, int index_2) {
    double temp;
    temp = random_array[index_1];
    random_array[index_1] = random_array[index_2];
    random_array[index_2] = temp;
    return random_array;
}

//----------------------------------------------------------------------------------------------------------------
// RANDOM NUMBER GENERATOR FUNCTION
//
// Generates an array of random floating point numbers within a specified range [min, max].
//
// Parameters:
// - int num: The number of random numbers to generate.
// - double min: The minimum value of the desired random range.
// - double max: The maximum value of the desired random range.
//
// Returns:
// - A pointer to the generated array of random numbers.
//
// How it works:
// - Allocates memory for an array of doubles.
// - Fills the array with random numbers within the specified range using 'rand()'.
//----------------------------------------------------------------------------------------------------------------
double *random_number_generator(int num, double min, double max) {
    int counter;
    double *random_array_gen = malloc(num * sizeof(double));

    for (counter = 0; counter < num; counter++) {
        random_array_gen[counter] = min + (max - min) * (double)(rand()) / RAND_MAX;
    }
    return random_array_gen;
}

//----------------------------------------------------------------------------------------------------------------
// SELECTION SORT FUNCTION
//
// Sorts an array of random numbers in ascending order using the selection sort algorithm.
//
// Parameters:
// - double *random_array: The array to be sorted.
// - int num: The number of elements in the array.
//
// Returns:
// - A pointer to the sorted array.
//
// How it works:
// - Iterates over the array, finding the minimum element in the unsorted part and swapping it with the first element of the unsorted part.
//----------------------------------------------------------------------------------------------------------------
double *selection_sort(double *random_array, int num) {
    int counter = 0;
    int sort_counter = 0;
    int min_counter;

    for (counter = 0; counter < num - 1; counter++) {
        min_counter = counter;
        for (sort_counter = counter + 1; sort_counter < num; sort_counter++) {
            if (random_array[sort_counter] < random_array[min_counter]) {
                min_counter = sort_counter;
            }
        }
        if (min_counter != counter) {
            random_array = swap_numbers(random_array, counter, min_counter);
        }
    }
    return random_array;
}

//----------------------------------------------------------------------------------------------------------------
// MAIN FUNCTION
//
// Entry point of the program. Initializes parameters, generates random numbers, sorts them, and prints the results.
//
// How it works:
// - Prompts the user for range limits and the number of random numbers to generate.
// - Generates random numbers within the specified range.
// - Sorts the generated random numbers using selection sort.
// - Prints the unsorted and sorted random numbers.
//----------------------------------------------------------------------------------------------------------------
int main(int argc, char **argv) {
    // Initialize range limit parameters
    double min;
    double max;

    // Initialize amount of numbers parameter
    int num;

    // Initialize parameters
    double temp_num;
    int counter;
    
    double *sorted_array;

    // Initialize the seed using the system's time
    time_t t;
    srand((unsigned)time(&t));

    // Ask the user for limits
    printf("Enter the minimum limit of the range:");
    scanf("%lf", &min);

    printf("\nEnter the maximum limit of the range:");
    scanf("%lf", &max);

    // Check if the limits are correctly inputted
    if (min > max) {
        temp_num = min;
        min = max;
        max = temp_num;
    }

    // Ask the user for how many numbers to generate
    printf("\nEnter how many numbers to generate:");
    scanf("%d", &num);

    // Allocate memory for the random numbers array
    double *random_array = dvector(0, num - 1);

    // Generate random numbers
    random_array = random_number_generator(num, min, max);

    // Print the generated random numbers
    printf("\nThe random numbers that were generated are:");
    for (counter = 0; counter < num; counter++) {
        printf("\n%lf", random_array[counter]);
    }

    // Sort the random numbers
    sorted_array = selection_sort(random_array, num);

    // Print the sorted random numbers
    printf("\nThe random numbers sorted are:");
    for (counter = 0; counter < num; counter++) {
        printf("\n%lf", sorted_array[counter]);
    }

    printf("\n");

    // Free the allocated memory
    free_dvector(sorted_array, 0, num - 1);

    return 0;
}
