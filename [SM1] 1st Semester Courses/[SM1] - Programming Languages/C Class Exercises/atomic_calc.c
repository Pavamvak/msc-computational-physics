/*
Random Atom Generator
This C script generates 100 random atoms with random mass, positions, and velocities.
Each parameter's random value is generated with the srand to change the seed of the random generator.

Author: Panagiotis Vamvakas
Date: 08/01/2024
Version: 1.0
*/

#include <stdio.h>   // Standard Input Output library: Used for functions such as printf() and scanf().
#include <stdlib.h>  // Standard Library: Includes functions involving memory allocation, process control, conversions, and others.
#include <time.h>    // Time library: Used to manipulate date and time information.
#include <math.h>    // Math library: Provides mathematical functions like sqrt(), pow(), sin(), cos(), etc.

//----------------------------------------------------------------------------------------------------------------
// STRUCT DEFINIITION
//
// A 'struct' (short for "structure") in C is a user-defined data type
// that allows you to combine data items of different kinds. It is a way
// to group variables together under a single name, providing a convenient
// means of handling related data as a single unit. Imagine it is something
// like defining a matrix template that in each cell can hold a specific type of
// parameter. 
//
// 1. Definition:
// As in every 'C' variable, before accessing a 'struct' must be defined. A 'struct'
// is defined using the 'struct' keyword followed by the structure name and a block 
// of member variables enclosed in braces '{}'. Be very thoughtfull on the type of 
// each variable the struct is going to have, as they will be only able to assign the
// specified types you set for each variable. Think how the 'struct' is going to be used.

// 2. Members:
// Each member of a 'struct' can be of any data type, including other 'struct's.
// Members can be accessed using the dot '.' operator, once the struct is defined.

// 3. Memory Allocation:
// The memory allocation of a 'struct' is the sum of the memory allocations
// of its members. Think of what the 'struct' is going to be used for, to set the 
// necessary variable types. DO NOT USE VARIABLES OR TYPES THAT ARE NOT NECESSARY.
// For example, if a variable is a counter, then even though the type 'double' would
// not cause any errors, it would consume too much memory for no reason, when the 'integer'
// would also work just fine.
// 
// 4. Usage:
// 'Structs' are particularly useful for creating complex data structures
// like linked lists, trees, and databases. Using proper variable types and names, you
// can easily create complex structures to save and process all kinds of data.
//----------------------------------------------------------------------------------------------------------------

// Define a struct for position
struct Position {
    double x;
    double y;
    double z;
};

// Define the main struct containing mass, position, and velocity
struct Particle {
    double mass;
    struct Position position;
    double velocity;
};

//----------------------------------------------------------------------------------------------------------------
// RANDOM NUMBER FUNCTION
//
// In C, generating random numbers can be accomplished using the 'rand()' function, which is a part of the
// C standard library. However, 'rand()' generates a pseudo-random number between 0 and RAND_MAX. To generate
// random numbers within a specific range, custom functions are often created. Here, we have two such functions:
// 'randDouble' and 'randInt'.
//
//----------------------------------------------------------------------------------------------------------------
// Function: randDouble
//
// This function generates a random double precision floating-point number within a specified range [min, max].
//
// Parameters:
// - double min: The minimum value of the desired random range.
// - double max: The maximum value of the desired random range.
//
// How it works:
// - The 'rand()' function returns a pseudo-random integer between 0 and 'RAND_MAX'.
// - This integer is then normalized by dividing by 'RAND_MAX' to get a value between 0 and 1.
// - The normalized value is scaled to the desired range [min, max] by multiplying it by the 'min-max' difference and adding 'min'.
//
//----------------------------------------------------------------------------------------------------------------
// Function: randInt
//
// This function generates a random integer within a specified range [min, max].
//
// Parameters:
// - int min: The minimum value of the desired random range.
// - int max: The maximum value of the desired random range.
//
// How it works:
// - The 'rand()' function returns a pseudo-random integer between 0 and RAND_MAX.
// - The modulus operator '%' is used to limit the range of the random number to (max - min + 1).
// - The result is then shifted by adding 'min' to fall within the desired range [min, max].
//
double randDouble(double min, double max) {
    double random_double;
    random_double = min + (max - min) * (double)(rand())/RAND_MAX;
    return random_double;
}

int randInt(int min, int max) {
    return rand() % (max - min + 1) + min;
}

//----------------------------------------------------------------------------------------------------------------
// MAIN DEFINITION
// Every program must have a main function. It is essentially what is being executed. This initializes all 
// the necessary parameters, performs all the necessary actions, call the necessary functions (either those
// being user-made or native), and it is the input and output point of the program.
//
// The general approach that was used was to follow the below order of actions:
// 1. Parameter definition/ initialization.
// 2. User input.
// 3. Calculations.
// 4. Program output.
//
// From here it is pretty straight forward. No further explanation is necessary.
int main() {
    //-------------------------------------------------------------
    const double earths_mass = 5.972e24;        // Earth's mass in kilograms
    const double amu_to_kg = 1.66053906660e-27; // Define conversion constant from amu to kg
    const double k = 1.380649e-23;              // Boltzmann constant in J/K
    const double g = 6.674310e-11;              // Gravitational constant in SI

    // Initialize mass range limit parameters in amu
    int min_mass_amu;
    int max_mass_amu;

    // Initialize temperature range limit parameters
    double min_temp;
    double max_temp;

    // Initialize position range limit parameters
    float min;
    float max;
    
    // Initialize amount of atoms parameter
    int num;

    // Initialize other variables 
    int counter;
    float temp_num;

    // Initialize the seed using the system's time
    time_t t;

    // Chech if the limits are correclty inputted
    if (min > max){
        temp_num=min;
        min = max;
        max = temp_num;
    }

    //-------------------------------------------------------------
    // Ask the user for limits
    printf("Enter the minimum limit of the position (m):");
    scanf("%f", &min);

    printf("\nEnter the maximum limit of the position (m):");
    scanf("%f", &max);

    // Ask the user for mass limits in amu
    printf("Enter the minimum mass of the range (amu):");
    scanf("%d", &min_mass_amu);

    printf("\nEnter the maximum mass of the range (amu):");
    scanf("%d", &max_mass_amu);

    // Ask the user for temperature limits
    printf("Enter the minimum temperature (K):");
    scanf("%lf", &min_temp);

    printf("\nEnter the maximum temperature (K):");
    scanf("%lf", &max_temp);

    // Ask the user for how many numbers
    printf("\nEnter how many atoms to generate:");
    scanf("%i", &num);

    //-------------------------------------------------------------
    // Create and print the random numbers
    srand((unsigned) time(&t));

    struct Particle atoms[num]; // Array to hold "num" atoms

    // Create and initialize 'num' atoms with random parameters
    for (int i = 0; i < num; i++) {

        int mass_amu = randInt(min_mass_amu, max_mass_amu);
        atoms[i].mass = mass_amu * amu_to_kg; // Convert mass from amu to kg

        // Random position coordinates
        atoms[i].position.x = randDouble(min, max);
        atoms[i].position.y = randDouble(min, max);
        atoms[i].position.z = randDouble(min, max);

        // Generate a random temperature for the atom within the specified range
        double temp = randDouble(min_temp, max_temp);

        // Calculate v_rms for the atom
        atoms[i].velocity = sqrt(2 * k * temp / atoms[i].mass);
    }

    // Used for debugging
    printf("------------------------------------------------------------------\n");
    printf("First atom: Mass = %f, Position = (%f, %f, %f), Velocity = (%f)\n",
           atoms[0].mass,
           atoms[0].position.x, atoms[0].position.y, atoms[0].position.z,
           atoms[0].velocity);
    printf("------------------------------------------------------------------\n");
    
    // Calculate and print kinetic, dynamic and total energy for each atom
    for (int i = 0; i < num; i++) {
        double distance_squared = pow(atoms[i].position.x, 2) + pow(atoms[i].position.y, 2) + pow(atoms[i].position.z, 2);

        double kinetic_energy = 0.5 * atoms[i].mass * pow(atoms[i].velocity, 2);
        double dynamic_energy = g * atoms[i].mass * earths_mass / sqrt(distance_squared);

        double total_energy = kinetic_energy + dynamic_energy;
        printf("Atom %d: Kinetic Energy = %e, Dynamic Energy = %e, Total Energy = %e\n", i+1, kinetic_energy, dynamic_energy, total_energy);
    }
    return 0;
}
