#include<iostream>
#include<string>

using namespace std;

class Person{
    private:
        string name;
        string contactPhone;
        string contactAddress;
        int yearBirth;
    
    public:
        void setYearBirth(int y);
        int getYearBirth();
        void setName(string nm);
        string getName();
        void setContactPhone(string ph);
        string getContactPhone();
        void setContactAddress(string ad);
        string getContactAddress();
        void printPersonYearBirth();
        void printall();
};

// Definitions of the member functions

void Person::setYearBirth(int y){
    yearBirth = y;
}

int Person::getYearBirth(){
    return yearBirth;
}

void Person::setName(string nm){
    name = nm;
}

string Person::getName(){
    return name;
}

void Person::setContactPhone(string ph){
    contactPhone = ph;
}

string Person::getContactPhone(){
    return contactPhone;
}

void Person::setContactAddress(string ad){
    contactAddress = ad;
}

string Person::getContactAddress(){
    return contactAddress;
}

void Person::printPersonYearBirth(){
    cout << "Year of Birth: " << getYearBirth() << endl;
}

void Person::printall(){
    cout << "Name: " << getName() << endl;
    cout << "Phone: " << getContactPhone() << endl;
    cout << "Address: " << getContactAddress() << endl;
    cout << "Year of Birth: " << getYearBirth() << endl;
}

int main(){
    Person m;
    m.setYearBirth(1990);
    m.setName("John Doe");
    m.setContactPhone("123-456-7890");
    m.setContactAddress("123 Main St");

    m.printPersonYearBirth();
    m.printall();

    return 0;
}