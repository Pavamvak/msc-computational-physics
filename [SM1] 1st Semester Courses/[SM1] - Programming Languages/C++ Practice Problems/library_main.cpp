/*
Problem Statement
Create two classes: Book and Library. The Book class should contain the title, 
author, year of publication, and the ISBN number of the book. The Library class 
should store a collection of Book objects and allow adding and displaying books.
*/

#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Book{
    private:
        string title;
        string author;
        int year;
        float isbn;

    public:
        void set_title(string title);
        string get_title();
        void set_author(string author);
        string get_author();
        void set_year(int year);
        int get_year();
        void set_isbn(float isbn);
        float get_isbn();

        void print_book_details() {
            cout << "Title: " << title << endl;
            cout << "Author: " << author << endl;
            cout << "Year: " << year << endl;
            cout << "ISBN: " << isbn << endl;
        }
};

// Set functions
void Book::set_title(string title){
    this->title = title;
};

void Book::set_author(string author){
    this->author = author;
};

void Book::set_year(int year){
    this->year = year;
};

void Book::set_isbn(float isbn){
    this->isbn = isbn;
};

// Get functions
string Book::get_title(){
    return title;
};

string Book::get_author(){
    return author;
};

int Book::get_year(){
    return year;
};

float Book::get_isbn(){
    return isbn;
};

class Library{
    private:
        vector<Book> books;

    public:
        // Function to add a book to the library
        void add_book(string title, string author, int year, float isbn) {
            Book new_book; // Create a new Book object
            new_book.set_title(title);
            new_book.set_author(author);
            new_book.set_year(year);
            new_book.set_isbn(isbn);
            books.push_back(new_book); // Add the book to the vector
        }

        void display_books() {
            if (books.empty()) {
                cout << "No books in the library." << endl;
                return;
            }
            for (size_t i = 0; i < books.size(); i++) {
                cout << "Book " << i + 1 << " details:" << endl;
                books[i].print_book_details();
                cout << "-------------------" << endl;
            }
        }
};

// Main function
int main() {
    Library myLibrary;

    // Add books to the library
    myLibrary.add_book("The Great Gatsby", "F. Scott Fitzgerald", 1925, 9780743273565);
    myLibrary.add_book("1984", "George Orwell", 1949, 9780451524935);

    // Display all books in the library
    myLibrary.display_books();

    return 0;
}