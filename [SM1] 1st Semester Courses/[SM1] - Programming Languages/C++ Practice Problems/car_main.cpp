/*
Create two classes: Car and Dealership. The Car class will represent a 
single car's model, year, and price. The Dealership class will manage 
multiple Car objects.
*/

#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Car{
    private:
        string model;
        int year;
        float price;

    public:
        // Setter functions
        void set_model(string model){this->model = model;}
        void set_year(int year){this->year = year;}
        void set_price(float price){this->price = price;}

        // Getter functions
        string get_model(){return model;}
        int get_year(){return year;}
        float get_price(){return price;}

        // Print all details
        void print_details(){
            cout <<"Model:" << model << endl;
            cout <<"Year:" << year << endl;
            cout <<"Price:" << price << endl;
        }
};

class Dealership
{
private:
    vector<Car> cars;
public:
    void add_car(string model, int year, float price){
        Car new_car;
        new_car.set_model(model);
        new_car.set_year(year);
        new_car.set_price(price);
        cars.push_back(new_car);
    }

    void show_all_cars(){
        if (cars.empty()){
            cout << "No cars registered!" << endl;
            return;
        }
        for (size_t i=0; i < cars.size(); i++){
            cout << "Car " << i <<":"<< endl;
            cars[i].print_details();
            cout << "----------------------------"<< endl;
        }
    }
};

int main(){
    Dealership Audi;

    Audi.add_car("Audi A4", 2019, 28000.00);
    Audi.add_car("Audi Q5", 2018, 32000.00);
    Audi.add_car("Audi TT", 2022, 41000.00);
    Audi.add_car("Audi RS6", 2021, 90000.00);
    Audi.add_car("Audi A8", 2020, 85000.00);


    Audi.show_all_cars();

    return 0;
}