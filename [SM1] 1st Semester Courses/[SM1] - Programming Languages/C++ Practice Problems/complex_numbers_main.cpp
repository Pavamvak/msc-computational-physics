/*
Create a class Complex to represent a complex number and perform arithmetic operations on them. 
The class should be able to handle addition, subtraction and multiplication of complex 
numbers. The complex number should be represented in the form 

𝑎+𝑏𝑖

where a is the real part and b is the imaginary part.
*/

#include <iostream>
#include <cmath>

using namespace std;

class Complex {
    private:
        float real;
        float imag;

    public:
        // Constructors
        Complex() : real(0), imag(0) {}
        Complex(float r, float i) : real(r), imag(i) {}

        // Setters and getters
        void set_real(float r) { real = r; }
        void set_imag(float i) { imag = i; }
        float get_real() { return real; }
        float get_imag() { return imag; }

        // Overloading + operator for complex number addition
        Complex operator+(const Complex& other) {
            return Complex(real + other.real, imag + other.imag);
        }

        // Overloading - operator for complex number subtraction
        Complex operator-(const Complex& other) {
            return Complex(real - other.real, imag - other.imag);
        }

        // Overloading * operator for complex number multiplication
        Complex operator*(const Complex& other) {
            return Complex(real * other.real - imag * other.imag, real * other.imag + imag * other.real);
        }

        // Display the complex number
        void display() {
            if (imag >= 0)
                cout << real << " + " << imag << "i" << endl;
            else
                cout << real << " - " << abs(imag) << "i" << endl;
        }
};

// Main function
int main() {
    Complex c1(4, 5);
    Complex c2(2, 3);

    Complex sum = c1 + c2;
    Complex diff = c1 - c2;
    Complex prod = c1 * c2;

    cout << "First Complex Number: "; c1.display();
    cout << "Second Complex Number: "; c2.display();
    
    cout << "\nAddition: "; sum.display();
    cout << "Subtraction: "; diff.display();
    cout << "Multiplication: "; prod.display();

    return 0;
}