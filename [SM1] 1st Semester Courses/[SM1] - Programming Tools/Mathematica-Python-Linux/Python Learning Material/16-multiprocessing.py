#!/usr/bin/env python2.7
import multiprocessing
import time

def myprocess(n):
	print("Started process [%d]"%n)
	time.sleep(2)
	print("Finished process [%d]"%n)

if __name__ == '__main__':
	p = multiprocessing.Pool(4)
	print("Starting calculation with single processor")
	t0 = time.time()
	for i in range(4):
		myprocess(i)
	tend = time.time()
	total = tend - t0
	print("total time %f"%total)
	print("Starting calculation with multiple processors")
	t0 = time.time()
	r = p.map(myprocess, range(4))
	tend = time.time()
	total = tend - t0
	print("total time %f"%total)
