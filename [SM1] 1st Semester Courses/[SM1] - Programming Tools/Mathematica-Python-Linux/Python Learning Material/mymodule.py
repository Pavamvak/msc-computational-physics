import numpy as np
def norm(vec):
    sum = 0
    for i in vec:
        sum+=i*i
    return np.sqrt(sum)