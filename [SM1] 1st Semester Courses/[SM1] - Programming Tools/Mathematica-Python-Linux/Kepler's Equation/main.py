import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp
from scipy.optimize import root_scalar, newton

def ode_system(t, y):
    """Defines the ODE system x''(t) = (3/2) * x(t)^2"""
    x, v = y  # y[0] = x (position), y[1] = v (velocity)
    dxdt = v  # First equation: dx/dt = v
    dvdt = (3/2) * x**2  # Second equation: dv/dt = (3/2) * x^2
    return [dxdt, dvdt]

def solve_ode(v0):
    """Solves the ODE with initial condition x(0) = 4, x'(0) = v0."""
    t_span = (0, 1)
    y0 = [4, v0]
    sol = solve_ivp(ode_system, t_span, y0, t_eval=np.linspace(0, 1, 100), method='RK45')
    return sol

# Task 1: Solve for different initial velocities
v0_values = np.arange(-40, -4, 5)
plt.figure(figsize=(8,6))
for v0 in v0_values:
    sol = solve_ode(v0)
    plt.plot(sol.t, sol.y[0], label=f'v0={v0}')
plt.xlabel('Time t')
plt.ylabel('x(t)')
plt.legend()
plt.title('Solutions of the ODE for different initial velocities')
plt.grid()
plt.show()

# Task 2: Shooting method to find v0 such that x(1) = 0
def shooting_function(v0):
    sol = solve_ode(v0)
    return sol.y[0, -1]  # x(1)

# Plot the function F(v0) to find the correct root bracket
v0_range = np.linspace(-50, 0, 100)
F_values = [shooting_function(v) for v in v0_range]

plt.figure(figsize=(8,6))
plt.plot(v0_range, F_values, label='F(v0) = x(1)')
plt.axhline(0, color='black', linestyle='--')
plt.xlabel('Initial Velocity v0')
plt.ylabel('F(v0) = x(1)')
plt.title('Shooting function F(v0)')
plt.legend()
plt.grid()
plt.show()

# Check function values at bracket edges
print(f"F(-40) = {shooting_function(-40)}")
print(f"F(-5) = {shooting_function(-5)}")

# Finding roots of F(v0) with corrected bracket
try:
    root_result = root_scalar(shooting_function, bracket=[-50, 0], method='brentq')
    if root_result.converged:
        best_v0 = root_result.root
    else:
        raise ValueError("Brent's method did not converge, trying Newton's method")
except ValueError:
    best_v0 = newton(shooting_function, -20)  # Use an estimate from the plot

print(f'Found root: v0 = {best_v0}')
sol_best = solve_ode(best_v0)

plt.figure(figsize=(8,6))
plt.plot(sol_best.t, sol_best.y[0], label=f'Best v0={best_v0:.5f}')
plt.xlabel('Time t')
plt.ylabel('x(t)')
plt.legend()
plt.title('Solution using the best initial velocity')
plt.grid()
plt.show()
