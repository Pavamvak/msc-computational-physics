// Libraries
#include <stdio.h>
#include <omp.h>

#define N 12

int main(void) {

    // Variable declaration
    int myid,
        nThreads,
        i,
        iStart,
        iEnd,
        A[N];

    for(i=0;i<N;i++) A[i] = i;

    #pragma omp parallel shared(nThreads, A) \
                        private(myid, i, iStart, iEnd) \
                        default(none)
    {
        myid = omp_get_thread_num();
        nThreads = omp_get_num_threads();

        // Calculate the start and end indices for the current thread's portion of the array
        iStart = myid*N/nThreads;
        iEnd = (myid+1)*N/nThreads;

        if(myid == nThreads-1) iEnd = N;

        printf("myid= %d iStart=%d iEnd=%d\n",myid, iStart,iEnd);
        
        // Each thread increments its assigned portion of the array by 1
        for(i=iStart;i<iEnd;i++) A[i] +=1;
    }

    for(i=0;i<N;i++) printf("A[%d] = %d\n",i,A[i]);

    return 0;

}