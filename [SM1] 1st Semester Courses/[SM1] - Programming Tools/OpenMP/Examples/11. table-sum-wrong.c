// Libraries
#include <stdio.h>
#include <omp.h>

#define N 10

int main(void) {

// Variable initialization
int i,
    sum,
    a[N];

// Initialize array with each element having its index as value
for(i=0;i<N;i++) a[i] = i;

sum = 0;

// Parallel processing
// CORRECTION: Changed `sum` variable from shader to reduction, so that each thread calculates,
// its part of the sum separately and then add (using the `+` symbol) each sum together.
#pragma omp parallel for shared(a) private(i) \
                        reduction(+:sum) \
                        default(none)
    for(i=0;i<N;i++) sum += a[i];

// Print results
printf("sum = %d\n",sum);

return 0;

}