// Libraries
#include <stdio.h>
#include <omp.h>

#define N 10

    int main(void) {

    // Variable declaration
    int i,
        sum,
        a[N];

    // Initialize array
    for(i=0;i<N;i++) a[i] = i;

    sum = 0;

    // Parallel processing
    #pragma omp parallel for shared(a) private(i) \
                        reduction(+:sum) \
                        default(none)
        for(i=0;i<N;i++) sum += a[i];

    printf("sum = %d\n",sum);

    return 0;

}