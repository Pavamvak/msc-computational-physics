// Libraries
#include <stdio.h>
#include <omp.h>

int main(void) {

    int nThreads;

    // Set how many threads to use (uncomment to use)
    //omp_set_num_threads(4);

    // Run in parallel
    #pragma omp parallel 
    {
    int myid = omp_get_thread_num(); 
    printf("Thread %d says: Hello World!\n",myid);

    // Add the condition so that it only runs by thread 0
    // The difference between `omp_get_num_threads() and `omp_get_num_procs();`
    // is that the first one gives the number of threads being used and the second
    // give the number of threads in total available.
    if(myid == 0) nThreads = omp_get_num_threads();
    }

    // Print the total number of threads
    printf("The total number of threads used was %d.\n",nThreads);

    return 0;
}
