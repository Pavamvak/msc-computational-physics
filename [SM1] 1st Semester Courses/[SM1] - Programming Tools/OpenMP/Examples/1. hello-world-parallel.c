// Libraries
#include <stdio.h>
#include <omp.h>

int main(void) {

    // Print the number of logical cores (threads) available
    int num_cores = omp_get_num_procs();
    printf("Number of logical cores: %d\n", num_cores);

    // Set how many threads to use
    omp_set_num_threads(4);

    // Print in parallel for each thread the message
    #pragma omp parallel
    {
    printf("Hello World!\n");
    }

    return 0;

}
