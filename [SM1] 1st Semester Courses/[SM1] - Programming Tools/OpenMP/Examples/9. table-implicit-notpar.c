// Libraries
#include <stdio.h>
#include <omp.h>

#define N 12

int main(void) {

    // Variable declaration
    int i,
        A[N];

    // Initialize array with each element having its index as value
    for(i=0;i<N;i++) A[i] = i;

    // Parallel processing
    #pragma omp parallel shared(A) private(i) default(none)
    {
        // CORRECTION: Change `i=1` to `i=0` to access all elements from the first
        // CORRECTION: Change `A[i] =A[i-1]` to `A[i] +=1` to add 1
        #pragma omp for
        for(i=0;i<N;i++) A[i] +=1;
    }

    // Print results
    for(i=0;i<N;i++) printf("A[%d] = %d\n",i,A[i]);

    return 0;

}