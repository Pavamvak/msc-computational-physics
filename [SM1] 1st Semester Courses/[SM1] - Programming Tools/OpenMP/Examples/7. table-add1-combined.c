// Libraries
#include <stdio.h>
#include <omp.h>

#define N 12

int main(void) {

    // Variable declaration
    int i,
        A[N];

    for(i=0;i<N;i++) A[i] = i;

    // Parallel for loop with OpenMP
    // The array A is shared between all threads, and the loop variable i is private to each thread
    #pragma omp parallel for shared(A) private(i) default(none)
    for(i=0;i<N;i++) A[i] +=1;

    for(i=0;i<N;i++) printf("A[%d] = %d\n",i,A[i]);

    return 0;

}