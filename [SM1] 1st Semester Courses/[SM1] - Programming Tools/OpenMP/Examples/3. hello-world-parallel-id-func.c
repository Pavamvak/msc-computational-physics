// Libraries
#include <stdio.h>
#include <omp.h>

// Function to print a message
void printmessage(int myid)
{
  printf("Thread %d says: Hello World!\n",myid);
}

int main(void) {

  int nThreads;

  // Run in parallel, same process as the `hello-world-parallel-id.c`
  // but with functions.
  #pragma omp parallel
  {
  int myid = omp_get_thread_num();

  printmessage(myid);

  if(myid==0) nThreads = omp_get_num_threads();
  }

  printf("The total number of threads used was %d.\n",nThreads);

  return 0;
}
