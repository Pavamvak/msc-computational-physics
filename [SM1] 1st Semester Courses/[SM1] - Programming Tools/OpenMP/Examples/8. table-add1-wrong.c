// Libraries
#include <stdio.h>
#include <omp.h>

#define N 12

int main(void) {

    // Variable declaration
    int i,
        A[N];

    // Initialize array with each element having its index as value
    for(i=0;i<N;i++) A[i] = i;

    // Parallel processing
    #pragma omp parallel shared(A) private(i) default(none)
    {
        // CORRECTION: Added `#pragma omp for` so that OpenMP schedules automatically the loop
        #pragma omp for
        for(i=0;i<N;i++) A[i] +=1;
    }

    // Print result
    for(i=0;i<N;i++) printf("A[%d] = %d\n",i,A[i]);

    return 0;

}