// Libraries
#include <stdio.h>
#include <omp.h>

#define N 12

int main(void) {

    // Variable declaration
    int i,
        A[N];

    for(i=0;i<N;i++) A[i] = i;

    // Start of the parallel region with OpenMP
    // The array A is shared between all threads, and the loop variable i is private to each thread
    #pragma omp parallel shared(A) private(i) default(none)
    {
        // Parallel for loop to increment each element of the array
        #pragma omp for
        for(i=0;i<N;i++) A[i] +=1;

    }

    for(i=0;i<N;i++) printf("A[%d] = %d\n",i,A[i]);

    return 0;

}