#ifndef SAVE_H
#define SAVE_H

void save_to_csv(long long* points, double** errors, double** exec_times, int num_simulations, int num_threads);

#endif