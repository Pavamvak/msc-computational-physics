// Standard Libraries
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// OpenMP Libraries
#include <omp.h>

// Custom Libraries
#include "montecarlo.h"

/**
 * @brief Simulates the Monte Carlo method to estimate the value of Pi.
 * 
 * This function performs a Monte Carlo simulation by randomly generating points
 * within a square, and calculating how many fall within a quarter circle of radius 1.
 * The ratio of points inside the circle to the total points is used to estimate Pi.
 * 
 * The function also supports parallel execution using OpenMP, and the number of threads 
 * can be specified as a parameter.
 * 
 * @param num_points The number of points to generate in the simulation.
 * @param pi_estimation Pointer to a variable where the Pi estimation will be stored.
 * @param exec_time Pointer to a variable where the execution time will be stored.
 * @param num_threads The number of threads to use in the simulation. If 0 or negative, all available threads will be used.
 */void Monte_Carlo_Pi_Simulation(long long num_points, double* pi_estimation, double* exec_time, int num_threads) {
    long long points_in_circle = 0;

    // Set the number of threads if specified
    if (num_threads > 0) {
        omp_set_num_threads(num_threads);
    }

    double start_time = omp_get_wtime();

    #pragma omp parallel
    {
        unsigned int seed = (unsigned int)time(NULL) ^ omp_get_thread_num();
        long long local_points_in_circle = 0;

        #pragma omp for
        for (long long i = 0; i < num_points; i++) {
            double x = (double)rand_r(&seed) / RAND_MAX;
            double y = (double)rand_r(&seed) / RAND_MAX;
            if (x * x + y * y <= 1.0) {
                local_points_in_circle++;
            }
        }

        #pragma omp atomic
        points_in_circle += local_points_in_circle;
    }

    // Estimate Pi
    *pi_estimation = 4.0 * (double)points_in_circle / (double)num_points;

    // Stop timing
    double end_time = omp_get_wtime();
    *exec_time = end_time - start_time;
}