// Standard Libraries
#include <stdio.h>
#include <sys/stat.h>

/**
 * @brief Saves the Pi estimation errors and execution times to CSV files.
 * 
 * This function saves the errors and execution times for the Monte Carlo simulations 
 * to CSV files in a `data` folder. It creates the folder if it doesn't exist, and
 * the output CSV files are named `errors.csv` and `execution_times.csv`. Each file 
 * contains the results for different thread counts and number of points.
 * 
 * @param points Array of points used for the simulations (e.g., 10^1, 10^2, ... 10^10).
 * @param errors 2D array of errors for each simulation, where rows correspond to different 
 *               point counts and columns correspond to different thread counts.
 * @param exec_times 2D array of execution times for each simulation, structured similarly 
 *                   to the errors array.
 * @param num_simulations The number of different simulations (number of rows in the arrays).
 * @param num_threads The number of different thread counts used (number of columns in the arrays).
 */
void save_to_csv(long long* points, double** errors, double** exec_times, int num_simulations, int num_threads) {

    struct stat st = {0};
    if (stat("data", &st) == -1) {
        mkdir("data", 0700);  // Create the data directory with read/write/execute permissions
    }

    /**
     * Save errors to a CSV file named "data/errors.csv".
     * Each row corresponds to a different number of points, and each column represents 
     * a different thread count.
     */
    FILE *errorFile = fopen("data/errors.csv", "w");
    if (errorFile == NULL) {
        printf("Error opening errors.csv for writing.\n");
        return;
    }
    
    // Write header (thread numbers)
    fprintf(errorFile, "Points");
    for (int t = 1; t <= num_threads; t++) {
        fprintf(errorFile, ",Threads_%d", t);
    }
    fprintf(errorFile, "\n");

    // Write the data (errors)
    for (int i = 0; i < num_simulations; i++) {
        fprintf(errorFile, "%lld", points[i]);
        for (int t = 0; t < num_threads; t++) {
            fprintf(errorFile, ",%lf", errors[i][t]);
        }
        fprintf(errorFile, "\n");
    }

    fclose(errorFile);

    /**
     * Save execution times to a CSV file named "data/execution_times.csv".
     * Similar structure to the errors file, where each row represents the number of points
     * and each column represents the execution time for a specific thread count.
     */
    FILE *execTimeFile = fopen("data/execution_times.csv", "w");
    if (execTimeFile == NULL) {
        printf("Error opening execution_times.csv for writing.\n");
        return;
    }

    // Write header (thread numbers)
    fprintf(execTimeFile, "Points");
    for (int t = 1; t <= num_threads; t++) {
        fprintf(execTimeFile, ",Threads_%d", t);
    }
    fprintf(execTimeFile, "\n");

    // Write the data (execution times)
    for (int i = 0; i < num_simulations; i++) {
        fprintf(execTimeFile, "%lld", points[i]);
        for (int t = 0; t < num_threads; t++) {
            fprintf(execTimeFile, ",%lf", exec_times[i][t]);
        }
        fprintf(execTimeFile, "\n");
    }

    fclose(execTimeFile);
}