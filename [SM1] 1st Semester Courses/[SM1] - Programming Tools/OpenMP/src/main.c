// Standard Libraries
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// OpenMP Libraries
#include <omp.h> 

// Custom Libraries
#include "montecarlo.h"
#include "save.h"

// Constants
#define NUM_SIMULATIONS 10          // Number of different simulations
#define PI 3.14159265358979323846   // Value of Pi for error calculation

/**
 * @brief Main function to perform Monte Carlo simulations to estimate Pi.
 * 
 * This function runs Monte Carlo simulations for different numbers of points,
 * using an increasing number of threads from 1 to the maximum available threads.
 * The results (Pi estimations, errors, execution times) are saved to CSV files,
 * and dynamically allocated memory is properly freed.
 * 
 * The number of points is defined from the constant `NUM_SIMULATIONS` above, to 
 * run for 10^1 up to 10^10 points. Increase/ decrease this constant is more/less
 * points are required.
 * 
 * @return int Returns 0 (if the program finishes successfully).
 */
int main() {
    // Initiate variable to dynamically allocate memory
    long long points[NUM_SIMULATIONS];
    int max_threads = omp_get_max_threads();

    // Dynamically allocate 2D arrays for errors and execution times
    // Rows: Different point simulations; Columns: Different thread counts
    double** errors = (double**)malloc(NUM_SIMULATIONS * sizeof(double*));
    double** exec_times = (double**)malloc(NUM_SIMULATIONS * sizeof(double*));

    for (int i = 0; i < NUM_SIMULATIONS; i++) {
        errors[i] = (double*)malloc(max_threads * sizeof(double));
        exec_times[i] = (double*)malloc(max_threads * sizeof(double));
    }

    double pi_estimations[NUM_SIMULATIONS];

    // Generate simulation points
    for (int i = 0; i < NUM_SIMULATIONS; i++) {
        points[i] = pow(10, i + 1);
    }

    /**
     * Loop over different thread counts, from 1 to the maximum available threads.
     * For each number of threads, run the Monte Carlo simulation for each point count.
     */
    for (int num_threads = 1; num_threads <= max_threads; num_threads++) {
        printf("Running simulations with %d thread(s):\n", num_threads);

        // Run Monte Carlo simulations for each number of points
        for (int i = 0; i < NUM_SIMULATIONS; i++) {
            Monte_Carlo_Pi_Simulation(points[i], &pi_estimations[i], &exec_times[i][num_threads-1], num_threads);
            errors[i][num_threads-1] = fabs(PI - pi_estimations[i]);

            // Print the result for the current simulation (mostly for overview)
            printf("Threads: %d, Points: %lld, Pi estimation: %.10f, Error: %.10f, Execution Time: %.5f seconds\n",
                   num_threads, points[i], pi_estimations[i], errors[i][num_threads-1], exec_times[i][num_threads-1]);
        }
    }

    /**
     * Save the results (errors and execution times) to CSV files using the save_to_csv function.
     * The results are stored in the 'data' folder for further analysis.
     */
    save_to_csv(points, (double**)errors, (double**)exec_times, NUM_SIMULATIONS, max_threads);

    /**
     * Free the dynamically allocated memory for the 2D arrays (errors and exec_times).
     * Ensure that memory for each row is freed first, followed by freeing the top-level array.
     */
    for (int i = 0; i < NUM_SIMULATIONS; i++) {
        free(errors[i]);
        free(exec_times[i]);
    }
    free(errors);
    free(exec_times);

    return 0;
}