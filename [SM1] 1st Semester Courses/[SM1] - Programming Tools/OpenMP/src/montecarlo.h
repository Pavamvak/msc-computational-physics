#ifndef MONTECARLO_H
#define MONTECARLO_H

void Monte_Carlo_Pi_Simulation(long long num_points, double* pi_estimation, double* exec_time, int num_threads);

#endif