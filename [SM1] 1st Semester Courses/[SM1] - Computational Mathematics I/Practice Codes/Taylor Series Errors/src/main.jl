# Main function to run the Taylor series approximation
function main(x, n_figures)

    open("taylor_series_output.txt", "w") do file
        println(file, "Taylor Series Approximation                                                                                                  Vamvakas Panagiotis")
        println(file)
        println(file, "Approximating e^$x using Taylor series.")
        println(file, "Accuracy needed is $n_figures significant figures:")
        println(file, "-"^145)
        println(file)
        println(file, "$(rpad("Iterations", 14)) |$(rpad("Estimation", 29)) |$(rpad("True Value", 29)) |$(rpad("True Percent Relative Error", 29)) |$(rpad("Approximate Percent Relative Error", 39))|")
        println(file, "-"^145)

        previous_estimation = 0.0

        approx_error = 100
        sb_error = scarborough_error(n_figures)

        iteration = 1

        while approx_error >= sb_error
            current_estimation = taylor_series_ex(x, iteration)
            true_value = true_value_ex(x)

            true_error = true_percent_relative_error(true_value, current_estimation)
            approx_error = approximation_error_percentage(current_estimation, previous_estimation)

            println(file, "$(rpad(iteration, 14)) |$(rpad(current_estimation, 29)) |$(rpad(true_value, 29)) |$(rpad(string(true_error, '%'), 29)) |$(rpad(string(approx_error, '%'), 39))|")

            previous_estimation = current_estimation
            iteration = iteration + 1
        end
    end
end
