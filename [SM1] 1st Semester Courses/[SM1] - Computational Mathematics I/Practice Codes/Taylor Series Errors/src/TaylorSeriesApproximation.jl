module TaylorSeriesApproximation

    include("TaylorSeries.jl")
    include("ErrorCalculations.jl")
    include("main.jl")   

    export main
end
