# Function to calculate the nth term of e^x Taylor series
function taylor_series_term(x, n)
    term = 1.0
    for i in 1:n
        term *= x / i
    end
    return term
end

# Function to calculate e^x using Taylor series up to n terms
function taylor_series_ex(x, n)
    sum = 1.0  # e^x = 1 + x + x^2/2! + ...
    for i in 1:n-1
        sum += taylor_series_term(x, i)
    end
    return sum
end
