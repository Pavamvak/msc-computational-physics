# Function to calculate true percent relative error
function true_percent_relative_error(true_value, approx_value)
    return abs((true_value - approx_value) / true_value) * 100
end

# Function to calculate the true value of e^x
function true_value_ex(x)
    return exp(x)
end

# Function to calculate the approximation error percentage
function approximation_error_percentage(current_estimation, previous_estimation)
    # Handle the case when the first term is being computed
    if previous_estimation == 0.0
        return 100.0  # Initial error can be assumed to be 100%
    else
        return abs((current_estimation - previous_estimation) / current_estimation) * 100
    end
end

# Function to calculate the Scarborough error estimate
function scarborough_error(significant_figures)
    return 0.5 * 10.0^(2 - significant_figures)
end