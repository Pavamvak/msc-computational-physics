Numerical Integration Log
This file logs the results of numerically computing the double integral of a function
using increasing numbers of subdivisions. The function used is 'x^2 + y^3',
integrated over x in the range (0, 1) and y in the range scaled by x from (1, 2).

Column Descriptions:
Subdivisions: Number of subdivisions used for numerical integration
Result: Numerical result of the integral
Absolute Percentage Error: Percentage error relative to the exact value (1)
Computation Time (s): Time taken to compute the integral

--------------------------------------------------------------------------------

Subdivisions        Result              Absolute Percentage Error     Computation Time (s)
1000                1.0000016500001259  0.0001650000125863471         0.032898902893066406
2000                1.000000412500008   4.1250000792913966e-5         0.004745960235595703
3000                1.000000183333334   1.8333333406950203e-5         0.010659933090209961
4000                1.0000001031249997  1.0312499965081656e-5         0.018910884857177734
5000                1.000000066000001   6.600000101997239e-6          0.04515504837036133
6000                1.000000045833332   4.583333201857442e-6          0.04294109344482422
7000                1.0000000336734691  3.3673469124195776e-6         0.05847287178039551
8000                1.0000000257812531  2.578125313235091e-6          0.07839798927307129
9000                1.0000000203703692  2.037036916924251e-6          0.0985250473022461
10000               1.0000000165        1.6500000032948492e-6         0.12047982215881348
