"""
    trapezoidal_rule(f, a, b, n)

Compute the integral of a function using the trapezoidal rule.

# Arguments
- `f`: A function to be integrated. It must be a single-variable function.
- `a`: The lower bound of the integration interval.
- `b`: The upper bound of the integration interval.
- `n`: The number of sub-intervals to divide the [a, b] interval into. 
       A higher value increases the approximation accuracy.

# Returns
- The approximate integral of `f` over the interval [a, b] using the trapezoidal rule.
"""
function trapezoidal_rule(f, a, b, n)
    h = (b - a) / n
    sum = 0.5 * (f(a) + f(b))
    for i = 1:n-1
        sum += f(a + i * h)
    end
    return sum * h
end

"""
    compute_numerical_integral(equation_str, x_range, y_bounds, n)

Compute a numerical integral of a two-variable function specified as a string.

# Arguments
- `equation_str`: A string representing the two-variable function to integrate.
- `x_range`: A tuple (x_min, x_max) representing the range of integration for the outer integral.
- `y_bounds`: A tuple (y_min_factor, y_max_factor) representing the factors to multiply with `x` 
               to get the range of integration for the inner integral.
- `n`: The number of sub-intervals for numerical integration. Higher values increase accuracy.

# Returns
- The approximate value of the double integral computed numerically.
"""
function compute_numerical_integral(equation_str, x_range, y_bounds, n)
    @info "Analytical integration started for: $equation_str"
    @info "Inner integral in the range of: $y_bounds"
    @info "Outer integral in the range of: $x_range"

    @syms x y
    # Convert the string equation to a symbolic expression and then to a function
    equation_sym = sympify(equation_str)

    # Convert the symbolic expression to a callable function using lambdify
    f = lambdify(equation_sym, [x, y])  # Pass variables as an array

    # Starting the timer
    start_time = time()

    inner_integral(x_val) = trapezoidal_rule(y -> f(x_val, y), y_bounds[1] * x_val, y_bounds[2] * x_val, n)

    outer_integral = trapezoidal_rule(inner_integral, x_range[1], x_range[2], n)

    # Stopping the timer
    end_time = time()
    elapsed_time = end_time - start_time
 
    # Logging information
    @info "Analytical integration performed"
    @info "Computation time: $elapsed_time seconds"
    return outer_integral
end
