# Main function to run the program
function main()

    # Corrected custom format function
    function custom_format(io, log_record)
        level = log_record[:level]
        message = log_record[:message]
        write(io, string(level, ": ", message, "\n"))
    end

    # Open a file for logging
    log_file = open("double_integral_calculation.log", "w")

    # Create a logger with the custom format
    custom_logger = FormatLogger(custom_format, log_file)

    # Set the global logger to custom logger
    global_logger(custom_logger)

    @info "------------------------------------------------"
    @info "Welcome to the Integral Calculator"

    # Get the equation and variables
    equation_str, variables = get_equation()
    @info "Equation entered: $equation_str with variables $variables"

    # Get the ranges
    println("Enter the outer range (e.g., 1, 2):")
    range1 = get_ranges()

    println("Enter the inner range (e.g., 1, 2):")
    range2 = get_ranges()
    @info "Ranges entered: $range1 and $range2"

    # Ask the user to choose a method
    println("Choose a method for solving the integral:")
    println("1: Analytical")
    println("2: Numerical")
    println("3: Monte Carlo")
    method_choice = readline()

    # Solve the integral using the chosen method
    result = nothing
    if method_choice == "1"
        @info "Analytical method chosen"
        result = compute_analytical_integral(equation_str, range1, range2)

    elseif method_choice == "2"
        @info "Numerical method chosen"

        # Ask the user to enter the number of subdivisions
        n = get_num_subdivisions()

        result = compute_numerical_integral(equation_str, range1, range2, n)

    elseif method_choice == "3"
        @info "Monte Carlo method chosen"
        # Ask the user to enter the number of subdivisions
        n = get_num_subdivisions()

        result = monte_carlo_integration(equation_str, range1, range2, n)
    else
        @error "Invalid method choice. Please restart and choose a valid option."
        return
    end

    # Print the result
    @info "Result of the integral: $result"
end
