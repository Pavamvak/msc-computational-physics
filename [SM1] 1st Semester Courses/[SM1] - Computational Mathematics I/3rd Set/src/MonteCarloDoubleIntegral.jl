"""
    monte_carlo_integration(equation::String, x_range::Tuple, y_range::Tuple, samples::Int)

Compute a numerical integral of a two-variable function using the Monte Carlo method.

# Arguments
- `equation`: A string representation of the function to be integrated. 
               The function should be in terms of `x` and `y`.
- `x_range`: A tuple `(x_min, x_max)` representing the range of `x` values for sampling.
- `y_range`: A tuple `(y_min, y_max)` representing the range of `y` values for sampling.
             The actual `y_min` used in the function will be set to 0.0, as required by the Monte Carlo method.
- `samples`: The number of random samples to use for the estimation. 
              A higher number of samples typically leads to a more accurate estimate.

# Returns
- An estimate of the integral of the given function over the specified range using the Monte Carlo method.
"""
function monte_carlo_integration(equation::String, x_range::Tuple, y_range::Tuple, samples::Int)
    # Parse the equation string
    expr = Meta.parse(equation)
    f = eval(:( (x, y) -> $expr ))

    # Extract x and y range
    (x_min, x_max) = x_range
    (y_min, y_max) = y_range

    # Monte Carlo requires y min to be equal to 0
    y_min = 0.0

    # Initialize the sum of the function values
    sum = 0.0

    # Generate random points and compute the function value
    for _ in 1:samples
        x = x_min + (x_max - x_min) * rand()
        y = y_min + (y_max - y_min) * rand()

        # Check if the point is within the integration bounds
        if x <= y && y <= 2x
            sum += Base.invokelatest(f, x, y)
        end
    end

    # Calculate the area of the sampling rectangle
    area = (x_max - x_min) * (y_max - y_min)

    # Return the Monte Carlo estimate of the integral
    return (area * sum) / samples
end