"""
    get_equation()

Prompt the user to enter an algebraic equation as a string. 
The function will continue to prompt until an equation with exactly two variables is entered.

# Returns
- `equation_str`: The entered equation as a string.
- `variables`: A list of the two variables used in the equation.
"""
function get_equation()
    while true
        println("Enter the equation with two variables (e.g., x^2 + y^3):")
        equation_str = readline()

        # Parse the equation and extract variables
        equation = sympify(equation_str)
        variables = free_symbols(equation)

        if length(variables) == 2
            return equation_str, variables  # Return equation string and the variables
        else
            println("Error: Please enter an equation with exactly two variables.")
        end
    end
end

"""
    get_ranges()

Prompt the user to enter a range for the integral calculation. 
Each range should be entered in the format 'a, b', where a and b are numbers.

# Returns
- `range1`: The entered range as tuples (a, b).
"""
function get_ranges()
    while true
        try
            # Asking for the first range
            range1_str = readline()
            range1 = split(range1_str, ",")
            range1 = (parse(Int, strip(range1[1])), parse(Int, strip(range1[2])))

            # Check if the ranges are valid (i.e., two elements each)
            if length(range1) == 2
                return range1
            else
                println("Error: The range must have exactly two numbers. Please re-enter.")
            end
        catch
            println("Error: Invalid input format. Please enter the range in 'a, b' format.")
        end
    end
end

"""
    get_num_subdivisions()

Prompt the user to enter the number of subdivisions (n) for numerical integration.

This function repeatedly prompts the user until a valid positive integer is entered.
The entered value is used to determine the number of subdivisions for the numerical
integration process, impacting the accuracy and computational cost of the integration.

# Returns
- `n`: The number of subdivisions as a positive integer.

# Example Usage
```julia
n = get_num_subdivisions()
"""
function get_num_subdivisions()
    while true
        try
            println("Enter the number of subdivisions (n) for numerical integration:")
            n_str = readline()
            n = parse(Int, n_str)

            if n > 0
                return n
            else
                println("Error: Please enter a positive integer.")
            end
        catch
            println("Error: Invalid input. Please enter a valid integer.")
        end
    end
end
