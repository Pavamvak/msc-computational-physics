"""
    compute_analytical_integral(equation_str::String, x_range::Tuple, y_range::Tuple)

Compute the double integral of a given function analytically. The function should be expressed
as a string in terms of 'x' and 'y'. The integral is computed first with respect to 'y' over a range
that scales with 'x', and then with respect to 'x' over a specified range.

# Arguments
- `equation_str::String`: A string representing the function to be integrated. The function should be in terms of 'x' and 'y'.
- `x_range::Tuple`: A tuple representing the range for the outer integral (x). It should be in the form of (lower_bound, upper_bound).
- `y_range::Tuple`: A tuple representing the scaling factors for the inner integral (y). The actual integration limits for 'y' are these 
factors times 'x'.

# Returns
- The result of the double integral as a symbolic expression.

# Example
```julia
result = compute_analytical_integral("x^2 + y^3", (0, 1), (1, 2))
"""
function compute_analytical_integral(equation_str, x_range, y_range)
    @info "Analytical integration started for: $equation_str"
    @info "Outer integral in the range of: $x_range"

    # Starting the timer
    start_time = time()

    @syms x y
    # Convert the string equation to a symbolic expression
    equation_sym = sympify(equation_str)

    # Adjust the range for y by multiplying with x
    y_range_scaled = (y, y_range[1] * x, y_range[2] * x)
    @info "Inner integral in the range of: $y_range_scaled"

    # Define the range for x
    x_range_sym = (x, x_range[1], x_range[2])

    # Perform the inner integral with scaled range for y
    inner_integral = integrate(equation_sym, y_range_scaled)

    # Perform the outer integral
    outer_integral = integrate(inner_integral, x_range_sym)

    # Stopping the timer
    end_time = time()
    elapsed_time = end_time - start_time

    # Logging information
    @info "Analytical integration performed"
    @info "Computation time: $elapsed_time seconds"

    return outer_integral
end