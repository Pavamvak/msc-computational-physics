using Random
using Dates
using Plots

function monteCarloDoubleIntegral(equation::String, x_range::Tuple, y_range::Tuple, samples::Int)
    expr = Meta.parse(equation)
    f = eval(:( (x, y) -> $expr ))

    (x_min, x_max) = x_range
    (y_min, y_max) = y_range

    sum = 0.0

    for _ in 1:samples
        x = x_min + (x_max - x_min) * rand()
        y = y_min + (y_max - y_min) * rand()

        if x <= y && y <= 2x
            sum += Base.invokelatest(f, x, y)
        end
    end

    area = (x_max - x_min) * (y_max - y_min)
    return (area * sum) / samples
end

# Define the parameters
equation = "x^2 + y^3"
x_range = (0, 1)
y_range = (0, 2)
sample_sizes = [10^i for i in 1:9]  # Sample sizes from 10 to 1,000,000,000

# Arrays to store results for plotting
errors = []
times = []

# Open a file to save the results
open("monte_carlo_results.txt", "w") do file
    println(file, "Monte Carlo Integration Results")
    println(file, "Equation: ", equation)
    println(file, "x range: ", x_range)
    println(file, "y range: ", y_range)
    println(file, "-"^80)
    println(file, rpad("Sample Size", 25) * rpad("Result", 25) * rpad("Absolute Error (%)", 25) * "Computation Time (s)")

    for samples in sample_sizes
        start_time = now()
        result = monteCarloDoubleIntegral(equation, x_range, y_range, samples)
        elapsed_time = now() - start_time

        # Convert elapsed_time to seconds
        elapsed_seconds = Dates.value(elapsed_time) / 1000  # Convert milliseconds to seconds

        # Calculate absolute percentage error
        error = abs((result - 1) / 1) * 100
        push!(errors, error)
        push!(times, elapsed_seconds)

        # Convert numerical values to strings for concatenation
        println(file, rpad(samples, 25) * rpad(string(result), 25) * rpad(string(error), 25) * string(elapsed_seconds))
    end
end

println("Monte Carlo integration results saved to monte_carlo_results.txt")

# Plotting
p1 = plot(sample_sizes, errors, title = "Absolute Percentage Error", xlabel = "Sample Size", ylabel = "Error (%)", xscale = :log10, legend = false)
p2 = plot(sample_sizes, times, title = "Computation Time", xlabel = "Sample Size", ylabel = "Time (s)", xscale = :log10, legend = false)

plot(p1, p2, layout = (2, 1), size = (600, 800))
savefig("monte_carlo_analysis.png")

println("Plots saved to monte_carlo_analysis.png")