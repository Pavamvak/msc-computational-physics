using SymPy
using Plots

"""
`trapezoidal_rule(f, a, b, n)`

Compute the integral of a function `f` using the trapezoidal rule.

# Arguments
- `f`: Function to be integrated
- `a`: Lower bound of integration
- `b`: Upper bound of integration
- `n`: Number of subdivisions or trapezoids

# Returns
- Approximate integral of `f` from `a` to `b`
"""
function trapezoidal_rule(f, a, b, n)
    h = (b - a) / n
    sum = 0.5 * (f(a) + f(b))
    for i = 1:n-1
        sum += f(a + i * h)
    end
    return sum * h
end

"""
`compute_numerical_integral(f, x_range, y_bounds, n)`

Compute the double integral of a two-variable function `f` over a rectangular area.

# Arguments
- `f`: Two-variable function to be integrated
- `x_range`: Tuple representing the range of integration for the outer integral
- `y_bounds`: Tuple representing the bounds of integration for the inner integral, scaled by x
- `n`: Number of subdivisions for both the inner and outer integrals

# Returns
- Approximate double integral of `f`
"""
function compute_numerical_integral(f, x_range, y_bounds, n)
    inner_integral(x_val) = trapezoidal_rule(y -> f(x_val, y), y_bounds[1] * x_val, y_bounds[2] * x_val, n)
    return trapezoidal_rule(inner_integral, x_range[1], x_range[2], n)
end

# Define the function to integrate
f(x, y) = x^2 + y^3

# Set up the ranges and subdivisions
x_range = (0, 1)
y_bounds = (1, 2)
subdivisions = collect(range(1000, 10000, step=1000))

# Open a file to save the results
open("integration_results_10000.txt", "w") do file
    # Write a descriptive header to the file
    println(file, "Numerical Integration Log")
    println(file, "This file logs the results of numerically computing the double integral of the function 'x^2 + y^3',")
    println(file, "integrated over x in the range (0, 1) and y in the range scaled by x from (1, 2).\n")

    # Column descriptions for the output file
    println(file, "Column Descriptions:")
    println(file, "Subdivisions: Number of subdivisions used for numerical integration")
    println(file, "Result: Numerical result of the integral")
    println(file, "Absolute Percentage Error: Percentage error relative to the exact value (1)")
    println(file, "Computation Time (s): Time taken to compute the integral\n")
    println(file, "-"^80, "\n")
    println(file, rpad("Subdivisions", 20) * rpad("Result", 20) * rpad("Absolute Percentage Error", 30) * "Computation Time (s)")

    # Initialize arrays to store data for plotting
    errors = []
    subs = []
    times = []

    # Iterate over the number of subdivisions to compute the integral
    for n in subdivisions
        # Start the timer
        start_time = time()

        # Compute the integral with the current number of subdivisions
        result = compute_numerical_integral(f, x_range, y_bounds, n)
        
        # Stop the timer and calculate the elapsed time
        elapsed_time = time() - start_time

        # Calculate the absolute percentage error
        abs_percent_error = abs((result - 1) / 1) * 100

        # Write the current results to the file
        println(file, rpad(n, 20) * rpad(string(result), 20) * rpad(string(abs_percent_error), 30) * string(elapsed_time))

        # Store data for plotting
        push!(errors, abs_percent_error)
        push!(subs, n)
        push!(times, elapsed_time)
    end

    # Plot the absolute percentage errors and computation times
    p1 = plot(subs, errors, title = "Error and Computation Time by Number of Subdivisions", xlabel = "Number of Subdivisions", ylabel = "Absolute Percentage Error (%)", label = "Error", legend = :topright, color = :blue)
    p2 = twinx()
    plot!(p2, subs, times, ylabel = "Computation Time (s)", label = "Time", legend = :topright, color = :red)
    
    # Save the plot to a file
    savefig("error_and_time_plot_10000.png")
end
