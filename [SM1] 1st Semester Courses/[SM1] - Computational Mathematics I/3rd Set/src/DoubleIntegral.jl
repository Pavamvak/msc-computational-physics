module DoubleIntegral

    using Plots
    using LinearAlgebra
    using Logging
    using LoggingExtras
    using Profile
    using Random
    using SymPy
    using Distributions

    include("AnalyticalDoubleIntegral.jl")
    include("DataGetter.jl")
    include("DoubleIntegralCalculation.jl")
    include("MonteCarloDoubleIntegral.jl")
    include("NumericalDoubleIntegral.jl")

    export main

end