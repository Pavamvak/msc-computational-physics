function gaussian_elimination(A::Matrix{Float64})
    """
    Perform Gaussian Elimination on a matrix augmented with the identity matrix.

    Args:
    A (Matrix{Float64}): A square matrix to be inverted.

    Returns:
    Matrix{Float64}: The inverted matrix.

    Notes:
    This function does not handle singular matrices or implement pivoting.
    """
    n = size(A, 1)
    # Augmenting A with the identity matrix
    A_aug = [A I]

    # Forward Elimination
    for i = 1:n
        # Make the diagonal element 1, and above and below zeros
        for j = i+1:n
            factor = A_aug[j, i] / A_aug[i, i]
            A_aug[j, :] -= factor * A_aug[i, :]
        end
    end

    # Back Substitution
    for i = n:-1:1
        A_aug[i, :] /= A_aug[i, i]
        for j = i-1:-1:1
            factor = A_aug[j, i]
            A_aug[j, :] -= factor * A_aug[i, :]
        end
    end

    return A_aug[:, n+1:end]
end

function gauss_invert_matrix(A::Matrix{Float64})
    """
    Inverts a square matrix using Gaussian Elimination.

    Args:
    A (Matrix{Float64}): A square matrix to be inverted.

    Returns:
    Matrix{Float64}: The inverted matrix if A is non-singular.

    Throws:
    Error if the matrix A is not square.

    Notes:
    This function checks if the matrix is square and then performs Gaussian Elimination.
    It does not handle singular matrices or implement pivoting for numerical stability.
    """
    if size(A, 1) != size(A, 2)
        error("Matrix must be square")
    end
    return gaussian_elimination(A)
end
