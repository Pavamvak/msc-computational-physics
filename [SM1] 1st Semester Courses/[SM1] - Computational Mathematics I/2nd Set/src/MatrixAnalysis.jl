module MatrixAnalysis

    using Plots
    using LinearAlgebra
    using Logging
    using LoggingExtras
    using Profile
    using Random

    include("MatrixInput.jl")
    include("ValidationFunctions.jl")
    include("LUMatrixGeneration.jl")
    include("LUDecomposition.jl")
    include("LUMatrixInversion.jl")
    include("GaussElimination.jl")
    include("GaussMatrixInversion.jl")
    include("BuiltMatrixInversion.jl")
    include("AutoMatrixInversionRunner.jl")
    include("MaxEigen.jl")

    export input_matrix, calculate_L, calculate_U, LU_Matrix_Inversion, Gauss_Matrix_Inversion, built_Matrix_Inversion, Auto_Runner_Inversion, run_power_method
end