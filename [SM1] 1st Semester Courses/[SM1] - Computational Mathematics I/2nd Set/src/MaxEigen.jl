function power_method(A::Matrix{Float64}, max_iter::Int = 1000, tolerance::Float64 = 1e-10)
    n = size(A, 1)
    x = randn(n)  # Initialize x with random values
    x /= norm(x)  # Normalize x

    x_next = x
    lambda_old = 0.0
    lambda = Inf
    for k = 1:max_iter
        start_time = time_ns()

        # Calculate x^(n) and x^(n+1)
        x_n = A^k * x
        x_next = A^(k+1) * x
        

        # Find the maximum component in absolute value from x_n and x_next
        max_index_n = argmax(abs.(x_n))
        max_index_next = argmax(abs.(x_next))

        largest_xn = x_n[max_index_n]
        largest_xnext = x_next[max_index_next]

        # Calculate lambda using the largest components
        lambda = largest_xnext / largest_xn

        # Logging and checking for convergence
        elapsed_time = (time_ns() - start_time) / 1e9  # Convert nanoseconds to seconds
        @info "Time taken for matrix inversion: $elapsed_time seconds"

        if abs(lambda - lambda_old) < tolerance * abs(lambda)
            x_next /= norm(x_next)  # Normalize x_next
            break
        end

        lambda_old = lambda
    end

     return lambda, x_next
end

function validate_with_builtin(A::Matrix{Float64}, calculated_eigenvalue, calculated_eigenvector, tolerance::Float64)
    """
    Compares the calculated dominant eigenvalue and eigenvector with the results obtained from Julia's built-in eigen function.

    # Arguments
    - `A`: The matrix used for the power method.
    - `calculated_eigenvalue`: The dominant eigenvalue obtained from the power method.
    - `calculated_eigenvector`: The corresponding eigenvector obtained from the power method.
    - `tolerance`: The tolerance for considering differences as acceptable.

    # Returns
    - Two booleans indicating whether the differences in eigenvalue and eigenvector are within the specified tolerance.
    """
    
    # Using Julia's built-in eigen function to find eigenvalues and eigenvectors
    eigenvalues, eigenvectors = eigen(A)

    # Find the dominant eigenvalue and corresponding eigenvector from built-in results
    dominant_index = argmax(abs.(eigenvalues))
    actual_dominant_eigenvalue = eigenvalues[dominant_index]
    actual_dominant_eigenvector = eigenvectors[:, dominant_index]

    # Calculate differences
    eigenvalue_diff = abs(calculated_eigenvalue - actual_dominant_eigenvalue)
    eigenvector_diff = norm(calculated_eigenvector - actual_dominant_eigenvector)

    # Check differences against the tolerance
    eigenvalue_within_tolerance = eigenvalue_diff < tolerance
    eigenvector_within_tolerance = eigenvector_diff < tolerance

    return eigenvalue_within_tolerance, eigenvector_within_tolerance
end


function run_power_method()

    function custom_format(io, log_record)
        level = log_record[:level]
        message = log_record[:message]
        write(io, string(level, ": ", message, "\n"))
    end

    # Open a file for logging
    log_file = open("max_eigen.log", "w")

    # Create a logger with the custom format
    custom_logger = FormatLogger(custom_format, log_file)

    # Set the global logger to custom logger
    global_logger(custom_logger)

    @info "-------------------------------------------------"
    @info "Maximum EigenValue and EigenVector"
    # Input matrix and tolerance
    matrix = input_matrix()  # Ensure this function exists and returns a square matrix
    @info "Entered matrix: $matrix"

    tolerance = 1e-6  # Adjust as needed

    # Calculate dominant eigenvalue and eigenvector using power method
    lambda, eigenvector = power_method(matrix)
    @info "Calculated Dominant Eigenvalue $lambda"
    @info "Corresponding Eigenvector $eigenvector"

    # Validate results with built-in function and log the validation
    eigenvalue_within_tolerance, eigenvector_within_tolerance = validate_with_builtin(matrix, lambda, eigenvector, tolerance)
    
    @info "Validation Results"
    @info "Is Calculated Dominant Eigenvalue within Tolerance?" 
    if eigenvalue_within_tolerance
        @info "Passed"
    else
        @info "Failed"
    end

    @info "Is Corresponding Eigenvector within Tolerance?" 
    if eigenvector_within_tolerance
        @info "Passed"
    else
        @info "Failed"
    end

    close(log_file)
end