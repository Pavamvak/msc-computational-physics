function calculate_c(L, b)
    """
    calculate_c(L, b)

    Calculates the vector c by solving the system Lc = b using forward substitution.

    Args:
        L (Matrix{Float64}): Lower triangular matrix.
        b (Vector{Float64}): Right-hand side column vector.

    Returns:
        c (Vector{Float64}): Solution vector.

    Throws:
        `SingularException` if L is singular or not lower triangular.
    """
    n = size(L, 1)
    c = zeros(Float64, n)

    # Perform forward substitution to solve Lc = b
    c[1] = b[1] / L[1, 1]

    for i = 2:n
        sum = 0
        for j = 1:i-1
            sum += L[i, j] * c[j]
        end
        c[i] = (b[i] - sum) / L[i, i]
    end

    return c
end

function calculate_x(U, c)
    """
    calculate_x(U, c)

    Calculates the vector x by solving the system Ux = c using backward substitution.

    Args:
        U (Matrix{Float64}): Upper triangular matrix.
        c (Vector{Float64}): Right-hand side column vector.

    Returns:
        x (Vector{Float64}): Solution vector.

    Throws:
        `SingularException` if U is singular or not upper triangular.
    """
    n = size(U, 1)
    x = zeros(Float64, n)

    # Perform backward substitution to solve Ux = c
    for i = n:-1:1
        if abs(U[i, i]) < 1e-10  # Threshold for considering a value as zero
            throw(SingularException("Matrix U is singular or nearly singular"))
        end
        sum = 0.0
        for j = i+1:n
            sum += U[i, j] * x[j]
        end
        x[i] = (c[i] - sum) / U[i, i]
    end

    return x
end

function invert_matrix_with_LU(matrix, L, U)
    """
    invert_matrix_with_LU(matrix, L, U)

    Calculates the inverse of a matrix using precomputed L (lower triangular)
    and U (upper triangular) matrices from LU decomposition.

    Args:
        matrix (Matrix{Float64}): The square matrix to be inverted.
        L (Matrix{Float64}): Lower triangular matrix.
        U (Matrix{Float64}): Upper triangular matrix.

    Returns:
        inv_matrix (Matrix{Float64}): The inverted matrix.

    Throws:
        `SingularException` if the matrices are singular or not triangular.
    """

    n = size(matrix, 1)
    inv_matrix = zeros(Float64, n, n)

    for j = 1:n
        b = zeros(Float64, n)
        b[j] = 1.0

        # Solve Lc = b for c using forward substitution
        c = calculate_c(L, b)
        #c_valid = validate_c(L, c, b)
        #@info "Calculated c for column $j" c c_valid

        # Solve Ux = c for x using backward substitution
        x = calculate_x(U, c)
        #x_valid = validate_x(U, x, c)
        #@info "Calculated x for column $j" x x_valid

        # Assign the solution x as the j-th column of the inverse matrix
        inv_matrix[:, j] = x

        # Logging validation results
        # if c_valid
        #     @info "Validation passed for c in column $j" c
        # else
        #     @error "Validation failed for c in column $j" c
        # end
        # if x_valid
        #     @info "Validation passed for x in column $j" x
        # else
        #     @error "Validation failed for x in column $j" x
        # end
    end

    return inv_matrix
end