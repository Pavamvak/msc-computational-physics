function LU_creation(matrix)
    """
    LU_creation(matrix::Matrix{Float64}) -> (Matrix{Float64}, Matrix{Float64})

    Performs LU decomposition on a given square matrix. LU decomposition factors a matrix as the product of a lower triangular matrix L and an upper triangular matrix U.

    # Arguments
    - `matrix`: A square matrix to be decomposed.

    # Returns
    - A tuple (L, U) where:
        - `L` is the lower triangular matrix.
        - `U` is the upper triangular matrix.

    # Details
    The function iterates over each element, calculating the corresponding values for L and U using the standard LU decomposition algorithm. The diagonal elements of L are set to 1. The elements of U and non-diagonal elements of L are calculated based on the current state of L and U and the original matrix.

    # Example
    ```julia
    A = [4 3; 6 3]
    L, U = LU_creation(A)
    """
    
    N = size(matrix, 1)
    L = zeros(N, N)
    U = zeros(N, N)

    for i = 1:N
        L[i, i] = 1  # Diagonal elements of L are 1
        for j = 1:i
            # Calculate U[i, j] using the formula for j <= i
            sum_U = sum([L[j, k] * U[k, i] for k = 1:j-1])
            U[j, i] = matrix[j, i] - sum_U

            # Calculate L[i, j] using the formula for j <= i
            if i > j
                sum_L = sum([L[i, k] * U[k, j] for k = 1:j-1])
                L[i, j] = (matrix[i, j] - sum_L) / U[j, j]
            end
        end
    end
    return L, U
end