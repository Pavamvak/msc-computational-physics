function Gauss_Matrix_Inversion(matrix = nothing, use_custom_logger = true)
    """
    Gauss_Matrix_Inversion(matrix::Matrix{Float64} = nothing, use_custom_logger::Bool = true)

    Performs matrix inversion using the Gauss Elimination method and logs the process.

    # Arguments
    - `matrix` (optional): A square matrix to be inverted. If not provided, the user will be prompted to input a matrix.
    - `use_custom_logger` (optional, default=true): Determines whether to use the custom logger or the global logger.

    # Behavior
    - If `use_custom_logger` is true, logs are written to "matrix_gauss.log" using a custom format.
    - Validates the inverted matrix and logs the validation results.
    - Prints the inverted matrix to the console.

    # Returns
    - Nothing. However, the function logs various stages of matrix inversion and validation, and prints the inverted matrix.

    # Example
    ```julia
    Gauss_Matrix_Inversion()  # Will prompt for matrix input and use a custom logger
    Gauss_Matrix_Inversion(some_matrix, false)  # Will use the given matrix and the global logger
    """
    
    # Corrected custom format function
    function custom_format(io, log_record)
        level = log_record[:level]
        message = log_record[:message]
        write(io, string(level, ": ", message, "\n"))
    end

    if use_custom_logger
        # Open a file for logging
        log_file = open("matrix_gauss.log", "w")

        # Create a logger with the custom format
        custom_logger = FormatLogger(custom_format, log_file)

        # Set the global logger to custom logger
        global_logger(custom_logger)
    end

    @info "-------------------------------------------------"
    @info "Matrix Inversion and Gauss Elimination Validation"
    
    # Prompt the user to input a square matrix using input_matrix(), if matrix is not provided
    if isnothing(matrix)
        matrix = input_matrix()
    end

    @info "Entered matrix: $matrix"

    start_time = time_ns()
    # Invert the matrix using L and U matrices
    inv_matrix = gauss_invert_matrix(matrix)
    elapsed_time = (time_ns() - start_time) / 1e9  # Convert nanoseconds to seconds
    @info "Time taken for matrix inversion: $elapsed_time seconds"
    
    # Validate the inverted matrix
    if validate_inverse(matrix, inv_matrix)
        @info "Matrix inversion validation passed."
    else
        @error "Matrix inversion validation failed."
    end

    @info "Inverted Matrix: $inv_matrix"

    println("The inverted matrix is:")
    println(inv_matrix)  

    if use_custom_logger
        close(log_file)
    end
    
end