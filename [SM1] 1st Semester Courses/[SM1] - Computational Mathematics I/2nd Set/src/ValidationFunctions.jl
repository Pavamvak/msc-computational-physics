function validate_inverse(matrix, inv_matrix, tol=1e-6)
    """
    validate_inverse(matrix, inv_matrix, tol)

    Validates that the product of a matrix and its inverse is approximately the identity matrix.

    Args:
        matrix (Matrix{Float64}): The original square matrix.
        inv_matrix (Matrix{Float64}): The inverted matrix to be validated.
        tol (Float64, optional): Tolerance for numerical comparisons.

    Returns:
        valid (Bool): True if the product is close to the identity matrix, False otherwise.
    """
    identity_matrix = Matrix{Float64}(I, size(matrix)...)
    product = matrix * inv_matrix
    return norm(product - identity_matrix) < tol
end

function validate_LU_decomposition(matrix, L, U, tol=1e-6)
    """
    validate_LU_decomposition(matrix, L, U, tol)

    Validates that the product of L and U matrices equals the original matrix within a specified tolerance.

    Args:
        matrix (Matrix{Float64}): The original square matrix.
        L (Matrix{Float64}): The lower triangular matrix.
        U (Matrix{Float64}): The upper triangular matrix.
        tol (Float64, optional): Tolerance for numerical comparisons.

    Returns:
        valid (Bool): True if the product L * U equals the original matrix, False otherwise.
    """
    # Calculate the product of L, and U
    reconstruction = L * U

    # Check if the reconstructed matrix is approximately equal to the original matrix
    return all(abs.(matrix - reconstruction) .< tol)
end

function validate_c(L, c, b)
    """
    validate_c(L, c, b)

    Validates the calculation of vector c by verifying that Lc is approximately equal to b.

    Args:
        L (Matrix{Float64}): Lower triangular matrix.
        c (Vector{Float64}): Calculated solution vector.
        b (Vector{Float64}): Right-hand side column vector.

    Returns:
        valid (Bool): True if Lc is approximately equal to b, False otherwise.
    """
    Lc = L * c
    return isapprox(Lc, b)
end

function validate_x(U, x, c)
    """
    validate_x(U, x, c)

    Validates the calculation of vector x by verifying that Ux is approximately equal to c.

    Args:
        U (Matrix{Float64}): Upper triangular matrix.
        x (Vector{Float64}): Calculated solution vector.
        c (Vector{Float64}): Right-hand side column vector.

    Returns:
        valid (Bool): True if Ux is approximately equal to c, False otherwise.
    """
    Ux = U * x
    return isapprox(Ux, c)
end

function is_lower_triangular(matrix)
    """
    is_lower_triangular(matrix::Matrix{Float64}) -> Bool

    Determines whether a given square matrix is lower triangular.

    A matrix is lower triangular if all the elements above the diagonal are zero.

    # Arguments
    - `matrix`: The matrix to be checked.

    # Returns
    - `true` if the matrix is lower triangular, `false` otherwise.

    # Example
    ```julia
    A = [1 0 0; 2 1 0; 3 4 1]
    is_lower_triangular(A)  # Should return true
    """
    for i = 1:size(matrix, 1)
        for j = i+1:size(matrix, 2)
            if matrix[i, j] != 0
                return false
            end
        end
    end
    return true
end

function is_upper_triangular(matrix)
    """
    is_upper_triangular(matrix::Matrix{Float64}) -> Bool

    Determines whether a given square matrix is upper triangular.

    A matrix is upper triangular if all the elements below the diagonal are zero.

    # Arguments
    - `matrix`: The matrix to be checked.
    
    # Returns
    - `true` if the matrix is upper triangular, `false` otherwise.

    # Example
    julia```
    A = [1 2 3; 0 1 4; 0 0 1]
    is_upper_triangular(A)  # Should return true
    """
    for i = 1:size(matrix, 1)
        for j = 1:i-1
            if matrix[i, j] != 0
                return false
            end
        end
    end
    return true
end

