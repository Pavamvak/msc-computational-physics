function LU_Matrix_Inversion(matrix = nothing, use_custom_logger = true)
    """
    LU_Matrix_Inversion(matrix::Matrix{Float64} = nothing, use_custom_logger::Bool = true)

    Performs matrix inversion using LU decomposition method and logs the process. It uses a two-step approach: first, it decomposes the matrix into L and U matrices using LU decomposition, then it calculates the inverse of the matrix using these L and U matrices.

    # Arguments
    - `matrix` (optional): A square matrix to be inverted. If not provided, the user will be prompted to input a matrix.
    - `use_custom_logger` (optional, default=true): Determines whether to use a custom logger or the global logger.

    # Behavior
    - If `use_custom_logger` is true, logs are written to "matrix_decomposition.log" using a custom format.
    - The function calculates the L and U matrices, logs the calculation details, and then inverts the matrix using these matrices.
    - Validates the decomposition and the inverse matrix and logs the validation results.
    - Prints the inverted matrix to the console.

    # Returns
    - Nothing. However, the function logs various stages of matrix inversion and validation, and prints the inverted matrix.

    # Example
    ```julia
    A = [4 3; 6 3]
    LU_Matrix_Inversion(A)  # Will use the given matrix and a custom logger
    LU_Matrix_Inversion(A, false)  # Will use the given matrix and the global logger
    """
    # Corrected custom format function
    function custom_format(io, log_record)
        level = log_record[:level]
        message = log_record[:message]
        write(io, string(level, ": ", message, "\n"))
    end

    if use_custom_logger
        # Open a file for logging
        log_file = open("matrix_decomposition.log", "w")

        # Create a logger with the custom format
        custom_logger = FormatLogger(custom_format, log_file)

        # Set the global logger to custom logger
        global_logger(custom_logger)
    end

    @info "------------------------------------------------"
    @info "Matrix Inversion and LU Decomposition Validation"
    
    #  Prompt the user to input a square matrix using input_matrix(), if matrix is not provided
    if isnothing(matrix)
        matrix = input_matrix()
    end
    
    @info "Entered matrix: $matrix"

    start_time = time_ns()
    # Calculate L and U matrices
    L, U = LU_creation(matrix)
    @info "Lower Triangular Matrix L: $L"
    @info "Upper Triangular Matrix U: $U"
    elapsed_time = (time_ns() - start_time) / 1e9 
    @info "Time taken for LU creation: $elapsed_time seconds"

    # Validate L and U matrices
    if validate_LU_decomposition(matrix, L, U)
        @info "LU decomposition validation passed."
    else
        @error "LU decomposition validation failed."
        return
    end

    if is_lower_triangular(L)
        @info "Matrix L is lower triangular."
    else
        @error "Matrix L is not lower triangular."
    end

    if is_upper_triangular(U)
        @info "Matrix U is upper triangular."
    else
        @error "Matrix U is not upper triangular"
    end

    start_time = time_ns()
    # Invert the matrix using L and U matrices
    inv_matrix = invert_matrix_with_LU(matrix, L, U)
    elapsed_time = (time_ns() - start_time) / 1e9  # Convert nanoseconds to seconds
    @info "Time taken for matrix inversion: $elapsed_time seconds"
    
    # Validate the inverted matrix
    if validate_inverse(matrix, inv_matrix)
        @info "Matrix inversion validation passed."
    else
        @error "Matrix inversion validation failed."
    end

    @info "Inverted Matrix: $inv_matrix"

    println("The inverted matrix is:")
    println(inv_matrix)  

    if use_custom_logger
        close(log_file)
    end
    
end