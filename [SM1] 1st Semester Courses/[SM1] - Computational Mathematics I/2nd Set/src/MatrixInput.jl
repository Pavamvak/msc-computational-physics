function input_matrix()
    """
    input_matrix()

    Prompts the user to enter a square matrix in MATLAB-style format. The input should be enclosed in square brackets
    and semicolons to represent rows and columns. Elements can be separated by commas or spaces.

    Example format: [1, 2, 3; 5, 2, 1; 4, 5, 9]

    This function ensures that the input is a valid square matrix of type `Matrix{Float64}`.

    Returns:
        matrix (Matrix{Float64}): The square matrix entered by the user.

    Throws:
        `ErrorException` if the input cannot be parsed or is not a valid square matrix.
    """

    println("Enter a square matrix in MATLAB style format.")
    println("Example format: [1, 2, 3; 5, 2, 1; 4, 5, 9]")
    println("This represents a 3x3 matrix {Float64}:\n1.0 2.0 3.0\n5.0 2.0 1.0\n4.0 5.0 9.0")

    while true
        println("\nPlease enter your square matrix:")
        matrix_str = readline()
        try
            # Replace commas with spaces in the input string
            matrix_str = replace(matrix_str, "," => " ")

            # Parse the string into a Julia expression
            parsed_expr = Meta.parse(matrix_str)
            # Evaluate the expression to get the matrix
            matrix = eval(parsed_expr)

            # Check if the matrix is square
            if size(matrix, 1) != size(matrix, 2)
                println("Error: The matrix is not square. Number of rows and columns must be equal.")
                continue
            end

            # Convert the matrix to Matrix{Float64}
            matrix = convert(Matrix{Float64}, matrix)

            return matrix
        catch e
            println("Error parsing the matrix. Ensure your input is in the correct format and contains only numerical values.")
            println("Example format: [1.0, 2.0, 3.0; 5.0, 2.0, 1.0; 4.0, 5.0, 9.0]")
        end
    end
end
