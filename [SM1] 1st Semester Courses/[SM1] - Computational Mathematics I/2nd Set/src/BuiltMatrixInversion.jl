function built_Matrix_Inversion(matrix = nothing, use_custom_logger = true)
    """
    Performs matrix inversion using built-in Julia functions and logs the process.

    This function prompts the user for a square matrix, inverts it using Julia's built-in inv() function,
    and logs the entire process including validation of the inverted matrix.

    Notes:
    - The user is prompted to input a square matrix using a separate function `input_matrix()`.
    - The inverted matrix is validated with `validate_inverse(matrix, inv_matrix)`.
    - The timing for the inversion process is recorded and logged.
    - A custom log format is used for logging the process to 'matrix_built.log'.

    Parameters:
    - matrix (optional): A square matrix to be inverted.
    - use_custom_logger (optional, default=true): Determines whether to use the custom logger or the global logger.
    """

    # Custom format function for the logger
    function custom_format(io, log_record)
        level = log_record[:level]
        message = log_record[:message]
        write(io, string(level, ": ", message, "\n"))
    end

    if use_custom_logger
        # Open a file for logging
        log_file = open("matrix_built.log", "w")

        # Create a logger with the custom format
        custom_logger = FormatLogger(custom_format, log_file)

        # Set the global logger to custom logger
        global_logger(custom_logger)
    end

    @info "------------------------------------------------"
    @info "Matrix Inversion and LU Decomposition Validation"
    
    # Prompt the user to input a square matrix using input_matrix(), if matrix is not provided
    if isnothing(matrix)
        matrix = input_matrix()
    end
    
    @info "Entered matrix: $matrix"
    
    @info "Entered matrix: $matrix"

    start_time = time_ns()
    inv_matrix = inv(matrix)
    elapsed_time = (time_ns() - start_time) / 1e9
    @info "Time taken for matrix inversion: $elapsed_time seconds"

    # Validate the inverted matrix
    if validate_inverse(matrix, inv_matrix)
        @info "Matrix inversion validation passed."
    else
        @error "Matrix inversion validation failed."
    end

    @info "Inverted Matrix: $inv_matrix"

    println("The inverted matrix is:")
    println(inv_matrix)  

    if use_custom_logger
        close(log_file)
    end
    
end
