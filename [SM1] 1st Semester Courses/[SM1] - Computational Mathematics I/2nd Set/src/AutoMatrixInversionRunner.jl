function matrix_to_string(matrix)
    """
    matrix_to_string(matrix::Matrix{Float64}) -> String

    Converts a given matrix into a string representation in Julia matrix format.

    # Arguments
    - `matrix`: The matrix to be converted.

    # Returns
    - A string representation of the matrix in the format `[row1; row2; ...]`, where each row is separated 
    by a semicolon and each element in a row is separated by a comma.
    """
    string("[", join((join(row, ", ") for row in eachrow(matrix)), "; "), "]")
end

function simulate_user_input(input_string, function_to_run)
    """
    simulate_user_input(input_string::String, function_to_run::Function)

    Simulates user input by setting `stdin` to a given input string and running the specified function.

    # Arguments
    - `input_string`: The string to be used as simulated user input.
    - `function_to_run`: The function to be run using the simulated input.

    # Notes
    - Restores the original `stdin` after the function is executed.
    """
    original_stdin = stdin
    input_buffer = IOBuffer(input_string)
    try
        Base.stdin = input_buffer
        function_to_run()
    finally
        Base.stdin = original_stdin
    end
end

function custom_format(io, log_record)
    """
    custom_format(io::IO, log_record::Logging.LogRecord)

    A custom log format function that writes log messages to the given I/O stream.

    # Arguments
    - `io`: The I/O stream to write the log message to.
    - `log_record`: The log record containing the log level and message.

    # Behavior
    - Writes log messages in the format "<level>: <message>\n".
    """
    level = log_record[:level]
    message = log_record[:message]
    write(io, string(level, ": ", message, "\n"))
end

function run_inversion_function(inversion_function, num_runs = 15)
    """
    run_inversion_function(inversion_function::Function, num_runs::Int = 15)

    Runs the specified matrix inversion function multiple times with matrices of increasing size and logs the process.

    # Arguments
    - `inversion_function`: The matrix inversion function to run.
    - `num_runs`: The number of times to run the inversion function.

    # Behavior
    - Generates a random matrix of increasing size for each run.
    - Logs each run's process and results to a file named "matrix_inversion_runs_<function_name>.log".
    """
    # Get the function name as a string
    function_name = string(nameof(inversion_function))

    # Create a unique log filename for each inversion function
    log_filename = "matrix_inversion_runs_$(function_name).log"

    # Open a file for logging
    log_file = open(log_filename, "w")

    # Create a logger with the custom format
    custom_logger = FormatLogger(custom_format, log_file)

    # Set the global logger to custom logger
    global_logger(custom_logger)

    for i in 1:num_runs
        # Generate a random matrix with increasing size
        matrix_size = i + 3  # Starts from 4x4 and increases
        matrix = rand(Float64, matrix_size, matrix_size) * 10 

        matrix_string = matrix_to_string(matrix)
        
        # Simulate user input and run the inversion function
        simulate_user_input(matrix_string, () -> inversion_function(matrix, false))
    end

    close(log_file)
end

function Auto_Runner_Inversion()
    """
    Auto_Runner_Inversion()

    The main function to choose and run a matrix inversion method multiple times.

    # User Interaction
    - Prompts the user to choose a matrix inversion method from the available options.
    - Runs the selected method multiple times using `run_inversion_function`.

    # Notes
    - Invalid choice leads to a prompt message and terminates the process.
    """
    println("Choose a matrix inversion method (1: LU, 2: Gauss, 3: Built-in):")
    choice = readline()

    if choice == "1"
        run_inversion_function(LU_Matrix_Inversion)
    elseif choice == "2"
        run_inversion_function(Gauss_Matrix_Inversion)
    elseif choice == "3"
        run_inversion_function(built_Matrix_Inversion)
    else
        println("Invalid choice")
    end
end
