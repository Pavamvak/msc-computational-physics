"""
    get_equation()

Prompt the user to enter an algebraic equation as a string. 
The function will continue to prompt until an equation with exactly one variable is entered.

# Returns
- `equation_str`: The entered equation as a string.
- `variable`: The variable used in the equation.
"""
function get_equation()
    while true
        println("Enter the equation (e.g., x^2 - 4):")
        equation_str = readline()

        # Parse the equation and extract variables
        equation = sympify(equation_str)
        variables = free_symbols(equation)

        if length(variables) == 1
            return equation_str, variables[1]  # Return equation string and the variable
        else
            println("Error: Please enter an equation with exactly one variable.")
        end
    end
end

"""
    get_range()

Prompt the user to enter a range in the format 'start:end'.
The function ensures the correct format and parses the input into a Julia range.

# Returns
- A Julia range from `start_range` to `end_range` with a calculated step size.
"""
function get_range()
    println("Enter the range in format 'start:end' (e.g., -10:10):")
    
    while true
        try
            user_input = readline()
            split_input = split(user_input, ":")
            
            if length(split_input) != 2
                error("Invalid format. Please enter the range as 'start:end'.")
            end
            
            start_range = parse(Float64, split_input[1])
            end_range = parse(Float64, split_input[2])

            # Calculate step size
            step_size = (end_range - start_range) / 1000

            # Construct and return the range
            return start_range:step_size:end_range
        catch e
            println("Error: ", e)
            println("Please enter a valid range in the format 'start:end'.")
        end
    end
end

"""
    get_number_of_roots()

Prompt the user to enter the expected number of roots.
This function will continue to prompt until a valid integer is entered.

# Returns
- The entered integer representing the number of expected roots.
"""
function get_number_of_roots()
    while true
        try
            println("Enter the number of roots you expect:")
            return parse(Int, readline())
        catch e
            println("Invalid input. Please enter an integer.")
        end
    end
end

"""
    get_tolerance()

Prompt the user to enter the desired number of significant figures for calculations.
The function ensures the input is an integer.

# Returns
- The entered integer representing the number of significant figures.
"""
function get_tolerance()
    while true
        try
            println("Enter the number of significant figures:")
            return parse(Int, readline())
        catch e
            println("Invalid input. Please enter an integer.")
        end
    end
end

"""
    get_root_finding_method_choice()

Prompt the user to choose a root-finding method.
Options are 1 for Bisection, 2 for False Position, and 3 for Simple Fixed Point Iteration.
The function ensures a valid choice is made.

# Returns
- The entered integer representing the user's choice of root-finding method.
"""
function get_root_finding_method_choice()
    while true
        try
            println("Choose a root finding method, 1 for Bisection, 2 for False Position, and 3 for Simple Fixed Point Iteration:")
            method_choice = parse(Int, readline())

            if method_choice in [1, 2, 3]
                return method_choice
            else
                println("Invalid choice. Please enter 1, 2, or 3.")
            end
        catch e
            if isa(e, ArgumentError)
                println("Invalid input. Please enter an integer.")
            else
                println("An unexpected error occurred: ", e)
            end
        end
    end
end
