header_fixed_point = """
Simple Fixed Point Iteration Root Finding Log
This file logs the iterative process of finding roots using the Simple Fixed Point Iteration Method.
Each section starts with a note indicating the number of the root being searched for.

The Simple Fixed Point Iteration Method iteratively refines an initial guess by applying a transformation function g(x), aiming for convergence where x = g(x).

Column Descriptions:
xi: Current estimate of the root
g(xi): Function value at xi
ea: Approximate relative error

Iterations continue until the root is found within the specified tolerance or the maximum number of iterations is reached.
"""

function simple_fixed_point_iteration(g, brackets, tol, max_iter)

    open("FixedPointIteration_Log.txt", "w") do io
        write(io, header_fixed_point)
        write(io, repeat("-", 180) * "\n\n")

        root_number = 1
        roots = []
        iterations = []
        ea_values = []
        et_values = []

        for bracket in brackets
            initial_guess = bracket[1]  # Use the first element of the bracket as the initial guess
            x = initial_guess
            prev_x = 0.0
            ea = 100.0  # Start with a large error

            write(io, "\nSearching for root #$(root_number)\n")
            write(io, "Initial guess: $(initial_guess), Tolerance: $(tol)\n")
            write(io, rpad("xi", 30) * rpad("g(xi)", 30) * rpad("ea", 30) * rpad("et", 30) * "\n")
            write(io, repeat("-", 90) * "\n")

            # Prepare data for plotting
            x_values = []
            gx_values = []
            convergence_lines = []

            for iter in 1:max_iter
                x = g(x)  # Apply the transformation function

                push!(x_values, prev_x)
                push!(gx_values, x)
                push!(convergence_lines, [(prev_x, prev_x), (prev_x, x)])
                push!(iterations, iter)

                # Calculate true percentative error
                et = true_percent_relative_error(x)
                push!(et_values, et)

                # Calculate the approximate error
                ea = approximation_error_percentage(x, prev_x)
                push!(ea_values, ea)
                
                write(io, rpad(string(x), 30) * rpad(string(g(x)), 30) * rpad(string(ea), 30) * rpad(string(et), 30) * "\n")

                if ea < tol
                    push!(roots, x)
                    break
                end

                prev_x = x
            end

            if ea >= tol
                println("Max iterations reached for bracket $bracket without finding root within tolerance.")
            end

            # Generate plot
            plot!(x_values, gx_values, label="y = g(x)", color=:blue)
            plot!(x_values, x_values, label="y = x", color=:red, linestyle=:dash)
            for line in convergence_lines
                plot!([line[1][1], line[2][1]], [line[1][2], line[2][2]], color=:green, linestyle=:dot)
            end
            
            root_number += 1
        end

        # Show and save the plot
        title!("Convergence Plot for Fixed Point Iteration")
        xlabel!("x")
        ylabel!("g(x)")
        savefig("FixedPointIteration_ConvergencePlot.png")
        
        plt_2 = plot(iterations, ea_values, title = "Fixed Point Method - ea over Iterations", xlabel = "Iteration", ylabel = "ea", legend = false)
        savefig(plt_2, "Fixed_Point_Method_App_Error_Plot.png")

        plt_3 = plot(iterations, et_values, title = "Fixed Point Method - et over Iterations", xlabel = "Iteration", ylabel = "et", legend = false)
        savefig(plt_3, "Fixed_Point_Method_True_Error_Plot.png")

        return roots
    end
end