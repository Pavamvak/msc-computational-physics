# Header for the log file
header_initial_guess = """
Bracket Finding Log
This file logs the iterative process of finding brackets (where a function changes sign) 
within a specified range. Each section starts with a note indicating the number of root being searched for.

Column Descriptions:
x1: Starting point of the interval
x2: Ending point of the interval
f(x1): Function value at x1
f(x2): Function value at x2
Sign: '+' if f(x1) * f(x2) > 0, '-' if f(x1) * f(x2) < 0 
"""

"""
    find_initial_brackets(f, x_range, n_roots)

Find initial brackets for root finding in a given range for a specified function.

# Arguments
- `f`: A function for which the roots are to be found. It should be a function of one variable.
- `x_range`: The range of x values (as a Julia Range) over which to search for roots.
- `n_roots`: The expected number of roots within the given range.

# Returns
- `brackets`: A list of tuples, each representing a bracket where a root is expected. 
  A bracket is a pair of values (x1, x2) where the function changes sign.
- `roots`: A list of values where the function is exactly zero (exact roots).

# Procedure
1. The function divides the provided range into sub-intervals.
2. For each sub-interval, it checks if there's a sign change in the function values at the interval's endpoints.
3. If a sign change is detected, the interval is added to the `brackets` list as a potential root bracket.
4. If the function value is exactly zero at any point, that point is added to the `roots` list.
5. The search continues until the expected number of roots (`n_roots`) is found or the entire range is covered.
6. The function also logs detailed information about the search process to a file named "InitialGuess_Log.txt".

# Usage
This function is useful in numerical methods for root finding where an initial guess of the roots' locations is required. 
It's typically used before applying more precise root-finding algorithms like the bisection method, false position method, or Newton-Raphson method.

# Example
```julia
f = x -> x^2 - 4
x_range = -10:0.01:10
n_roots = 2
brackets, exact_roots = find_initial_brackets(f, x_range, n_roots)```
"""
function find_initial_brackets(f, x_range, n_roots)
    x_vals = collect(x_range)
    start_val = first(x_vals)
    stop_val = last(x_vals)
    step = (stop_val - start_val) / 10
    brackets = Tuple{Float64, Float64}[]
    roots = Float64[]
    root_count = 1

    # Open a file in write mode
    open("InitialGuess_Log.txt", "w") do io
        # Write the header
        write(io, header_initial_guess)
        write(io, repeat("-", 55) * "\n\n")

        # Initial section for the first root
        write(io, "\nSearching initial guess for 1st root\n")
        write(io, rpad("x1", 5) * rpad("x2", 5) * rpad("f(x1)", 20) * rpad("f(x2)", 20) * rpad("Sign", 5) * "\n")
        write(io, repeat("-", 55) * "\n")
        
        x1 = start_val
        f1 = f(x1)
        

        for x2 in start_val + step : step : stop_val
            f2 = f(x2)

            # Write a new section header if searching for a new root
            if length(brackets) + length(roots) > root_count
                root_count = length(brackets) + length(roots)
                write(io, "\nSearching initial guess for $(root_count)th root\n")
                write(io, lpad("x1", 5) * lpad("x2", 5) * lpad("f(x1)", 20) * lpad("f(x2)", 20) * lpad("Sign", 5) * "\n")
                write(io, repeat("-", 55) * "\n")
            end

            # Determine the sign
            sign_str = f1 * f2 > 0 ? "+" : "-"

            # Logging the values
            write(io, rpad(string(x1), 5) * rpad(string(x2), 5) * rpad(string(f1), 20) * rpad(string(f2), 20) * rpad(sign_str, 5) * "\n")

            # Check for sign change indicating a bracket
            if f1 * f2 < 0
                push!(brackets, (Float64(x1), Float64(x2)))
                if length(brackets) + length(roots) == n_roots
                    break
                end
                x1, f1 = x2, f2
            end

            # Check for exact roots
            if f2 == 0
                push!(roots, Float64(x2))
                if length(brackets) + length(roots) == n_roots
                    break
                end
                x1, f1 = x2 + step, f(x2 + step)
            elseif f1 * f2 >= 0
                x1, f1 = x2, f2
            end
        end
    end

    if length(brackets) + length(roots) < n_roots
        println("Warning: Found fewer brackets/roots than expected.")
    end

    return brackets, roots
end
