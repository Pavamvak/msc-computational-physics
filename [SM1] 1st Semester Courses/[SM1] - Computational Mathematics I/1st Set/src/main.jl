function main()
    # Get user inputs
    equation_str, variable = get_equation()
    x_range = get_range()

    # Plot the equation
    equation_plt = plot_user_equation(equation_str, variable, x_range)
    savefig(equation_plt, "Equation_Plot.png")
    display(equation_plt)

    # Get the number of roots
    n_roots = get_number_of_roots()

    # Find initial brackets for root finding
    equation = sympify(equation_str)
    f = lambdify(equation, [variable])
    brackets, exact_roots = find_initial_brackets(f, x_range, n_roots)

    # Check if exact roots were found
    if !isempty(exact_roots)
        println("Exact roots found at:")
        for root in exact_roots
            println(root)
        end
    else
        println("No exact roots found.")
    end

    # Discuss brackets regardless of whether exact roots were found
    if !isempty(brackets)
        println("Initial brackets for root finding: ", brackets)
    else
        println("No brackets found.")
    end

    method_choice = get_root_finding_method_choice()

    # Process brackets with chosen method
    if !isempty(brackets)

        # Get the number of significant figures from the user
        significant_figures = get_tolerance()

        # Calculate the tolerance using the Scarborough criteria
        tolerance = scarborough_error(significant_figures)

        roots = []
        if method_choice == 1
            # Process each bracket with the bisection method
            roots = bisection(f, brackets, tolerance, 10000)
            println("Roots found using bisection:")
        elseif method_choice == 2
            # Process each bracket with the False Position method
            roots = false_position(f, brackets, tolerance, 10000)
            println("Roots found using False Position:")
        elseif method_choice == 3
            # Transform f(x) to g(x) for Simple Fixed Point Iteration Method
            g = create_gx(equation, variable)

            # Check if g(x) is suitable for Simple Fixed Point Iteration Method
            suitability = is_suitable_for_fixed_point(f, variable, brackets)
            # Process each bracket with the Simple Fixed Point Iteration Method
            roots = simple_fixed_point_iteration(g, brackets, tolerance, 10000)
            println("Roots found using Simple Fixed Point Iteration:")      
        
        else
            println("Invalid method choice.")
        end

        for root in roots
            println(root)
        end

    end

end
