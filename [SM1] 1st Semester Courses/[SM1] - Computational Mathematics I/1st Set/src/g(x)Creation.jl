function create_gx(equation, variable)
    gx = lambdify(ln(2-variable), [variable])
    return gx
end