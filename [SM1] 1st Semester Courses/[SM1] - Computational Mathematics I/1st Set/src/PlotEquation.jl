gr()

function plot_user_equation(equation_str, variable, x_range)
    equation = sympify(equation_str)
    f = lambdify(equation, [variable])

    # Create the initial plot
    plt = plot(x_range, [f(xi) for xi in x_range], label="f($variable)", 
               title="Plot of the inputted equation", 
               xlabel=string(variable), ylabel="f($variable)",
               subtitle="Identify number of roots and discontinuities, to enter them at the program.")

    # Add y=0 line
    plot!(plt, x_range, zeros(length(x_range)), label="", color=:black)

    # Add grid and subgrid
    plot!(plt, grid=true)

    return plt
end
