# Header for the log file
header_bisection = """
Bisection Root Finding Log
This file logs the iterative process of finding the roots using the Bisection Method
within a specified range. Each section starts with a note indicating the number of root being searched for.

Column Descriptions:
x1: Lower guess
x2: Upper guess
xr: Estimation root
f(x1): Function value at x1
f(x2): Function value at x2
f(xr): Function values at xr
ea: Approximate Error Percentage
et: True Error Percentage
"""


"""
    bisection(f, brackets, tol, max_iter)

Performs the Bisection Method to find roots of a given function `f` within specified brackets.

# Parameters
- `f`: The function for which the roots are to be found. It should be a continuous function.
- `brackets`: An array of tuples, where each tuple `(x1, x2)` represents an interval within which a root is suspected. If a single tuple is provided, it is converted into an array.
- `tol`: The tolerance level for the approximate error. The iteration stops when the error falls below this level.
- `max_iter`: The maximum number of iterations to perform. This prevents infinite looping in case convergence criteria are not met.

# Description
The function implements the Bisection Method, a root-finding method that repeatedly bisects an interval and then selects a subinterval in which a root must lie for further processing.

For each bracket, the function:
- Checks if the function changes sign over the interval. If not, it moves to the next bracket.
- Iteratively calculates the midpoint (`xr`) and evaluates the function at this point.
- Determines the new interval to use in the next iteration based on where the sign change occurs.
- Stops iterating if the approximate error (`ea`) falls below `tol` or if the maximum number of iterations (`max_iter`) is reached.

The function logs detailed information about each iteration to a file and generates plots showing the behavior of `f(xr)` and the approximate error over iterations.

# Returns
- `roots`: An array containing the roots found within each provided bracket.

# Example Usage
```julia
f = x -> x^2 - 4
roots = bisection(f, [(1.0, 3.0), (3.0, 5.0)], 0.01, 100)
println(roots)
```
"""
function bisection(f, brackets, tol, max_iter)
    # Check if brackets is a single tuple, convert it to an array of tuples if necessary
    if typeof(brackets[1]) == Float64
        brackets = [brackets]
    end

    open("BisectionRoot_Log.txt", "w") do io
        write(io, header_bisection)
        write(io, repeat("-", 180) * "\n\n")

        root_number = 1
        roots = []
        fxr_values = []
        ea_values = []
        et_values = []
        iterations = []

        for bracket in brackets
            x1, x2 = bracket
            fx1 = f(x1)
            fx2 = f(x2)
            prev_xr = 0.0
            ea = 100

            if fx1 * fx2 > 0
                println("Function does not change sign over the interval [$(x1), $(x2)].")
                continue
            end

            write(io, "\nSearching initial guess for root #$(root_number)\n")
            write(io, "Scarborough criterion: $(tol)\n")
            write(io, rpad("x1", 30) * rpad("x2", 30) * rpad("xr", 30) * rpad("f(x1)", 30) * rpad("f(x2)", 30) * rpad("f(xr)", 30) * rpad("ea", 30) * rpad("et", 30) * "\n")
            write(io, repeat("-", 210) * "\n")

            for iter in 1:max_iter
                xr = (x1 + x2) / 2
                fxr = f(xr)

                # Store fxr, ea and iteration number for plotting
                push!(fxr_values, fxr)
                push!(ea_values, ea)
                push!(iterations, iter)

                # Calculate the true percentative error
                et = true_percent_relative_error(xr)
                push!(et_values, et)

                # Calculate the approximate error
                ea = approximation_error_percentage(xr, prev_xr)
                prev_xr = xr

                write(io, rpad(string(x1), 30) * rpad(string(x2), 30) * rpad(string(xr), 30) * rpad(string(fx1), 30) * rpad(string(fx2), 30) * rpad(string(fxr), 30) * rpad(string(ea), 30) * rpad(string(et), 30) * "\n")

                if ea < tol
                    push!(roots, xr)
                    break
                end

                if fx1 * fxr < 0
                    x2, fx2 = xr, fxr
                else
                    x1, fx1 = xr, fxr
                end
            end

            if length(roots) < root_number
                println("Max iterations reached without convergence for root #$(root_number).")
            end

            root_number += 1
        end

        # Plotting
        plt = plot(iterations, fxr_values, title = "Bisection Method - f(xr) over Iterations", xlabel = "Iteration", ylabel = "f(xr)", legend = false)
        savefig(plt, "Bisection_Method_Plot.png")

        plt_2 = plot(iterations, ea_values, title = "Bisection Method - ea over Iterations", xlabel = "Iteration", ylabel = "ea", legend = false)
        savefig(plt_2, "Bisection_Method_App_Error_Plot.png")

        plt_3 = plot(iterations, et_values, title = "Bisection Method - et over Iterations", xlabel = "Iteration", ylabel = "et", legend = false)
        savefig(plt_3, "Bisection_Method_True_Error_Plot.png")

        return roots

    end
end