# Header for the log file
header_false_position = """
False Position Root Finding Log
This file logs the iterative process of finding roots using the False Position Method
within specified brackets. Each section starts with a note indicating the number of the root being searched for.

The False Position Method, also known as the Regula Falsi Method, iteratively refines the bracketing interval to zero in on a root. It does so by using a straight line connecting the function values at the bracketing points and finding the x-intercept of this line.

Column Descriptions:
x1: Lower boundary of the current interval
x2: Upper boundary of the current interval
xr: Current estimate of the root
f(x1): Function value at x1
f(x2): Function value at x2
f(xr): Function value at the estimated root xr
ea: Approximate Error Percentage
et: True Error Percentage

Iterations continue until the root is found within the specified tolerance or the maximum number of iterations is reached.
"""

function false_position(f, brackets, tol, max_iter)
    # Check if brackets is a single tuple, convert it to an array of tuples if necessary
    if typeof(brackets[1]) == Float64
        brackets = [brackets]
    end

    open("FalsePositionRoot_Log.txt", "w") do io
        write(io, header_false_position)
        write(io, repeat("-", 210) * "\n\n")

        root_number = 1
        roots = []
        fxr_values = []
        iterations = []
        ea_values = []
        et_values = []

        for bracket in brackets
            x1, x2 = bracket
            fx1 = f(x1)
            fx2 = f(x2)
            prev_xr = 0.0

            if fx1 * fx2 > 0
                println("Function does not change sign over the interval [$(x1), $(x2)].")
                continue
            end

            write(io, "\nSearching initial guess for root #$(root_number)\n")
            write(io, "Scarborough criterion: $(tol)\n")
            write(io, rpad("x1", 30) * rpad("x2", 30) * rpad("xr", 30) * rpad("f(x1)", 30) * rpad("f(x2)", 30) * rpad("f(xr)", 30) * rpad("ea", 30) * rpad("et", 30) * "\n")
            write(io, repeat("-", 210) * "\n")

            for iter in 1:max_iter
                xr = x1 - (fx1 * (x2 - x1)) / (fx2 - fx1)
                fxr = f(xr)

                # Store fxr and iteration number for plotting
                push!(fxr_values, fxr)
                push!(iterations, iter)

                # Calculate true percentative error
                et = true_percent_relative_error(xr)
                push!(et_values, et)

                # Calculate the approximate error
                ea = approximation_error_percentage(xr, prev_xr)
                push!(ea_values, ea)
                prev_xr = xr

                write(io, rpad(string(x1), 30) * rpad(string(x2), 30) * rpad(string(xr), 30) * rpad(string(fx1), 30) * rpad(string(fx2), 30) * rpad(string(fxr), 30) * rpad(string(ea), 30) * rpad(string(et), 30) * "\n")

                if ea < tol
                    push!(roots, xr)
                    break
                end

                # Update the bounds
                if fx1 * fxr < 0
                    x2, fx2 = xr, fxr
                else
                    x1, fx1 = xr, fxr
                end
            end

            if length(roots) < root_number
                println("Max iterations reached without convergence for root #$(root_number).")
            end

            root_number += 1
        end

        # Plotting
        plt = plot(iterations, fxr_values, title = "False Position Method - f(xr) over Iterations", xlabel = "Iteration", ylabel = "f(xr)", legend = false)
        savefig(plt, "False_Position_Method_Plot.png")

        plt_2 = plot(iterations, ea_values, title = "False Position Method - ea over Iterations", xlabel = "Iteration", ylabel = "ea", legend = false)
        savefig(plt_2, "False_Position_Method_App_Error_Plot.png")

        plt_3 = plot(iterations, et_values, title = "False Position Method - et over Iterations", xlabel = "Iteration", ylabel = "et", legend = false)
        savefig(plt_3, "False_Position_Method_True_Error_Plot.png")

        return roots
        
    end
end
