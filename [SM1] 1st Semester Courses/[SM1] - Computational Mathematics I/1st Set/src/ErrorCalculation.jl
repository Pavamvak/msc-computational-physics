"""
    approximation_error_percentage(current_estimation, previous_estimation)

Calculates the percentage error between the current and previous estimations of a root or variable.

# Parameters
- `current_estimation`: The current estimated value of the root or variable.
- `previous_estimation`: The previous estimated value of the root or variable.

# Returns
- `error_percentage`: The approximation error as a percentage. For the initial estimate (where the previous estimation is 0.0), it returns 100%.

# Description
The function calculates the approximation error by comparing the current estimate with the previous estimate. This is often used in iterative methods to determine the convergence and accuracy of the current estimate.

# Example Usage
```julia
current = 3.14
previous = 3.1415
error = approximation_error_percentage(current, previous)
println("Approximation Error: $error %")
```
"""
# Function to calculate the approximation error percentage
function approximation_error_percentage(current_estimation, previous_estimation)
    # Handle the case when the first term is being computed
    if previous_estimation == 0.0
        return 100.0  # Initial error can be assumed to be 100%
    else
        return abs((current_estimation - previous_estimation) / current_estimation) * 100
    end
end


"""
    scarborough_error(significant_figures)

Calculates the Scarborough error estimate based on the desired number of significant figures.

# Parameters
- `significant_figures`: An integer representing the number of significant figures desired for the accuracy of a calculation.

# Description
The Scarborough error estimate is a method to determine the allowable error in numerical computations, especially in iterative methods. It is based on the number of significant figures desired in the result. This function calculates the error tolerance that corresponds to a given number of significant figures.

The formula used is:
    Error Estimate = 0.5 * 10^(2 - significant_figures)

For example, if 3 significant figures are desired, the error estimate would be 0.5 * 10^(-1) or 0.05. This means that the calculated value should be accurate to within 0.05 to be considered valid for three significant figures.

# Returns
- The calculated error estimate, which represents the maximum allowable deviation from the true value for the given number of significant figures.

# Example Usage
```julia
error_tolerance = scarborough_error(4)
println("Error tolerance for 4 significant figures: ", error_tolerance)
```
"""
# Function to calculate the Scarborough error estimate
function scarborough_error(significant_figures)
    return 0.5 * 10.0^(2 - significant_figures)
end

"""
            true_percent_relative_error(f, approximation, true_value)

Calculate the true percentage error for a single approximation for a specific SymPy lambdified function.

# Arguments
- `approximation::Float64`: A single approximation value.

# Returns
- `Float64`: The true percentage error corresponding to the approximation.

# Example
```julia
approximation = 0.7
error = calculate_true_percentage_error(approximation)
```
"""
function true_percent_relative_error(approximation)

    true_value = 0.4428544010023885831413279999993368197162621293734796847177330769
    error = abs((true_value - approximation) / true_value) * 100

    return error
end
