module FindingRoot

    using Plots
    using SymPy

    include("DataGetter.jl")
    include("ErrorCalculation.jl")
    include("PlotEquation.jl")
    include("InitialGuess.jl")
    include("BisectionFunction.jl")
    include("FalsePositionMethod.jl")
    include("FixedPointIterationFunction.jl")
    include("g(x)Check.jl")
    include("g(x)Creation.jl")
    include("main.jl")

    export main, bisection, false_position, simple_fixed_point_iteration

end