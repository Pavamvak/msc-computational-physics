function is_suitable_for_fixed_point(f, variable, x_ranges)
    @vars x  # Define a symbolic variable
    g = x - f(x)
    dg = diff(g, x)  # Derivative of g with respect to x

    # Iterate through each range in x_ranges
    for x_range in x_ranges
        a, b = x_range  # Extract the start and end of the range

        # Check if |g'(x)| < 1 for all x in the current range
        suitable = true
        for val in range(a, b, length=100)  # Sample points within the current range
            dg_val = subs(dg, x => val)
            if abs(dg_val) >= 1
                suitable = false
                break
            end
        end

        # If the current range is not suitable, return false
        if !suitable
            return false
        end
    end

    # If all ranges are suitable, return true
    return true
end
