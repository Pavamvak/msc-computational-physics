% This script demonstrates that the variance of the sum of two
% independent random variables X and Y is equal to the sum of their
% variances, i.e., Var[X + Y] = Var[X] + Var[Y], by simulating a large
% number of values from X and Y that follow a two-variable normal
% distribution.
%
% The script uses the mvnrnd function to generate samples from the
% multivariate normal distribution, calculates the empirical variances
% of X, Y, and X+Y, and compares the variance of X+Y to the sum of
% variances of X and Y.
%
% Author: Vamvakas Panagiotis
% Date: 4/11/2023
% Mail: pavamvak@gmail.com
% Version: 1.0

% Define the number of samples
num_samples = 10000;

% Define the means of X and Y
mu_X = 0;
mu_Y = 0;

% Define the variances of X and Y
var_X = 1; % Variance of X
var_Y = 1; % Variance of Y

% Since X and Y are independent, their covariance is 0
cov_XY = 0;

% Create the mean vector and covariance matrix for the multivariate normal distribution
mean_vector = [mu_X mu_Y];
cov_matrix = [var_X cov_XY; cov_XY var_Y];

% Generate the random samples using mvnrnd
random_samples = mvnrnd(mean_vector, cov_matrix, num_samples);

% Extract samples for X and Y
X_samples = random_samples(:, 1);
Y_samples = random_samples(:, 2);

% Calculate the variance of X, Y, and X+Y
var_X_empirical = var(X_samples);
var_Y_empirical = var(Y_samples);
var_X_plus_Y_empirical = var(X_samples + Y_samples);

% Display the results
fprintf('Empirical Variance of X: %f\n', var_X_empirical);
fprintf('Empirical Variance of Y: %f\n', var_Y_empirical);
fprintf('Empirical Variance of X + Y: %f\n', var_X_plus_Y_empirical);
fprintf('Sum of Variances of X and Y: %f\n', var_X_empirical + var_Y_empirical);

% Check if empirical variances are approximately equal
tolerance = 1e-1; % Define a tolerance level
if abs(var_X_plus_Y_empirical - (var_X_empirical + var_Y_empirical)) < tolerance
    fprintf('The empirical variances confirm that Var[X + Y] = Var[X] + Var[Y] within the tolerance level.\n');
else
    fprintf('The empirical variances do not confirm that Var[X + Y] = Var[X] + Var[Y] within the tolerance level.\n');
end

% Plot the points X and Y
figure;
scatter(X_samples, Y_samples, 'filled');
title('Scatter Plot of X and Y Samples');
xlabel('X Values');
ylabel('Y Values');
grid on;