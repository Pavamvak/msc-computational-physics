% Exponential Random Number Generator with User Interaction
% This script generates a set of random numbers from an exponential 
% distribution and creates a histogram of those numbers. The user 
% is given the option to use Simulink capabilities for plotting. If 
% Simulink is not used, the script defaults to MATLAB's hist function. 
% The script also superimposes the theoretical probability density function 
% (PDF) of the exponential distribution over the histogram.
%
% Author: Vamvakas Panagiotis
% Date: 4/11/2023
% Mail: pavamvak@gmail.com
% Version: 1.0

clc;

% Ask user whether to use Simulink capabilities or not
prompt = 'Do you want to use Simulink capabilities? Yes or No [Y/N]: ';
str = input(prompt, 's');

% Error handling for user choice
while isempty(str) || (~strcmpi(str, 'Y') && ~strcmpi(str, 'N'))
    disp('Error: Please enter "Y" for Yes or "N" for No.');
    str = input(prompt, 's');
end

% Define the number of random numbers
num = 1000;

% Define the rate parameter for the exponential distribution
lambda = 1;

% Generate N uniformly distributed random numbers from U[0, 1]
uniformRandomNumbers = rand(num, 1);

% Transform the uniform random numbers to exponential distribution
% using the inverse CDF
exponentialRandomNumbers = -log(1 - uniformRandomNumbers) / lambda;

% Check user input and plot accordingly
if strcmpi(str, 'Y')
    % User chose to use Simulink capabilities (Histogram)
    histogram(exponentialRandomNumbers, 50, 'Normalization', 'pdf');
else
    % User chose not to use Simulink capabilities (hist)
    [n, xout] = hist(exponentialRandomNumbers, 50);
    binWidth = xout(2) - xout(1); % Calculate the width of each bin
    n = n / (sum(n) * binWidth); % Normalize the bin counts
    bar(xout, n, 'w'); % Create a bar graph
end

% Regardless of the choice, we will plot the PDF
hold on;

% Define a range for x based on the range of the data for plotting the PDF
x = 0:0.1:max(exponentialRandomNumbers);

% Calculate the PDF of the exponential distribution over the range of x
pdf = lambda * exp(-lambda * x);

% Plot the PDF on the same figure as the histogram
plot(x, pdf, 'r', 'LineWidth', 2);

% Label the axes, add a title and legend
xlabel('x');
ylabel('Probability Density');
title('Histogram and PDF of Exponential Distribution with \lambda = 1');
legend('Histogram', 'Exponential PDF');

% Release the hold on the current figure
hold off;
