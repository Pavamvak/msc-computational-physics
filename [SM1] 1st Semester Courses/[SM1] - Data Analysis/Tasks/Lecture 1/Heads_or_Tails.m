% Coin Toss Simulation
% This script simulates a series of coin tosses and calculates the number 
% of heads and tails, as well as their respective percentages. The user has 
% the option to view dynamic plots of the simulation or to simply calculate 
% the results without plotting.
%
% Features include:
%   - User input for the number of tosses with input validation.
%   - Real-time updating plots showing proportions of heads and tails.
%   - Pie chart for visual comparison of heads and tails count.
%   - Normal distribution approximation of the proportions.
%   - Text boxes in the figure showing the current count and percentage of 
% heads and tails.
%   - Console output of final heads and tails counts and their percentages.
%
% Author: Vamvakas Panagiotis
% Date: 4/11/2023
% Mail: pavamvak@gmail.com
% Version: 1.0
% 
% Notes: 
% - This script requires the Statistics and Machine Learning Toolbox for
% the 'normpdf' function.
% - Recommended to keep the coin tosses below 100.000 if you want to also
% use the dynamic plotting option, due to computational overhead.

clc;

% Ask the user if they want dynamic plotting with error handling
dynamicPlotting = false; % Default value
while true
    dynamicPlottingResponse = input('Do you want dynamic plotting? (yes/no): ', 's');
    if strcmpi(dynamicPlottingResponse, 'yes')
        dynamicPlotting = true;
        break;
    elseif strcmpi(dynamicPlottingResponse, 'no')
        dynamicPlotting = false;
        break;
    else
        disp('Invalid response. Please type "yes" or "no".');
    end
end

% Initialize numTosses to an empty array
numTosses = [];

% Keep asking the user for input until a valid number is entered
while isempty(numTosses) || ~isnumeric(numTosses) || mod(numTosses, 1) ~= 0 || numTosses <= 0
    numTosses = input('Enter the number of coin tosses (positive integer): ');
    if ~isnumeric(numTosses) || mod(numTosses, 1) ~= 0 || numTosses <= 0
        disp('Invalid input. Please enter a positive integer.');
    end
end

% Set plot update interval
plotupdate = numTosses / 100;

% Initialize the results array
results = zeros(numTosses, 1);

% Initialize proportions array for dynamic plotting
proportions = zeros(numTosses, 2); % Column 1 for heads, Column 2 for tails

% Set the plotting parameters if dynamic plotting is enabled
if dynamicPlotting
    % Set the figure size
    figure('Position', [100, 100, 1024, 768]);

    % Set up individual display boxes for the counts and percentages with 
    % specified appearance
    hTextHeadsCount = annotation('textbox', [0.15, 0.01, 0.15, 0.04], ...
        'String', 'Heads: 0', 'EdgeColor', 'black', 'BackgroundColor', ...
        'white', 'HorizontalAlignment', 'center');
    hTextHeadsPercent = annotation('textbox', [0.30, 0.01, 0.15, 0.04], ...
        'String', '0%', 'EdgeColor', 'black', 'BackgroundColor', ...
        'white', 'HorizontalAlignment', 'center');
    hTextTailsCount = annotation('textbox', [0.60, 0.01, 0.15, 0.04], ...
        'String', 'Tails: 0', 'EdgeColor', 'black', 'BackgroundColor', ...
        'white', 'HorizontalAlignment', 'center');
    hTextTailsPercent = annotation('textbox', [0.75, 0.01, 0.15, 0.04], ...
        'String', '0%', 'EdgeColor', 'black', 'BackgroundColor', ...
        'white', 'HorizontalAlignment', 'center');
end

% Simulate the coin tosses
for i = 1:numTosses
    % Generate a random integer: 0 for heads, 1 for tails
    results(i) = randi([0, 1], 1, 1);

    % Calculate the number of heads and tails up to the i-th toss
    numHeads = sum(results(1:i) == 0);
    numTails = sum(results(1:i) == 1);
    
    % Update proportions
    proportions(i, 1) = numHeads / i;
    proportions(i, 2) = numTails / i;

    % Plot the results if the dynamic plotting is enabled
    if dynamicPlotting
        % Update the plots at a specific interval
        if mod(i, plotupdate) == 0 || i == numTosses % Also update on the last toss
            % Normal distribution parameters
            mu = 0.5; % mean
    
            % Proportion line plot
            subplot(2, 2, 1);
            plot(1:i, proportions(1:i, 1), 'b-', 'LineWidth', 2); % Heads in blue
            hold on;
            plot(1:i, proportions(1:i, 2), 'r-', 'LineWidth', 2); % Tails in red
            yline(0.5, '--k', 'LineWidth', 1.5); % Expected mean line at y=0.5
            hold off;
            xlim([1 numTosses]);
            ylim([0 1]);
            xlabel('Number of Tosses');
            ylabel('Proportion');
            title('Proportion Over Time');
            legend('Heads', 'Tails', 'Expected Mean');
    
            % Pie chart for heads and tails
            subplot(2, 2, 2);
            pie([numHeads, numTails], {'Heads', 'Tails'});
            title('Heads vs Tails');
    
            % Binomial distribution for heads
            subplot(2, 2, 3);
            x_values = 0:i;
            y_values = binopdf(x_values, i, mu);
            plot(x_values, y_values, 'b-', 'LineWidth', 2);
            xlim([0 i]);
            xlabel('Number of Heads');
            ylabel('Probability');
            title('Binomial Distribution for Heads');
    
            % Binomial distribution for tails
            subplot(2, 2, 4);
            % Since the number of tails is just the total tosses minus the 
            % number of heads, the binomial distribution is the same
            plot(x_values, y_values, 'r-', 'LineWidth', 2);
            xlim([0 i]);
            xlabel('Number of Tails');
            ylabel('Probability');
            title('Binomial Distribution for Tails');
    
            % Calculate percentages
            percentHeads = (numHeads / i) * 100;
            percentTails = (numTails / i) * 100;
    
            % Update the text boxes with the current counts and percentages
            set(hTextHeadsCount, 'String', sprintf('Heads: %d', numHeads));
            set(hTextHeadsPercent, 'String', sprintf('%.2f%%', percentHeads));
            set(hTextTailsCount, 'String', sprintf('Tails: %d', numTails));
            set(hTextTailsPercent, 'String', sprintf('%.2f%%', percentTails));
            
            % Force MATLAB to update the figure window
            drawnow;
        end
    end
end

% Display the final counts and percentage probabilities in the command window
numHeads = sum(results == 0);
numTails = sum(results == 1);

% Calculate percentages
percentHeads = (numHeads / numTosses) * 100;
percentTails = (numTails / numTosses) * 100;

% Display the results
fprintf('Final number of Heads: %d\n', numHeads);
fprintf('Final number of Tails: %d\n', numTails);
fprintf('Final percentage of Heads: %.2f%%\n', percentHeads);
fprintf('Final percentage of Tails: %.2f%%\n', percentTails);
