% Overvoltage Confidence Interval Analysis
%
% This script performs statistical analysis on overvoltage trigger data.
% It includes:
% (a) Calculation of the 95% confidence interval for the variance.
% (b) Hypothesis testing for the standard deviation.
% (c) Calculation of the 95% confidence interval for the mean.
% (d) Hypothesis testing to check if the mean is significantly different 
% from 52 kV.
% Additionally, a box plot is generated to visualize the data distribution.
%
% Data Description:
% The data represents the trigger voltage of overvoltage events in a circuit.
%
% Author: Vamvakas Panagiotis
% Date: 03/12/2023
% Mail: pavamvak@gmail.com
% Version: 1.0

clc;

raw_data = [41; 48; 50; 52; 46; 50; 50; 52; 47; 50; 50; 53; 47; 50; 50;...
    53; 48; 50; 52; 53; 50; 50; 52; 53; 50; 50; 53; 53; 50; 50; 53; 53;...
    50; 52; 53; 54; 50; 52; 53; 54; 50; 53; 53; 55; 50; 55; 57; 68];
%% (a) 95% confidence interval for the variance using vartest
% Confidence level, default is 95% so it is not needed
confidence_parameter = 0.05;

% Calculate confidence interval for the variance
[~, ~, ci] = vartest(raw_data, var(raw_data), 'Alpha', ...
    confidence_parameter, 'Tail', 'both');

% Displaying the results
disp('--------------------------------------------------');
disp('Section (a): 95% Confidence Interval for the Variance');
disp('--------------------------------------------------');
disp('TThe variance will be in a 95% confidence interval of:');
disp(ci);
disp(' ');

%% (b) Hypothesis test for the standard deviation
% Hypothesized variance (square of the standard deviation)
hypothesized_variance = 5^2;

% Conducting the hypothesis test
[h, p, ci] = vartest(raw_data, hypothesized_variance, 'Alpha', ...
    confidence_parameter);

% Displaying the results
disp('--------------------------------------------------');
disp('Section (b): Hypothesis Test for the Standard Deviation');
disp('--------------------------------------------------');
disp(['Test result (h = 0 means do not reject H0; h = 1 means reject ' ...
    'H0): ', num2str(h)]);
disp(['P-value of the test: ', num2str(p)]);
disp(['95% confidence interval for the variance: [', num2str(ci(1)), ...
    ', ', num2str(ci(2)), ']']);
disp(' ');

%% (c) 95% confidence interval for the mean
% Sample mean and standard deviation
sample_mean = mean(raw_data);
sample_std = std(raw_data);

% Sample size
n = length(raw_data);

% Standard error of the mean
sem = sample_std / sqrt(n);

% Confidence level and degrees of freedom
confidence_level = 0.975; % for two-tailed
df = n - 1;

% t-value for the 95% confidence interval
t_value = tinv(confidence_level, df);

% Calculating the confidence interval
conf_int_mean = [sample_mean - t_value * sem, sample_mean + t_value * sem];

% Displaying the results
disp('--------------------------------------------------');
disp('Section (c): 95% Confidence Interval for the Mean');
disp('--------------------------------------------------');
disp(['95% confidence interval for the mean: [', ...
    num2str(conf_int_mean(1)), ', ', num2str(conf_int_mean(2)), ']']);
disp(' ');

%% (d) Test if mean is not 52 kV
hypothesized_mean = 52;
[h, p, ci] = ttest(raw_data, hypothesized_mean);

% Displaying the results
disp('--------------------------------------------------');
disp('Section (d): Hypothesis Test if Mean is Not 52 kV');
disp('--------------------------------------------------');
disp(['Test result (h = 0 means do not reject H0; h = 1 means ' ...
    'reject H0): ', num2str(h)]);
disp(['P-value of the test: ', num2str(p)]);
disp(['95% confidence interval for the mean: [', num2str(ci(1)), ', ', ...
    num2str(ci(2)), ']']);
disp(' ');

% Creating the box plot
figure(1);
boxplot(raw_data);
title('Box Plot of Overvoltage trigger Voltage Data');
ylabel('Voltage (kV)');
xlabel('Columns')

% Adding a line for the hypothesized mean (Question d)
hold on;
line(xlim(), [52 52], 'Color', 'red', 'LineStyle', '--');
hold off;