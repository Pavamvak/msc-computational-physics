% Overvoltage Goodness-of-Fit Analysis

clc;

% Raw data of overvoltage trigger voltage
raw_data = [41; 48; 50; 52; 46; 50; 50; 52; 47; 50; 50; 53; 47; 50; 50;...
    53; 48; 50; 52; 53; 50; 50; 52; 53; 50; 50; 53; 53; 50; 50; 53; 53;...
    50; 52; 53; 54; 50; 52; 53; 54; 50; 53; 53; 55; 50; 55; 57; 68];

% Define the number of bins for the chi-square test
num_bins = 10;

% Perform Chi-Square Goodness-of-Fit Test
[h, p] = chi2gof(raw_data, 'NBins', num_bins, 'CDF', {@normcdf, mean(raw_data), std(raw_data)});

% Displaying the results
disp(['Chi-Square Goodness-of-Fit Test Result (h = 0 means do not reject H0; h = 1 means reject H0): ', num2str(h)]);
disp(['P-value of the test: ', num2str(p)]);
