% Reciprocal Means Analysis
% This script investigates whether E[1/X] is equal to 1/E[X] for a random
% variable X that follows an absolutely continuous distribution. It
% performs Monte Carlo simulations for three different ranges of X: [1,2],
% [0,1], and [-1,1], computing the expected values as the number of
% iterations increases. The results are plotted for analysis.
%
% The script uses the `computeMeans` custom function, which is defined in
% another file.
%
% Output:
%   Three plots are generated, one for each range, displaying the
%   relationship between the mean of X, the reciprocal of the mean of X, 
%   and the mean of 1/X as the sample size increases.
%
% See also: computeMeans
%
% Author: Vamvakas Panagiotis
% Date: 19/11/2023
% Mail: pavamvak@gmail.com
% Version: 1.1


clc;

% Calculate for the range [1, 2]
[mean_X_12, mean_inv_X_12, iterations] = computeMeans(1, 2);
% Calculate for the range [0, 1]
[mean_X_01, mean_inv_X_01, ~] = computeMeans(0, 1);
% Calculate for the range [-1, 1] (note that this will include NaNs)
[mean_X_m11, mean_inv_X_m11, ~] = computeMeans(-1, 1);

% Plotting code for all ranges
figure;

% Plot for [1,2]
subplot(3,1,1);
loglog(iterations, 1./mean_X_12, 'r-', 'DisplayName', '1/E[X] for [1,2]');
hold on;
loglog(iterations, mean_inv_X_12, 'g', 'DisplayName', 'E[1/X] for [1,2]');
xlabel('Number of iterations (n)');
ylabel('Mean values');
legend('show');
title('Mean of X vs. Reciprocal of the Mean vs. Mean of 1/X for X in [1,2]');
hold off;

% Plot for [0,1]
subplot(3,1,2);
loglog(iterations, 1./mean_X_01, 'r-', 'DisplayName', '1/E[X] for [0,1]');
hold on;
loglog(iterations, mean_inv_X_01, 'g', 'DisplayName', 'E[1/X] for [0,1]');
xlabel('Number of iterations (n)');
ylabel('Mean values');
legend('show');
title('Mean of X vs. Reciprocal of the Mean vs. Mean of 1/X for X in [0,1]');
hold off;

% Plot for [-1,1]
subplot(3,1,3);
plot(iterations, 1./mean_X_m11, 'r-', 'DisplayName', '1/E[X] for [-1,1]');
hold on;
plot(iterations, mean_inv_X_m11, 'g', 'DisplayName', 'E[1/X] for [-1,1]');
xlabel('Number of iterations (n)');
ylabel('Mean values');
legend('show');
title('Mean of X vs. Reciprocal of the Mean vs. Mean of 1/X for X in [-1,1]');
hold off;

function [mean_X, mean_inv_X, iterations] = computeMeans(lower_bound, upper_bound)
% computeMeans Computes the mean of X and mean of 1/X for a given range
%
% This function performs Monte Carlo simulations to estimate the mean of a
% random variable X and the mean of its reciprocal, 1/X, for an absolutely
% continuous uniform distribution within a specified range. It is designed
% to be called multiple times for different ranges of X.
%
% Inputs:
%   lower_bound - The lower bound of the range for the uniform distribution
%   upper_bound - The upper bound of the range for the uniform distribution
%
% Outputs:
%   mean_X      - A vector containing the means of X for increasing sample sizes
%   mean_inv_X  - A vector containing the means of 1/X for increasing sample sizes
%   iterations  - A vector containing the number of samples used for each computation
%
% Examples:
%   [mean_X, mean_inv_X, iterations] = computeMeans(1, 2);
%   [mean_X, mean_inv_X, iterations] = computeMeans(0, 1);
%
% See also: rand, logspace, mean
%
% Author: Vamvakas Panagiotis
% Date: 4/11/2023
% Mail: pavamvak@gmail.com
% Version: 1.0

    % Define the number of iterations for the computational experiment
    iterations = logspace(1, 6, 100); % from 10 to 1,000,000 samples
    mean_X = zeros(size(iterations));
    mean_inv_X = zeros(size(iterations));

    % Loop over the number of iterations to compute the mean values
    for i = 1:length(iterations)
        n = round(iterations(i)); % Ensure n is an integer for rand
        X = lower_bound + (upper_bound - lower_bound) * rand(n, 1); % Uniform distribution in the given range
        
         % Modify X to avoid division by zero (i.e., when X is exactly zero)
        X(X == 0) = eps; 

        mean_inv_X(i) = mean(1./X);
        mean_X(i) = mean(X);
        
    end
end
