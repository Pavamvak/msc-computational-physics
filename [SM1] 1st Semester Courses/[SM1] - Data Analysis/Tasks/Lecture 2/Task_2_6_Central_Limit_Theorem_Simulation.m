% Central Limit Theorem Simulation
% This script demonstrates the Central Limit Theorem (CLT) using MATLAB.
% It generates 10,000 sample means (Y) from samples of 100 random variables,
% each drawn from a uniform distribution in the range [0, 1]. It then plots
% a histogram of these sample means and overlays the theoretical normal 
% distribution curve.
%
% Author: Vamvakas Panagiotis
% Date: 17/11/2023
% Mail: pavamvak@gmail.com
% Version: 1.0

% Define the parameters
n = 100; % Number of random variables in each sample
N = 10000; % Total number of samples
lowerBound = 0; % Lower bound of uniform distribution
upperBound = 1; % Upper bound of uniform distribution

% Generate the samples and calculate their means
sampleMeans = zeros(N, 1);
for i = 1:N
    sample = lowerBound + (upperBound - lowerBound) * rand(n, 1);
    sampleMeans(i) = mean(sample);
end

% Theoretical normal distribution parameters
mu = (lowerBound + upperBound) / 2;
sigma = sqrt((upperBound - lowerBound)^2 / (12 * n));

% Generate values for the normal distribution curve
x = linspace(min(sampleMeans), max(sampleMeans), 100);
normalCurve = normpdf(x, mu, sigma);

% Plot the histogram and the normal distribution curve
histogram(sampleMeans,50 ,'Normalization', 'pdf'); % Histogram of sample means
hold on;
plot(x, normalCurve, 'r', 'LineWidth', 2); % Theoretical normal curve
hold off;

title('Central Limit Theorem Simulation');
xlabel('Sample Mean');
ylabel('Probability Density');
legend('Sample Means Histogram', 'Normal Distribution Curve');
