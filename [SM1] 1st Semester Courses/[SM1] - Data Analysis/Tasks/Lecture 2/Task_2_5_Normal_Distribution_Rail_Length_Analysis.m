% Normal Distribution Rail Length Analysis
% This script calculates the probability of a rail being discarded
% and determines the length limit for a maximum discard rate of 1%.
% The lengths of the rails produced by a machine are normally distributed
% with a mean (mu) of 4 meters and a deviation of 0.01 meters.
% Rails shorter than 3.9 meters are discarded.
%
% See also: normcdf, norminv
%
% Author: Vamvakas Panagiotis
% Date: 17/11/2023
% Mail: pavamvak@gmail.com
% Version: 1.0

% Parameters of the normal distribution
mu = 4;     % Mean length of the rails
sigma = sqrt(0.01); % Standard deviation of the rail lengths

% Probability of a rail being discarded (length < 3.9 meters)
discard_probability = normcdf(3.9, mu, sigma);

% Display the probability
fprintf('Probability of a rail being discarded: %f\n', discard_probability);

% Find the length limit for which only 1% of the rails are discarded
length_limit = norminv(0.01, mu, sigma);

% Display the length limit
fprintf('Length limit for max 1%% of the rails to be discarded: %f meters\n', length_limit);

% Plotting the CDF of the rail lengths
x_values = 3.7:0.001:4.3; % Range of rail lengths for plotting
cdf_values = normcdf(x_values, mu, sigma); % CDF values for each x

% Create the plot
figure;
plot(x_values, cdf_values, 'b-', 'LineWidth', 2);
grid on;
title('Cumulative Distribution Function of Rail Lengths');
xlabel('Rail Length (meters)');
ylabel('Probability');
hold on;

% Coloring the area under the CDF curve for lengths < 3.9 meters
area(x_values(x_values <= 3.9), cdf_values(x_values <= 3.9), 'FaceColor', 'red', 'EdgeColor', 'none');

% Coloring the area under the CDF curve for the bottom 1% of lengths
area(x_values(x_values <= length_limit), cdf_values(x_values <= length_limit), 'FaceColor', 'green', 'EdgeColor', 'none');

% Marking the discard probability and the 1% length limit on the plot
plot(3.9, discard_probability, 'ko'); % Black circle at discard probability
plot(length_limit, 0.01, 'ko'); % Black circle at 1% length limit

% Adding legends for clarity
legend('CDF of Rail Lengths', 'Discard Probability Area', '1% Length Limit Area', 'Key Points');
hold off;