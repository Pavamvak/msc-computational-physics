# Maximum Likelihood Estimator for Exponential Distribution

Given a set of independent observations $` \{x_1, x_2, ..., x_n\} `$ from an exponential distribution with an unknown parameter $` \tau `$, we aim to find the maximum likelihood estimator (MLE) for $` \tau `$.

The probability density function (PDF) of an exponential distribution is given by:
```math
f(x;\tau) = \frac{1}{\tau}e^{-\frac{x}{\tau}}
```
where $\tau$ is the rate parameter of the distribution.

For independent observations, the likelihood function is the product of the PDFs of each observation:
```math
L(\tau) = \prod_{i=1}^{n} f(x_i;\tau) \\
L(\tau) = \prod_{i=1}^{n} \frac{1}{\tau}e^{-\frac{x_i}{\tau}} 
```
The log-likelihood function, which is easier to work with for optimization, is the natural logarithm of the likelihood function.
```math
\ln L(\tau) = \ln \left( \prod_{i=1}^{n} \frac{1}{\tau}e^{-\frac{x_i}{\tau}} \right)
```
Using the property that the logarithm of a product is the sum of the logarithms, we expand this to:
```math
\ln L(\tau) = \sum_{i=1}^{n} \ln \left( \frac{1}{\tau}e^{-\frac{x_i}{\tau}} \right)\\

\ln L(\tau) = \sum_{i=1}^{n} \left( \ln \left( \frac{1}{\tau} \right) + \ln \left( e^{-\frac{x_i}{\tau}} \right) \right)\\

\ln L(\tau) = \sum_{i=1}^{n} \left( -\ln(\tau) - \frac{x_i}{\tau} \right)
```
This simplified form of the log-likelihood function is easier to differentiate and optimize compared to the original likelihood function.

Taking the derivative of the log-likelihood function with respect to $\tau$ gives:
```math
\frac{d}{d\tau} \ln L(\tau) = \sum_{i=1}^{n} \left( -\frac{1}{\tau} + \frac{x_i}{\tau^2} \right)
```
After taking the derivative of the log-likelihood function with respect to $\tau$, we set it equal to zero to find the maximum:
```math
\frac{d}{d\tau} \ln L(\tau) = \sum_{i=1}^{n} \left( -\frac{1}{\tau} + \frac{x_i}{\tau^2} \right) = 0
```
This equation represents the critical point where the log-likelihood function reaches its maximum. To solve for $\tau$, we first simplify the equation:
```math
-\frac{n}{\tau} + \frac{\sum_{i=1}^{n} x_i}{\tau^2} = 0\\

\frac{\sum_{i=1}^{n} x_i}{\tau^2} = \frac{n}{\tau}\\

\tau = \frac{\sum_{i=1}^{n} x_i}{n}
```
This means that the MLE of $\tau$ for an exponential distribution is the sample mean of the observations.