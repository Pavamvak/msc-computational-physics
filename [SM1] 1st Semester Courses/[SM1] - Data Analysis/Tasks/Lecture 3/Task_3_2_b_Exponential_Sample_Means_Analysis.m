% Exponential Sample Means Analysis
% This script generates multiple samples from an exponential distribution,
% calculates the sample means, and compares these with a normal distribution
% that approximates the distribution of the sample means according to the
% Central Limit Theorem. The script visualizes this comparison using histograms
% and normal distribution plots for different values of M (number of samples),
% n (sample size), and tau (exponential distribution parameter).
%
% Author: Vamvakas Panagiotis
% Date: 02/12/2023
% Mail: pavamvak@gmail.com
% Version: 1.0

clc;

% Initialize figure counter to display all plots
figure_counter = 1;

% Example calls to the function with different combinations of M, n, and tau
expSampleMeans(1000, 10, 1.2, figure_counter);
figure_counter = figure_counter + 1;
expSampleMeans(500, 20, 0.8, figure_counter);
figure_counter = figure_counter + 1;
expSampleMeans(1000, 30, 1.5, figure_counter);

function expSampleMeans(M, n, tau, figure_counter)
    % Generate M samples of size n from an exponential distribution with parameter tau
    % Calculate the sample mean for each of the M samples
    sampleMeans = zeros(1, M);
    for i = 1:M
        sample = exprnd(tau, [1, n]); % Generate a sample of size n
        sampleMeans(i) = mean(sample); % Calculate the mean of the sample
    end
    
    % Create a histogram of the M sample means
    figure(figure_counter);
    histogram(sampleMeans, 'Normalization', 'pdf');
    hold on;
    
    % Normal distribution for comparison
    x = linspace(min(sampleMeans), max(sampleMeans), 100);
    normal_pdf = normpdf(x, tau, sqrt(tau^2/n));
    plot(x, normal_pdf, 'LineWidth', 2);
    hold off;

    title(sprintf('Histogram of Sample Means with Normal Distribution (M=%d, n=%d, tau=%f)', M, n, tau));
    xlabel('Sample Mean');
    ylabel('Probability Density');
    
    % Display the actual tau and the mean of the sample means
    disp(['Actual tau: ', num2str(tau)]);
    disp(['Mean of sample means: ', num2str(mean(sampleMeans))]);
end
