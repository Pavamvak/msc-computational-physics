% Poisson Sample Means Analysis
% This script generates multiple samples from a Poisson distribution,
% calculates the sample means, and compares these with a normal distribution
% that approximates the distribution of the sample means according to the
% Central Limit Theorem. The script visualizes this comparison using histograms
% and normal distribution plots for different values of M (number of samples),
% n (sample size), and lambda (Poisson distribution parameter).
%
% Author: Vamvakas Panagiotis
% Date: 02/12/2023
% Mail: pavamvak@gmail.com
% Version: 1.0

clc;

% Intialize figure counter to diplay all plots
figure_counter = 1;
% Example calls to the function with different combinations of M, n, and lambda
poissonSampleMeans(1000, 10, 5, figure_counter);
figure_counter = figure_counter + 1;
poissonSampleMeans(500, 20, 3, figure_counter);
figure_counter = figure_counter + 1;
poissonSampleMeans(1000, 30, 10, figure_counter);

function poissonSampleMeans(M, n, lambda, figure_counter)
    % Generate M samples of size n from a Poisson distribution with parameter lambda
    % Calculate the sample mean for each of the M samples
    sampleMeans = zeros(1, M);
    for i = 1:M
        sample = poissrnd(lambda, [1, n]); % Generate a sample of size n
        sampleMeans(i) = mean(sample); % Calculate the mean of the sample
    end
    
    % Create a histogram of the M sample means
    figure(figure_counter);
    histogram(sampleMeans, 'Normalization', 'pdf');
    hold on;
    
    % Normal distribution for comparison
    x = linspace(min(sampleMeans), max(sampleMeans), 100);
    normal_pdf = normpdf(x, lambda, sqrt(lambda/n));
    plot(x, normal_pdf, 'LineWidth', 2);
    hold off;

    title(sprintf('Histogram of Sample Means with Normal Distribution (M=%d, n=%d, lambda=%f)', M, n, lambda));
    xlabel('Sample Mean');
    ylabel('Probability Density');
    
    % Display the actual lambda and the mean of the sample means
    disp(['Actual lambda: ', num2str(lambda)]);
    disp(['Mean of sample means: ', num2str(mean(sampleMeans))]);
end

