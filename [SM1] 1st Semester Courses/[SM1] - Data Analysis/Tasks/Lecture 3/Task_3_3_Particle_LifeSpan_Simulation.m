% Particle LifeSpan Simulation
% This script simulates the lifespan of particles following an exponential distribution
% with a mean lifespan τ. It calculates the 95% confidence interval for the mean lifespan
% for each sample and checks whether τ is included within these intervals. The analysis
% is performed for two different sample sizes to illustrate the effect of sample size on
% the accuracy of confidence intervals. The script calculates the percentage of times
% the real mean (τ) is included in the 95% confidence intervals over a large number of samples.
%
% Scenarios:
% a) Calculate M = 1000 samples of size n=5 and find the percentage of confidence intervals containing τ.
% b) Repeat for M=1000 samples but with n=100 and compare the percentages.
%
% The script uses t-test for calculating the confidence intervals and assumes
% a Student's t-distribution for the sample means.
%
% Author: Vamvakas Panagiotis
% Date: 02/12/2023
% Mail: pavamvak@gmail.com
% Version: 1.0

clc;

% Define parameters
tau = 15; % Mean lifespan in minutes
M = 1000; % Number of samples
n_values = [5, 100, 1000]; % Different sample sizes
alpha = 0.05; % Significance level for 95% confidence

% Loop over each sample size
for n = n_values
    count_in_interval = 0; % Initialize counter

    % Generate M samples and calculate confidence intervals
    for i = 1:M
        sample = exprnd(tau, [1, n]); % Generate a sample of size n
        [~, ~, ci] = ttest(sample, tau, 'Alpha', alpha); % 95% confidence interval
        
        % Check if the true mean (tau) falls within the confidence interval
        if ci(1) <= tau && ci(2) >= tau
            count_in_interval = count_in_interval + 1;
        end
    end

    % Calculate and display the percentage
    percentage = (count_in_interval / M) * 100;
    disp(['For n = ', num2str(n), ', the true mean is within the 95% confidence interval ', ...
          num2str(percentage), '% of the time.']);
end
