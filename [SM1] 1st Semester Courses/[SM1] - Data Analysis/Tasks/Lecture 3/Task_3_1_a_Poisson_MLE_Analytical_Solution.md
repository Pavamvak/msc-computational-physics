Maximum Likelihood Estimator for Poisson Distribution

Given a set of independent observations $` {x_1, x_2, ..., x_n} `$ from a Poisson distribution with an unknown parameter $` \lambda `$, we aim to find the maximum likelihood estimator (MLE) for $` \lambda `$. We anticipate that the MLE will be equal to the sample's mean.

The probability mass function (PMF) of a Poisson distribution is given by:
```math
f(x;\lambda) = \frac{e^{-\lambda} \lambda^x}{x!}
```
where $` \lambda `$ is the rate parameter of the distribution.

For independent observations, the likelihood function is the product of the PMFs of each observation:
```math
L(\lambda) = \prod_{i=1}^{n} f(x_i;\lambda) \\
L(\lambda) = \prod_{i=1}^{n} \frac{e^{-\lambda} \lambda^{x_i}}{x_i!} 
```
The log-likelihood function simplifies the calculation and maximization process, especially for products of probabilities, which is common in likelihood functions. It is the natural logarithm of the likelihood function.
```math
\ln L(\lambda) = \ln \left( \prod_{i=1}^{n} \frac{e^{-\lambda} \lambda^{x_i}}{x_i!} \right)
```
Using the property that the logarithm of a product is the sum of the logarithms, we expand this to:
```math
\ln L(\lambda) = \sum_{i=1}^{n} \ln \left( \frac{e^{-\lambda} \lambda^{x_i}}{x_i!} \right)\\

\ln L(\lambda) = \sum_{i=1}^{n} \left( \ln(e^{-\lambda}) + \ln(\lambda^{x_i}) - \ln(x_i!) \right)\\

\ln L(\lambda) = \sum_{i=1}^{n} \left( -\lambda + x_i \ln(\lambda) - \ln(x_i!) \right)
```
This simplified form of the log-likelihood function is easier to differentiate and optimize compared to the original likelihood function.

Taking the derivative of the log-likelihood function with respect to $` \lambda `$ gives:
```math
\frac{d}{d\lambda} \ln L(\lambda) = \sum_{i=1}^{n} \left( -1 + \frac{x_i}{\lambda} \right)
```
After taking the derivative of the log-likelihood function with respect to $` \lambda `$, we set it equal to zero to find the maximum:
```math
\frac{d}{d\lambda} \log L(\lambda) = \sum_{i=1}^{n} \left( -1 + \frac{x_i}{\lambda} \right) = 0
```
This equation represents the critical point where the log-likelihood function reaches its maximum. To solve for $` \lambda `$, we first simplify the equation:
```math
\sum_{i=1}^{n} \left( -1 + \frac{x_i}{\lambda} \right) = \sum_{i=1}^{n} -1 + \sum_{i=1}^{n} \frac{x_i}{\lambda} = 0
```
Notice that the sum of $`-1`$ repeated $`n`$ times is $`-n`$:
```math
-n + \sum_{i=1}^{n} \frac{x_i}{\lambda} = 0
```
Now, rearrange the equation to isolate the term with $` \lambda `$:
```math
\sum_{i=1}^{n} \frac{x_i}{\lambda} = n\\

\frac{1}{\lambda} \sum_{i=1}^{n} x_i = n\\

\lambda = \frac{1}{n} \sum_{i=1}^{n} x_i
```
This means that the MLE of $` \lambda `$ for a Poisson distribution is the sample mean of the observations.
